#!/bin/sh

#Load the scalatest environment in the repl for running test cases from src/test

#a hack to set up the scala imports
echo 'import org.scalatest._; import beagle._; import beagle.test._;
println("Unit tests can be run using the command: run (new TestNameSuite)")' > .tmp_scalatest_pre

#test if beagle has been built already
if [ ! -e target ]
  then sbt compile
fi

echo "Tests:\n$(ls src/test/*.scala)\n"

(exec scala -cp "$(pwd)/target/scala-2.11/classes:$(pwd)/lib/scalatest_2.11-2.2.0.jar" -i .tmp_scalatest_pre) &&
rm .tmp_scalatest_pre
