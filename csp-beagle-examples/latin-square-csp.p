%% Uses CSP specific predicates
%% Problem is that lots of reasoning is still done on the foreground
%% Becomes tractable when using alldiff predicates though :)

%% Latin squares of size 3.
%% Run this with
%% beagle -negsel -nodefine latin-square-3.p
%% Note: with -nosplit the runtimes are considerably longer

tff(a_type,type,( a: ($int * $int) > $fd(0,2) )).

tff(diag_type,type,( diag: $int > $fd(0,2) )).

%% Alldifferent between all elements in a row
%tff(r, axiom, (
%  ! [I: $fd(0,2), J1: $fd(0,2), J2: $fd(0,2)] :
%    ( (J1!=J2) => (a(I,J1) != a(I,J2)) ))).
    
%%Reformulation using alldiff predicate
tff(alldiff1, axiom, (
	![I: $fd(0,2)] : $$alldiff(a(I,0),a(I,1),a(I,2))
)).

%% Alldifferent between all elements in a column
%tff(r, axiom, (
%  ! [I1: $fd(0,2), I2: $fd(0,2), J: $fd(0,2)] :
%    ( ( I1!=I2 ) => (a(I1,J) != a(I2,J)) ))).

%%Reformulation using alldiff predicate
tff(alldiff2, axiom, (
	![J: $fd(0,2)] : $$alldiff(a(0,J),a(1,J),a(2,J))
)).

%% The above alldifferent constraints would constitute the background theory for the constraint 
%% solver

%% Contrived usage of free function symbols
%% Diagonal elements
tff(d, axiom, (
  ! [I: $fd(0,2)] : (diag(I) = a(I,I))
  )).
%% Want diagonal elements to be not decreasing (satisfiable)
tff(d, axiom, (
  ! [I: $fd(0,2), J:$fd(0,2)] : ($lesseq(I, J) => $lesseq(diag(I), diag(J))))).

%certain ground formulae help to speed the process...
%tff(d,axiom,( diag(0)=0 | diag(0)=1 | diag(0)=2) ).
%tff(d,axiom,( ?[X: $fd(0,2)]: diag(X) = 0 )).

%% Want diagonal elements to be increasing (unsatisfiable, can be refuted quickly)
% tff(d, axiom, (
%   ! [I: $fd(0,2), J:$fd(0,2)] : ($lesseq(I, J) => $less(diag(I), diag(J))))).

