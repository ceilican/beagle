BEAGLE - a theorem prover for hierarchic superposition
======================================================

Beagle is an automated theorem prover for first-order logic with equality over
linear integer/rational/real arithmetic. It accepts formulas in the [FOF, TFF, and
TFF-INT formats of the TPTP syntax](http://www.cs.miami.edu/~tptp/TPTP/TR/TPTPTR.shtml). 
It also accepts [SMT-LIB](http://www.smtlib.org/) input files. See the 
[SMTtoTPTP tool](https://bitbucket.org/peba123/smttotptp) for more details on this.

A more experimantal version of Beagle is maintained at https://bitbucket.org/joshbax189/beagle .

Installation
============

Beagle is available at https://bitbucket.org/peba123/beagle .
The distribution includes a pre-compiled jar archive and the Scala sources.

Running the pre-compiled Java archive
-------------------------------------

Run

    java -jar /PATH/TO/HERE/target/scala-2.11/beagle.jar [OPTION...] FILE

where FILE is an SMT-LIB version 2 or a TPTP TFF file.
See the `examples` subdirectory for some test problems.

Run

    java -jar /PATH/TO/HERE/target/scala-2.11/beagle.jar -help

to obtain basic usage information.

Installation from sources
-------------------------

Beagle can be built with sbt, the Scala Build Tool.
The sbt tool installs the scala compiler, if needed.

If Beagle is to be compiled once-and-forall the probably easiest way 
is to create a Java jar-archive and execute that.
Steps:

(1) Install [sbt](http://www.scala-sbt.org/). 

(2) In the current directory invoke `sbt assembly`. 

If successful, this creates the file `target/scala-2.11/beagle.jar`

Caveat: the file `project/plugins.sbt` contains a line like 

	addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.9.2")

Please update the specified version string (here: "0.9.2") with
a more recent one from https://github.com/sbt/sbt-assembly
should this version not be available.

(3) Run

    java -jar PATH/TO/HERE/target/scala-2.11/beagle.jar [OPTION...] FILE

as described above.

Alternatively to (2) above, `sbt compile` compiles the sources into Scala
class files.  Assuming a Scala 2.11 runtime environment is installed, the
supplied shell script `beagle` can then be used instead of (3). Run

    /PATH/TO/HERE/beagle -help

to get some basic usage information.

Documentation
=============
Documentation beyond what `beagle -help` gives you is not yet available.

Precedences
-----------
Beagle, a superposition prover, works internally with term orderings (LPO if theories are present, KBO otherwise). 
The precedences among operators for LPO can be specified in the input TPTP file as follows:

    %$ :prec f > g > h
    %$ :prec f > k > l

Beagle will linearize the stated precedences into a total ordering on operator symbols.

The corresponding syntax in SMT-LIB input files is as follows:

    (set-option :prec ("f > g > h" "f > k > l"))

Publications
============

- [Peter Baumgartner, Uwe Waldmann. Hierarchic superposition with weak
  abstraction](http://www.nicta.com.au/pub?id=6667)

- [Peter Baumgartner, Joshua Bax, Uwe Waldmann. Finite Quantification in
  Hierarchic Theorem Proving](http://www.nicta.com.au/pub?id=7842)

- [Peter Baumgartner, Joshua Bax. Proving Infinite
  Satisfiability](http://www.nicta.com.au/pub?id=7341)

- [Geoff Sutcliffe, Stephan Schulz, Koen Claessen, Peter Baumgartner. The TPTP
  Typed First-order Form and Arithmetic](http://www.nicta.com.au/pub?id=4724)

Contact
=======

Peter Baumgartner

http://users.cecs.anu.edu.au/~baumgart/

Email: Peter.Baumgartner@nicta.com.au

Usage reports, comments and suggestions are gratefully received.