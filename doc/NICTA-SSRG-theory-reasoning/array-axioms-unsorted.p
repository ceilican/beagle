fof(ax1,axiom,(
     ! [U,V,W] : read(write(U,V,W),V) = W )).

fof(ax2,axiom,(
    ! [X,Y,Z,X1] :
      ( Y = Z
      | read(write(X,Y,X1),Z) = read(X,Z) ) )).
