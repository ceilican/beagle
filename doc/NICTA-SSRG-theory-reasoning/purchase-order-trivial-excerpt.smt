(set-option :pull-nested-quantifiers true)
(set-option :mbqi true)
(set-option :macro-finder true)
(declare-datatypes () ((|objtype0| (|mk-objtype0| (|objtype0-nrStockItems| Int) (|objtype0-order| (List Int))))))
(define-fun |objtype0-nrStockItems-upd| ((v Int) (obj |objtype0|)) |objtype0| (|mk-objtype0| v (|objtype0-order| obj) ))
(define-fun |objtype0-order-upd| ((v (List Int)) (obj |objtype0|)) |objtype0| (|mk-objtype0| (|objtype0-nrStockItems| obj) v ))
; Declaring string literals

; About to translate definitions
; calculated topo-sort of call-graph: List(inRange)
; declaring inRange's type
(declare-fun |inRange| ((List Int) Int )Bool)
; declaring recursive inRange as constraint
(assert (forall ((|l0| (List Int))) (forall ((|n0| Int)) (= (inRange |l0| |n0|) (or (= |l0| nil) (and (and (<= 0 (head |l0|)) (< (head |l0|) |n0|)) (inRange (tail |l0|) |n0|)))))))
; Defined names: List(inRange)
; formula (∃db. (let db_1 = db in (((db_1.nrStockItems >= 0) ∧ ¬(db_1.order = [| |])) ∧ inRange(db_1.order,db_1.nrStockItems))))
(assert (exists ((|db0| |objtype0|)) (let ((|db1| |db0|)) (and (and (>= (|objtype0-nrStockItems| |db1|) 0) (not (= (|objtype0-order| |db1|) nil))) (inRange (|objtype0-order| |db1|) (|objtype0-nrStockItems| |db1|))))))
(check-sat)
