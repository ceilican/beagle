(set-option :pull-nested-quantifiers true)
(set-option :mbqi true)
(set-option :macro-finder true)
(set-option :produce-models true) ; enable model generation
(set-option :produce-proofs true) ; enable proof generation

(declare-sort I 0)
(declare-fun p (Int I) Bool)

;; (assert (forall ((i Int) (j Int) (x I)) 
;; 		(implies (not (= i j)) (or (p i x) (p j x)))))
;; (assert (forall ((i Int) (j Int) (x I)) 
;; 		(implies (not (= i j)) (or (not (p i x)) (not (p j x))))))
;; (check-sat)

(assert (forall ((i Int) (j Int) (x I)) 
		(or (<= i j) (p (+ i 1) x) (p (+ j 2) x))))
(assert (forall ((i Int) (j Int) (x I)) 
		(or (<= i j) (not (p (+ i 3) x)) (not (p (+ j 4) x)))))
(check-sat)
(get-proof)

;; (declare-fun p (Int) Bool)

;; (assert (forall ((i Int) (j Int)) 
;; 		(implies (not (= i j)) (or (p i) (p j)))))
;; (assert (forall ((i Int) (j Int)) 
;; 		(implies (not (= i j)) (or (not (p i)) (not (p j))))))
;; (check-sat)

