%% Example from the paper

tff(p_type,type,(
    p: ($int * $i) > $o )).

tff(a, axiom, ( 
   ! [I:$int, J:$int, X:$i] :
     ( $lesseq(I, J)
     | p($sum(I,1), X) 
     | p($sum(J,2), X) ))).

tff(a, axiom, ( 
   ! [I:$int, J:$int, X:$i] :
     ( $lesseq(I, J)
     | ~p($sum(I,3), X) 
     | ~p($sum(J,4), X) ))).
