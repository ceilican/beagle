import AssemblyKeys._

name := "beagle"

version := "0.9"

scalaVersion := "2.11.6"

//incOptions := incOptions.value.withNameHashing(true) 

scalaSource in Compile <<= baseDirectory(_ / "src/")

unmanagedBase in (Compile, run) <<= baseDirectory(_ / "lib/")

mainClass in (Compile, run) := Some("beagle.main")

retrieveManaged := true

// scalacOptions += "-g:vars"

// scalacOptions += "-unchecked"

// scalacOptions += "-Dscalac.patmat.analysisBudget=620"

// scalacOptions += "-optimise"

// libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.1"

//scalaCheck libraries
resolvers ++= Seq(
  "Sonatype Releases" at "http://oss.sonatype.org/content/repositories/releases"
)

libraryDependencies ++= Seq(
  "org.scalacheck" %% "scalacheck" % "1.11.4"// % "test"
)

scalacOptions ++= List("-feature", "-deprecation")

scalacOptions ++= List("-language:implicitConversions", "-language:postfixOps")

//want to exclude test code from final jar
assemblySettings

mainClass in assembly := Some("beagle.main")

jarName in assembly := "beagle.jar"

test in assembly := {}

//this excludes beagle/test but not scalatest...
mergeStrategy in assembly <<= (mergeStrategy in assembly) { (old) =>
  {
    case PathList("beagle","test",xs @ _*) => MergeStrategy.discard
    case x => old(x)
  }
}

//exclude scalatest
excludedJars in assembly <<= (fullClasspath in assembly) map { cp => 
  cp filter {_.data.getName matches """scalatest.*\.jar"""}
}

