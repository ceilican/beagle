#!/bin/bash

#REQUIRES: bash, sbt, scala, svn, firefox

# HOW TO RUN:
# ----------------------------------
# Expects to run from the beagle directory
# ./json_test.sh [-t | TIMEOUT] [-o | BEAGLE OPTION STRING] [FILES]

# More info:
# ----------------------------------
# Logs output to a single json formatted file in json_logs/
# name files as [TIME]_[DATE]_[VERSION].json, e.g. 1501_290414_474.json
# these can be opened manually in from the webpage using the form displayLogs.html#FILENAME, e.g. displayLogs.html#1501_290414_474.json
# or you can choose the relevant file from the menu on the page and it will load.

# metadata recorded: beagle SVN version, date, options, timeout
# for each file: Name, Time, Status
# status is one of: Theorem, Unsatisfiable, Countersatisfiable, Unknown, Timeout, Error

#TODO- filter by problem sets & redraw
#TODO- clean UI (esp. for diff mode)
#-panning/scroll graph, only show ~20 or so
#TODO- don't produce any output if something goes wrong...

#Note that time might be different from beagle reported time, uses /usr/bin/time instead
# if timeout, time=-1

#structure:
# {
# version:[SVN version of /src],
# date:d,
# options:[string following -o],
# timeout:[string following -t],
# results=[{result}]
# }

# result={
# name:
# time:
# status:
# expected:
# }

ROOT=`pwd`

#this folder used for output of trace files
OUT_DIR="./tools/json_logs"
if [ ! -d "$OUT_DIR" ]
then
    mkdir $OUT_DIR
fi


if [ ! $TPTPHome ]
then
    TPTPHome="/home/jbax/TPTP-v5.4.0"
fi

#BEAGLE="java -jar target/scala-2.10/beagle.jar"
BEAGLE='scala -Dfile.encoding=UTF-8 -J-Xss400m -classpath '$ROOT'/lib/"*":'$ROOT'/target/scala-2.11/classes/ beagle.main'

echo $BEAGLE

# Format to use when printing time statistics- see time man page.
TIME_F='TIME=%U'

# time out suffix can be s, m or h- see sleep man page
TIME_LIM=5m

PROVER_OPTS=''

#parse options
while getopts "o:t:m:" optname
  do
    case "$optname" in
      "o")
        echo "Prover options are: $OPTARG"
        PROVER_OPTS=$OPTARG
        ;;
      "t")
        echo "Timeout set to: $OPTARG"
        TIME_LIM=$OPTARG
        ;;
      "m")
	echo "Message set to: $OPTARG"
	MSG=$OPTARG
	MSG_SET=1
	;;
      "?")
        echo "Unknown option $OPTARG"
        ;;
      ":")
        echo "No argument value for option $OPTARG"
        ;;
      *)
      # Should not occur
        echo "Unknown error while processing options"
        exit
        ;;
    esac
    
  done

shift $((OPTIND - 1))
#echo $OPTIND

VERSION=`svn info src | grep Revision | cut -d' ' -f2`
echo "Building latest beagle from src version $VERSION"
sbt compile

#array counter
i=0

num_probs=$#

declare -a RESULTS

for prob in $*
do
    echo "Problem $(($i+1)) of $num_probs: "`basename $prob`
    TMP_OUT="$OUT_DIR/`basename $prob`.tmp"
    #( /usr/bin/time -f$TIME_F -a -o$TMP_OUT $BEAGLE $PROVER_OPTS -tptp $TPTPHome $prob > $TMP_OUT || echo $? > $TMP_OUT ) & pid=$!
    #jvm_pid=$(ps | grep java | cut -d ' ' -f 1)
    #( sleep $TIME_LIM && kill $pid) 2>/dev/null & watcher=$!
    
    #CPULimitedRun always gives exit status 0 even in timeout
    #so append error stream to tmp file
    #( `/usr/bin/time -f$TIME_F -a -o$TMP_OUT ./CPULimitedRun $TIME_LIM $BEAGLE $PROVER_OPTS -tptp $TPTPHome $prob 1> $TMP_OUT` ) & pid=$!

    #print theorem status from file
    expected=`grep "Status" $prob | cut -d: -f2`
    expected=${expected:1}
    #default value if not found:
    [ ! "$expected" ] && expected="?"

     #if wait $pid 2>/dev/null; then
    # 	#if an error then tmp has only the error code
    # 	# if [ `wc -w $TMP_OUT | cut -d' ' -f1` = '1' ]; then 
    # 	#     status='Error'
    # 	#     time=-1
    # 	# else
    # 	#     # expect format 'SZS status X for FILE
    # 	#     status=`grep "SZS" $TMP_OUT | cut -d' ' -f3`
    # 	#     time=`grep "TIME" $TMP_OUT | cut -d'=' -f2`
    #     # fi
        
  	#pkill -HUP -P $watcher
    	#wait $watcher
    # else
    # 	status="Timeout"
    # 	time=-1
    #fi

    err=`/usr/bin/time -f$TIME_F -a -o$TMP_OUT ./tools/CPULimitedRun $TIME_LIM $BEAGLE $PROVER_OPTS -tptp $TPTPHome $prob 2>&1 >$TMP_OUT`
    pid=$!
    if wait $pid 2>/dev/null; then
	if [ -z "$err" ]; then
    	    # expect format 'SZS status X for FILE
    	    status=`grep "SZS" $TMP_OUT | cut -d' ' -f3`
    	    time=`grep "TIME" $TMP_OUT | cut -d'=' -f2`
	elif [ -n "`echo "$err" | grep 'time limit exceeded' -`" ]; then
    	    echo 'Timeout!'
    	    status='Timeout'
    	    time=-1
	else
	    echo $err
    	    status='Error'
    	    time=-1
	fi
    fi

    RESULTS[$i]='{"name":"'`basename $prob`'","time":'$time',"status":"'$status'","expected":"'$expected'","path":"'$prob'"}'
    i=$(($i + 1))
	
done

rm $OUT_DIR/*'.tmp'

#ask the user for an optional message to describe the particular run
if [ $MSG_SET != 1 ]; then
    echo 'Enter a one line description of this run:'
    read MSG
fi

#print all of results to json out file
FILENAME="`date +%H%M_%d%m%y`_$VERSION"
[ ! -f "$OUT_DIR/index" ] && echo $FILENAME > "$OUT_DIR/index" || echo "$FILENAME '$MSG'" >> "$OUT_DIR/index"

OUTFILE="$OUT_DIR/$FILENAME.json"

#RESULTS concatenated with ','
RES_STR=""
for s in ${RESULTS[*]}
do
    RES_STR="$RES_STR,$s"
done
RES_STR=${RES_STR:1} #drop first comma

echo '{"version":"'$VERSION'","date":"'`date`'","options":"'$PROVER_OPTS'","timeout":"'$TIME_LIM'","results":['$RES_STR']}' > $OUTFILE

#launch results in browser
firefox "./tools/displayLog.html#$FILENAME.json"
