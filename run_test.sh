#!/bin/sh

# Run from the root beagle directory
# Runs beagle on problems given in input, options can be passed to the prover
# via the command line also, use option 'o'.
# Adds time statistics for all problems and breaks if different status is reported

#default problem directory, not really used
ROOT="./test"

#this folder used for output of trace files
if [ ! -d "out" ]
then
    mkdir out
fi
OUT_DIR="./out"

if [ ! $TPTPHome ]
then
    TPTPHome="/home/jbax/TPTP-v5.4.0"
fi

BEAGLE="./beagle"

# Format to use when printing time statistics- see time man page.
TIME_F='user: %Us\nmem: %Kk'

# time out suffix can be s, m or h- see timeout man page
TIME_LIM=5m

PROVER_OPTS=''

#parse options
while getopts "o:t:r:" optname
  do
    case "$optname" in
      "o")
        echo "Prover options are: $OPTARG"
        PROVER_OPTS=$OPTARG
        shift
	shift
        ;;
      "t")
        echo "Timeout set to: $OPTARG"
        TIME_LIM=$OPTARG
        shift
        shift
        ;;
      "r")
	echo "Report output to: $OPTARG"
	REPORT_FILE=$OPTARG
	DO_REPORT=1
	shift
	shift
	;;
      "?")
        echo "Unknown option $OPTARG"
        shift
        ;;
      ":")
        echo "No argument value for option $OPTARG"
        ;;
      *)
      # Should not occur
        echo "Unknown error while processing options"
        exit
        ;;
    esac
    
  done

#counter for batch progress
i=1

num_probs=$#

# for each problem (text file) in user input
for prob in $*
do
	#reset timeout flag
	timeout=0

	# report start
	echo "Problem $i of $num_probs: "`basename $prob`
	echo "Now starting beagle on "$prob""
	
	out=$OUT_DIR"/"`basename $prob`.out
	
	# run beagle with choco, dump text to CSP_DIR
	timeout --foreground $TIME_LIM \
		/usr/bin/time -f "$TIME_F" -a -o $out \
		$BEAGLE $PROVER_OPTS -tptp $TPTPHome $prob > $out
	
	#print theorem status from file
	expected=`grep "Status" $prob | cut -d : -f 2`
	echo "Expected: $expected"
	
	#check for timeout-
        #124 is timeout status returned by timeout command.
	if [ $? -eq 124 ] 
	then
		echo "-Beagle timed out."
		echo "Time out" >> $out
		timeout=1
	fi
	
	# report
	status=`grep "SZS" $out`
	
	echo "Beagle: $status"

	[ $DO_REPORT ] && (
	    echo "$prob\t$expected\t$status" >> $REPORT_FILE
	)

	# report- exit if differences
	#if [ "$status_csp" != "$status_norm" ]
	#then
	#	echo "ERROR- CSP and Cooper report different things"
	#	exit
	#elif [ $timeout ]
	#then
	#	echo "Timeout, results ignored..."
	#else
	#	echo "Done $i\nChoco: $status_csp\nCooper: $status_norm\n"
	#fi
	
	echo "-------------\n"
	
	i=$(($i + 1))
	
done
