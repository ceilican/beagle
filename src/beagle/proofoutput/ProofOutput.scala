package beagle.proofoutput

import java.io.File
import java.io.PrintWriter

import beagle._
import fol._
import datastructures._
import util.TFFPrinter
import util.Reporter

//TODO- set of support not handled
//TODO- report chaining inferences

/*
 * In derivations the annotated formulae names must be unique, so that
 * parent references (see <inference_record>) are unambiguous.
 */

/**
 * As per TFF Grammar
 * default for usefulInfo below is [status(thm)]
 */

//an info item is a <formula_item> | <inference_item> | <general_function>
//a formula item is a description item or a quote item-<iquote_item>s are used for recording exactly what the system output about
// the inference step
/*
an inference item is  <inference_status> | <assumptions_record> |
                         <new_symbol_record> | <refutation>
<inference_status>   :== status(<status_value>) | <inference_info>
%----These are the success status values from the SZS ontology. The most
%----commonly used values are:
%----  thm - Every model of the parent formulae is a model of the inferred
%----        formula. Regular logical consequences.
%----  cth - Every model of the parent formulae is a model of the negation of
%----        the inferred formula. Used for negation of conjectures in FOF to
%----        CNF conversion.
%----  esa - There exists a model of the parent formulae iff there exists a
%----        model of the inferred formula. Used for Skolemization steps.
%----For the full hierarchy see the SZSOntology file distributed with the TPTP.
<status_value>       :== suc | unp | sap | esa | sat | fsa | thm | eqv | tac |
                         wec | eth | tau | wtc | wth | cax | sca | tca | wca |
                         cup | csp | ecs | csa | cth | ceq | unc | wcc | ect |
                         fun | uns | wuc | wct | scc | uca | noc
%----<inference_info> is used to record standard information associated with an
%----arbitrary inference rule. The <inference_rule> is the same as the
%----<inference_rule> of the <inference_record>. The <atomic_word> indicates
%----the information being recorded in the <general_list>. The <atomic_word>
%----are (loosely) set by TPTP conventions, and include esplit, sr_split, and
%----discharge.
<inference_info>     :== <inference_rule>(<atomic_word>,<general_list>)

refutation/assumptions not used


%----A <new_symbol_record> provides information about a newly introduced symbol.
<new_symbol_record>  :== new_symbols(<atomic_word>,[<new_symbol_list>])
<new_symbol_list>    :== <principal_symbol> |
                         <principal_symbol>,<new_symbol_list>
                         * 
                         */
                         
//so info items are either inference status or new symbol records
//inference status

//TODO- new symbol marker
//TODO- definition marker

/*
<formula_role>       :== axiom | hypothesis | definition | assumption |
                         lemma | theorem | conjecture | negated_conjecture |
                         plain | fi_domain | fi_functors | fi_predicates |
                         type | unknown
%----"axiom"s are accepted, without proof. There is no guarantee that the
%----axioms of a problem are consistent.
%----"hypothesis"s are assumed to be true for a particular problem, and are
%----used like "axiom"s.
%----"definition"s are intended to define symbols. They are either universally
%----quantified equations, or universally quantified equivalences with an
%----atomic lefthand side. They can be treated like "axiom"s.
%----"assumption"s can be used like axioms, but must be discharged before a
%----derivation is complete.
%----"lemma"s and "theorem"s have been proven from the "axiom"s. They can be
%----used like "axiom"s in problems, and a problem containing a non-redundant
%----"lemma" or theorem" is ill-formed. They can also appear in derivations.
%----"theorem"s are more important than "lemma"s from the user perspective.
%----"conjecture"s are to be proven from the "axiom"(-like) formulae. A problem
%----is solved only when all "conjecture"s are proven.
%----"negated_conjecture"s are formed from negation of a "conjecture" (usually
%----in a FOF to CNF conversion).
%----"plain"s have no specified user semantics.
%----"fi_domain", "fi_functors", and "fi_predicates" are used to record the
%----domain, interpretation of functors, and interpretation of predicates, for
%----a finite interpretation.
%----"type" defines the type globally for one symbol; treat as $true.
%----"unknown"s have unknown role, and this is an error situation.
*/

/**
 * Superclass of `InfRecord` and `Justified`, needed because inferences
 * can depend on either an inference or a justified fact.
 */
abstract class InfOrJust {
  /** The id  of the clause either produced by the inference or previously justified. */
  val id: Int
}

/**
 * Represents leaves on the proof tree, usually axioms or tautologies.
 */
case class Justified(id: Int) extends InfOrJust

/**
 * Record the fact that cl was produced from parents using the rule with name `rule`.
 * This also contains most of the fields found in the TPTP entry for inferences.
 */
class InfRecord(val cl: Clause, rule: String, val parents: List[InfOrJust]) extends InfOrJust {
  val id = cl.id
  val role = "plain"
  val annotations = "inference("+rule+",[status(thm)],"+parents.map(_.id).mkString("[",",","]")+")"

  override def toString() = {
    //val c = Clause(cl.lits.filterNot(_.isTrivialNeg),cl.idxRelevant,cl.age)
    //TFFPrinter.customClauseToString(c,id.toString,role,annotations)
    TFFPrinter.customClauseToString(cl,id.toString,role,annotations)
  }
}

/* Types of inferences */

case class Paramod(result: Clause, from: InfOrJust, into: InfOrJust) extends InfRecord(result,"paramodulation",List(from,into)) 
case class Ref(result: Clause, parent: InfOrJust) extends InfRecord(result, "refutation", List(parent))
case class Fact(result: Clause, parent: InfOrJust) extends InfRecord(result, "factoring", List(parent))
case class Simp(result: Clause, reason: String, using: List[InfOrJust]) extends InfRecord(result, "simp_"+reason, using)
case class LSplit(result: Clause, parent: InfOrJust) extends InfRecord(result, "split_left", List(parent))
case class RSplit(result: Clause, parent: InfOrJust) extends InfRecord(result, "split_right", List(parent))

case class CooperUnsatCore(coreClauses: List[InfOrJust]) extends InfRecord(Clause(List(),Set(),0).asInstanceOf[Clause], "cooperUnsatCore", coreClauses)

case class Cooper(result: Clause, parent: InfOrJust) extends InfRecord(result, "cooper", List(parent))

//case class CooperAxiom(result: Clause) extends InfRecord(result, "cooper_axiom", List())


/**
 * Inference of the form: `tff(_id_, axiom, _formula_, file(_filename_, axiom))`
 * @todo filename is not printed, how do we resolve axioms from lemma files?
 */
case class Axiom(result: Clause) extends InfRecord(result, "axiom", List()){
  override val role = "axiom"
  override val annotations = "file('',axiom)"
}

case class Definition(result: Clause, parent: InfOrJust) 
     extends InfRecord(result, "definition", List(parent)) {

  val defSymbolName = result.lits.head.eqn.rhs.toString
  override val annotations = "introduced(definition,[new_symbols(definition, ["+defSymbolName+"])])"
}

/**
 * Writes a proof using TPTP syntax.
 * It does this by storing `justifications' for every new clause in a stack;
 * when these are added to `old` the explanation is popped and written to file.
 * @param consoleReporter the reporter to use for writing to the console.
 */
class TFFProofReporter(outputFilename: String, consoleReporter: Reporter = util.DebugReporter) extends Reporter {

  private val writer = new PrintWriter(new File(outputFilename))

  val falseClause = Clause(List(),Set(),0).asInstanceOf[Clause]

  /** Stores proof trees for clauses in neu */
  var justifications = new Map.WithDefault(Map[Int,InfOrJust](), (id: Int) => Justified(id))

  /** Records the ids of clauses that are axioms or whose justifications have been printed */
  var justified = Set[Long]()

  /**
   * Store the split history
   */
  object SplitClauses {
    var splitClauses = List[Clause]()
    
    //split on C -> current DL+=1
    def newSplit(cl: Clause): Unit = {
      splitClauses :+= cl
    }
    
    //backtrack to DL x -> add RHS clauses from last split at that DL
    def backtrack(target: Int): Clause = {
      val res = splitClauses(target)
      splitClauses = splitClauses.take(target)
      res
    }
  }

  /* Methods from Reporter, these should pass their parameters thru to consoleReporter */

  override def onPara(from: Clause, into: Clause, res: Clause, sigma: Subst) {
    consoleReporter.onPara(from, into, res, sigma)
    // println(s"${res.id} by para from ${from.id} into ${into.id}")
    justifications += 
      (res.id -> Paramod(res, justifications(from.id), justifications(into.id)))
  }
  
  override def onInference(selected: Clause, result: Clause, infType: String) {
    consoleReporter.onInference(selected, result, infType)
    justifications +=
      (result.id -> 
       (infType match {
	 case "Ref" => Ref(result, justifications(selected.id))
	 case "Fact" => Fact(result, justifications(selected.id))
   case "Cooper" => Cooper(result, justifications(selected.id))
       }))
  }
  
  /**
   * Called when a new split is made.
   * @param result the left clause of the split
   * @param decisionLevel the current decision level, this will be
   * backtracked to if the LH clause is refuted.
   */
  override def onSplit(selected: Clause, result: List[Clause], decisionLevel: Int) {
    SplitClauses.newSplit(selected)
    result foreach { cl => 
      justifications += (cl.id -> LSplit(cl, justifications(selected.id)))
    }
  }
    
  /**
   * Selected clause has been deleted by simplification.
   */
  override def onClauseDeleted(deleted: Clause) {
    consoleReporter.onClauseDeleted(deleted)
    justifications -= deleted.id
  }
    
  /**
   * An added BG clause is inconsistent wrt other BG clauses
   */
  override def onBGInconsistent(cl: Clause, old: Iterable[Clause]) {
    consoleReporter.onBGInconsistent(cl, old)

    //can immediately print false to file
    val parents = (cl.asInstanceOf[Clause].id :: 
		     old.map(_.asInstanceOf[Clause].id).toList)
		    .map(Justified(_))
    //val inf = new InfRecord(falseClause, "bg_solver_close", parents)
    val inf = CooperUnsatCore(parents)
        
    writer.println(inf)
  }

  override def selectedClause(cl: Clause, decisionLevel: Int) { 
    consoleReporter.selectedClause(cl, decisionLevel)
  }

  override def onNewState(old: ListClauseSet, neu: PQClauseSet) {
    consoleReporter.onNewState(old, neu)
  }
    
  /**
   * @param level is the target decision level
   * @param old is the set of retained clauses at the new decision level
   * @param neu is the set of new clauses at the new decision level
   */
  override def onBacktrack(level: Int, rightClauses: List[Clause], old: ClauseSet, neu: ClauseSet) = {
    super.onBacktrack(level, rightClauses, old, neu)

    //print right clauses to file and retrieve original parent.
    val parent = SplitClauses.backtrack(level).asInstanceOf[Clause]
    val pInf = justifications(parent.id)
    
    rightClauses foreach { case c: Clause => 
      justifications += (c.id -> RSplit(c, pInf)) 
    }
  }
  
  override def onSimplified(selected: Clause, simplified: Clause) {
    consoleReporter.onSimplified(selected, simplified)
    println(s"${simplified.id} by simp of ${selected.id}")
    if (simplified.isEmpty) //must print justification for empty
      onClauseAdded(simplified)
  }
  
  override def detailedSimp(reason: String, selected: Clause, simplified: Clause, clUsed: List[Clause]) {
    consoleReporter.detailedSimp(reason, selected, simplified, clUsed)
    println(s"${simplified.id} by detailedsimp of ${selected.id}")
    justifications += 
      (simplified.id -> 
       Simp(simplified, "demod", clUsed map { c => justifications(c.asInstanceOf[Clause].id) }))
  }

  override def onDefine(selected: Clause, defClauses: Iterable[Clause], newClause: Clause) = {
    consoleReporter.onDefine(selected, defClauses, newClause)
    // Peter: not sure if this is the right thing
    defClauses foreach { defClause =>
      val defInf = Definition(defClause, justifications(selected.id))
      justifications += (defClause.id -> defInf)
      justifications += (newClause.id -> Simp(newClause, "demod", justifications(selected.id) :: defInf :: Nil))
    }
  }
  
  /**
   * A clause has been added to `old`- the set of retained clauses.
   * Notice that this is the point that the proof tree of clauses in `neu`
   * are popped from justifications and printed.
   */
  override def onClauseAdded(cl: Clause) {
    consoleReporter.onClauseAdded(cl)
    //print the whole justification tree if needed
    println("justified:")
    println(justified)
    println("justifications:")
    println(justifications)

    //recurse over the proof trees
    def recSearch(inf: InfRecord) {
      for(p <- inf.parents) {
        	p match {
        	  case Justified(_) => ()
        	  case i2: InfRecord if(!justified(i2.id)) => recSearch(i2)
        	  case _ => ()
        	}
      }
      //by now, all of infs parents are printed or justified
      //safe to print 
      writer.println(inf)
      
      println("adding " + inf.id + " to justified and removing it from justifications")
      
      justified+=inf.id
      justifications-=inf.id
    }
    
    val id = cl.id
    
    justifications(id) match {
      case Justified(_) => {
	      if(!justified(id)) {
          println("Exception!!!!!!!!!!!!!!!!")
          println(id)
          println(justifications(id))
          println(justified(id))
          
          throw new Exception(s"$cl with id=$id does not have a justification")  
        }
	      // else already justified so ok...
      }
      case inf: InfRecord => recSearch(inf)
    }
  }
  
  //def onNewState(old: ListClauseSet, neu: PQClauseSet) = ()
  
  /** Add all of the initial clauses as axiom inferences to `justified`. */
  override def onProofStart(initClauses: List[Clause]) = {
    super.onProofStart(initClauses)

    initClauses foreach { case c: Clause => 
      justifications += (c.id -> Axiom(c)) 
    }
  }
  
  override def onClose(decisionLevel: Int, emptyCl: Clause) {
    consoleReporter.onClose(decisionLevel, emptyCl)

    //some clause in neu is the empty clause, find and give to add
    /*
    val closeCl = justifications.values.find { case i:InfRecord => i.cl.isEmpty; case _:Justified => false }
    closeCl match {
      case Some(i:InfRecord) => this.onClauseAdded(i.cl)
      case _ => throw new Exception("Close thrown but no empty clause found in justifications.")
    }
    */
    this.onClauseAdded(emptyCl)
  }

  //def onClauseSubsumed(cl: Clause, subsumed: Clause) {}
  
  /**
   * Called when a proof is successfully finished.
   */
  //def onProofEnd() = {}
  
  /**
   * Always called when program exits- either normally or terminated.
   * Use for finishing writes etc.
   */
  override def onExit() = {
    consoleReporter.onExit()
    writer.close
  }
  
}

case class InputRecord(file: String, 
		       fs: List[Formula], 
		       conjecture: Option[Formula], 
		       lemmas: List[InputRecord],
		       axioms: List[Formula])
