
package beagle

import fol._
import datastructures._

package object util {

  //TODO- for backwards compatibility, move to a more sensible place later
  //TODO- does Flags object still exist?
  val flags = new Flags

  // Used for handling conjectureFGOps in TPTPParser
  implicit class MyOptionSet[T](o: Option[Set[T]]) {
    def optUnion(p: Option[Set[T]]) = 
        (o, p) match {
          case (None, None) => None
          case (Some(oOps), None) => Some(oOps)
          case (None, Some(pOps)) => Some(pOps)
          case (Some(oOps), Some(pOps)) => Some(oOps union pOps)
        }
  }

  implicit class MySet[T](s: Set[T]) {
    /** set intersection emptiness test - not directly in the scala API it seems */
    def intersects(s1: Set[T]) = s exists { s1.contains(_) }
  }

  // implicit def toMySet[T](s: Set[T]) = new MySet(s)

  implicit class MyList[T](l: List[T]) {

    def removeNth(n: Int) = {
      //equal to l.take(n-1)++l.drop(n+1)
      //@annotation.tailrec
      def hRemoveNth(l: List[T], n: Int): List[T] = {
        // could do a better non-tail recursive version
        if (n == 0)
          l.tail
        else
          l.head :: hRemoveNth(l.tail, n - 1)
      }
      hRemoveNth(l, n)
    }

    def replaceNth(n: Int, x: T) = 
      l.updated(n,x)
      /*{
      //equal to l.updated(n,x)
      def hReplaceNth(l: List[T], n: Int, x: T): List[T] = {
        // could do a better non-tail recursive version
        if (n == 0)
          x :: l.tail
        else
          l.head :: hReplaceNth(l.tail, n - 1, x)
      }
      hReplaceNth(l, n, x)
    }*/

    /** Removes the first element that satisfies the predicate p.
     * Returns the removed element and the list with the element removed, if such an element exists
      */
    def extract(p: T => Boolean): Option[(T, List[T])] = {
      var rest = l
      var soFar = List[T]()
      while (!rest.isEmpty) {
        if (p(rest.head))
          return Some((rest.head, soFar.reverse ::: rest.tail))
        soFar ::= rest.head
        rest = rest.tail
      }
      // Coming here means nothing hs been found
      return None
    }

    def toListSet = {
      var res = List.empty[T]
      for (x ← l)
        if (!(res contains x)) res ::= x
      res.reverse
    }

    def toMyString(toStringFn: T ⇒ String, ifEmpty: String, lsep: String, isep: String, rsep: String): String =
      if (l.isEmpty)
        ifEmpty
      else
        lsep + l.map(toStringFn(_)).reduceLeft(_ + isep + _) + rsep

    // Convenience Functions
    def toMyString(ifEmpty: String, lsep: String, isep: String, rsep: String): String =
      toMyString(x ⇒ x.toString, ifEmpty, lsep, isep, rsep)

    def toMyString(lsep: String, isep: String, rsep: String): String =
      toMyString(lsep + rsep, lsep, isep, rsep)

  }

  // implicit def toMyList[T](s: List[T]) = new MyList(s)

  var printer: Printer = DefaultPrinter
  var reporter: Reporter = DefaultReporter
  
  def verbose = !flags.quietFlag.value
  def debug = flags.debug.value == "inf"
  
  /** 
   *  Hidden flag which can be set during parsing to indicate a clause set with
   *  no equality. In this case we should not perform positive superposition as it
   *  is always trivial.
   */
  var noEquality = false;

  /**
   * Set to true if there are no BG theory operators at all in the input.
   * This way we can short-circuit all abstraction.
   * @deprecated
   */
  var noAbstraction = false

  var warningCnt = 0
  def warning(s: String) {
    warningCnt += 1
    reporter.log("Warning: " + s)
  }

  class Counter {
    var ctr = 0;
    def next() = {
      ctr += 1;
      ctr
    }
  }

  abstract class Message extends Exception {
    val s: String
  }

  case class GeneralError(s: String) extends Message

  case class SyntaxError(s: String) extends Message

  case class InternalError(s: String) extends Message

  case class CmdlineError(s: String) extends Message

  //TODO- this can usually be replaced by using the ??? operator
  // which throws a similar exception when called.
  case class NotImplemented(method: String) extends Exception(method+" is not implemented")

  /**
   * Thrown when a computation produces a value larger than MaxInt
   * Ideally we could just return a Long or BigInt instead but everywhere else
   * we have used Int so would need to cast or use some clever implicits to
   * avoid changing everything.
   */
  case object IntOverflowException extends Exception

  // def gcd(a: Int, b: Int) = gcd1(a max b, a min b)
  // def gcd1(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
  
  /*
   * When broken into two methods you can get a stack overflow exception
   * for large numbers or frequent calls.
   * Putting it in a single method and using the final keyword allows
   * the scala compiler to recognise the recursion and optimise.
   */
  final def gcd(x: Long, y: Long): Long = {
    val a = x.abs //Math.abs(x)
    val b = y.abs //Math.abs(y)
    val c = a max b
    val d = a min b
    
    if (d == 0) c
    else gcd(d, c % d)
  }

  def gcd(x: Int, y: Int): Int = gcd(x.toLong,y.toLong).toInt
  
  /**
   * @return least positive common multiple of a and b.
   * @throws IntOverflow if LCM of a and b is larger than Int.MaxValue.
   */
  def lcm(a: Int, b: Int) = {
    if(a==0 && b==0) 0
    else {
      val res = a.abs * (b.abs / gcd(a, b))
      if(res < 0) throw IntOverflowException
      res
    }
  }
  
  //TODO- check fastest LCM?
  //1- l.reduce(lcm)
  //2- lcm(l)
  //3- lcmCachedGCD(l)

  /**
   * LCM for lists which checks for integer overflow.
   * Use this instead of list.reduce(lcm(_,_))
   * @throws IntOverflowException if integer overflow is detected.
   */
  def lcm(l: List[Int]): Int = {
    def h(ls: List[Int], acc: Int): Int = {
      if(ls.isEmpty) acc
      else {
	val a = ls.head
	//in the final product the gcd of a & x is contributed by a
	val nextls = ls.tail.map(x => x/gcd(a,x))
	//apply the same to the tail and store in acc
	if(acc*a < 0) throw IntOverflowException
	h(nextls,acc*a)
      }
    }
    
    h(l.map(_.abs).sorted.reverse,1)
  }

  /**
   * LCM version using caching of some GCDs
   * Probably not needed since bug was actually integer overflow not
   * performance related.
   */
  /*
  def lcmCachedGCD(l: List[Int]): Int = {
    
    //lcm(a1*a2,c) = a1*a2*c/(gcd(a1,c)*gcd(a2,c))
    //usually a1 is a cached gcd...
    //returns (lcm(a1*a2,b),gcd(a1*a2,b))
    def lcm(a1: Int, a2: Int, b: Int): (Int,Int) = {
      val d = gcd(a1,b)*gcd(a2,b)
      val res = Math.abs(a1)*Math.abs(a2)*(Math.abs(b)/d)
      if(res < 0) throw new InternalError("Integer overflow while computing LCM")
      (res,d)
    }

    //invariant r.head is always last lcm and gcd|r.head
    def lcmInner(r: List[Int],gcd: Int): Int = {
      r match {
	case List() => 0
	case List(lcm) => lcm
	case lcmAcc::next::t => {
	  val (l,gcdNext) = lcm(gcd,lcmAcc/gcd,next)
	  lcmInner( l :: t, gcdNext)
	}
      }
    }

    if(l.isEmpty) 1 //TODO- correct?
    else if (l.tail.isEmpty) Math.abs(l.head)
    else if (l.indexOf(0) >= 0) 0
    else {
      var posL = l.map(Math.abs(_)) //lcm is always positive
      val noMultiples = posL.filterNot(x => posL.exists(y => y!=x && y%x==0)).distinct
      val in = noMultiples
      lcmInner(in,gcd(in.head,in.tail.head))
    }
  }
*/
}
