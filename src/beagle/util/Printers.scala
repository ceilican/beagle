package beagle.util

import beagle._
import datastructures._
import fol._
import term._

/**
 * Switchable definitions for how terms, clauses and formulas should be
 * printed. The two main instances implement printing for normal mode
 * which uses ASCII symbols e.g. ∨, ∧ in formulas and TPTP mode which
 * substitutes the usual TPTP equivalents e.g. |, & and also adds an
 * appropriate prefix for clauses e.g. `tff(name,role, ...).`
 * This is not enough to get clean input for another prover as there is
 * still some program output in the trace. Hence the need for `Reporter`
 * classes.
 * The current printer is stored in `util.printer`, which is set by `main`
 * depending on the initial value of the `-print` flag.
 * Note that currently setting the `-proof` flag forces a TFFPrinter.
 */
abstract class Printer {
  //Common symbols
  val negSym = "¬"
  val orSym = "∨"
  val andSym = "∧"
  val impSym = "⇒"
  val convImpSym = "⇐"
  val iffSym = "⇔"
  val iffNotSym = "⇎"
  val emptyClauseSym = "□"
  val equalSym = "≈"
  val equalOrderedSym = "→"
  val forallSym = "∀"
  val existSym = "∃"
  val falseSym = "⊥"
  val trueSym = "⊤"

  /** Prefix for skolem constants/functions generated via CNF transform */
  val skolemCNFPref = "#skf_"
  /** Prefix for skolem constants generated via define */
  val skolemEltPref = "#e_"
    
  //Common format functions
  val sumFF = Operator.infix(" + ")
  val uminusFF = Operator.prefix("-")
  val greaterFF = Operator.infix(" > ")
  val dividesFF = Operator.infix("|")
  val lessFF = Operator.infix(" < ")
  val differenceFF = Operator.infix(" - ")
  val productFF = Operator.infix("·")
  val quotientFF = Operator.infix("/")
  val lessEqFF = Operator.infix(" ≤ ")
  val greaterEqFF = Operator.infix(" ≥ ")
  
  def sortToString(s: Sort): String
  def opToString(op: Operator): String
  def arityToString(a: Arity): String
  def clauseToString(c: Clause): String
  def litToString(l: Lit): String
  def eqnToString(e: Eqn): String
  //def atomToString(a: Atom): String
  def domElemToString[T](d: DomElem[T]): String
  def quantifierToString(formula: QuantForm): String
  def varToString(v: Var): String
  def signatureToString(s: Signature): String
  def letFormulaToString(f: LetFormula): String
  def letTermToString(t: Let): String
  def pp(f: Formula): Unit // pretty printer

  /** Used by pretty printer */
  def nlspaces(n: Int) {
    println()
    for (i <- 1 to n) print(' ')
  }

}


object DefaultPrinter extends Printer {
   
  def sortToString(s: Sort): String = s.name 
  
  def opToString(op: Operator): String = 
    if (beagle.util.flags.longNames.value) op.name else op.shortName
  
  def arityToString(a: Arity): String = 
    a.argsSorts.mkString(" × ") + " → " + a.resSort
  
  def clauseToString(c: Clause): String = {
    (if (flags.raw.value) {
      def annotation(i: Int) =
	(if (c.iMaximal contains i) "*" else "") + (if (c.iEligible contains i) "+" else "")
      // Not very elegant
      (if (c.isEmpty) emptyClauseSym
       else {
	 var res = c.lits(0).toString + annotation(0)
	 for (i ← 1 until c.length)
	   res = res + " "+orSym+" " + c.lits(i).toString + annotation(i)
	 res
       }) // + (if (idxRelevant.isEmpty) "" else ":" + idxRelevant.toList.toMyString("{", ", ", "}"))
    } else {
      if (c.unabstrCautious.isEmpty) emptyClauseSym 
      else c.unabstrCautious.lits.tail.foldLeft(c.unabstrCautious.lits.head.toString)((s, l) ⇒ s + " "+orSym+" " + l.toString)
    }) // + ":" + (c.operators filter { op => op != TTOp && op.kind == FG })  // + ":" + c.weight + ":" + c.age  
    // Cannot do this: infinite recursion
    // + (if (c.isBG && !c.isSolverClause)
    //           " (as BG solver clauses: " + c.asSolverClauses.get.toMyString("{", ", ", "}") + ")"
    //         else "" )
	  }
	
  def litToString(l: Lit): String =
    (l.eqn match {
      case PredEqn(equ) ⇒ (if (l.isPositive) "" else negSym) + l.eqn.toString // Works also for PseudoVar and Assert  
      case _ ⇒ (if (l.isPositive) l.eqn.toString else negSym+"(" + l.eqn.toString + ")")
    }) // + (if (l.canonical != l) "{" + l.canonical + "}" else "")
  
  def eqnToString(e: Eqn): String = {
    (e match {
      case PredEqn(a) ⇒ a.toString// + "{" + a.canonical + "}"
      case _ ⇒ {
        val eqsym = if (e.isOrdered) equalOrderedSym else equalSym
        if (e.sort == Signature.ISort)
          e.lhs.toString + eqsym + e.rhs.toString
        else
          e.lhs.toString + " " + eqsym + "_" + e.sort + " " + e.rhs.toString  // + ":" + weight
      }
    }) // + e.termIndex 
  }
  /*
  def atomToString(a: Atom): String =
    // Generic equality "=" is not an operator in our signatures, hence check explicity
    this match {
      case Equation(s, t) ⇒ "(" + s.toString + " = " + t.toString + ")"
      case _ ⇒ Sigma.findFormatFn(pred, Arity(args.sortsOf, Signature.OSort)) match {
        case Some(fn) ⇒ fn(args)
        case None     ⇒ pred + args.toMyString("", "(", ",", ")") // Fallback 
      }
    }
  */
  
  def domElemToString[T](d: DomElem[T]): String = d.value.toString
  
  def quantifierToString(formula: QuantForm): String = 
    "(" + formula.q + " " + (formula.xs map { _.toStringWithSort }).toMyString("", " ", "") + " " + formula.f + ")"
  
  def letFormulaToString(letf: LetFormula): String = 
    "(let " + (letf.bindings map {
      case (v, t) => v + equalSym + t
    }).toMyString("", " ", "") + " in " + letf.body + ")"

  def letTermToString(let: Let): String = 
    "(let " + (let.bindings map {
      case (v, t) => v + equalSym + t
    }).toMyString("", " ", "") + " in " + let.body + ")"

  def varToString(v: Var): String = 
    v.name + (if (v.index > 0) "_" + v.index else "") + (if (v.isAbstVar) "ᵃ" else "")
  
  /** Does not print defined FG operators unless debug is set */
  def signatureToString(s: Signature): String = {
    "Background sorts: " + s.bgSorts.toList.mkString("{", ", ", "}") + "\n" +
    "Foreground sorts: " + s.fgSorts.toList.mkString("{", ", ", "}") + "\n" +
    "Background operators: " + "\n" +
    s.bgOperators.map(op ⇒ "    " + op.name + ": " + op.arity).mkString("\n") + "\n" +
    "Foreground operators: " + "\n" +
    s.fgOperators.collect({case op if(debug || !op.defined) ⇒ "    " + op.name + ": " + op.arity}).mkString("\n") + "\n" +
    "Precedence among foreground operators: " + 
    ( s.fgOperators.toList.sortWith(s.gtrPrecSimple(_, _)) map { _.name }).mkString(" > ") + "\n" +
    "Templates for Foreground sorts\n" +
    ( s.fgSorts filterNot { so => 
        so == Signature.OSort | so == Signature.TSort 
      } map { so => 
        "    " + so + ": " + s.templates(so).mkString(", ") 
      } mkString("\n"))
  }

  def pp(f: Formula, indent: Int) {
    f match {
      case a @ Atom(_, _) => print(a)
      case UnOpForm(op, f) => {
        print(op)
        pp(f, indent+1)
      }
      case BinOpForm(op, f1, f2) =>
        op match {
          case And | Or => {
            val fs = f.toList(op)
            print("( ");
            for (fss <- fs.tails; if !fss.isEmpty) {
              pp(fss.head, indent+2)
              // More formulas?
              if (!fss.tail.isEmpty) {
                nlspaces(indent)
                print(op.toString + " ")
              }
            }
            print(")")
          }
          case _ => {
            print("( "); pp(f1, indent+2)
            nlspaces(indent)
            print(op.toString + " ")
            pp(f2, indent+2); print(")")
          }
        }
      case QuantForm(q, xs, f) => {
        print(q + " " + (xs map { _.toStringWithSort }).toMyString("", " ", ""))
        nlspaces(indent+2)
        pp(f, indent+2)
      }
      case LetFormula(bindings, body) => {
        print("(let " + (bindings map {
          case (v, t) => v + " = " + t
        }).toMyString("", " ", "") + " in ")
        nlspaces(indent+2)
        pp(body, indent+2)
        print(")")
      }
      case ITEForm(cond, thenForm, elseForm) => {
        print("$ite_f(" + cond + ",")
        nlspaces(indent+2); pp(thenForm, indent+2); print(",")
        nlspaces(indent+2); pp(elseForm, indent+2); print(")")
      }
    }

  }

  def pp(f: Formula) { pp(f, 0) }
}

object TFFPrinter extends Printer {
  override val negSym = "~"
  override val orSym = "|"
  override val andSym = "&"
  override val impSym = "=>"
  override val convImpSym = "<="
  override val iffSym = "<=>"
  //val iffNotSym = "⇎"
  //val emptyClauseSym = "□"
  override val equalSym = "="
  override val equalOrderedSym = "="
  override val forallSym = "!"
  override val existSym = "?"
  override val falseSym = "$false"
  override val trueSym = "$true"
    
  override val sumFF = Operator.functional("$sum")
  override val uminusFF = Operator.functional("$uminus")
  override val greaterFF = Operator.functional("$greater")
  override val dividesFF = Operator.functional("$divides")
  override val lessFF = Operator.functional("$less")
  override val differenceFF = Operator.functional("$difference")
  override val productFF = Operator.functional("$product")
  override val quotientFF = Operator.functional("$quotient")
  override val lessEqFF = Operator.functional("$lesseq")
  override val greaterEqFF = Operator.functional("$greatereq")

  //Note that TFF does not parse names with leading # sign
  override val skolemCNFPref = "skF_"
  override val skolemEltPref = "skE_"
  
 //Assume we never print predefined types like $i, $o, $tType etc.
  //Perhaps we should check for this?
  def sortToString(s: Sort): String = "tff("+s.name+", type, "+s.name+": $tType "+")."
  
  def opToString(op: Operator): String = 
    "tff("+op.name+", type, "+op.name+": "+ op.arity.toString+")."
    
  def arityToString(a: Arity): String = 
    (if (a.nrArgs > 1) a.argsSorts.map(_.name).mkString("(", " * ", ")")+" > "
    else if (a.nrArgs==1) a.argsSorts.head.name+" > "
    else "")+a.resSort.name
  
  def clauseToString(c: Clause): String = 
    "tff("+"c_"+c.id+", axiom, "+
     (if (c.unabstrCautious.isEmpty) falseSym 
      else if (c.unabstrCautious.vars.isEmpty)
        c.unabstrCautious.lits.mkString("("," "+orSym+" ",")")
      else
        "("+forallSym+c.unabstrCautious.vars.map(x => x+": "+x.sort.name).mkString("[",", ","]: ")+
           c.unabstrCautious.lits.mkString("(", " "+orSym+" ",")")+")"
	   //c.unabstr.toFormula.toString)+
     )+" )."

  /** Used in proof output to add specific annotations */
  def customClauseToString(c: Clause, n: String = "", role: String="", ann: String=""): String = 
    "tff("+(if (n!="") n else "c_"+"a"+c.age+"_w"+c.weight)+","+
	    (if (role!="") role else "axiom")+",\n\t"+
	    (if (c.unabstrCautious.isEmpty) falseSym 
	     else if (c.unabstrCautious.vars.isEmpty)
	       c.unabstrCautious.lits.mkString("("," "+orSym+" ",")")
	     else
	        "("+forallSym+c.unabstrCautious.vars.map(x => x+": "+x.sort.name).mkString("[",", ","]: ")+
	        	c.unabstrCautious.lits.mkString("(", " "+orSym+" ",")")+")"
	        //c.unabstr.toFormula.toString)+
	      )+
	     (if(ann!="") ",\n\t"+ann else "")+
    " )." 
	      
  def litToString(l: Lit): String =
  	l.eqn match {
      case PredEqn(equ) ⇒ (if (l.isPositive) "" else negSym) + l.eqn.toString
      case _ ⇒ (if (l.isPositive) l.eqn.toString else l.eqn.lhs.toString+"!="+l.eqn.rhs.toString)
    }
  
  def eqnToString(e: Eqn): String = {
    e match {
      case PredEqn(a) ⇒ a.toString
      case _ ⇒ e.lhs.toString + equalSym + e.rhs.toString
    }
  }
  
  //def atomToString(a: Atom): String
  
  def quantifierToString(formula: QuantForm): String = 
    "(" + formula.q + (formula.xs map { _.toStringWithSort }).mkString("[",", ","]")+ ": " + formula.f + ")"
    
  def letFormulaToString(letf: LetFormula): String = 
    "($let_vf(" + 
      (letf.bindings map { case (v,t) => v + "=" + t }).toMyString("[", ", ", "]") + ", " + letf.body + "))"

  def letTermToString(let: Let): String = 
    "($let_vt(" + 
      (let.bindings map { case (v,t) => v + "=" + t }).toMyString("[", ", ", "]") + ", " + let.body + "))"

  def varToString(v: Var): String = v.name.capitalize + (if (v.index > 0) "_" + v.index else "") + (if (v.isInstanceOf[AbstVar]) "a" else "")
  
  def domElemToString[T](d: DomElem[T]): String = {
    val s = d.value.toString
    if(s.charAt(0)=='-') "$uminus("+s.substring(1)+")"
    else s
  }
  
  private val builtinSorts = List("$i","$o","$tType")
 // private val builtinFGOps = List("$true","$false")
  private val builtinBGOps = List("$sum","$uminus","$difference","$greater","$greatereq",
		  		  "$less","$lesseq","$quotient","$product","$divides",
		  		  "$is_int","$is_rat")
  
  def signatureToString(s: Signature): String = {
    //Operator precedence
    "%$ " + s.fgOperators.toList.filterNot(_.defined)
    		                    .sortWith(s.gtrPrecSimple(_, _))
    		                    .map(_.name)
    		                    .mkString(" > ")+"\n"+
    //ignore built in sorts: $i, $o, $tType
    "\n%Foreground sorts:\n"+
    s.fgSorts.filterNot(sort => builtinSorts.contains(sort.name)).mkString("\n")+"\n"+
    //ignore built in operators eg. $greater, $false
    "\n%Background operators:\n"+
    s.bgOperators.filterNot(op => builtinBGOps.contains(op.name)).mkString("\n")+"\n"+
    "\n%Constants introduced via Define:\n"+
    Term.defineMap.values.map("tff(define,type,"+_+": $int ).").mkString("\n")+"\n"+
    "\n%Foreground operators:\n"+
    s.fgOperators.filterNot(_.defined).mkString("\n")
    //s.fgOperators.filterNot(op => builtinFGOps.contains(op.name)).mkString("\n")
  }

  // The code below is a bit repetitive with the code for the default printer above,
  // but it is probably not worth factoring out the commonalities.
  def pp(f: Formula, indent: Int) {
    f match {
      case a @ Atom(_, _) => print(a)
      case UnOpForm(op, f) => {
        print(op)
        pp(f, indent+1)
      }
      case BinOpForm(op, f1, f2) =>
        op match {
          case And | Or => {
            val fs = f.toList(op)
            print("( ");
            for (fss <- fs.tails; if !fss.isEmpty) {
              pp(fss.head, indent+2)
              // More formulas?
              if (!fss.tail.isEmpty) {
                nlspaces(indent - (op.toString.length - 1))
                print(op.toString + " ")
              }
            }
            print(")")
          }
          case _ => {
            print("( "); pp(f1, indent+2)
            nlspaces(indent - (op.toString.length - 1))
            print(op.toString + " ")
            pp(f2, indent+2); print(")")
          }
        }
      case QuantForm(q, xs, f) => {
        print("( " + q + (xs map { _.toStringWithSort.capitalize }).mkString("[",", ","]") + ": ")
        nlspaces(indent+2)
        pp(f, indent+2)
        print(")")
      }
      case LetFormula(bindings, body) => {
        print("($let_vf(" +
          (bindings map { case (v,t) => v + " = " + t }).toMyString("[", ", ", "]") + ", ")
        nlspaces(indent+2)
        pp(body, indent+2)
        print("))")
      }
    }
  }

  def pp(f: Formula) { 
    print("tff(formula, axiom, (")
    nlspaces(2)
    pp(f, 2) 
    print(")).")
  }

}
