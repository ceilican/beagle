package beagle.util

import beagle._
import datastructures._
import fol._
import term._

/**
 * This defines the messages which are printed out on various beagle actions.
 * It allows swapping between debug/regular/database levels of reporting.
 * Removes clutter from main code- instead of having logv(...); logd(...); logdb(...)
 * can just have a single statement.
 * 
 * Reporter should have the output expected when the -v flag is passed.
 * TODO- should be objects...
 *  Peter 2/4/2014: Reporter now is quiet, use VerboseReporter instead
 */

class Reporter {

  val isDebugReporter = false
  
  /** For debugging beagle inferences*/
  def debug[T](cl: T): T = cl
  
  /** For debugging cooper inferences */
  def debugCooper[T](cl: T): T = cl
  
  /** For messages to the user */
  def log[T](cl: T) = cl

  /**
   * Print the given string if in debug mode, i.e. only works if given class is
   * a subclass of debug.
   */
  def debug(msg: => String) {}
  def debugCooper(msg: => String) {}
  def log(msg: String) {}
  
  def onPara(from: Clause, into: Clause, res: Clause, sigma: Subst) {}
  
  def onInference(selected: Clause, result: Clause, infType: String) {}
  
  /**
   * Called when a new split is made.
   * @param result the left clause of the split
   * @param decisionLevel the current decision level, this will be
   * backtracked to if the LH clause is refuted.
   */
  def onSplit(selected: Clause, result: List[Clause], decisionLevel: Int) = {}
  
  def onInst(selected: Clause, result: List[Clause]) = {}
  
  /**
   * Selected clause has been deleted by simplification.
   */
  def onClauseDeleted(deleted: Clause) = {}
  
  /**
   * Clause has been selected for the next inference rule.
   */
  def selectedClause(cl: Clause, decisionLevel: Int) = {}
  
  /**
   * An added BG clause is inconsistent w.r.t. other BG clauses
   */
  def onBGInconsistent(cl: Clause, old: Iterable[Clause]) {}
  
  /**
   * @param level is the target decision level
   * @param old is the set of retained clauses at the new decision level
   * @param neu is the set of new clauses at the new decision level
   */
  def onBacktrack(level: Int, rightClauses: List[Clause], old: ClauseSet, neu: ClauseSet) {}
  
  /** A miscellaneous simplification of `selected` */
  def onSimplified(selected: Clause, simplified: Clause) {}

  /** Only used for proof reporter */
  def detailedSimp(reason: String, selected: Clause, simplified: Clause, clUsed: List[Clause]) {}
  
  /** Called if the define rule has been successfully applied.
   * @param selected The clause selected for a define inference,
   * it provides the ground BSFG terms for the LHS of the define equations.
   * @param defClauses The definitions found in `selected`
   * @param newClause `selected` demodulated with `defClauses`
   * @see [[beagle.calculus.derivationrules.infDefine]]
   */
  def onDefine(selected: Clause, defClauses: Iterable[Clause], newClause: Clause) = {}
  
  /** A clause has been added to old- the set of retained clauses */
  def onClauseAdded(cl: Clause) = {}
  
  def onNewState(old: ListClauseSet, neu: PQClauseSet) = {}
  
  def onProofStart(initClauses: List[Clause]) = {}
  
  def onClose(decisionLevel: Int, emptyCl: Clause) = {}

  def onClauseSubsumed(cl: Clause, subsumed: Clause) = {}
  
  /**
   * Called when a proof is successfully finished.
   */
  def onProofEnd() {}
  
  /**
   * Always called when program exits- either normally or terminated.
   * Use for finishing writes etc.
   */
  def onExit() {}
  
  def showClauseSets(old: ClauseSet,neu: ClauseSet) {
    println("======== Clause sets ==================")
    println("                                   new:")
    neu.show()
    println("                                   old:")
    old.show()
    println()
  }
}

class VerboseReporter extends Reporter {
  
  override def selectedClause(cl: Clause, decisionLevel: Int) { 
    println((if (decisionLevel==0) "" else "[" + decisionLevel + "] ") + cl)
  }
  
  override def onBacktrack(level: Int, rightClauses: List[Clause], old: ClauseSet, neu: ClauseSet) = {
    println("=== Backtrack to level " + level + " ===")
  }
  
  // override def onProofStart(initClauses: List[Clause]) = {
  //   println("\nProving...")
  // }
  
  override  def onClose(decisionLevel: Int, emptyCl: Clause) {
    println((if (decisionLevel==0) "" else "[" + decisionLevel + "] ") + emptyCl)
  }

  override def log[T](cl: T) = { println(cl); cl }
  override def log(msg: String) { println(msg) }

}

object DefaultReporter extends VerboseReporter

object QuietReporter extends Reporter 

/*
/**
 * Output statistics to file. Not set by commmand line, but set in code.
 * This version will output the number of maximal literals in a clause and
 * the total literals in the clause (unabstracted?).
 */
class CustomReporter extends Reporter {
  //create file for printing stats
  //val file = new File("clause_weights.out")
  val file = new File("split_times.out")
  val pw = new PrintWriter(file)
  
  override def onPara(from: Clause, into: Clause, res: Clause, sigma: Subst) {
    //output the # of maximal literals in the clause vs the total number
    /*pw.println(
		sigma(from).iMaximal.length+
		" "+
		from.length
		)
    pw.println(
		sigma(into).iMaximal.length+
		" "+
		into.length
		)*/
  }
  
  override def onInference(selected: Clause, result: Clause, infType: String) {
    /*pw.println(
    		sigma(selected).iMaximal.length+
    		" "+
    		selected.length
    		)*/
  }
  
  override def onSplit(selected: Clause, result: Clause, decisionLevel: Int) {
  }
  
  override def onExit() {
    pw.close()
  }
}
*/

/**
 * Gives the level of output expected when the -d flag is given.
 */
object DebugReporter extends VerboseReporter {

  override val isDebugReporter = true

  override def debug[T](cl: T): T = { println(cl); cl }
  
  override def debug(msg: => String) {
    println(msg)
  }
  
  override def onPara(from: Clause, into: Clause, res: Clause, sigma: Subst) {
    println("\n" + res + " by Para from " + from + " into " + into)
  }
  
  override def onInference(selected: Clause, result: Clause, infType: String) {
    println("\n" + result + " by "+infType+" from " + selected)
  }

  override def onSplit(selected: Clause, result: List[Clause], decisionLevel: Int) {
    println("Split with clause(s) " + result.mkString(", "))
  }
  
  override def onClauseDeleted(deleted: Clause) {
    println("Delete clause " + deleted)
  }
  
  override def onClauseSubsumed(cl: Clause, subsumed: Clause) {
    println(cl + " subsumes " + subsumed)
  }
  
  override def selectedClause(cl: Clause, decisionLevel: Int) {
    println("Selected clause: ")
    super.selectedClause(cl, decisionLevel)
  }
  
  override def onBGInconsistent(cl: Clause, old: Iterable[Clause]) {
    println("BG Clause " + cl + " is inconsistent with BG clauses ")
    for (c <- old) println(c)
  }
  
  override def onDefine(selected: Clause, defClauses: Iterable[Clause], newClause: Clause) {
    println("Define on " + selected + ", giving " + defClauses + " and " + newClause)
  }
  
  override def onInst(selected: Clause, result: List[Clause]) {
    println("Inst on " + selected + " yields " + result)
  }
  
  override def onSimplified(selected: Clause, simplified: Clause) {
    println("   " + simplified + " by simplification of " + selected)
  }
  
  override def onNewState(old: ListClauseSet, neu: PQClauseSet) {
    showClauseSets(old,neu)
  }
  
  override def onBacktrack(level: Int, rightClauses: List[Clause], old: ClauseSet, neu: ClauseSet) {
    super.onBacktrack(level, rightClauses,old,neu)
    println("Right clauses are:")
    rightClauses foreach {println _}
    println("\nState is now:")
    showClauseSets(old,neu)
  }
  
}

/**
 * Gives the level of output expected when the -d flag is given.
 */

object DebugCooperReporter extends VerboseReporter {

  override val isDebugReporter = true

  override def debugCooper[T](cl: T): T = { println(cl); cl }
  
  override def debugCooper(msg: => String) {
    println(msg)
  }
}

/* //For writing proof output to a DB- not used in trunk version
class DBReporter(file: String, flagStr: String) extends Reporter{
  val db = new DB(util.flags.dbName.value, file, flagStr)
  
  override def onProofStart(initClauses: List[Clause]) {
    initClauses foreach { db.write(_) }
  }
  
  override def onNewState(old: ListClauseSet, neu: PQClauseSet) {
    db.newState(List(), List())
  }
  
  override def onPara(from: Clause, into: Clause, res: Clause, l:Term, r: Term, s: Term, t: Term, sigma: Subst) {
    db.writeParaInf(from,into,res)
  }
  
  //usually ref or fact
  override def onInference(selected: Clause, result: Clause, sigma: Subst, infType: String) {
    db.writeInference(selected,result,infType)
  }
  
  override def onBacktrack(level: Int) {
    super.onBacktrack(level)
    db.newBranch(level)
  }
  
  override def onSplit(selected: Clause, result: Clause, decisionLevel: Int) {
    db.writeSplitInf(selected,result,decisionLevel)
  }
  
  override def onClauseDeleted(deleted: Clause) {
    db.setClauseDeleted(deleted)
  }
  
  override def onDefine(selected: Clause, defClause: Clause, result: Clause) {
    db.writeInference(selected,defClause,"def")
  }
}
*/
