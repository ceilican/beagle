package beagle.parser

import scala.util.matching.Regex
import scala.util.parsing.combinator._
import java.io.FileReader

import beagle._
import fol._
import term._
import util._
import Signature._
import bgtheory.LIA.IntSort
import bgtheory.LIA.DomElemInt
import bgtheory.LRA.RatSort
import bgtheory.LRA.DomElemRat
import bgtheory.LFA.RealSort
import bgtheory.LFA.DomElemReal

/**
 * A parser for TPTP, both FOF and TFF
 * @param incDir is directory relative to which include statements are (tried to be) expanded, could be relative, e.g. "."
 */
class TPTPParser(inputSignature: Signature, genvars: Boolean, incDir: String) extends JavaTokenParsers with PackratParsers {

  // The bound variables when descending into a formula
  var vars = List.empty[Var]
  // Because I did not find a way to pass around these variables 
  // in parsing, vars is built up and restored explictly in a stack
  var varsStack = List.empty[List[Var]]

  // The "conjecture"s among the parsed formulas
  var conjectures = List.empty[Formula]
  
  //If CNF we have to force any numbers to take type $i TODO
  var forceSingleType = flags.formatFlag.value=="cnf";

  var signature = inputSignature // The signature extended with the declared operators and sorts  
  var signatureStack = List.empty[Signature] // Same thing as varsStack above for variables

  var domElemInts = Set.empty[DomElemInt] // all the integer domain elements ever parsed
  

  /**
   * The grammar rules
   */
  lazy val TPTP_input: PackratParser[List[List[Formula]]] = rep(annotated_formula | precs | comment | include )

  lazy val annotated_formula =
    // TPTP_input requires a list, because include above returns a list
    (fof_annotated_logic_formula ^^ { f => List(f) }) |
      (tff_annotated_type_formula ^^ { _ ⇒ List() }) |
      (tff_annotated_logic_formula ^^ { f => List(f) }) |
      (cnf_annotated_formula ^^ { List(_) })

  lazy val fof_annotated_logic_formula = "fof" ~ "(" ~ (atomic_word | wholeNumber) ~ "," ~
    atomic_word ~ "," ~ fof_logic_formula ~ ")" <~ "." ^^ {
      case "fof" ~ "(" ~ name ~ "," ~ role ~ "," ~ f ~ ")" ⇒
        role match {
          case "conjecture" ⇒ {
            conjectures ::= f
            Neg(f)
          }
          // "type" just returns TrueAtom - deal with that above
          // case "type" => xxx
          case _ ⇒ f // Assume f sits on the premise side
        }
    }

  lazy val cnf_annotated_formula = "cnf" ~ "(" ~ (atomic_word | wholeNumber) ~ "," ~
    atomic_word ~ "," ~ cnf_formula ~ ")" <~ "." ^^ {
      case "cnf" ~ "(" ~ name ~ "," ~ role ~ "," ~ lits ~ ")" ⇒ {
        if (role == "conjecture")
          println("Warning: cnf formula specified as conjecture, which is ambiguous wrt quantifiers")
        // Need to explictly quantify
        if (lits.vars.isEmpty) lits.toOr else Forall(lits.vars.toList, lits.toOr)
      }
    }

  /**
   * TFF types
   */

  // In fact, non-null annotatations are currently not accepted
  // Slightly rewritten version of the BNF rule in the TPTP report, to discrimate
  // between type and non-type very early, thus helping the parser.
  lazy val tff_annotated_type_formula = "tff" ~ "(" ~ (atomic_word | wholeNumber) ~ "," ~ "type" ~ "," ~
    tff_typed_atom ~ ")" <~ "." ^^ {
      // It's there just for its side effect
      _ ⇒ ()
    }

  lazy val tff_annotated_logic_formula = "tff" ~ "(" ~ (atomic_word | wholeNumber) ~ "," ~
    formula_role_other_than_type ~ "," ~ tff_formula ~ opt("," ~> annotation) ~ ")" <~ "." ^^ {
      case "tff" ~ "(" ~ name ~ "," ~ role ~ "," ~ f ~ annot ~ ")" ⇒
        role match {
          case "conjecture" ⇒ {
            conjectures ::=  f
            Neg(f.setAnnotation(annot))
          }
          case _ ⇒ f.setAnnotation(annot) // Assume f sits on the premise side
        }
    }

  lazy val annotation = term

  lazy val formula_role_other_than_type = commit(
    // "type" | 
    "axiom" | "hypothesis" | "definition" | "assumption" | "lemma" | "theorem" |
      "conjecture" | "negated_conjecture" | "unknown" | atomic_word)

  // tff_typed_atom can appear only at toplevel
  lazy val tff_typed_atom: PackratParser[Unit] =
    ((tff_untyped_atom ~ ":" ~ tff_top_level_type) ^^ {
      // could declare a new type or a symbol of an existing type
      case typeName ~ ":" ~ Arity0(TSort) ⇒ {
        // println("declare type " + Type(typeName))
        signature += FGSort(typeName)
        // println("declared")
        // return a dummy
        // TrueAtom
      }
      case symbolName ~ ":" ~ arity ⇒ {
        // println("declare arity " + (symbolName -> arity))
        if (arity.argsSorts contains OSort)
          throw SyntaxError("type declaration for " +
            symbolName + ": " + arity + ": argument type cannot be $oType")
        // todo: generalize: can we do something sensible
        // if all argtypes are foreground types?
        // eg "car" would fall under this scheme.
        if (arity.argsSorts.isEmpty &&
          // Prop variables are better handled
          // by the foreground
          arity.resSort != OSort &&
          (arity.resSort.thyKind == BG) &&
          flags.paramsOpSet.value == "BG")
          // only in this case we have a BG operator
          signature += Operator(BG, symbolName, arity)
        else
          signature += Operator(FG, symbolName, arity)
        // return a dummy
        // println("declared")
        // TrueAtom
      }
    }) |
      ("(" ~> tff_typed_atom <~ ")")
  lazy val tff_untyped_atom = atomic_word

  // This results in a Arity in our terminology
  lazy val tff_top_level_type: PackratParser[Arity] =
    tff_mapping_type |
      (tff_atomic_type ^^ { (sort: Type) ⇒ Arity0(sort) }) |
      tff_quantified_type

  //todo- necessary?
  lazy val tff_monotype: PackratParser[Arity] = 
    tff_mapping_type |
      (tff_atomic_type ^^ { (sort: Type) ⇒ Arity0(sort) })

  lazy val tff_quantified_type: PackratParser[Arity] = 
    ( "!>" ~ "[" ~ tff_varlist ~ "]" ~ ":" ~ tff_monotype ) ^^ {
      throw new Exception("TFF1 support not yet implemented")
    }

  lazy val tff_mapping_type: PackratParser[Arity] =
    ((tff_unitary_type ~ ">" ~ tff_atomic_type) ^^ {
      case argsSorts ~ ">" ~ resSort ⇒
        Arity(argsSorts, resSort)
    }) |
      ("(" ~> tff_mapping_type <~ ")")

  lazy val tff_unitary_type = (tff_atomic_type ^^ { List(_) }) |
    ("(" ~> tff_xprod_type <~ ")")

  lazy val tff_xprod_type: PackratParser[List[Type]] =
    (tff_atomic_type ~ "*" ~ rep1sep(tff_atomic_type, "*") ^^ {
      case t1 ~ "*" ~ tail ⇒ t1 :: tail
    }) |
      ("(" ~> tff_xprod_type <~ ")")

  /**
   * TFF formulas
   */

  lazy val tff_formula = tff_logic_formula
  lazy val tff_logic_formula = tff_binary_formula | tff_unitary_formula
  lazy val tff_binary_formula = tff_binary_nonassoc | tff_binary_assoc
  lazy val tff_binary_nonassoc = tff_unitary_formula ~
    binary_nonassoc_connective ~
    tff_unitary_formula ^^ {
      case f1 ~ op ~ f2 ⇒ op(f1, f2)
    }
  lazy val tff_binary_assoc = tff_or_formula | tff_and_formula
  lazy val tff_or_formula =
    tff_unitary_formula ~ "|" ~ rep1sep(tff_unitary_formula, "|") ^^ {
      case f1 ~ "|" ~ tail ⇒ {
        f1 :: tail reduceLeft { Or(_, _) }
      }
    }
  lazy val tff_and_formula =
    tff_unitary_formula ~ "&" ~ rep1sep(tff_unitary_formula, "&") ^^ {
      case f1 ~ "&" ~ tail ⇒ {
        f1 :: tail reduceLeft { And(_, _) }
      }
    }
  lazy val tff_unitary_formula: PackratParser[Formula] =
    tff_quantified_formula |
    tff_conditional | 
    tff_unary_formula |
  // tff_let must come before atom, because atom can start with keyword, which parses $let_XX
  // and so the parser would go into the wrong production
    tff_let_vf |
    tff_let |
    atom |
      "(" ~> tff_logic_formula <~ ")"
  lazy val tff_unary_formula = "~" ~> tff_unitary_formula ^^ { Neg(_) }
  lazy val tff_quantified_formula: PackratParser[Formula] =
    (((forallSign ^^ { _ ⇒ { Forall(_, _) } }) |
      ("?" ^^ { _ ⇒ { Exists(_, _) } })) ~
      "[" ~ tff_varlist ~ "]" ^^ {
        case q ~ "[" ~ vl ~ "]" ⇒ {
          // remember current varTypes for later restoration
          varsStack ::= vars
          vars = vl ++ vars
          // println("declare " + vl)
          // Return the partially instantiated Quantifier-Formula
          (f: Formula) ⇒ q(vl, f)
        }
      }) ~ ":" ~ tff_unitary_formula ^^ {
        // Put together the two parts, quantification and formula
        case quantTemplate ~ ":" ~ f ⇒ {
          // restore varTypes as it was before
          vars = varsStack.head
          varsStack = varsStack.tail
          quantTemplate(f)
        }
      }

  lazy val tff_conditional = "$ite_f" ~ "(" ~ tff_logic_formula ~ "," ~ tff_logic_formula ~ "," ~ tff_logic_formula ~ ")" ^^ {
    case "$ite_f" ~ "(" ~ if_f ~ "," ~ then_f ~ "," ~ else_f ~ ")" => 
      // Very simplistic. Could look for polarity this occurs under and dependany on that chose a different translation
      // And(Implies(if_f, then_f), Implies(Neg(if_f), else_f))
      ITEForm(if_f, then_f, else_f)
  }


  // Cannot use 'variable' for variable bound in let, because variable annotated a type
  lazy val letvariable: PackratParser[String] = regex(new Regex("[A-Z][a-zA-Z0-9_]*"))

  lazy val tff_let_binding: PackratParser[(Var, Term)] =
    letvariable ~ equalsSign ~ term ^^ {
      case vname ~ _ ~ s ⇒ {
        if (s.sort.thyKind == BG && !genvars)
          (AbstVar(vname, s.sort), s)
        else
          (GenVar(vname, s.sort), s)
      }
    }

  lazy val tff_let_bindings: PackratParser[List[(Var, Term)]] = 
    rep1sep(tff_let_binding, ",")

  // let v=s in f, where s is a term and f is a formula
  // This uses the usual semantics of let-expressions, not the special one of the other
  // let-constructs in the TPTP.
/*
  lazy val tff_let_vf: PackratParser[Formula] = 
    ("$let_vf" ~ "(" ~ letvariable ~ equalsSign ~ term ^^ {
      case  "$let_vf" ~ "(" ~ vname ~ _ ~ s  => {
        // First we create the variable from vname and the sort of s
        val v =
          if (s.sort.kind == BG && !genvars)
            AbstVar(vname, s.sort)
          else
            GenVar(vname, s.sort)
        // Make v known when parsing the in-term t
        varsStack ::= vars
        vars = v :: vars
        // return the binding
        (v, s)
      }
    }) ~ "," ~ tff_unitary_formula ~ ")" ^^ {
      case binding ~ "," ~ t ~ ")" => {
        // Remove the let-binding again
        vars = varsStack.head
        varsStack = varsStack.tail
        // This is the result
        LetFormula(binding._1, binding._2, t)
      }
    }
 */

 // parallel version
  lazy val tff_let_vf: PackratParser[Formula] = 
    ("$let_vf" ~> "(" ~> "[" ~> tff_let_bindings <~ "]" ^^ {
      case bindings => {
        val letVars = bindings map { _._1 }
        varsStack ::= vars
        vars = letVars ::: vars
        bindings
      }
    }) ~ "," ~ tff_unitary_formula ~ ")" ^^ {
      case bindings ~ "," ~ t ~ ")" => {
        // Remove the let-binding again
        vars = varsStack.head
        varsStack = varsStack.tail
        // This is the result
        LetFormula(bindings, t)
      }
    }

  lazy val let_term = let_tt | let_ft

  lazy val tff_let: PackratParser[Formula] = tff_let_tf | tff_let_ff

  /*
   let-binders:
  - The functors defined on the lhs can or cannot be globally defined.
    Any global definition would be overshadowed. Correct?
  - Substitution semantics: innermost or outermost strategy? This is not an
    issue because the substitution semantics requires renaming also of the
    function symbols when a substitution is applied to a let-term. 
   (Beta-reduction is confluent. )
    Beagle implements innermost substitution, so this is not an issue.
  - what happens when the functor on the lhs has a different arity to the
    usages of that functor in the let body? Error? Ignore?
    Beagle: Ignore
   */

  lazy val tff_let_tf: PackratParser[Formula] = 
    ("$let_tf" ~ "(" ~ tff_let_term_defn ^^ {
      case _ ~ _ ~ varsAndBinding  => {
        val (vl, ((functor, args), rhs)) = varsAndBinding
        // args is/must be a list of pairwise different variables
        // todo: check that
        if (vl.vars != args.vars)
          throw SyntaxError("let-binder: quantified variables do not match lhs: " + varsAndBinding)
        // Create the operator for the defined function ... 
        val op = Operator(FG, functor, Arity((args map { _.sort }), rhs.sort))
        signatureStack ::= signature
        signature += op
        // Finally we can built the lhs (a Term)
        val lhs = NonDomElemFunTerm(op, args)
        // The variables in lhs are better be taken as general variables
        // hence rename into general variables
        val rho = Term.mkRenamingIntoGenVars(vl)
        // return the pair that is used for the replacement in the next step
        (rho(lhs).asInstanceOf[FunTerm], rho(rhs))
      }
    }) ~ "," ~ tff_formula ~ ")" ^^ {
      case (lhs, rhs) ~ _ ~ formula ~ _ => {
        signature = signatureStack.head
        signatureStack = signatureStack.tail
        formula.replace(lhs, rhs)
      }
    }

  lazy val tff_let_term_defn: PackratParser[(List[Var], ((String, List[Term]), Term))] = 
    ( forallSign ~> opt("[" ~> tff_varlist <~ "]") ^^ {
      case None =>  {
        varsStack ::= vars  // No change to vars
        List.empty
      }
      case Some(vl) => {
        varsStack ::= vars
        vars = vl ++ vars  
        // return the list of new variables
        vl
      }
    }) ~ ":" ~ tff_let_term_binding ^^ {
      case varlist ~ _ ~ let_term_binding => {
        // Remove again the variables defined above
        vars = varsStack.head
        varsStack = varsStack.tail
        (varlist, let_term_binding)
      }
    } |
    tff_let_term_binding ^^ { 
      case let_term_binding => (List.empty, let_term_binding)
    }

  lazy val tff_let_term_binding: PackratParser[((String, List[Term]), Term)] = 
    fun_def_lhs ~ equalsSign ~ term ^^ {
      case (functor, args) ~ _ ~ t ⇒ {
        // this is only a sanity check:
        // remove - TPTP does not seem to enforce that
        // signature.checkedEquation(x, t)
        ((functor, args), t)
      } 
    } | 
  "(" ~> tff_let_term_binding <~ ")"

  lazy val let_tt: PackratParser[Term] = 
    ("$let_tt" ~ "(" ~ tff_let_term_defn ^^ {
      case _ ~ _ ~ varsAndBinding  => {
        val (vl, ((functor, args), rhs)) = varsAndBinding
        // args is/must be a list of pairwise different variables
        // todo: check that
        if (vl.vars != args.vars)
          throw SyntaxError("let-binder: quantified variables do not match lhs: " + varsAndBinding)
        // Create the operator for the defined function ... 
        val op = Operator(FG, functor, Arity((args map { _.sort }), rhs.sort))
        // .. and put it into the actual signature
        signatureStack ::= signature
        signature += op
        // Finally we can built the lhs (a Term)
        val lhs = NonDomElemFunTerm(op, args)
        // The variables in lhs are better be taken as general variables
        // hence rename into general variables
        val rho = Term.mkRenamingIntoGenVars(vl)
        // return the pair that is used for the replacement in the next step
        (rho(lhs).asInstanceOf[FunTerm], rho(rhs))
      }
    }) ~ "," ~ term ~ ")" ^^ {
      case (lhs, rhs) ~ _ ~ term ~ _ => {
        signature = signatureStack.head
        signatureStack = signatureStack.tail
        term.replace(lhs, rhs)
      }
    }

  lazy val let_ft: PackratParser[Term] = 
    ("$let_ft" ~ "(" ~ tff_let_formula_defn ^^ {
      case _ ~ _ ~ varsAndBinding  => {
        val (vl, ((functor, args), rhs)) = varsAndBinding
        // args is/must be a list of pairwise different variables
        // todo: check that
        if (vl.vars != args.vars)
          throw SyntaxError("let-binder: quantified variables do not match lhs: " + varsAndBinding)
        // Create the operator for the defined function ... 
        val op = Operator(FG, functor, Arity((args map { _.sort }), OSort))
        // .. and put it into the actual signature
        signatureStack ::= signature
        signature += op
        // Finally we can built the lhs (a Term)
        val lhs = Atom(functor, args)
        // The variables in lhs are better be taken as general variables
        // hence rename into general variables
        val rho = Term.mkRenamingIntoGenVars(vl)
        // return the pair that is used for the replacement in the next step
        (rho(lhs).asInstanceOf[Atom], rho(rhs))
      }
    }) ~ "," ~ term ~ ")" ^^ {
      case (lhs, rhs) ~ _ ~ t ~ _ => {
        signature = signatureStack.head
        signatureStack = signatureStack.tail
        t.replace(lhs, rhs)
      }
    }


  lazy val tff_let_ff: PackratParser[Formula] = 
    ("$let_ff" ~ "(" ~ tff_let_formula_defn ^^ {
      case _ ~ _ ~ varsAndBinding  => {
        val (vl, ((functor, args), rhs)) = varsAndBinding
        // args is/must be a list of pairwise different variables
        // todo: check that
        if (vl.vars != args.vars)
          throw SyntaxError("let-binder: quantified variables do not match lhs: " + varsAndBinding)
        // Create the operator for the defined function
        val op = Operator(FG, functor, Arity((args map { _.sort }), OSort))
        signatureStack ::= signature
        signature += op
        // Finally we can built the lhs (a Term)
        val lhs = Atom(functor, args)
        // The variables in lhs are better be taken as general variables
        // hence rename into general variables
        val rho = Term.mkRenamingIntoGenVars(vl)
        // return the pair that is used for the replacement in the next step
        (rho(lhs).asInstanceOf[Atom], rho(rhs))
      }
    }) ~ "," ~ tff_formula ~ ")" ^^ {
      case (lhs, rhs) ~ _ ~ formula ~ _ => {
        signature = signatureStack.head
        signatureStack = signatureStack.tail
        formula.replace(lhs, rhs)
      }
    }

  lazy val tff_let_formula_defn: PackratParser[(List[Var], ((String, List[Term]), Formula))] = 
    ( forallSign ~> opt("[" ~> tff_varlist <~ "]") ^^ {
      case None =>  {
        varsStack ::= vars  // No change to vars
        List.empty
      }
      case Some(vl) => {
        varsStack ::= vars
        vars = vl ++ vars  
        // return the list of new variables
        vl
      }
    }) ~ ":" ~ tff_let_formula_binding ^^ {
      case varlist ~ _ ~ let_formula_binding => {
        // Remove again the variables defined above
        vars = varsStack.head
        varsStack = varsStack.tail
        (varlist, let_formula_binding)
      }
    } |
    tff_let_formula_binding ^^ { 
      case let_formula_binding => (List.empty, let_formula_binding)
    }

  lazy val tff_let_formula_binding: PackratParser[((String, List[Term]), Formula)] =
    fun_def_lhs ~ "<=>" ~ tff_unitary_formula ^^ {
      case (functor, args) ~ _ ~ f ⇒ {
        // this is only a sanity check:
        // remove - TPTP does not seem to enforce that
        // signature.checkedEquation(x, t)
        ((functor, args), f)
      }
    } |
  "(" ~> tff_let_formula_binding <~ ")"


  // Variable list
  lazy val tff_varlist: PackratParser[List[Var]] = rep1sep(tff_var, ",")

  lazy val tff_var: PackratParser[Var] =
    (variable ~ ":" ~ tff_atomic_type ^^ {
      case x ~ ":" ~ sort ⇒ {
        val sortChecked = signature.typeExistsChecked(sort)
        if (sort.thyKind == BG && !genvars)
          AbstVar(x.name, sortChecked)
        else
          GenVar(x.name, sortChecked)
      }
    }) |
      (variable <~ guard(not(":")) ^^ {
        // default type of variables (in quantifications) is IType
        x ⇒ GenVar(x.name, ISort)
      })

  //all types are checked here...
  lazy val tff_atomic_type: PackratParser[Type] =
    defined_type |
      (atomic_word ~ "(" ~ tff_type_arguments ~ ")") ^^ { 
	case w ~ "(" ~ args ~ ")" => {
	  util.reporter.log("** Warning TFF1 not yet supported")
	  //todo- support implicit introduction of type constructors
	  val Some(f) = signature.findOperator(w,args.map(_ => Signature.TSort))
	  signature.checkedTypeTerm(w, args)
	}} |
      (atomic_word ^^ { case s => signature.typeExistsChecked(FGSort(s)) }) |   // predefined type
      variable ^^ { case x => throw new Exception("Variable type application"+x+"- TFF1 not yet implemented") }

  // predefined type
  lazy val defined_type: PackratParser[Type] =
    (("$oType" | "$o") ^^ { _ ⇒ OSort }) |
      (("$iType" | ("$i" <~ guard(not("nt")))) ^^ { _ ⇒ ISort }) |
      ("$tType" ^^ { _ ⇒ TSort }) |
      ("$int" ^^ { _ ⇒
        signature.findSort("$int") match {
          case None    ⇒ throw SyntaxError("Error: type $int has not been declared")
          case Some(s) ⇒ s
        }
      }) | //use the IntSort specified by the current signature
      ("$rat" ^^ { _ ⇒ RatSort }) |
      ("$real" ^^ { _ ⇒ RealSort })

  lazy val tff_type_arguments: PackratParser[List[Type]] = rep1sep(tff_atomic_type,",")

  /*
 * CNF formulas
 */

  lazy val cnf_formula = "(" ~> disjunction <~ ")" | disjunction
  lazy val disjunction = rep1sep(literal, "|")
  lazy val literal: PackratParser[Formula] = atom | ("~" ~> atom ^^ { Neg(_) })

  /*
     * FOF formulas
     */

  lazy val fof_logic_formula = fof_binary_formula | fof_unitary_formula
  lazy val fof_binary_formula = fof_binary_nonassoc | fof_binary_assoc
  lazy val fof_binary_nonassoc = fof_unitary_formula ~
    binary_nonassoc_connective ~
    fof_unitary_formula ^^ {
      case f1 ~ op ~ f2 ⇒ op(f1, f2)
    }
  lazy val fof_binary_assoc = fof_or_formula | fof_and_formula
  lazy val fof_or_formula =
    fof_unitary_formula ~ "|" ~ rep1sep(fof_unitary_formula, "|") ^^ {
      case f1 ~ "|" ~ tail ⇒ {
        f1 :: tail reduceLeft { Or(_, _) }
      }
    }
  lazy val fof_and_formula =
    fof_unitary_formula ~ "&" ~ rep1sep(fof_unitary_formula, "&") ^^ {
      case f1 ~ "&" ~ tail ⇒ {
        f1 :: tail reduceLeft { And(_, _) }
      }
    }
  lazy val fof_unitary_formula: PackratParser[Formula] =
    fof_quantified_formula |
      fof_unary_formula |
      atom |
      "(" ~> fof_logic_formula <~ ")"
  lazy val fof_unary_formula = "~" ~> fof_unitary_formula ^^ { Neg(_) }
  lazy val fof_quantified_formula: PackratParser[Formula] =
    (((forallSign ^^ { _ ⇒ { Forall(_, _) } }) |
      ("?" ^^ { _ ⇒ { Exists(_, _) } })) ~
      "[" ~ fof_varlist ~ "]" ^^ {
        case q ~ "[" ~ vl ~ "]" ⇒ {
          // remember current varTypes for later restoration
          varsStack ::= vars
          vars = vl ++ vars
          // Return the partially instantiated Quantifier-Formula
          (f: Formula) ⇒ q(vl, f)
        }
      }) ~ ":" ~ fof_unitary_formula ^^ {
        // Put together the two parts, quantification and formula
        case quantTemplate ~ ":" ~ f ⇒ {
          // restore varTypes as it was before
          vars = varsStack.head
          varsStack = varsStack.tail
          quantTemplate(f)
        }
      }

  // Variable list
  lazy val fof_varlist: PackratParser[List[Var]] = rep1sep(variable, ",")

  /**
   * Definitions common to TFF and FOF
   */

  lazy val binary_nonassoc_connective =
    ("=>" ^^ { _ ⇒ Implies(_, _) }) |
      ("<=" <~ guard(not(">")) ^^ { _ ⇒ Implied(_, _) }) |
      ("<=>" ^^ { _ ⇒ Iff(_, _) }) |
      ("<~>" ^^ { _ ⇒ IffNot(_, _) })

  // Atom
  // Difficulty is determining the type. If fun(args...) has been read 
  // it is possible that fun(args...) is an atom or the lhs of an equation.
  // Determining the type hence nees to be deferred until "=" (or "!=") is 
  // seen (or not). Once that is clear, the signature is extended 
  // appropriately. It can only be done this late because otherwise the signature
  // might be extended unappropriately, and backtracking (in the parser)
  // cannot undo that.

  lazy val atom =
    ("$true" ^^ { _ ⇒ TrueAtom }) |
      ("$false" ^^ { _ ⇒ FalseAtom }) |
      // eqn with lhs a variable
      (variable ~ equalsSign ~ term ^^ {
        case x ~ _ ~ t ⇒ signature.checkedEquation(x, t)
      }) |
      (variable ~ "!=" ~ term ^^ {
        case x ~ _ ~ t ⇒ Neg(signature.checkedEquation(x, t))
      }) |
      (domElem ~ equalsSign ~ term ^^ {
        case c ~ _ ~ t ⇒ signature.checkedEquation(c, t)
      }) |
      (domElem ~ "!=" ~ term ^^ {
        case c ~ _ ~ t ⇒ Neg(signature.checkedEquation(c, t))
      }) |
      (conditional_term ~ equalsSign ~ term ^^ {
        case c ~ _ ~ t ⇒ signature.checkedEquation(c, t)
      }) |
      (conditional_term ~ "!=" ~ term ^^ {
        case c ~ _ ~ t ⇒ Neg(signature.checkedEquation(c, t))
      }) |
      (prepro_term ~ equalsSign ~ term ^^ {
        case c ~ _ ~ t ⇒ signature.checkedEquation(c, t)
      }) |
      (prepro_term ~ "!=" ~ term ^^ {
        case c ~ _ ~ t ⇒ Neg(signature.checkedEquation(c, t))
      }) |
      // functor with or without arguments
      (((functor ~ "(" ~ termlist ~ ")" ^^ {
        case functor ~ "(" ~ termlist ~ ")" ⇒ (functor, termlist)
      }) |
        (functor ~ guard(not("(")) ^^ {
          case functor ~ _ ⇒ (functor, List())
        })) ~
        // Up to here the above could be an atom or the lhs of an equation.
        // The following three cases all return a template for a (dis)equation or an atom
        ((equalsSign ~ term ^^ {
          case _ ~ t ⇒
            (functor: String, args: List[Term]) ⇒ {
              //if (signature.findOperator(functor, args.sortsOf) == None) 
              //  signature += Operator(FG, functor, Arity((args map { _ ⇒ ISort }), ISort))
	      signature = signature.findOrInsertOp(functor, args.sortsOf)._2
              signature.checkedEquation(signature.checkedFunTerm(functor, args), t)
            }
        }) |
          ("!=" ~ term ^^ {
            case _ ~ t ⇒
              (functor: String, args: List[Term]) ⇒ {
                // if (signature.findOperator(functor, args.sortsOf) == None)
                //   signature += Operator(FG, functor, Arity((args map { _ ⇒ ISort }), ISort))
		signature = signature.findOrInsertOp(functor, args.sortsOf)._2
                Neg(signature.checkedEquation(signature.checkedFunTerm(functor, args), t))
              }
          }) |
          // The case of an atom
          (guard(not(equalsSign | "!=")) ^^ {
            case _ ⇒
              (functor: String, args: List[Term]) ⇒ {
                if (signature.findOperator(functor, args.sortsOf) == None)
                  signature += Operator(FG, functor, Arity((args map { _ ⇒ ISort }), OSort))
                signature.checkedAtom(functor, args)
              }
          })) ^^
          // Put together the results of the parsing obtained so far
          { case (functor, args) ~ fTemplate ⇒ fTemplate(functor, args) })

  // Terms
  // Parsing (of atoms) is such that whenever a term is to be parsed
  // it is clear it must be a term (no backtracking), hence as soon
  // as a term is found the signature can be extended.
  lazy val term: PackratParser[Term] = let_vt | let_term | conditional_term | nonvar_term | variable

  // let v=s in t, where s and t are terms.
  // This uses the usual semantics of let-expressions, not the special one of the other
  // let-constructs in the TPTP.
  lazy val let_vt: PackratParser[Term] = 
    ("$let_vt" ~> "(" ~> "[" ~> tff_let_bindings <~ "]" ^^ {
      case  bindings => {
        val letVars = bindings map { _._1 }
        varsStack ::= vars
        vars = letVars ::: vars
        bindings
      }
    }) ~ "," ~ term ~ ")" ^^ {
      case bindings ~ "," ~ t ~ ")" => {
        // Remove the let-binding again
        vars = varsStack.head
        varsStack = varsStack.tail
        // This is the result
        Let(bindings, t)
      }
    }

  lazy val nonvar_term: PackratParser[Term] = prepro_term | funterm | constant | domElem
  lazy val variable: PackratParser[Var] = regex(new Regex("[A-Z][a-zA-Z0-9_]*")) ^^ {
    // check if the variable has been declared in vars
    name ⇒
      (vars find { _.name == name }) match {
        case None    ⇒ GenVar(name, ISort) // default sort is $i
        case Some(x) ⇒ x // use the declared one
      }
  }
  lazy val funterm: PackratParser[FunTerm] = functor ~ "(" ~ termlist ~ ")" ^^ {
    case functor ~ "(" ~ termlist ~ ")" ⇒ {
      if (!signature.existsOp(functor)) {
        // Create a default type declaration
      	val defaultOp = Operator(FG, functor, Arity((termlist map { _ ⇒ ISort }), ISort))
      	util.reporter.log("** Warning: Implicit introduction of "+defaultOp)
        signature += defaultOp
      }
      signature.checkedFunTerm(functor, termlist)
    }
  }

  lazy val fun_def_lhs: PackratParser[(String, List[Term])] = functor ~ "(" ~ termlist ~ ")" ^^ {
      case functor ~ "(" ~ termlist ~ ")" ⇒ { (functor, termlist) }
    } | 
  functor ^^ { (_, List.empty) }


  //TODO- ground versions of these can be evaluated by the parser
  lazy val prepro_quotient = regex("""\$quotient_[eft]""".r)
  lazy val prepro_remainder = regex("""\$remainder_[eft]""".r)

  lazy val prepro_term = prepro_quotient ~ "(" ~ domElem ~ "," ~ domElem ~ ")" ^^ {
    case opType ~ "(" ~ num ~ "," ~ denom ~ ")" => {
      (num, denom) match {
	case (DomElemInt(n), DomElemInt(d)) => {
	  if (opType.last=='e') { //euclidean
	    val res = 
	      if(n < 0) Math.ceil(n/d)
	      else Math.floor(n/d)
	    bgtheory.LIA.DomElemInt(res.toInt)
	  }
	  else if (opType.last=='f') //floor
	    bgtheory.LIA.DomElemInt(Math.floor(n/d).toInt)
	  else  //must be 't'- truncate
	    bgtheory.LIA.DomElemInt((n/d).toInt)
	}
	case _ => 
	  throw SyntaxError("Non-ground invocations of $quotient_[etf] not yet supported")
      }
    }
  }

  lazy val termlist = rep1sep(term, ",")

  lazy val conditional_term = "$ite_t" ~> "(" ~> tff_logic_formula ~ "," ~ term ~ "," ~ term <~ ")" ^^ {
    case cond ~ "," ~ thenTerm ~ "," ~ elseTerm => {
      if (thenTerm.sort != elseTerm.sort)
          throw SyntaxError("ite: then-term and else-term have different sorts: %s, %s".format(thenTerm, elseTerm))
      else
        ITE(cond, thenTerm, elseTerm)
    }
  }

  // symbolic constant
  lazy val constant: PackratParser[FunTerm] =
    // a constant cannot be followed by a parenthesis, would see a FunTerm instead
    // Use atomic_word instead of functor?
    guard(functor ~ not("(")) ~> functor ^^ { functor ⇒
      if (!signature.existsOp(functor))
        // The kind of the symbolic constants depends on the flag "params"
        signature += Operator(FG, functor, Arity0(ISort))
      signature.checkedFunTerm(functor, List.empty)
    }

  // Domain elements
  // todo: get rid of hard-coding recognisers for background types
  lazy val domElem = if(!forceSingleType) domElemInt | domElemRat | domElemReal else domElemConst

  lazy val domElemInt =
    regex("""[+-]?[0-9]+""".r) <~ guard(not("/" | ".")) ^^ {
      // regex(new Regex(Signature.IntSort.domElemRegEx)) ^^ {
      s ⇒
	try {
          val d = bgtheory.LIA.DomElemInt(s.toInt)
          domElemInts += d
          d
	} catch {
	  case e: NumberFormatException => 
	    throw SyntaxError("Large ints currently unsupported "+e.getMessage)
	}
    }

  lazy val domElemRat =
    regex("""[+-]?[0-9]+""".r) ~ "/" ~ regex("""[0-9]+""".r) ^^ {
      case numer ~ "/" ~ denom ⇒
        DomElemRat(Rat(numer.toInt, denom.toInt))
    }

  lazy val domElemReal =
    regex("""[+-]?[0-9]+""".r) ~ "." ~ regex("""[0-9]+""".r) ^^ {
      case a ~ "." ~ b ⇒ {
        def pow10(i: Int) = { var res = 1; var j = i; while (j > 0) { res *= 10; j -= 1 }; res }
        DomElemReal(Rat((a + b).toInt, pow10(b.length)))
        // DomElemRat(Rat((a+b).toInt, pow10(b.length)))
      }
    }
  
  //force numeric constants to parse as FG constants in the case of CNF
  lazy val domElemConst: PackratParser[FunTerm] =
    regex("""[0-9]+""".r) <~ guard(not("/" | ".")) ^^ {
      s ⇒
      if (!signature.existsOp(s))
        signature += Operator(FG, s, Arity0(ISort))
      signature.checkedFunTerm(s, List.empty)
    }
    
  // lexical: don't confuse = with => (the lexer is greedy)
  lazy val equalsSign = "=" <~ guard(not(">"))

  lazy val forallSign = "!" <~ guard(not("="))

  lazy val functor = keyword | atomic_word | system_pred

  lazy val atomic_word: PackratParser[String] =
    (regex("""'[^']*'""".r) ^^ { _.drop(1).dropRight(1) }) |
      regex("[a-z][a-zA-Z0-9_]*".r)

  lazy val keyword = regex("[$][a-z_]+".r)

  lazy val system_pred = regex("\\$\\$[a-z_]+".r)

  /* Could be specific (but why?)
    lazy val keyword = 
      "$uminus"     |
      "$sum"        |
      "$difference" |
      "$product"    |
      ("$less" <~ guard(not("eq")))   |
      "$lesseq"     |
      ("$greater" <~ guard(not("eq")))  |
      "$greatereq"  |
      "$evaleq"     
*/

  // Operator precedences. Use TPTP "defined comment" syntax %$ for that
  // E.g. %$ :prec f > g > h
  lazy val precs: PackratParser[List[Formula]] =
    regex("%\\$[ \t]+:prec".r) ~> atomic_word ~ ">" ~ rep1sep(atomic_word, ">") ^^ { 
      case f ~ ">" ~ fs => {
        var res = List.empty[Formula]
        var left = f
        for (right <- fs) {
          res ::= Atom("$gtrPrec", List(FGConst(left, Signature.ISort), FGConst(right, Signature.ISort)))
          left = right
        } 
        res.reverse
      }
    }

  // Parsing of comments is not optimal as they may not appear
  // inside formulas - essentially they are an atom
  lazy val comment: PackratParser[List[Formula]] =
    """%.*""".r ^^ (x ⇒ List(Comment(x)))

  lazy val include: PackratParser[List[Formula]] =
    "include" ~> "(" ~> atomic_word <~ ")" <~ "." ^^ {
      /* todo: From the TPTP spec:
       Include files with relative path names are expected to be found either 
       under the directory of the current file, or if not found there then under 
       the directory specified in the $TPTP environment variable.
       */
      case fileName ⇒ {
        val inIncDirFileName = incDir + "/" + fileName
        val realFileName = 
         // check if file inIncDirFileName exists, if so take it
          if (new java.io.File(inIncDirFileName).canRead())
            inIncDirFileName
          else {
            // Use $TPTP environment variable unless overridden with -tptp on the command line
            // println("xxx " + new java.io.File(fileName).getParent())
            if (flags.tptpHome.value != "")
              flags.tptpHome.value + "/" + fileName
            else
              System.getenv("TPTP") + "/" + fileName
          }
        val (newFs, newSig, newConjectures, deis) = TPTPParser.parseTPTPFile(realFileName, signature, genvars)
        // The signature might have been extended, hence
        signature = newSig
        conjectures = newConjectures ::: conjectures
        domElemInts ++= deis
        newFs
      }
    }

  /*
  /**
   * checkedXX: creates an XX, type-checked against sig and varTypes
   */
  def checkedEquation(s: Term, t: Term) = {
    
    if (s.sort != OSort && (s.sort sameUnderlyingSort t.sort))
      Equation(s, t) // For simplicity, no need to discover BG Equations in the parser
    else
      throw SyntaxError("ill-sorted (dis)equation: between " + s + " and " + t)
  }

  def checkedAtom(pred: String, args: List[Term]) =
    // Assume that pred has been entered into sig already
    signature.findOperator(pred) match {
      case Some(Operator(_, _, Arity(argsSorts, OSort))) ⇒
        if (argsSorts.zip(args map {_.sort}).forall(x => x._2 sameUnderlyingSort x._1)) 
          Atom(pred, args)
        else
          throw SyntaxError("ill-sorted atom: " + Atom(pred, args))
      case _ ⇒ throw SyntaxError("internal error: checkAtom called on non-declared predicate symbol " + pred)
    }

  def checkedFunTerm(fun: String, args: List[Term]): FunTerm = {
    // Assume that fun has been entered into sig already
    val op = signature.findOperator(fun).get
    if (args.isEmpty)
      // A constant. 
      SymConst(op)
    else if (op.arity.argsSorts.zip(args map {_.sort}).forall(x => x._2 sameUnderlyingSort x._1))
      // Type Checking OK 
      PFunTerm(op, args)
    else
      throw SyntaxError("ill-sorted term: " + PFunTerm(op, args))
  }

  def typeExistsChecked(sort: Sort) =
    if (signature.sorts contains sort)
      sort
    else
      throw SyntaxError("Error: type has not been declared: " + sort)
      * 
      */
}

object TPTPParser {

  // val tffParser = new TPTPParser(Signature.signatureEmpty, genvars = false)

  def parseFormula(s: String, inputSignature: Signature, genvars: Boolean): Formula = {
    val tffParser = new TPTPParser(inputSignature, genvars, incDir = ".")
    tffParser.parseAll(tffParser.tff_logic_formula, s).get
  }

  /*
   * Main entry point for parsing.
   * parseTPTPFile(fileName: String, inputSignature: Signature, genvars: Boolean)
   * @param genvars if true then BG-sorted variables are of kind 'ordinary' else 'abstracted'
   * @return a quadruple (tffs, outputSignature, conjectures, domElemInts)
   * where tffs is the list of all formulas, including the conjectures, and
   * conjectures are the conjectures among the tffs.
   */
  def parseTPTPFile(fileName: String, inputSignature: Signature, genvars: Boolean) = {

    Timer.parsing.start()
    def isGtrPrec(f: Formula) = f match {
      case Atom("$gtrPrec", List(_, _)) => true
      case _ => false
    }
    val thisDir = new java.io.File(fileName).getParent()
    val incDir =
      if (thisDir == null)
        // no parent directory specified
          "."
      else
        thisDir

    val myTPTPParser = new TPTPParser(inputSignature, genvars, incDir = incDir)
    val reader = new FileReader(fileName)
    try {
      import myTPTPParser._
      parseAll[List[List[Formula]]](myTPTPParser.TPTP_input, reader) match {
        case Success(x, _) => {
          val tffsAndPrecs = x.flatten.filter(!_.isComment)
          val (precsAtoms, tffs) = tffsAndPrecs partition { isGtrPrec(_) }
          val precs = precsAtoms map {
            case Atom("$gtrPrec", List(FGConst(Operator(FG, f, _)), FGConst(Operator(FG, g, _)))) => (f, g)
          }
          Timer.parsing.stop()
          (tffs, new Signature(myTPTPParser.signature.sorts,
            myTPTPParser.signature.operators,
            myTPTPParser.signature.precs ::: precs),
            myTPTPParser.conjectures,
            myTPTPParser.domElemInts)
        }
        case NoSuccess(err, next) => {
          Timer.parsing.stop()
          throw SyntaxError("Failed to parse input file: (line " + next.pos.line +
            ", column " + next.pos.column + "):\n" + err + "\n" +
            next.pos.longString)
        }
      }
    } catch {
      case e : IllegalArgumentException ⇒ { 
        // caused by dangling references
        Timer.parsing.stop()
        throw SyntaxError("Failed to parse input file: "+e.getMessage)
      }
    }
  }
}
