package beagle.parser

// import scala.util.matching.Regex
// import scala.util.parsing.combinator._
// import java.io.FileReader
import scala.collection.immutable.ListMap

import beagle._
import fol._
import term._
import Signature._
import bgtheory._
import bgtheory.LIA.DomElemInt
import smttotptp._
import beagle.util._

/**
 * A parser for SMT-Lib Formulas
 */
object SMTLibParser {

  case class Error(s: String) extends Exception

  /*
   * Definitions related to populating the target signature.
   * We use TPTP syntax entities, hence the following definitions.
   */


  def warning(msg: String) {
    System.err.println("warning: " + msg)
  }

  /*
   * Export the given AST into a signature.
   * Returns a triple (sig, formulas, domElemInts) where domElemInts
   *  is the set of integer domain elements found.
   */ 

  def export(ast: AST, genvars: Boolean, precs: List[(String, String)]) = {

    // The signature we are going to extend by this export
    var sig = LRA.addStandardOperators(LIA.addStandardOperators(signatureEmpty)).setPrecs(precs)

    var domElemInts = Set.empty[DomElemInt] // all the integer domain elements ever parsed

    import ast._

    def varToTPTP(v: Var) = v.toString

    // The concrete sorts used in all terms
    var sorts = Set.empty[CSort]

    def varWithSortToTPTP(vs: (Var, CSort)) = varToTPTP(vs._1) + ":" + sortToTPTP(vs._2)

    def varWithSortToVar(vs: (Var, CSort)) = {
      val sortSig = sortInSig(vs._2)
      val vName = varToTPTP(vs._1)
      if (sortSig.thyKind == BG && !genvars)
        AbstVar(vName, sortSig)
      else
        GenVar(vName, sortSig)
    }

    def sortToTPTP(sort: CSort, toAtomicWordFlag: Boolean = true): String = {
      import beagle.util._ 

      val CSort(SortSym(name), args) = sort
      name match {
        case "Bool" if toAtomicWordFlag => "$o"
        case "Int" if toAtomicWordFlag => "$int"
        case "Real" if toAtomicWordFlag => "$rat"
        case _ => {
          val s = name + (args map { sortToTPTP(_, false) }).toMyString("", "[", ",", "]")
          s // if (toAtomicWordFlag) toAtomicWord(s) else s
        }
      }
    }

    // get the type of the given sort in sig
    def sortInSig(sort: CSort) =  sig.findSort(sortToTPTP(sort)).get

    val tffPredefSorts = Set(ast.IntSort, ast.RealSort, ast.BoolSort)

    def funToTPTP(fun: FunSym, argsSorts: List[CSort], resSort: CSort) = {
      import beagle.util._ 
      val FunSym(name) = fun
      val m = Map(
        "true" -> "$true",
        "false" -> "$false",
        "<" -> "$less",
        ">" -> "$greater",
        "<=" -> "$lesseq",
        ">=" -> "$greatereq",
        "+" -> "$sum",
        "-" -> "$difference",
        "*" -> "$product",
        "/" -> "$quotient")

      // unary minus is a special case, as it's overloaded with subtraction
      if ((name == "-") && (argsSorts.length == 1))
        "$uminus"
      else m.get(name) match {
        case Some(s) => s
        case None => if ((declaredCFuns contains fun) || (definedFuns contains fun))
          // non-parametric case, don't need sort annotations to make the function name unique
          name // toAtomicWord(name)
        else
          // toAtomicWord(
            name + ":" + (argsSorts map { sortToTPTP(_, false) }).toMyString("", "(", "*", ")") +
              (if (argsSorts.isEmpty) "" else ">") + sortToTPTP(resSort, false)
          // )
      }
    }

    def funRankToTPTP(funRank: (FunSym, List[CSort], CSort)) = {
      import beagle.util._ 
      val (fun, argsSorts, resSort) = funRank
      funToTPTP(fun, argsSorts, resSort) + ": " +
        (if (argsSorts.isEmpty) ""
        else
          ((if (argsSorts.tail.isEmpty)
            sortToTPTP(argsSorts.head)
          else
            (argsSorts map { sortToTPTP(_) }).toMyString("", "(", " * ", ")")) + " > ")) + sortToTPTP(resSort)
    }

    var funRanks = Set.empty[(FunSym, List[CSort], CSort)]

    val leftAssocOps = Set(FunSym("xor"), FunSym("+"), FunSym("-"), FunSym("*"), FunSym("/"), FunSym("div"))
    val rightAssocOps = Set(FunSym("=>"), FunSym("implies"), FunSym("and"), FunSym("or"))
    val chainableOps = Set(FunSym("="), FunSym("<"), FunSym(">"), FunSym("<="), FunSym(">="))
    val pairwiseOps = Set(FunSym("distinct"))
    val allOps = leftAssocOps ++ rightAssocOps ++ chainableOps ++ pairwiseOps

    def expandLeftAssoc(fun: FunSym, args: List[Term]) = args.reduceRight((s, t) => App(fun, List(s, t)))
    def expandRightAssoc(fun: FunSym, args: List[Term]) = args.reduceLeft((s, t) => App(fun, List(s, t)))
    def expandChainable(op: FunSym, args: List[Term]) =
      App(FunSym("and"),
        (for (
          ts <- args.tails;
          if (!ts.isEmpty) && (!ts.tail.isEmpty)
        ) yield App(op, List(ts.head, ts.tail.head))).toList)
    def expandPairwise(op: FunSym, args: List[Term]) =
      App(FunSym("and"),
        (for (
          ss <- args.tails;
          if (!ss.isEmpty);
          ts <- ss.tail.tails;
          if (!ts.isEmpty)
        ) yield App(op, List(ss.head, ts.head))).toList)


    /*
     * Convert an SMTTerm to a Beagle Term
     * Assume that sig has been extended to contain the neccessary ranks
     * 
     */
    def toTerm(t: Term, varSort: ListMap[Var, CSort]): term.Term = {
      import beagle.util._ 
      if (!t.attributes.isEmpty)
        beagle.util.warning("ignoring attributes" + t.attributes.toMyString(" ", " ", " ") + "of term " + t)

      val resSort = t.xsort(varSort)
      val resSortSig = sortInSig(resSort)

      t match {
        case Const(const) => 
          const match {
            case Numeral(n) => bgtheory.LIA.DomElemInt(n)
            case Decimal(Rat(num, denom)) => 
              bgtheory.LRA.DomElemRat(beagle.util.Rat(num.toInt, denom.toInt))
            case StringSExpr(s) => throw this.Error("Cannot handle strings: " + s)
          }
        case v: Var => {
          val vName = varToTPTP(v)
          if (resSortSig.thyKind == BG && !genvars)
            AbstVar(vName, resSortSig)
          else
            GenVar(vName, resSortSig)
        }

        case App(FunSym("ite"), List(c, t, e)) =>
          throw this.Error("ite (if-then-else) not yet implemented. Sorry.")

        // Applications of associatove, chainable and pairwise operators:
        case App(fun @ FunSym(_), args) if (args.length) > 2 && (leftAssocOps contains fun) =>
          toTerm(expandLeftAssoc(fun, args), varSort)
        case App(fun @ FunSym(_), args) if (args.length) > 2 && (rightAssocOps contains fun) =>
          toTerm(expandRightAssoc(fun, args), varSort)
        case App(fun @ FunSym(_), args) if (args.length) > 2 && (chainableOps contains fun) =>
          toTerm(expandChainable(fun, args), varSort)
        case App(fun @ FunSym(_), args) if (args.length) > 2 && (pairwiseOps contains fun) =>
          toTerm(expandPairwise(fun, args), varSort)

       // "ordinary" function applications
        case App(fun @ FunSym(name), args) => {
          val argsSorts = args map { _.xsort(varSort) }
          NonDomElemFunTerm(sig.findOperator(funToTPTP(fun, argsSorts, resSort), argsSorts map { sortInSig(_) }).get,
            args map { toTerm(_, varSort) })
        }
        case App(As(fun @ FunSym(name), asSort), args) => {
          val argsSorts = args map { _.xsort(varSort) }
          NonDomElemFunTerm(sig.findOperator(funToTPTP(fun, argsSorts, resSort), argsSorts map { sortInSig(_) }).get,
            args map { toTerm(_, varSort) })
        }
        case Let(bindings, body) => {
          val bodyVarSort = varSort ++ (bindings map { case Equal(v, t) => (v, t.xsort(varSort)) })
          term.Let((bindings map { 
            case Equal(v, t) => {
              val vName = varToTPTP(v)
              val tSortSig = sig.findSort(sortToTPTP(t.xsort(varSort))).get
              (if (tSortSig.thyKind == BG && !genvars)
                AbstVar(vName, tSortSig)
              else
                GenVar(vName, tSortSig), toTerm(t, varSort)) 
            }
          }), toTerm(body, bodyVarSort))
        }
        case _ => throw this.Error("cannot convert term: " + t)
      }
    }


    /*
     * Convert an SMTTerm to a Beagle Formula
     * Assume that sig has been extended to contain the neccessary ranks
     * 
     */
    def toFormula(t: Term, varSort: ListMap[Var, CSort]): fol.Formula = {

      import beagle.util._ 
      assume(t.xsort(varSort) == BoolSort)

      t match {
        // Applications of associatove, chainable and pairwise operators:
        case App(fun @ FunSym(_), args) if (args.length) > 2 && (leftAssocOps contains fun) =>
          toFormula(expandLeftAssoc(fun, args), varSort)
        case App(fun @ FunSym(_), args) if (args.length) > 2 && (rightAssocOps contains fun) =>
          toFormula(expandRightAssoc(fun, args), varSort)
        case App(fun @ FunSym(_), args) if (args.length) > 2 && (chainableOps contains fun) =>
          toFormula(expandChainable(fun, args), varSort)
        case App(fun @ FunSym(_), args) if (args.length) > 2 && (pairwiseOps contains fun) =>
          toFormula(expandPairwise(fun, args), varSort)

        // special cases
        case App(FunSym("="), List(s, t)) =>  
          // Equations between BoolSorted terms are OK, hence check
          if (s.xsort(varSort) == BoolSort)
            Iff(toFormula(s, varSort), toFormula(t, varSort))
          else
            Equation(toTerm(s, varSort), toTerm(t, varSort))

        case App(FunSym("distinct"), List(s, t)) =>
          Neg(Equation(toTerm(s, varSort), toTerm(t, varSort)))

        case App(FunSym("and"), List()) => TrueAtom
        case App(FunSym("and"), List(s)) => toFormula(s, varSort)
        case App(FunSym("and"), List(s, t)) => And(toFormula(s, varSort), toFormula(t, varSort))

        case App(FunSym("or"), List()) => FalseAtom
        case App(FunSym("or"), List(s)) => toFormula(s, varSort)
        case App(FunSym("or"), List(s, t)) => Or(toFormula(s, varSort), toFormula(t, varSort))

        case App(FunSym("not"), List(s)) => Neg(toFormula(s, varSort))

        case App(FunSym(op), List(s, t)) if op == "implies" || op == "=>" =>
          Implies(toFormula(s, varSort), toFormula(t, varSort))
 
// not handled yet; todo
        case App(FunSym("ite"), List(c, t, e)) =>
          throw this.Error("ite (if-then-else) not yet implemented. Sorry.")
          // (if (t.xsort(varSort) == BoolSort) "$ite_f" else "$ite_t") +
          //   "(" + toTPTP(c, varSort) + ", " + toTPTP(t, varSort) + ", " + toTPTP(e, varSort) + ")"

       // Ordinary atoms
        case App(fun @ FunSym(name), args) => {
          val argsSorts = args map { _.xsort(varSort) }
          Atom(funToTPTP(fun, argsSorts, BoolSort),
            args map { toTerm(_, varSort) })
        }


// should never occur
/*
        case App(As(fun @ FunSym(name), asSort), args) => {
          val argsSorts = args map { _.xsort(varSort) }
          PFunTerm(sig.findOperator(funToTPTP(fun, argsSorts, resSort)).get,
            args map { toTerm(_, varSort) })
        }

 */
        case Exists(varsWithSorts, body) => 
          fol.Exists(varsWithSorts map { varWithSortToVar(_) }, 
            toFormula(body, varSort ++ varsWithSorts))

        case Forall(varsWithSorts, body) => 
          fol.Forall(varsWithSorts map { varWithSortToVar(_) }, 
            toFormula(body, varSort ++ varsWithSorts))

        case Let(bindings, body) => {
          val bodyVarSort = varSort ++ (bindings map { case Equal(v, t) => (v, t.xsort(varSort)) })
          fol.LetFormula((bindings map { 
            case Equal(v, t) => {
              val vName = varToTPTP(v)
              val tSortSig = sig.findSort(sortToTPTP(t.xsort(varSort))).get
              (if (tSortSig.thyKind == BG && !genvars)
                AbstVar(vName, tSortSig)
              else
                GenVar(vName, tSortSig), toTerm(t, varSort)) 
            }
          }), toFormula(body, bodyVarSort))
        }
        case _ => throw this.Error("cannot convert formula: " + t)
      }
    }


    /*
     * Analyze the given term and populate sig according to all subterms
     */ 

    def analyze(t: Term, varSort: ListMap[Var, CSort]) {
      import beagle.util._ 
      if (!t.attributes.isEmpty)
        beagle.util.warning("ignoring attributes" + t.attributes.toMyString(" ", " ", " ") + "of term " + t)

      val resSort = t.xsort(varSort)
      sorts += resSort

      t match {
        case Const(const) => {
          const match {
            case Numeral(n) => domElemInts += bgtheory.LIA.DomElemInt(n)
            case Decimal(Rat(num, denom)) => ()
            case StringSExpr(s) => throw this.Error("Cannot handle strings: " + s)
          }
        }
        case v: Var => ()

        // Applications of associatove, chainable and pairwise operators:
        case App(fun @ FunSym(_), args) if (allOps contains fun) =>
          args foreach { analyze(_, varSort) }

        case App(FunSym(op), args) if (List("ite", "not") contains op) => 
          args foreach { analyze(_, varSort) }

        // "ordinary" function applications
        case App(fun @ FunSym(name), args) => {
          val argsSorts = args map { _.xsort(varSort) }
          funRanks += ((fun, argsSorts, resSort))
          args foreach { analyze(_, varSort) }
        }

        case App(As(fun @ FunSym(name), asSort), args) => {
          sorts += asSort
          val argsSorts = args map { _.xsort(varSort) }
          funRanks += ((fun, argsSorts, asSort))
          args foreach { analyze(_, varSort) }
        }

        case Exists(varsWithSorts, body) => analyze(body, varSort ++ varsWithSorts)
        case Forall(varsWithSorts, body) => analyze(body, varSort ++ varsWithSorts)
        
        case Let(bindings, body) => {
          val bodyVarSort = varSort ++ (bindings map { case Equal(v, t) => (v, t.xsort(varSort)) })
          bindings foreach { case Equal(_, t) => analyze(t, varSort) }
          analyze(body, bodyVarSort)
        }

        case _ => throw this.Error("cannot analyze term: " + t)
      }
    }

    assertions foreach { analyze(_, ListMap.empty) }

    // Types
    for (
      sort <- sorts;
      if (! (tffPredefSorts contains sort)))
      sig += FGSort(sortToTPTP(sort))

    // Declarations
    // Don't need to write declarations for predefined operators
    val tffPredefFuns = Set(FunSym("true"), FunSym("false"), FunSym("<"), FunSym(">"), FunSym("<="), FunSym(">="), FunSym("+"), FunSym("-"), FunSym("*"), FunSym("/"))

    for (
      funRank <- funRanks;
      if (!(tffPredefFuns contains funRank._1))
    ) {
      val (fun, argsSorts, resSort) = funRank
      // Convert the argument sorts of funRank to a list of sorts already declared 
      // in sig
      val argsSortsSig =  
        sig += Operator(
          if (argsSorts.isEmpty &&
            // Prop variables are better handled
            // by the foreground
            resSort != BoolSort &&
            sortInSig(resSort).thyKind == BG &&
            beagle.util.flags.paramsOpSet.value == "BG")
            // only in this case we have a BG operator
            BG
          else FG, 
          funToTPTP(fun, argsSorts, resSort), Arity(argsSorts map { sortInSig(_) }, sortInSig(resSort)))
    }

    // Assertions
    val formulas = assertions map { toFormula(_, ListMap.empty) }

    (sig, formulas, domElemInts)
  }

  /*
   * Main entry point for parsing.
   * parseSMTLibFile(fileName: String, genvars: Boolean)
   * if genvars is true then BG-sorted variables are of kind 'ordinary' else 'abstracted'
   * @return a quadruple (tffs, outputSignature, None, domElemInts)
   *  The "None" component indicates "no conjecture". SMT-LIB does not have 
   *  the concept of a conjecture.
   */
  def parseSMTLibFile(fileName: String, genvars: Boolean) = {

    Timer.parsing.start()

    val ast = new AST
    var precs = List.empty[(String, String)]

    try {
      val cmds = SExprParser.parseSMTLibFile(fileName)
      // If array/list command line options are given we need to declare them before parsing.
      // Notice corresponding option settings in the input file are possibly overridden
      if (beagle.util.flags.listsFlag.value) ast.addListDatatype()
      if (beagle.util.flags.arraysFlag.value) ast.addArrayDeclarations()

      // println("Parsing into abstract syntax")
      ast.parseCommands(cmds)
      import ast._
      // if (ast.logic == None)
      //   println("smttotptp: warning: no set-logic command, assuming LIRA + datatype extension")

      // println("Options given:")
      // ast.options foreach { println(_) }

      //if (!quietFlag) ast.show()

      // println("Expanding defined functions")
      ast.expandDefinedFunctions()

      // Can add axioms only now, after function definitions have been expanded
      if (getOptionStringValue(":declare-arrays") == Some("true") || beagle.util.flags.arraysFlag.value)
        ast.addArrayAxioms()

      ast.addDatatypeAxioms()

      if (!(getOptionStringValue(":expand-let") == Some("false")))
        ast.expandLet()

      if (! (getOptionStringValue(":expand-ite") == Some("false")))
        ast.expandIte()

      def precError[T](s:T) {
        System.err.println("Malformed :prec option: " + s)
        sys.exit(1)

      }

      def addPrecs(s: String) {
        (s.split(Array('>', ' ')) filterNot { _.isEmpty }).toList match {
          case p :: ps => {
            var res = List.empty[(String, String)]
            var left = p
            for (right <- ps) {
              res ::= (left, right)
              left = right
            }
            precs :::= res.reverse
          }
          case x => precError(x)
        }
      }

      (ast.options find { _.key == Keyword(":prec") }) match {
        case Some(Attribute(_, Some(StringSExpr(s)))) => 
          addPrecs(s)
        case Some(Attribute(_, Some(SExprList(h)))) => 
          h foreach { 
            _ match {
              case StringSExpr(s) => addPrecs(s)
              case x => precError(x)
            }
          }
        case None => ()
        case x => precError(x)
      }

    } catch {
      case ast.Error(s) => {
        System.err.println("AST error: " + s)
        sys.exit(1)
      }
      case SExprParser.Error(s) => {
        System.err.println("SExpr parser error: " + s)
        sys.exit(1)
      }
    }

    // ast.show()

    val (sig, formulas, domElemInts) = export(ast, genvars, precs)

    Timer.parsing.stop()

    (formulas, sig, List.empty, domElemInts)

    // (tffs, new Signature(myTPTPParser.signature.sorts, myTPTPParser.signature.operators, myTPTPParser.signature.precs ::: precs), 
    //      myTPTPParser.haveConjecture, myTPTPParser.domElemInts)
  }

}

