package beagle.finite

import beagle._
import beagle.main._
import calculus._
import fol._
import term._
import bgtheory.LIA._
import datastructures._
import util._

/**
 * Implements the variant aware algorithm for the workshop paper.
 * Note that we no longer need `FQClauseSet` since there is no need to
 * track the origin of variables.
 */
object FiniteSearchContext {
  
  /**
   * Generate a set of assumptions from an input clause set,
   * removing any variants.
   * Clauses which have ground instances of BSFG terms are not treated,
   * since the define rule still applies.
   * TODO For consistency it might be better to include these here too.
   * What happens if we have a non-FQ clause?
   * TODO- take FQClauseSet args?
   */
  def generate(cl: List[FQClause]): Set[SimpAssump] = {
    var a = Set.empty[SimpAssump]

    for(c <- cl;
	if(c.hasFQVars);  //what about ground clauses? these are handled by define
	t <- c.maxBSFGTerms) {

      println("Generate "+c)
      println("replacing maximal BSFG term "+t)

      //find the relevant FQ vars in C
      //rather, we should make a new SubstSet by taking domains of vars in t
      //val subs = new SubstSet(c.delta.filterKeys(t.vars(_)))
      val relVars = c.delta.filterKeys(t.vars(_))

      val (variants,diff) = a.partition(_.defTerm ~ t)

      if(variants.isEmpty) {
	println("No variant definitions found")
	a += SimpAssump(makeParam(t),t,relVars)
	//a += Assumption(makeParam(t),t,List(subs))
      }else {
	assume(variants.tail.isEmpty, "At most one variant should exist in the assumption set!")
	println("Variant definitions found:")
	println(variants.head)
	println()
	/*
	val newSubs = {
	  val a = variant.head
	  val mu = a.defTerm.matcher(t).get
	  assume(mu.isRenaming, "Expected matcher of variant terms to be renaming")
	    
	  //a.subs.map(_.applySubst(mu))
	  a.applySubst(mu)
	}

	val newA = 
	  if(newSubs.indexOf(subs) < 0)
	    Assumption(variant.head.alpha,t,(subs::newSubs))
	  else 
	    Assumption(variant.head.alpha,t,newSubs)
	*/
	val variant: SimpAssump = variants.head
	val mu = variant.defTerm.matcher(t).get
	assume(mu.isRenaming, "Expected matcher of variant terms to be renaming")
	
	//want to do union of delta so that context is compact
	//TODO- is this correct?
	val newDeltas = 
	  mu(variant).subs.map({case (x,d) => (x,relVars(x).union(d))})
	val newA = SimpAssump(variant.alpha,t,newDeltas)
	
	println("Updated assumption is "+newA)
	a = (diff + newA )
      }

      println("a is now:")
      a foreach {println _}
      println()
    }
    return a
  }

  /** Produce the instances of a clause under a given assumption set */
  def applyContext(c: FQClause, as: Set[SimpAssump]): Set[LabelledFQClause] = {
    println(s"apply: $c")

    /**
     * Recursive backtracking search over all possible matchers of assumptions with the given terms.
     * @param ct the list of terms to unify against.
     * @param mu the current open substitution.
     * @param used accumulates the assumptions as they are used.
     * @return a set of substitutions and the assumptions used to build that.
     * These assumptions should appear in the same order as the terms in ct i.e. the first
     * assumption parameter is substituted for the head of ct and the (matching) subst returned
     * should make ct.head and the definition term equal also.
     */
    def go(ct: List[Term], mu: Subst, used: List[SimpAssump]): Set[(Subst,List[SimpAssump])] = {
      println(s"go: $ct $mu $used")
      //find an assumption in as such that ct.head unifies with it modulo current mu
      //each list member is a clause instance
      if (ct.isEmpty) //you have matched all terms
	return Set((mu,used))
      else {
	(for(a1 <- as;
	     a = a1.fresh;
	     sigma <- ct.head mgu a.defTerm;
	     mu2 <- sigma combine mu;
	     if ( !(mu2.env.values exists { c => c.isBG && c.isSymConst }) )) yield //also check it is simple!!!  
	  go(mu2(ct.tail),mu2,a.applySubst(mu2)::used) //its ok to apply mu2 here since a is fresh
	 ).flatten
	 /*
	 what about f(X) | g(f(X))
	 get f(X), f(X)
	 1: a, f(X1) using [X->X1] w (a,f(X1))
	 2: a, a using [X1 -> X2]  w (a,f(X2))(a,f(X2))
	 final is [X->X2,X1->X2] w (a,f(X2))
	 */
	 //if mu cannot be extended with sigma yield nothing
      }
    }

    //TODO- when are duplicate assumptions merged?
    
    if(!c.hasFQVars) //don't do anything
      Set(new LabelledFQClause(c,Nil))
    else {
      //BSFG terms and their positions in c
      val (cBSFGTerms,posBSFG) = c.subTermsWithPos.toList collect { case (t,p) if t.isBSFG => (t,p) } unzip

      go(cBSFGTerms,Subst.empty,List()) map { case (subst,label) => {
	println(s"Using $subst we match ${label.mkString("\n")}")

	//here we replace maxBSFGTerms with label params
	var newC: FQClause = c
	//the BSFG terms are replaced by elements of label in the same order.
	for( (pos,param) <- posBSFG zip label.map(_.alpha) ) {
	  newC = newC.replaceAt(pos,param)
	}

	//take all of the domain predicates from the assumptions
	val newDomPreds: Map[Var,Domain[DomElemInt]] = label flatMap { _.subs } toMap
	//but these are stored in DNF form within assumptions, so must unfold them here, e.g.
	//(cl | [l1,l2,...])
	//(cl | [[s11,s12,...],[s21,s22,..],..])
	//(cl | [[s11 v s12 v...] & [s21 v s22 v ..] &..])

	//apply the matcher to the rest of the clause
	newC = newC.applySubst(subst)

	//collect the final result, i.e. include the domains of the assumptions in the clause
	var newCls: Vector[FQClause] = Vector(newC)
	
	/*for(Assumption(_,_,subs) <- label) {
	  newCls = 
	    for(cl1 <- newCls;
		ss <- subs) yield cl1.apply(ss.delta)
	}*/
	for(assump <- label)
	  newCls = newCls.map(_.apply(assump.subs))
	
	//then apply the labels
	newCls.map(new LabelledFQClause(_,label)).toSet

      }} flatten
    }
  }

  /** Produce sufficiently complete clauses from the current assumptions. */
  def flatten(as: Set[SimpAssump]): Set[LabelledFQClause] = {
    (for(a <- as) yield {
      println(s"flatten $a")
      //assume A = (α, t, St )
      val label = List(a) //all created clauses are labelled with A
      var e = a.defTerm
      //the set of definitions to return
      var defs = Set.empty[LabelledFQClause]

      //here we use minBSFGTerms for sufficient completeness
      //also since a.defTerm is also BSFG stop when just one is left as that is the top term.
      while(!e.minBSFGTerms.tail.isEmpty) {
	val s = e.minBSFGTerms.head
	val beta = makeParam(s)

	println(s"removing $s from $e")

	/* Need to convert from a substitution set to a domain predicate/formula
	 * which then becomes a delta map.
	 * Substitution sets can have disjunctions in their domain predicate representation,
	 * these are expanded here.
	 */
	/*for(ss <- a.subs;
	    if(s.vars forall { ss.delta.isDefinedAt(_) })) { //all s.vars must be FQ
	      //the ss are conjunctions
	      defs += new LabelledFQClause(
		FQClause( ss.delta, Clause( Eqn(s,beta).toLit :: Nil )),
		label)
	    }*/
	//simpler structure for assumptions now
	val deltas = a.subs filterKeys { s.vars(_) }
	defs += new LabelledFQClause(
		FQClause( deltas, Clause( Eqn(s,beta).toLit :: Nil )),
		label)

	e = e.replaceSubterm(s,beta)
	println(s"e = $e")
      }

      //return Def t ∪ {E ≈ α ← S t | A}
      defs + 
	(new LabelledFQClause(
	    FQClause(a.subs.filterKeys(e.vars(_)),Clause( Eqn(e,a.alpha).toLit::Nil )), 
	    label))
	/*(for(ss <- a.subs) yield {
	  new LabelledFQClause(
	    FQClause(ss.delta,Clause( Eqn(e,a.alpha).toLit::Nil )), 
	    label)
	})*/
    }).flatten
  }
  
  /**
   * Apply calculus rules to a set of labelledFQClauses until
   * saturation.
   * @return An empty clause (with appropriate labels) or a saturated
   * state.
   */
  def saturate(cls: List[LabelledFQClause]): Either[Clause,State] = {
    reporter.log("Sat. checking:\n"+cls.mkString("\n"))
    
    //they convert to LClauses, by taking the deltas to formulas:
    val gndClauses:List[LClause] = cls flatMap { case LabelledFQClause(cl,l) =>
      cl.toLogicalClauses //produces a list of clauses with the same assumptions?
	 .map(new LClause(_,l))
    }
 
    //here the proof search must respect labelled clauses.
    //Culprits are: Split, isConsistent
    FiniteSearch.proofSearch(gndClauses.toSet,100000)
  }

  /** The Gen-var algorithm as a whole */
  def run(cs: List[FQClause]) = {
    //generate the inital assumption set
    var as = generate(cs)

    println("generate(cs):")
    as foreach { println _ }

    val input: List[LabelledFQClause] = 
	cs.flatMap(applyContext(_,as)) ++ flatten(as)
      
    println("Input (from Apply+Flatten):")
    input foreach { println _ }
   /* 
    while(true) {
      val input: List[LabelledFQClause] = 
	cs.flatMap(applyContext(_,as)) ++ flatten(as)
      
      println("Input (from Apply+Flatten):")
      input foreach { println _ }
*/
      saturate(input) match {
	case Left(LabelledFQClause(_,l)) => { // ie. □ | l in saturate(input)
	  if(l.isEmpty || l.forall( _.isGround )) Unsat
	  else {
	    Unknown //TODO
            //take non-ground a=(α, t, S) ∈ L //but which one?
            //as = Update(a ,S) //lookup a in as and update with S (which could be different)
	  }
	}
	case Right(state) => Sat(state) //TIMEOUT?
      }
/*	}*/

 //   Unknown
  }

  /**
   * A regular clause (i.e. no domain predicates) and label set.
   * Labels can be extended.
   * Labels do not need simiplification (yet).
   * Substitutions applied to the clause also apply to the labels.
   * Problem is that there are two ways of applying a substitution:
   * cl.appySubst(s) and Clause(s(cl.lits))
   */
  class LClause(val cl: Clause, val label: List[SimpAssump]) 
	  extends Clause(cl.lits, cl.idxRelevant, cl.age) {

    /* Methods lifted from Clause */

    override def toString = cl.toString + " | " + label.mkString(", ")

    override def applySubst(s: Subst): LClause =
      if (s actsOn this)
	new LClause(cl.applySubst(s),label.map(_.applySubst(s)))
      else
	this

    override def modified(lits: List[Lit] = this.lits, 
			  idxRelevant: Set[Int] = this.idxRelevant,
			  age: Int = this.age,
			  parents: List[Int] = Nil): LClause = 
       new LClause(cl.modified(lits, idxRelevant, age, parents), label)

    /* New methods */

    def addAssumptions(as: List[SimpAssump]) = new LClause(cl,label:::as)

  }

  /** An `FQClause` with a list of the Assumptions it depends on. */
  class LabelledFQClause(val cl: FQClause, val label: List[SimpAssump]) {
    override def toString = cl.toString + " | "+label.mkString(", ")
  }

  object LabelledFQClause {
    def apply(c: LabelledFQClause, label: List[SimpAssump]) =
      new LabelledFQClause(c.cl,c.label ::: label)

    //extract an FQClause/Label pair
    def unapply(other: Any): Option[(FQClause,List[SimpAssump])] = other match {
      case lc: LabelledFQClause => Some((lc.cl,lc.label))
      case fqc: FQClause => Some((fqc,List()))
      case c: Clause => Some((FQClause(Map(),c),List()))      
      case _ => None
    }
  }

}


