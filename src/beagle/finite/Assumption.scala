package beagle.finite

import beagle._
import fol._
import term._
import datastructures._
import bgtheory.LIA._

/* Assumption is deprecated in favour of SimpAssump, because it has
 * simpler representation of substitutions.
 */

/**
 * The current set of definitions are encoded as assumptions.
 * @param subs is set of simple substitutions such that for σ ∈ subs,
 * tσ = a is ground.
 */
case class Assumption(alpha: SymConst, defTerm: Term, subs: List[SubstSet]) {
  override def toString = "("+defTerm + " ≈ " + alpha + " : "+subs.mkString("[",", ","]")+")"
  //TODO- might be better to do this as part of a constructor?
  def fresh = {
    val s = Term.mkRenaming(defTerm.vars)
    this.applySubst(s)
  }

  /** Substitutions apply to both the defTerm and substitution set */
  def applySubst(sigma: Subst) = 
    Assumption(alpha,sigma(defTerm),subs.map(_.applySubst(sigma)))

  def isGround = defTerm.isGround || subs.size <= 1
}

/**
 * SubstSets have representations as Formulas (i.e. domain predicates) and
 * sets of substitutions.
 * This stores both.
 * Note that this translates to a conjunction: for xi->Di in delta
 * we get D1.toFormula(x1) & ... & Dn.toFormula(xn)
 * Disjunctive combinations can be modelled by sets of `SubstSet`s
 */
class SubstSet(val delta: Map[Var,Domain[DomElemInt]]) {
  lazy val asSet = instances(delta).toSet
  //lazy val asFormula = delta map {case (x,d) =>  d.asFormula(x)} toAnd

  override def toString = "{"+delta.toString+"}"

  override def equals(other: Any) = other match {
    case ss: SubstSet => ss.delta==this.delta
    case _ => false
  }

  //TODO: Intersect?

  /**
   * Viewed as a union of substitutions, we want to include all
   * ground instances generated via this one and the other one
   * How is the map version modified?
   * x -> 3, x -> 4 => x -> [3,4]
   * but [x->3,y->4], [y -> 5], does not include [x->3,y->5]?
   * How is the formula modified? I suppose F1 | F2
   */
  //def union(other: SubstSet): SubstSet = this

  /** Perhaps this would make more sense inside of the assumption itself? */
  def applySubst(sigma: Subst): SubstSet = {
    //like for clauses- expect that sigma is simple, but it may contain parameters
    if(sigma.isEmpty) return this

    var updatedDeltas = delta
    
    for((x,t) <- sigma.env;
        if delta.isDefinedAt(x)) 
      t match {
        case y: Var if delta.isDefinedAt(y) => {
          //remove x->Dx mapping then add y->(Dx n Dy)
          updatedDeltas = (updatedDeltas-x) + 
	  (y->(delta(x).intersect(delta(y))))
        }
        case y: Var => {
          //y not in fqVars- safe to rename x to y
          val oldDx = updatedDeltas(x)
          updatedDeltas = (updatedDeltas-x)+(y->oldDx)
        }
        
        case d: DomElemInt if (delta(x)(d)) => {
          //remove the delta as the corresponding literal is satisfied
          updatedDeltas-=x
        }
        //t is a DomElemInt not in the delta replace with true lit
        case d: DomElemInt => {
          return EmptySubstSet
        }
        //TODO generalise domains to contain terms?
        case _  => 
          throw new Exception("Not implemented: substitution of FQVar "+
        		      x+" for complex term "+t+" in "+this)
        
      }
    return new SubstSet(updatedDeltas)
  }

}

object EmptySubstSet extends SubstSet(Map())

object SubstSet {
  def apply(delta: Map[Var,Domain[DomElemInt]]) = 
    if(delta.isEmpty) EmptySubstSet
    else new SubstSet(delta)
}

/** Essentially just an FQClause with a special form, note that subs==delta */
case class SimpAssump(alpha: SymConst, defTerm: Term, subs: Map[Var,Domain[DomElemInt]]) extends Expression[SimpAssump] {

  override def toString() = {
    defTerm.toString + " := " + alpha.toString + " <- " +
    (if(!subs.isEmpty)
      subs.map(x => { x._1+" ∈ "+x._2 }).mkString(", ")
     else "[]")
  }

  private def applyToDelta(s: Subst) = subs.foldLeft(Map[Var,Domain[DomElemInt]]())({
    case (fixed,(x,dx)) => 
      if(s.actsOn(x))
	s(x) match {
	  case y: Var if fixed.isDefinedAt(y) => //intersection
	    fixed+(y -> dx.intersect(fixed(y)))
          case y: Var => //simple renaming
	    fixed+(y -> dx)
          case d: DomElemInt if (dx(d)) => //subs with a satisfying Elt--dont need (x->dx) mapping
	    fixed
          case d: DomElemInt => { //subs with a non-satisfying Elt
	    /*updatedClause = (this.asInstanceOf[Clause]).modified(newID=true,
	     lits=Lit.TrueLit::updatedClause.lits)*/
	    throw new Exception(s"the assumption $this has been made redundant via substition")
	    fixed
	  }
          //TODO t is some other term- add the delta as a literal
          case _ if(dx.instantiateOnly) =>
	    throw new Exception("Not implemented: substitution of FQVar "+
        			x+" for complex term "+s(x)+" in "+this+
        			" requires a predicate encoding of set "+dx)
          case _ => {
            /*val lits = dx.asFormula(x)
	     if (lits.length > 1) */
	    throw new Exception("Not implemented: substitution of "+s(x)+" into "+dx)
	    /*else { //domain formula has the form (x=d) for a domain element d
	     updatedClause = (this.asInstanceOf[Clause]).modified(newID=true,
	     lits=updatedClause.lits:::s(lits.head))
	     fixed
	     }*/
          }
	}
		 else if(fixed.isDefinedAt(x))
		   fixed+(x -> dx.intersect(fixed(x)))
		 else
		   fixed+(x->dx)
  })
  
  private val defEqn = Eqn(defTerm,alpha)
 
  val kind: Expression.Kind = defEqn.kind
    
  def matchers(that: SimpAssump, gammas: List[Subst]): List[Subst] = ???

  def mgus(that: SimpAssump): List[Subst] = ???

  val depth: Int = defTerm.depth
  val maxBSFGTerms: Set[beagle.fol.term.Term] = Set(defTerm)
  val minBSFGTerms: Set[beagle.fol.term.Term] = Set(defTerm)
  val symConsts: Set[beagle.fol.term.SymConst] = defTerm.symConsts + alpha

  val operators = defEqn.operators

  val sorts = defTerm.sorts 

  //only vars in the term are relevant, others can be simplified
  val vars = defTerm.vars // ++ subs.keys

  def applySubst(s: Subst) = 
    if(s.actsOn(vars)) SimpAssump(alpha,s(defTerm),applyToDelta(s))
    else this

  //def fresh: SimpAssump = this.applySubst(Term.mkRenaming(vars))

  def asClause = FQClause(subs,Clause(List(Eqn(defTerm,alpha).toLit)))
}
