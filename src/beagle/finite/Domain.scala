package beagle.finite

import beagle._
import datastructures._
import fol._
import term._
import util._
import bgtheory.LIA._
import bgtheory.LIA.IntSort

//TODO- using DomElemInt in matchers triggers warnings on compilation, but this
// is no problem since we assume all $fin(..) predicates are over integers anyway.

/**
 * Could use this instead of Sets of elements. The idea
 * would be to abstract the transformation between set and
 * predicate.
 * TODO- could work better with implicits between set/formulas
 */
abstract class Domain[T] {
  
  /** Whether to use `asFormula` when converting to a proper clausal representation.
   * Effectively this applies to Domains which can only be represented using disjunctive
   * formulas, or for which the set of instances is deemed 'less complicated' than the
   * equivalent formula.
   */
  val instantiateOnly: Boolean
  
  /**
   * Membership relation
   */
  def apply(elt: T): Boolean
  
  /**
   * CNF representation of domain
   * ASSUMES THAT DOMAIN LIT IS NEGATIVE
   */
  def asFormula(x: Var): List[List[Lit]]
  
  val asSet: Set[T]
  
  def partition: (Domain[T],Domain[T])
  
  /**
   * TODO could use a supertype of T, e.g. intersect Int with Rat produces Rat...
   */
  def intersect(other: Domain[T]): Domain[T]
  
  def union(other: Domain[T]): Domain[T]
  
  /** Set difference between two domains */
  def diff(other: Domain[T]): Domain[T]
  
  /** Insert a single element into the domain */
  def +(elt: T): Domain[T]
  
  /* derived: */

  def isEmpty = size==0
  
  /** The `head` element of the `Set` representation of this Domain.
   * Since this is unsorted, the element returned by this is arbitrary.
   */
  def head = asSet.head
  
  def size = asSet.size
  
  /** Subset relation implemented via the set representation.
   * @note this may be inefficient as it requires explicitly constructing the
   * set representations of both `this` and `other`.
   */
  def subsetOf(other: Domain[T]): Boolean = asSet.subsetOf(other.asSet)
  
}

abstract class EmptyDomain[T] extends Domain[T] {
  
  val instantiateOnly = false
  
  override def toString() = "{}"
  
  /**
   * Membership relation
   */
  def apply(elt: T) = false
  
  /**
   * x notin {} <=> T
   */
  def asFormula(x: Var) = List(List(Lit.TrueLit))
  
  val asSet = Set[T]()
  
  def intersect(other: Domain[T]) = this
  
  def union(other: Domain[T]) = other
  
  def diff(other: Domain[T]) = other
  
  def partition = throw new Exception("Partition of empty set- possible loop?")
}

case object EmptyIntDomain extends EmptyDomain[DomElemInt] {
  override def equals(other: Any) = other match {
    //case EmptyIntDomain => true
    case dom: Domain[_] => dom.asSet.isEmpty
    case _ => false
  }
  
  def +(elt: DomElemInt) = SingletonDom(elt.value)
}

/**
 * A domain which represents things internally as sets.
 */
class SetDom(s: Set[DomElemInt]) extends Domain[DomElemInt] {
  
  val instantiateOnly = true
  
  override def equals(other: Any) = other match {
    case other: Domain[DomElemInt] => s==other.asSet
    case _ => false
  }
  
  override def toString = 
    if (s.size < 10) s.mkString("{",",","}")
    else "{"+s.head+",...,"+s.last+"}"
    
  /**
   * Membership relation
   */
  def apply(elt: DomElemInt) = s(elt)
  
  /**
   * CNF representation of domain:
   * x notin {a,b,c,d,...} <=> ~(x=a v x=b v x=c v ... ) <=> x!=a & x!=b & x!=c ...
   * @note with unabstraction this will produce a set of instances of the clause over x
   * hence the reason for the instantiateOnly flag- this lets the prover skip this step.
   */
  def asFormula(x: Var) = s.map(d => List(Lit(false,Eqn(x,d)))).toList
  
  val asSet = s
  
  def intersect(other: Domain[DomElemInt]) = new SetDom(s.intersect(other.asSet))
  
  def union(other: Domain[DomElemInt]) = new SetDom(s.union(other.asSet))
  
  def diff(other: Domain[DomElemInt]) = new SetDom(s.diff(other.asSet))
  
  def +(elt: DomElemInt) = new SetDom(s+elt)
  
  def partition = {
      val (p1,p2) = s.splitAt(this.size/2)
      (new SetDom(p1), new SetDom(p2))
    }
  
}

object SetDom {
  def apply(s: Set[DomElemInt]) = {
    if (s.isEmpty) EmptyIntDomain
    else if (s.size==1) SingletonDom(s.head.value)
    else new SetDom(s)
  }
}

/**
 * Internal representation as range iterators/inequalities.
 * Logically lb<=x<ub this agrees with the scala Range(lb,ub)
 * construct.
 */
class RangeDom(val lb: Int, val ub: Int) extends Domain[DomElemInt] {
  
  val instantiateOnly = false
  
  override def equals(other: Any) = other match {
    case SingletonDom(a) => a==lb && ub==a+1
    case RangeDom(a,b) => a==lb && b==ub
    case other: Domain[DomElemInt] => this.asSet==other.asSet
    case _ => false
  }
  
  override def toString = 
    "["+(
       if((ub-lb) > 5) lb+",...,"+(ub-1)
       else Range(lb,ub).mkString(",")
       )+"]"
    
  /**
   * Membership relation
   */
  def apply(elt: DomElemInt): Boolean = (lb <= elt.value) && (elt.value < ub)
  
  /**
   * CNF representation of domain:
   * x notin [a,...,b-1] <=> ~(a <= x) v ~(x < b)
   */
  def asFormula(x: Var) = List(List(Lit(false,LessEqEqn(DomElemInt(lb),x)),Lit(false,LessEqn(x,DomElemInt(ub)))))
  
  lazy val asSet = Range(lb,ub).map(DomElemInt(_)).toSet
  
  //for these methods, attempt to keep new Domains as ranges for as long as possible
  //TODO could use sets of RangeDoms to represent disjoint domains
  
  def intersect(other: Domain[DomElemInt]) = other match {
    case RangeDom(a,b) => {
      if(ub <= a || b <= lb) EmptyIntDomain
      else {
        val newLB = if(lb < a) a else lb
        val newUB = if (ub < b) ub else b
        RangeDom(newLB,newUB)
      }
    }
    case _ => other.intersect(this) //new SetDom(asSet.intersect(other.asSet))
  }
  
  def union(other: Domain[DomElemInt]) = other match {
    case RangeDom(a,b) if(a < ub && b >= lb)=> {
      if(lb <= a)
        if(b<=ub) this
        else RangeDom(lb,b)
      else //lb > a
        if(b<=ub) RangeDom(a,ub)
        else other
    }
    case RangeDom(a,b) => //all other ranges,e.g a>=ub | b < lb
      //TODO- this is where lists of RangeDoms would be useful
      new SetDom(asSet union other.asSet)
    case s if (s.asSet subsetOf this.asSet) => this
    case _ => other.union(this)
  }
  
  def diff(other: Domain[DomElemInt]) = other match {
    case SingletonDom(e) if(e==lb) => RangeDom(lb+1,ub)
    case SingletonDom(e) if(e==ub-1) => RangeDom(lb,ub-1)
    case RangeDom(a,b) if (a<=lb && b < ub) => RangeDom(b,ub)
    case RangeDom(a,b) if (a > lb && b >= ub) => RangeDom(lb,a)
    case RangeDom(a,b) if(a<=lb && b>=ub) => EmptyIntDomain
    case s if (!s.asSet.intersects(this.asSet)) => this
    case _ => new SetDom(asSet.diff(other.asSet))
  }
  
  def +(elt: DomElemInt) = 
    if(this(elt)) this
    else if(elt.value==ub) RangeDom(lb,ub+1)
    else if(elt.value==lb-1) RangeDom(elt.value,ub)
    else new SetDom(asSet+elt)
  
  def partition =
      (RangeDom(lb,lb+size/2), RangeDom(lb+size/2,ub))
  
  override def head = DomElemInt(lb)
}

case class SingletonDom(i: Int) extends Domain[DomElemInt] {
  private val ide = DomElemInt(i)
  
  val instantiateOnly = true
  
  override def toString = "{"+i+"}"

  override def equals(that: Any): Boolean = that match {
    case SingletonDom(j) => i==j
    case RangeDom(lb,ub) => (lb==i && ub==(i+1))
    case d: Domain[_]    => (d.asInstanceOf[Domain[DomElemInt]](ide) && d.size==1)
    case _               => false
  }
  
  /**
   * Membership relation
   */
  def apply(elt: DomElemInt) = elt==ide
  
  /**
   * CNF representation of domain
   * x notin {i} <=> x!=i
   */
  def asFormula(x: Var) = List(List(Lit(false,Eqn(x,ide))))
  
  val asSet = Set(ide)
  
  def partition = throw new Exception("Attempt to partition singleton domain may lead to loop!")
  
  /**
   * TODO could use a supertype of T, e.g. intersect Int with Rat produces Rat...
   */
  def intersect(other: Domain[DomElemInt]) = 
    if(other(ide)) this
    else EmptyIntDomain
  
  def union(other: Domain[DomElemInt]) = 
    if(other(ide)) other
    else other+ide
  
  def diff(other: Domain[DomElemInt]) = 
    if(other(ide)) EmptyIntDomain
    else this
  
  def +(elt: DomElemInt) = 
    if (elt.value == i+1) new RangeDom(i,i+2)
    else if (elt.value == i-1) new RangeDom(i-1,i+1)
    else if (elt==ide) this
    else new SetDom(Set(ide,elt))
  
  //derived:
  override def isEmpty = false
  
  override def head = ide
  
  override def size = 1
}

object RangeDom {
  
  def apply(lb: Int, ub: Int): Domain[DomElemInt] = {
    if(lb == ub) EmptyIntDomain
    else if (ub==lb+1) new SingletonDom(lb)
    else if (lb < ub) new RangeDom(lb,ub)
    else throw new Exception("Attempt to construct bad domain ["+lb+", ... ,"+ub+"]")
  }
  
  def unapply(other: Domain[DomElemInt]) = other match {
    case r: RangeDom => Some(r.lb,r.ub)
    //case SingletonDom(i) => Some(i,i+1) //TODO- this is bad because can match multiple times
    case _ => None
  }
}

/**
 * A range with a list of exception points which translates to
 * a <= x & x < b & x!=e1 & x!=e2 ....
 * i.e. ~(a <= x) | ~(x < b) | x=e1 | x=e2 | ... 
 * is this really going to be better in the saturation procedure ?
 * ASSUME that lb is never in except
 * @todo could replace RangeDom as RangeExcept(a,b,{})
 */
class RangeExcept(val lb: Int, val ub: Int, val except: Set[Int] = Set()) extends Domain[DomElemInt] {
  assert(!except(lb), 
	 "Lower bound found in exceptions- use companion constructor to get a simplified representation")

  val instantiateOnly = false
  
  override def equals(other: Any) = other match {
    case RangeExcept(a,b,ex) => ((a==lb && b==ub) && ex==except)
    case other: Domain[DomElemInt] => this.asSet==other.asSet
    case _ => false
  }
  
  override def toString = 
    "["+(
       if((ub-lb) > 5) lb+",...,"+(ub-1)
       else Range(lb,ub).mkString(",")
       )+"]"+
       "/{"+
       except.mkString(",")+
       "}"
  
  /**
   * Membership relation
   */
  def apply(elt: DomElemInt) = (lb <= elt.value) && (elt.value < ub) && !except(elt.value) 

  /**
   * CNF representation of domain:
   * x notin [a,...,b-1]\{e1,e2,...} <=>
   * ~(a <= x) v ~(x < b) v x=e1 v x=e2 v ... 
   */
  def asFormula(x: Var) = 
    List(
        List(
            Lit(false,LessEqEqn(DomElemInt(lb),x)),
    			Lit(false,LessEqn(x,DomElemInt(ub)))
    		):::(except.map(e => Lit(true,Eqn(x,DomElemInt(e))))).toList
    	)
  
  lazy val asSet = Range(lb,ub).filterNot(except(_))
				.map(DomElemInt(_))
				.toSet
  
  //for these methods, attempt to keep new Domains as ranges for as long as possible
  
  def intersect(other: Domain[DomElemInt]) = other match {
    /*
    case RangeDom(a,b) => {
      if(ub <= a || b <= lb) EmptyIntDomain
      else {
        val newLB = if(lb < a) a else lb
        val newUB = if (ub < b) ub else b
        //check newLB not among exceptions here
        RangeExcept(newLB,newUB,except.filter(x => newLB<=x && x<newUB))
      }
    }
    */
    case RangeExcept(a,b,ex) => {
      //same as for range but need to include other exceptions
      if(ub <= a || b <= lb) EmptyIntDomain
      else {
        val newLB = if(lb < a) a else lb
        val newUB = if (ub < b) ub else b
        RangeExcept(newLB,newUB,(except union ex))
      }
    }
    case _ => other.intersect(this)
  }
  
  def union(other: Domain[DomElemInt]) = other match {
    case SingletonDom(a) if(a>=lb && a<ub) => 
      if(except(a)) RangeExcept(lb,ub,except-a)
      else this
    case SingletonDom(a) => 
      if(a < lb) RangeExcept(a,ub,except union Range(a+1,lb).toSet)
      else if(a==ub) RangeExcept(lb,ub+1,except)
      //only remaining case: a>ub
      else RangeExcept(lb,a+1,except union Range(ub,a).toSet)
    /*
    case RangeDom(a,b) if(a < ub && b >= lb) => {
      val newLB = if(lb<=a) lb else a
      val newUB = if(b<=ub) ub else b
      //remove any exception points which are contained in other
      RangeExcept(newLB,newUB, except.filterNot(x => a<=x && x<b))
    }
    
    case RangeDom(a,b) if (a>=ub) => //all other ranges,e.g a>=ub | b < lb
      //TODO- how well does this work?
      //new SetDom(asSet union other.asSet)
      RangeExcept(lb,b,except union Range(ub,a).toSet)
    case RangeDom(a,b) if (b<lb) =>
      RangeExcept(a,ub,except union Range(b,lb).toSet)
    */
    case RangeExcept(a,b,ex) if(a < ub && b >= lb) => {
      val newLB = if(lb<=a) lb else a
      val newUB = if(b<=ub) ub else b
      RangeExcept(newLB,newUB,
    		  except.filterNot(i => other(DomElemInt(i))) union ex.filterNot(i => this(DomElemInt(i))))
    }
    case RangeExcept(a,b,ex) if (a>=ub) => //all other ranges,e.g a>=ub | b < lb
      //TODO- how well does this work?
      RangeExcept(lb,b,except union Range(ub,a).toSet union ex)
    case RangeExcept(a,b,ex) if (b<lb) =>
      RangeExcept(a,ub,except union Range(b,lb).toSet union ex)
    case s if (s.asSet subsetOf this.asSet) => this
    case _ => other.union(this)
  }
  
  def diff(other: Domain[DomElemInt]) = other match {
    case SingletonDom(e) => RangeExcept(lb,ub,except+e)
    /*
    case RangeDom(a,b) if (a<=lb && b < ub) => RangeExcept(b,ub,except)
    case RangeDom(a,b) if (a > lb && b >= ub) => RangeExcept(lb,a, except)
    case RangeDom(a,b) if(a<=lb && b>=ub) => EmptyIntDomain
    */
    case RangeExcept(a,b,ex) if(a<=lb && b>=ub && ex.subsetOf(except)) => EmptyIntDomain
    case RangeExcept(a,b,ex) if (a<=lb && b < ub) => RangeExcept(b,ub,except union ex)
    case RangeExcept(a,b,ex) if (a > lb && b >= ub) => RangeExcept(lb,a, except union ex)
    case s if (!s.asSet.intersects(this.asSet)) => this
    case _ => RangeExcept(lb,ub,except union other.asSet.map(_.value))//new SetDom(asSet.diff(other.asSet))
  }
  
  def +(elt: DomElemInt) = 
    if(this(elt)) this
    else if(elt.value==ub) RangeDom(lb,ub+1)
    else if(elt.value==lb-1) RangeDom(elt.value,ub)
    else if(except.contains(elt.value)) new RangeExcept(lb,ub,except-elt.value)
    //can you add the gap as exception points? e.g. [1,2,3]+7 => [1,...,7]/{4,5,6}
    else new SetDom(asSet+elt)
  
  def partition =
      (RangeExcept(lb,lb+size/2,except), RangeExcept(lb+size/2,ub,except))
  
  override def head = DomElemInt(lb)
  
  override def size = (ub-lb)-except.size
  
}

object RangeExcept {
  /** Attempt to find a more compact representation of the intended Domain */
  def apply(lb: Int, ub: Int, ex: Set[Int]): Domain[DomElemInt] = {
    //filter the exceptions to be in between lb & ub
    val except = ex.filter(x => lb<=x && x<ub)
    val size = ub-lb
    
    //where except is empty- identical to RangeDom
    if(except.isEmpty) return RangeDom(lb,ub)

    if(size==0) return EmptyIntDomain
      
    if ((size-except.size) == 1) 
      return new SingletonDom(Range(lb,ub).filterNot(except(_)).head)
    else if (except.size > size/2)
      //produce a set as there are more exceptions than values here
      return new SetDom(Range(lb,ub).filterNot(except(_)).map(DomElemInt(_)).toSet)
    else {
      var (newLB, newUB) = (lb,ub)
      //remove exception points by adjusting bounds if possible
      if (except(lb)) {
        val res = Range(lb,ub).find(!except(_))
        if(res==None) return EmptyIntDomain
        else newLB = res.get
      }
      if (except(ub-1)) {
        val res = Range(lb,ub).reverse.find(!except(_))
        if(res==None) return EmptyIntDomain
        else newUB = res.get+1
      }
      
      if(newLB!=lb || newUB!=ub) 
	return this.apply(newLB,newUB,except)
      else 
	return new RangeExcept(lb,ub,except)
    }
    /*}else
      new RangeExcept(lb,ub,except)*/
  }
  
  def unapply(other: Domain[DomElemInt]) = other match {
    case r: RangeExcept => Some(r.lb,r.ub,r.except)
    case r: RangeDom => Some(r.lb,r.ub,Set[Int]())
    //case SingletonDom(i) => Some(i,i+1)
    case _ => None
  }
  
  //TODO- can you convert general sets to this?
  //e.g.
  /*
  val lb = s.min
  val ub = s.max
  val r = Range(lb,ub)
  val ex = r.filterNot(s(_))
  RangeExcept(lb,ub,ex)
   */
  //perhaps you could replace the setDom constructor with this to experiment?
}
