package beagle.finite

import beagle._
import beagle.main._ //includes DerivationResult type
import datastructures._
import finite._
import fol._
import term._
import bgtheory.LIA._
import calculus._
import util._

/**
 * Contains find and checkSAT implementations
 */
object FiniteSearch {
  
  /**
   * Get a list of all partitions of a list of variables.
   */
  private def partitionsOf(x: List[Var]) = {
    val indices = Range(0, x.length).toList
    for {
      n <- 0 to x.length
      i <- indices.combinations(n)
    } yield {
      val y = i map { x(_) }
      val z = x filterNot { y contains _ }
      (y,z)
    }
  }

  /**
   * Add arbitrary finite quantifiers in all clauses which can be made
   * sufficiently complete.
   * @return None if the finite approach is not applicable.
   */
  def addFD(cs: List[Clause], range: (Int,Int)): Option[List[FQClause]] = {
    //note that so far this only works if
    //1. there are non-ground BSFG terms
    //2. all of the variables under the BSFG terms are int sorted
    //otherwise there are all sorts of errors and it is probably not correct to assume we can continue

    //these are the variables which occur below BSFG terms...
    val subVars = cs.flatMap(_.maxBSFGTerms).flatMap(_.vars)
    if(subVars.isEmpty || subVars.exists(_.sort.thyKind != BG))
      return None

    val newRange = new RangeExcept(range._1, range._2)

    //otherwise
    Option(
      for(c <- cs) yield {
	var fqMap = Map[Var, Domain[DomElemInt]]()

	for {
	  t <- c.maxBSFGTerms
	  x <- t.vars
	  if(!fqMap.isDefinedAt(x)) 
	} {
	  //add new quantifier/domain over range
	  fqMap += (x -> newRange)
	}

	FQClause(fqMap, c)
      })
  }
  
  /**
   * Remove all instances of Finite Range predicates from a clause set
   * and convert the 
   */
  def convertFDPreds(cs: List[Clause]): Set[FQClause] = {
    //check for bad usage:
    cs.map(c => {
      var newDeltas = Map[Var, Domain[DomElemInt]]()
      val newLits = c.lits.filter(l => l match {
          case Lit(false, FDEqn(x: Var, lower: DomElemInt, upper: DomElemInt)) => {
            //using ranges now:
            //val delta: Domain[DomElemInt] = RangeDom(lower.value,upper.value)
            val delta: Domain[DomElemInt] = RangeExcept(lower.value, upper.value, Set())
            newDeltas += (x -> delta)
            false
          }
          //TODO can you use these together to define non-contiguous finite ranges?
          // e.g. $fin(x,0,10) & ~$fin(x,3,5)
          case Lit(true, FDEqn(x: Var, lower, upper)) => 
            throw new Exception("Error processing "+l+"\n-Negative FD literals not implemented.")
          case Lit(_, FDEqn(_,_,_)) =>
            throw new Exception("Error processing "+l+"\n-Terms do not conform to expected usage pattern.")
          case _ => true
        })
      FQClause(newDeltas, Clause(newLits, c.idxRelevant, 0))
    }).toSet
  }
  
  /**
   * Adds new definitions for the BSFG terms in c at exception points given in pi.</br>
   * Assumes that the value sets of pi are subsets of the delta sets in c.
   * Assumes that when not defined, the value of pi is the empty set.
   *</br>
   * @param c The Finitely Quantified clause to rewrite.
   * @param pi A map from variables to domains over which exception points are to
   * be added.
   *</br>
   * @return A set of clauses equivalent to the original c but rewritten with the
   * assumptions that the BSFG terms of c are constant everywhere but at the given
   * exception points.
   */
  def finiteTransform(c: FQClause, pi: Map[Var,Domain[DomElemInt]]): Set[FQClause] = {
    if(c.fqVars.isEmpty) return Set(c)

    //Stats
    FDStats.updateExPoints(pi)
    FDStats.timeFD.start()
    //End stats
    
    var clsC = Set.empty[FQClause]
    var defC = Set.empty[FQClause]
    
    //diffPiX is x -> delX\piX for all x in X
    val diffPiX = c.delta.transform((y,d) => {
      if (pi.isDefinedAt(y)) d.diff(pi(y))
      else d
    })
    
    //for every partition {y1...yk} u {z1...zl} of c.fqVars:
    for((ys,zs) <- partitionsOf(c.fqVars.toList);
        diffPi = diffPiX.filterKeys(ys.contains(_));
        //for all subs [z1 -> d1, ... zl->dl] where di \in Pi_xi
        piZ = zs.map(x => (x->pi.getOrElse(x,EmptyIntDomain))).toMap;
        gamma <- instances(piZ)) {

      var e = gamma(c.unabstrAggressive)
      
      while(!e.minBSFGTerms.isEmpty) {
	val t = e.minBSFGTerms.head
	if(!(t.vars subsetOf c.fqVars)) 
	  return Set(c) //not an FQClause, nothing we can do, or skip to next BSFG term?
	else {
          //t=alpha
          val alphaDef = makeExceptionPoint(gamma(t).asInstanceOf[FunTerm])
          
          // add to DefC the clause t\gamma = alpha <- y1 \Pi_y1 ...
          val newDef = FQClause(diffPi,Clause(List(Lit(true,alphaDef)),Set[Int](),0))
          defC+=newDef
          
          // set E:=E[alpha]
          e = replaceTerm(e, alphaDef)
	}
      }

      //IMPORTANT! hard-coded simplification rule for prefixes here
      //i.e. 
      val eCore = gamma(e) 
      clsC+=FQClause(diffPi.filterKeys(eCore.vars.contains(_)),eCore)
    }
    
    FDStats.timeFD.stop()
    
    return (clsC++defC).flatMap(_.simplifyCheap)
  }

  /* Entry points for GMF algorithm */

  /** Run the GMF algorithm with a default range for each integer sorted variable occurring below
   * a BSFG operator.
   */
  def runWithDefaultRanges(in: List[Clause], timeout: Double, range: (Int,Int) = (-1000,1000)): DerivationResult = {
    reporter.debug("Running Finite Beagle using default range: " + range)

    addFD(in,range) map { cls =>
      runInner(cls.toSet,timeout) 
    } getOrElse {
      reporter.log("Could not apply finite state transform.")
      Unknown(new calculus.State(in, List()))  //if we cannot sensibly apply the FD transform
    }
  }

  /** checkSAT version which conforms to the `run` method in main */
  def run(in: List[Clause], timeout: Double): DerivationResult =
    runInner(convertFDPreds(in.map(_.fresh)), timeout)

  private def runInner(in: Set[FQClause], timeout: Double): DerivationResult = {

    //val n = convertFDPreds(in.map(_.fresh))
    val n = in
    //println("After conversion of FD predicates:")
    //printClauseSet(n.toList)
    
    var piX: Map[Var,Domain[DomElemInt]] = 
      n.foldLeft(Set[Var]())(_ ++ _.fqVars)
    	.map((_->EmptyIntDomain))
    	.toMap
    
    var lastMEmpty = Set[Clause]()
    
    var timeRemaining=timeout
    
    FDStats.FDLoopTimer.start()
    while(true) {
      FDStats.iterations+=1

      //println("Finite: timeRemaining="+timeRemaining)
      proverTimeRemaining = timeRemaining

      val m = new FQClauseSet(n.flatMap(finiteTransform(_,piX)))
      println("FD transform with pi="+piX)
      println("M is:")
      m.cs foreach {println _}
      println()
      
      //m[emptyX] is just m with all FQ clauses filtered
      val m_empty = m.nonFQCl
      
      //tests if both model is consistent & clause set is satisfied by model
      println("Testing satisfiability of full model")
      println("------------------------------------")
      isSatDR(m,timeRemaining) match {
	case Unsat => {
          println("Testing for unsatisfiability of non-finite components")
          println("------------------------------------")
          m_empty foreach {println _}
          if (lastMEmpty != m_empty &&
	     asDerivationRes(m_empty) == Unsat) {
	      println("M[empty] unsat")
	      return Unsat
	    } else { //loop
              println("M[empty] sat\n")
	      println()
	      println("Refining model using find")
              println("------------------------------------")
	      try { //Find can also throw timeout
		Find.find(m) match {
		  case Some((x,d)) => {
		    println("New exception point "+d+" for "+x)
		    piX += (x -> (piX(x) + d))
		  }
		  case None => {
		    println("Model not repairable by find()")
		    return Unsat
		  }
		}
	      } catch {
		case TIMEOUT => return Timeout
	      }
	    }
	}
	case s => return s //timeout or sat
      }
      //end of while loop
      timeRemaining-=FDStats.FDLoopTimer.sinceStarted()
      if(timeRemaining <= 0) return Timeout
    }
    return Unsat
  }

  //set from inside run
  private var proverTimeRemaining = util.flags.timeout.value.toDouble

  /**
   * Note that the saturation loop expects clauses are abstracted,
   * this is done here.
   * BUG- timeout does not work correctly when calling state.derive
   * @throws TIMEOUT
   */
  def proofSearch(clauses: Set[_<:Clause],timeout: Double=proverTimeRemaining) = {
    FDStats.proverCalls+=1

    val abstrClauses = clauses.map(_.weakAbstract).toList

    import scala.collection.mutable.HashMap
    //reset defineMap- TODO a nasty hack!
    //Term.defineMap=HashMap.empty[String,String]

    val state = (new State(abstrClauses, List()))
    state.derive(timeout)
  }
  
  /**
   * Abstract clauses, call prover and report result as a DerivationResult,
   * also catches timeout.
   */
  def asDerivationRes(clauses: Set[_<:Clause],timeout: Double=proverTimeRemaining): DerivationResult = {
    try {
      proofSearch(clauses, timeout) match {
	case Left(empty) => {
	  println("-----\nUNSAT\n")
          return Unsat
	}case Right(state) => {
	  println("-----\nSAT\n")
	  return Sat(state)
	}
      }
    }catch {
      case TIMEOUT => return Timeout //as a derivation result
    }
  }

  /**
   * Convert FQ clauses to regular clauses, run prover and return
   * result as a DerivationResult for use in main procedure.
   * @param t time limit for this phase.
   */
  def isSatDR(clauses: FQClauseSet, t: Double): DerivationResult = {
    reporter.log("Sat. checking:\n" + clauses.sortedCS.mkString("\n"))

    val gndClauseSet = clauses.cs.flatMap(_.toLogicalClauses).toSet
    
    FDStats.updateInstSize(gndClauseSet.size)
    
    asDerivationRes(gndClauseSet, t)
  }

  /** @return true if Sat */
  def isSat(clauses: FQClauseSet): Boolean =
    isSatDR(clauses,proverTimeRemaining) match {
      case Sat(_) => true
      case _ => false
    }
  
  /**
   * Wrap the default saturation loop 'proofSearch' in order to 
   * translate the FQ prefixes to logical form and remove the
   * DerivationResult return type.
   * @param printModel whether to print the final clause set if satisfiable.
   */
  /*def isSat(clauses: FQClauseSet,printModel: Boolean = false) = {
    FDStats.proverCalls+=1
    //FDStats.timeProver.start()
    
    println("Sat. checking:")
    //clauses.cs foreach {println _}
    
    val gndClauseSet = clauses.cs.flatMap(_.toLogicalClauses).toSet
    gndClauseSet foreach {println _}
    
    FDStats.updateInstSize(gndClauseSet.size)
    
    val res = proofSearch(gndClauseSet) match {
      case Sat(s) => {
        if(printModel) {
          println("Model: ")
          //s.old.clauses foreach { println _ }
          displayModel(s.old.clauses.toList)
        }
        true
      }
      case _ => false
    }
    
    //FDStats.timeProver.stop()
    res
  }*/
  
  /**
   * Rewrite using a new definition.
   * Only replace terms which are identical- one definition can apply in many
   * places but not to variants of the defined term.
   * TODO- this is possible, but really should be a superposition step.
   */
  def replaceTerm(cl: Clause, e: Eqn): Clause = {
    assume(e.rhs.isSymConst, "replaceTerm should only be called with definitions")

    def replaceInT(t: Term): Term =
      t match {
        case PFunTerm(op, args) ⇒ {
          // First demodulate the arguments
          val h = PFunTerm(op, args map { replaceInT(_) })
          // See if h can be demodulated at the toplevel
          // this is really an xor since we only define min BSFG terms
          if (e.lhs == h) return e.rhs
          else return h
        }
        case _ ⇒ return t // All other cases, i.e. Variables
      }

    def replaceInL(l: Lit): Lit =
      Lit(l.isPositive,
        Eqn(replaceInT(l.eqn.lhs), replaceInT(l.eqn.rhs)))

    val h = cl.lits map { replaceInL(_) }
    Clause(h, cl.idxRelevant, cl.age)
  }
  
  import bgtheory.LIA._

  /**
   * True if x is finitely quantified in cl
   * TODO- return the FQ representation
   */
  def isFQVar(x: Var, lits: List[Lit]): Boolean = {
    assume(x.isBG && lits.vars(x))

    val litsToCheck = lits.filter(_.vars(x))

    var lowerBounds = List[Int]()
    var upperBounds = List[Int]()
    var excluded = List[Int]()
    var possible = List[Int]()
    
    for(l <- litsToCheck) {
      l match {
	case Lit(false,LessEqEqn(DomElemInt(a),y)) if(y==x) => lowerBounds::=a
	case Lit(false,LessEqn(y,DomElemInt(a))) if(y==x) => upperBounds::=a
	case Lit(false,Eqn(DomElemInt(a),y)) if(y==x) => possible::=a
	case Lit(false,Eqn(y,DomElemInt(a))) if(y==x) => possible::=a
	case Lit(true,Eqn(DomElemInt(a),y)) if(y==x) => excluded::=a
	case Lit(true,Eqn(y,DomElemInt(a))) if(y==x) => excluded::=a
	case _ => ()
      }
    }
    
    ((!lowerBounds.isEmpty && !upperBounds.isEmpty) || !possible.isEmpty )
  }

  /**
   * Remove superfluous clauses & constants and sort remaining clauses
   * to help with debugging.
   */
  private def displayModel(cs: List[Clause]) {

    //if cl contains only 1 BSFG term &
    // isDefinition for that term...
    // We need to analyse the equation e in this unit clause and see if it defines t 
    // val e = unabstr(0).eqn
    // e.rhs.isBG && e.lhs >>~ t // E.g. f(x) = x+x is a definition for f(1)

    //so really we want to check- isDefinition for a BGConst too..
    //can we define as anything that has a definition symbol?
    //+usual definitions, e.g. f(5)=5

    //complex definitions e.g. f(x)=a <- delta(x)?

    //val defineConsts: Set[Term] = defineMap.values.toSet

    val defRegex = DefParam.regex

    def isFDDef(c: Clause) = {
      if(c.unabstrAggressive.isUnitClause && c.unabstrAggressive(0).isPositive) {
	c.unabstrAggressive(0).eqn match {
	  case Eqn(BGConst(_),t) if (t.isGround) => true
	  case Eqn(t,BGConst(_)) if (t.isGround) => true
	  case _ => c.isDefinition
	}
      }else false
    }

    /**
     * Whether the clause provides a FQ variable definition for a BSFG op.
     */
    def isDefClause(c: Clause): Boolean = {
      //c must contain one BSFG operator
      if(c.minBSFGTerms.size!=1) 
	return false
      
      //it must occur in one literal- the definition literal
      //it must have the form f(X)=a
      val (defLit,fqLits) = c.lits.partition(!_.isPureBG)

      if(defLit.size!=1 || !defLit.head.isPositive)
	return false

      //all variables in this literal must be FQ- found by checking the other lits
      if(!defLit.head.bgVars.forall(isFQVar(_,fqLits))) 
	return false

      val Lit(_,Eqn(PFunTerm(f,_),a)) = defLit.head
      
      //println("Defines "+f+" as "+a)

      return true
    }

    var (defs, clauses) = cs.partition(c => (isFDDef(c) || isDefClause(c)))
    
    //rewrite with definitions
    clauses = clauses.flatMap(_.reduce(ListClauseSet(defs.toSeq:_*)))
		//.flatMap(_.simplifyTrivial)

    //TODO- need QE or something to remove redundant BG literals
    //val res = bgtheory.LIA.LIASolverClauses.QE(clauses.filter(_.isPureBG).map(_.toFormula).toAnd)
    
    //remove redundant definitions
    //those which do not appear in the input signature
    //those which do not appear in the final clause set
    defs=defs.filter(d => !d.operators.intersect(Sigma.operators).isEmpty || 
			   clauses.exists(c => !d.operators.intersect(c.operators).isEmpty))

    //sort by what definition applies to
    
    println("Definitions:")
    println("------------")
    defs foreach {println _ }
    println()
    
    println("Clauses:")
    println("------------")
    clauses foreach { println _ }
    println()
  }
  
}

/**
 * A defined predicate used to specify finite ranges of integers in
 * TPTP input files. It is removed in preprocessing and replaced by FQClauses.
 * The expected use is $fin(X,lower,upper) and this translates to (0<=X & X<upper).
 * Added to full signature as a FG operator so not simplified by BG theory
 */
object FinitePred extends Operator(FG,"$fin",Arity3((IntSort,IntSort,IntSort),Signature.OSort))

/**
 * Unwraps terms of the sort $fin(x,l,u)=t
 */
object FDEqn {
  def unapply(e: Eqn) = 
    e.toAtom match {
      case Atom("$fin",List(x,l,u)) => Some(x,l,u)
      case _ => None
    }
}

//TODO append FDStats to normal stats
object FDStats {
  import util._

  var proverCalls = 0
  var iterations = 0
  var largestInstSize = 0
  var avgExPoints = 0
  
  /** Times the outer loop of checkSAT */
  val FDLoopTimer = new Timer("Time spent in FD Main loop")
  
  //var timeFD: Long = 0
  val timeFD = new Timer("Time spent on FD transform")
  //var timeFind: Long = 0
  val timeFind = new Timer("Time in Find")
  //var timeProver: Long = 0
  //TODO- I think this is the same as MainLoop timer?
  //val timeProver = new Timer("Time in saturation loop")
  
  def show() {
    //TODO-print the new timer stats
    println("Finite Beagle Stats")
    println("----------------------")
    println("Prover Calls                     : " + proverCalls)
    println("Iterations                       : " + iterations)
    println("Largest Instance Checked         : " + largestInstSize)
    println("Average Exception Points per Var : " + avgExPoints)
    println()/*
    println("Timing")
    println("----------------------")
    println("Total Time                       : " + totalTime + "ms")
    println("Time spent on FD transform       : " + timeFD + "ms")
    println("Time in Find                     : " + timeFind + "ms")
    println("Time in saturation loop          : " + timeProver + "ms")
    println()*/
    
  }
  
  def updateExPoints(pi: Map[Var,Domain[_]]) {
    if(!pi.isEmpty)
      avgExPoints = (pi.values.map(_.size).sum) / pi.size 
  }
  
  def updateInstSize(size: Int) {
    if (size > largestInstSize) largestInstSize=size
  }

}
