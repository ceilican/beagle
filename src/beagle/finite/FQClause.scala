package beagle.finite

import beagle._
import datastructures._
import fol._
import term._
import util._
import bgtheory.LIA._
import calculus.simplification
import datastructures.PMI._

//TODO:
//+ protect "definition" clauses from having define apply
// a definition in this case is f(X)=p <- ...
// so as a clause it is f(X)=p v C where C is pureBG
// cf usual isDefinition: finds the minBSFG term- f(X)
// calls isDefinitionFor(f(X))- requires that it is a unit clause

//don't want to weaken Clause.isDefinition
//perhaps we could make a special class of parameters- a definition contains 1 BSFG term and 1 def param

//+ ensure define (or equivalent) applies to ground clauses
// perhaps-
// either ensure that cl.isDefinition is true for these or
// set haveGBT and ensure these have an age > 0

/** Constructs and recognises definition parameters */ 
object DefParam {
  val regex = """α_[0-9]+"""
  val prefix = "α_"

  def apply(idx: Int) = 
    new SymConst(Operator(BG, prefix+idx, Arity0(IntSort)))

  def isDefParam(other: Term) = other match {
    case SymConst(Operator(BG, n, Arity0(IntSort))) if (n matches regex) => true
    case _ => false
  }
}

/**
 * A Finitely Quantified clause has structure D ∨ Δ_x1 ∨ Δ_x2 ... Δ_xn,
 * where D does not contain domain predicates, xᵢ≠xⱼ for all i,j
 * and every variable occurring below a free BG sorted op in D is
 * among x₁...xₙ.
 * @note that we do not check that constructed `FQClause`s actually are properly
 * finitely quantified.
 */
class FQClause(
  id: Int,
  lits: List[Lit],
  idxRelevant: Set[Int],
  age: Int,
  val delta: Map[Var,Domain[DomElemInt]]) extends Clause(lits,idxRelevant,age){
  
  /** Two FQClauses are equal iff both the clause part and the finite quantifier ranges are the same.
   * FQClauses can also match regular clauses if they have empty quantifiers.
   */
  override def equals(that: Any) = that match {
    case cc:FQClause => (this.clause==cc.clause && this.delta==cc.delta)
    case c: Clause => (this.delta.isEmpty && this.clause==c)
    case _ => false
  }
  //ensure that all clause operations that return a clause have constraint preserving versions

  /** Notice that this just returns the clause part minus the finite quantifiers/domain predicates.
   * There is no translation of these to a logical form.
   */
  lazy val clause = Clause(lits,idxRelevant,age)
  
  lazy val hasFQVars = !fqVars.isEmpty

  /**
   * The finitely quantified variables that this clause expects.
   * Not all of these necessarily occur in the clause, but the variables in
   * the clause must always be a subset of these.
   */
  lazy val fqVars = delta.keys.toSet
  
  //TODO- is this used?
  private def setID(newID:Int) =
    new FQClause(newID,this.lits,this.idxRelevant,this.age,this.delta)

  /** Lift to FQClause- must preserve ID */
  override def fresh(): FQClause = {
    val r = Term.mkRenaming(vars)
    this.applySubst(r).setID(this.id)
  }

  /**
   * Note that this is essentially the same as done for clauses only
   * now we need to apply the substitution to the finite domain predicates too.
   * Since we don't have access to the substitution when calling the parent clause,
   * we need to duplicate it here.
   */
/*
  private def unabstract(level: Int): FQClause = {
    //println("***Using unabstr")
    //println("before: "+this)
    //need to transform deltas to logical form to accumulate changes
    //but then transform back to set form?
    var CRes = List.empty[Lit]
    var sigma = Subst.empty // The substitution we are building from the disequations of the above form
    var touched = false
    for (l ← lits)
      // Invariant: sigma has been applied to CRes   
      l match { 
	case Lit(true, _) => CRes ::= sigma(l) // nothing to remove; maintain the invariant
	case Lit(false,e) => {
          val esigma = sigma(e)
          esigma.asVarTermPair match {
            //TODO perhaps some clever instantiation of finite sort.
            case Some((x, t)) if (
               ((level == 0 && t.isDomElem) ||
                (level == 1 && t.isBG) ||
                (level == 2)) && !x.occursIn(t)) ⇒ {
              // Cannot unabstract FG terms. E.g.
              // Cannot P(x) \/ x /= a unabstract to P(a)
              // The former clause could have non-redundant instances that are smaller than P(a)
              // e.g. P(c) \/ c /= a which is smaller than P(a) with the appropriate precedence,
              // so that P(a) cannot be used to entail P(c) \/ c /= a . 
              val delta = Subst(x -> t)
              sigma ++= delta
              CRes = delta(CRes) // maintain invariant
              touched = true
            }
            case _ ⇒
            // esigma is not a variable-term pair or occurs-check prevents building the substitution
            CRes ::= Lit(false, esigma) // nothing to remove; maintain the invariant
          }
	}
      }
    if(touched) {
      val newC: FQClause = new FQClause(id,CRes.reverse, idxRelevant, age, delta)
      val res = newC.applySubst(sigma)
      //println("after "+res)
      res
    }else this
  }
  */

  override def modified(lits: List[Lit] = this.lits, 
			idxRelevant: Set[Int] = this.idxRelevant, 
			age: Int = this.age,
			parents: List[Int] = Nil): FQClause = 
    //TODO- ignores parents
    new FQClause(Clause.idCtr.next, lits, idxRelevant, age, this.delta)
    
  /**
   * If a substitution identifies two FQ variables then their domains should be
   * merged.
   * @throws Exception
   */
  override def applySubst(s: Subst): FQClause = {
    //var updatedDeltas = delta
    var updatedClause = clause
    
    //first test if the substitutions is safe wrt the FQVars
    /*for((x,t) <- s.env;
        if fqVars(x)) 
      t match {
        case y: Var if fqVars(y) => {
          //remove x->Dx mapping then add y->(Dx n Dy)
          updatedDeltas = (updatedDeltas-x) + 
    			    (y->(delta(x).intersect(delta(y))))
        }
        case y: Var => {
          //y not in fqVars- safe to rename x to y
          val oldDx = updatedDeltas(x)
          updatedDeltas = (updatedDeltas-x)+(y->oldDx)
        }
        
        case d: DomElemInt if (delta(x)(d)) => {
          //remove the delta as the corresponding literal is satisfied
          updatedDeltas-=x
        }
        //t is a DomElemInt not in the delta replace with true lit
        case d: DomElemInt => {
          //just add T literal to the clause
          updatedClause = Clause(Lit.TrueLit::updatedClause.lits,
        		  			clause.idxRelevant,clause.age)
        }
        //TODO t is some other term- add the delta as a literal
        case _ if(delta(x).instantiateOnly) => 
          throw new Exception("Not implemented: substitution of FQVar "+
        					x+" for complex term "+t+" in "+this+
        					" requires a predicate encoding of set "+delta(x))
        case _ => {
          val lits = (delta(x).asFormula(x))
          if (lits.length > 1) 
    	    throw new Exception("Not implemented: substitution of "+t+" into "+delta(x))
          else {
            updatedClause = Clause(updatedClause.lits:::s(lits.head),
        		  			clause.idxRelevant,clause.age)
            updatedDeltas-x
          }
        }
    }*/

    val updatedDeltas = delta.foldLeft(Map[Var,Domain[DomElemInt]]())({
      case (fixed,(x,dx)) => 
	if(s.actsOn(x))
	  s(x) match {
	    case y: Var if fixed.isDefinedAt(y) => //intersection
	      fixed+(y -> dx.intersect(fixed(y)))
            case y: Var => //simple renaming
	      fixed+(y -> dx)
            case d: DomElemInt if (dx(d)) => //subs with a satisfying Elt--dont need (x->dx) mapping
	      fixed
            case d: DomElemInt => { //subs with a non-satisfying Elt
	      updatedClause = (this.asInstanceOf[Clause]).modified(lits = Lit.TrueLit :: updatedClause.lits)
	      fixed
	    }
            //TODO t is some other term- add the delta as a literal
            case _ if(dx.instantiateOnly) =>
	      throw new Exception("Not implemented: substitution of FQVar "+
        					x+" for complex term "+s(x)+" in "+this+
        					" requires a predicate encoding of set "+dx)
            case _ => {
              val lits = dx.asFormula(x)
	      if (lits.length > 1) 
		throw new Exception("Not implemented: substitution of "+s(x)+" into "+dx)
	      else { //domain formula has the form (x=d) for a domain element d
		updatedClause = (this.asInstanceOf[Clause]).modified(lits = updatedClause.lits ::: s(lits.head))
		fixed
	      }
            }
	  }
	else if(fixed.isDefinedAt(x))
	  fixed+(x -> dx.intersect(fixed(x)))
	else
	  fixed+(x->dx)
    })

    //otherwise just replace as usual in c, all deltas updated
   return FQClause(updatedDeltas,s(updatedClause))
  }
  
  /**
   * What to do when new ranges are substituted for FQ variables.
   * Note that this is distinct from what happens with a FD transform-
   * no new clauses are added.
   */
  def apply(piMap: Map[Var,Domain[DomElemInt]]): FQClause = {
    var newDelta: Map[Var,Domain[DomElemInt]] = delta
    var newClause = clause
    
    for((x,pi) <- piMap;
        if(newDelta.isDefinedAt(x))) {
      if(pi.asSet.subsetOf(newDelta(x).asSet)) {
        if(pi.size==1){ 
          newClause=Subst(x->pi.asSet.head)(clause)
          newDelta-=x
        }else if(pi.size==0) {
            return TrueFQClause
        }else newDelta = newDelta+(x->pi)
      }else //pi_x not a subset of delta_x
        throw new Exception("Attempted to violate FQ Clause constraints:"+
       		  				" substituting"+(x->pi)+" for "+(x->delta(x)))
      //if x is not defined, ignore
    }
    return FQClause(newDelta,newClause)
  }
  
  /**
   * Clause representation which includes the domain predicates as part of
   * the clause (i.e. literals not sets).
   * Can either instantiate or add predicates.
   */
  def toLogicalClauses: List[Clause] = {
    
    val (toInstantiate, toLits) = this.delta.partition(_._2.instantiateOnly)
    val newLits = 
      (for((x,dom) <- toLits;
          l = dom.asFormula(x)) yield {
        //l is the conjunction of disjunctions of literals which represents this domain
        //there should only be 1 of these otherwise we should instantiate
        if(l.length>1) 
          throw new Exception("A domain predicate "+x+"produced an unexpected number of clauses: "+l)
        
        l.head
      }).flatten
      .toList
    
    val newCl = Clause(lits:::newLits,idxRelevant,age)
    
    if(!toInstantiate.isEmpty)
      return instances(toInstantiate).map(_(newCl))
    else
      return List(newCl)
  }
  
  /**
   * Also removes clauses with trivial constraints
   */
  override def simplifyCheap: Iterable[FQClause] = {
    /**
     * Local modification of simplify
     * No BGSimp for FQClauses
     * Trivial simplification: removal of trivial negative equations,
     * tautology detection
     */
    def simpTrivialNoBG(cl: Clause): List[Clause] = {

      var resCl = cl

      //isTrivialNeg
      //removes a!=a literals
      resCl = Clause(resCl.lits.toListSet filterNot { _.isTrivialNeg }, resCl.idxRelevant, resCl.age)

      //isTautology
      //includes: where there is a literal: a=a, d1!=d2,
      //or pairs of complementary literals
      //--ok
      if (resCl.isTautology) List.empty
      else List(resCl.abstr)
    }
    
    //if no finite quantifiers, treat as a normal clause
    //otherwise, do not use BGSimp
    val simplified = 
      if(delta.isEmpty) simplification.simplifyCheap(clause)
      else simpTrivialNoBG(this)
    
      for(c <- simplified) yield {
	    FQClause(delta.filterKeys(c.vars(_)),c)
	  }
  }
  
  override def toString() = {
    (if(!delta.isEmpty)
      delta.map(x => { x._1+" ∈ "+x._2 }).mkString(", ") + " -> "
  	else "") +
  	(if (clause.isEmpty) 
  	  printer.emptyClauseSym 
	else
	  clause.lits.mkString(" "+printer.orSym+" "))
  }
  
  /** Lifted to `FQClause` by ignoring deltas */
  override def replaceAt(pos: Pos, t: Term): FQClause = 
    PMI.replaceAtList(lits, pos, t) map {
      new FQClause(Clause.idCtr.next(), _ , idxRelevant, age, delta)
    } getOrElse {
      this
    }
  
  /**
   * True if the given clause is a ground instance of this clause+constraint
   * Useful?
   */
  def gndInstOf(c: Clause): Boolean = {
    throw new Error("Not implemented")
  }

}

object FQClause {
  def apply(deltas: Map[Var, Domain[DomElemInt]], cl: Clause) =
    new FQClause(cl.id, cl.lits,cl.idxRelevant,cl.age, deltas)
  
  def unapply(c: Clause) = c match {
    case f: FQClause => Some(f.delta,f.clause)
    case _ => None
  }

  def isFQDefinition(c: Clause) = {
    // a definition in this case is f(X)=p <- ...
    // so as a Clause it is f(X)=p v C where C is pureBG
    //one impureBG lit f(X)=p should have that form
    //rest is pureBG
    //and vars subset of vars in head
    if(c.iNonPureBGLits.size != 1) false
    else {
      val head = c(c.iNonPureBGLits.head)
      val rest = c.iPureBGLits.map(c(_))
      val res = DefParam.isDefParam(head.eqn.rhs) && (head.vars subsetOf rest.vars)
      // reporter.debug(s"isFQDefinition reports $res")
      res
    }
  }
}

/**
 * What you get when you set a delta to be empty.
 */
object TrueFQClause extends FQClause(Clause.idCtr.next(),List(Lit.TrueLit),Set(),0,Map())

/**
 * Requires that variables always map to the same clauses.
 * A clause and definitions derived from the clause must use
 * the same variables.
 */
class FQClauseSet(val cs: Set[FQClause]) {
  /**
   * Variables map to the set of clauses which contain them.
   */
  val varMap: Map[Var, Set[FQClause]] = 
    cs.foldLeft(Map[Var,Set[FQClause]]()) ( (map, cl) => {
      map ++ (for(x <- cl.fqVars) yield (x->(map.getOrElse(x,Set())+cl)) )
    })
  
  /** The finite variables in this clause set sorted by name*/
  val fqVars = varMap.keys.toList.sortBy(x => (x.name+x.index))
  
  /**
   * Equivalent to m[x0->empty,...,xn->empty]
   * Note that some clauses may have FQ vars which do not actually occur in
   * the clause.
   * So setting the finite variable to empty should have no effect if that variable
   * is no longer in the clause.
   * @return clauses which have no FQ variables.
   */
  lazy val nonFQCl: Set[Clause] = cs.filterNot(c => c.fqVars.intersects(c.clause.vars))
                       				.map(_.clause)
  
  //TODO- could just return first set found and assume all sets are identical
  def delta(x: Var) = 
    varMap(x)
    	.map(_.delta(x))
    	.reduce(_ intersect _) 
  
  /**
   * Assume that any values which are not included remain the same,
   * e.g. m(x->empty) would be the same as m(x->empty,y->delta(y)...)
   */
  def apply(piMap: Map[Var, Domain[DomElemInt]]): FQClauseSet = {
    //apply subs to the appropriate clauses (or all?)
    //then simplify to remove empty deltas
    new FQClauseSet(
		    cs.map(_(piMap))
		      .filterNot(_==TrueFQClause))
  }

  def sortedCS: List[FQClause] = {
    val (units,other) = cs.partition(_.clause.isUnitClause)
    units.groupBy(_.clause(0).eqn.lhs).toList
      .sortWith(_._1.toString > _._1.toString)
      .flatMap(_._2) ::: other.toList
  }

  def size = cs.size
}
