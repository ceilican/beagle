package beagle.fol

import beagle._
//import bgtheory.CSP._
import term._
import bgtheory.LIA.DomElemInt
import datastructures.Eqn
import Signature._

object unification {

  case object UnifyFail extends Exception
  //case object MatchFail extends Exception

  def unifyList(l1: List[Term], l2: List[Term]): Subst = {
    var hl1 = l1 // we loop over l1 and
    var hl2 = l2 // we loop over l2
    var sigma = Subst.empty // the result substitution
    // invariant: sigma has still to be applied to all terms in hl1 and hl2
    while (!hl1.isEmpty & !hl2.isEmpty) {
      sigma = sigma ++ unify(sigma(hl1.head), sigma(hl2.head))
      hl1 = hl1.tail
      hl2 = hl2.tail
    }
    sigma
  }

  /** Computes a *simple* mgu if it exists. */
  def unify(s: Term, t: Term): Subst = {

    if (s.sort != t.sort)
      // Ill-sorted case. Can come up even in a sortchecked world, when s or t is a variable
      throw UnifyFail
    else
      (s, t) match {
        case (x: AbstVar, y: AbstVar) ⇒ if (x == y) Subst.empty else Subst(x->y)
        case (x: GenVar, y: GenVar)   ⇒ if (x == y) Subst.empty else Subst(x->y)
        case (x: GenVar, y: AbstVar)  ⇒ Subst(x->y)
        case (x: AbstVar, y: GenVar)  ⇒ Subst(y->x)
        case (x: AbstVar, t: FunTerm) ⇒
          if ((!t.isPureBG) || (x occursIn t)) 
            throw UnifyFail
          else {
            // if (!t.isDomElem)
            //   println("Warning: binding a non-domain element pure background term %s to an abstraction variable %s.".format(t, x))
            Subst(x -> t)
          }
        case (x: GenVar, t: FunTerm) ⇒
          if (x occursIn t) throw UnifyFail 
          else Subst(x -> t)
        case (s: FunTerm, y: Var) ⇒ unify(y, s)
        case (s: FunTerm, t: FunTerm) ⇒ {
          val (FunTerm(f, f_args), FunTerm(g, g_args)) = (s, t)
          if (f == g) unifyList(f_args, g_args) else throw UnifyFail
        }
      }
  }

  /**
   * Match `s` to `t` under a pre-existing substitution `sigma`.
   */
  def matchTo(s: Term, t: Term, sigma: Subst): Option[Subst] = matchTo(List(s),List(t),sigma.env)

  /** There is ambiguity here in the use of a substitution to represent the
   * partial matcher.
   * Under the usual substitution definition [X->X,X->Y] is the same as [X->Y].
   * But for matching they are different, since [X->X] means that X is neccessarily
   * bound to X, e.g. p(X,X) and p(X,Y) do not have a matcher.
   * The only operations of subst required here are actsOn and +; both of these
   * have equivalent `Map` operations, so it seems better to represent the partial
   * matcher as a `Map` until it is returned.
   */
  @annotation.tailrec
  def matchTo(l1: List[Term], l2: List[Term], mu: Map[Var,Term]): Option[Subst] =
    if (l1.isEmpty) Some(new Subst(mu))
    else if (!(l1.head.sort == l2.head.sort)) None
    else {
      (l1.head, l2.head) match {
        case (x: Var, t: Term) if (mu.isDefinedAt(x)) =>
            // check if the term bound to x is equal to t, if so nothing
            // needs to be done, otherwise fail
            if (mu(x) == t) matchTo(l1.tail,l2.tail,mu)
  	    else None
        // Can do a new binding for x - but make sure it is simple
        case (x: AbstVar, t: Term) if(t.isPureBG) => matchTo(l1.tail,l2.tail,mu + (x -> t))
	case (x: GenVar, t: Term)                 => matchTo(l1.tail,l2.tail,mu + (x -> t))
        case (s: FunTerm, y: Var)                 => None
        case (FunTerm(f, f_args), FunTerm(g, g_args)) if (f == g) => matchTo(f_args:::l1.tail, g_args:::l2.tail, mu)
	case _                                    => None
      }
  }

}
