package beagle.fol

import beagle._
import term._
import util._
import cnfconversion._
import cnfconversion.rules._
import datastructures._

/**
 * Representation of first-order formulas (not: clauses)
 */
abstract class Formula extends Expression[Formula] {

  def fold[A](base: Formula => A)(join: (A,A) => A): A = ???
  def foldPartial[A](base: PartialFunction[Formula,A])(join: (A,A) => A): A = ???

  def forall(pred: Formula => Boolean): Boolean = fold(pred)(_ && _)
  def atoms2 = foldPartial[List[Atom]]({ case a: Atom => List(a) })(_ ::: _)

  // Mixin (parts of) Expression
  def mgus(that: Formula) = ???
  def tmgus(that: Formula) = ???

  // delegate to subclasses
  def matchers(that: Formula, gammas: List[Subst]): List[Subst]

  /**
   * Collect all atoms when formula structure doesn't matter, e.g. used a lot
   * in cooper.
   */
  val atoms: List[Atom]/* = 
    this match {
      case a: Atom => List(a)
      case UnOpForm(_,f) => f.atoms
      case BinOpForm(_,f1,f2) => f1.atoms:::f2.atoms
      case QuantForm(_,_,f) => f.atoms
    }*/

  // New abstract members
  val isQuantifierFree: Boolean
  //val symConsts: Set[SymConst]

  /** Will be used to declare a formula as a BG theory lemma */
  var annotation: Option[Term] = None 

  /**
   * replace binds all occurrences of f-terms in this to the lhs via a matcher σ
   * and replaces all these occurrences by the corresponding (rhs)σ
   * @param lhs is a flat term over pairwise different variables, something like f(X,Y)
   */
  def replace(lhs: FunTerm, rhs: Term): Formula

  /**
   * Replace an atom with a formula?
   */
  def replace(lhs: Atom, rhs: Formula): Formula

  /**
   * Apply a function to top-level terms in a formula preserving the structure.
   * Can be used to lift term functions e.g. elimNonLinearMult and replaceAt.
   * Note that it does not affect variables in a quantifier prefix.
   */
  def map(m: Term => Term): Formula = this match {
    case Atom(pred, args) => Atom(pred, args map m)
    case UnOpForm(op, f) => UnOpForm(op, f.map(m))
    case BinOpForm(op, f1, f2) => BinOpForm(op, f1.map(m), f2.map(m))
    case QuantForm(q, xs, f) => QuantForm(q, xs, f.map(m))
    case ITEForm(cond, thenForm, elseForm) => ITEForm(cond.map(m), thenForm.map(m), elseForm.map(m))
    case lf : LetFormula => LetFormula(lf.letVars zip (lf.letTerms map m), lf.body.map(m))
    case h => throw new Exception(s"$h not supported by map")
  }

  /** Apply a transform to the atoms of f */
  def mapAtom(g: Atom => Atom): Formula = this match {
    case a: Atom => g(a)
    case UnOpForm(op, f) => UnOpForm(op, f.mapAtom(g))
    case BinOpForm(op, f1, f2) => BinOpForm(op, f1.mapAtom(g), f2.mapAtom(g))
    case QuantForm(q, xs, f) => QuantForm(q, xs, f.mapAtom(g))
    case h => throw new Exception(s"$h not supported by mapLits")
  }

  def iteExpanded: Formula

  def elimNonLinearMult: Formula = map( _.elimNonLinearMult )

  def setAnnotation(t: Option[Term]) = { annotation = t; this }
  // Assume that only a universal quantifier is present, if at all
  def matrix =
    this match {
      case Forall(_, f) ⇒ f
      case f            ⇒ f
    }
  lazy val compl = this match {
    case Neg(f) ⇒ f
    case _      ⇒ Neg(this)
  }

  /** Convenience method, could be moved to Expression if symConsts is defined there too */
  def contains(x: VarOrSymConst): Boolean = x match {
    case x: Var => this.vars(x)
    case x: SymConst => this.symConsts(x)
  }

  /**
   * Convert this, a nested formula of operators `op`, to a list.
   * This is always possible, the result will be a singleton of the operator
   * if this does not match.
   */
  @scala.annotation.tailrec
  final def toList(op: BinOp, acc: List[Formula] = Nil, open: List[Formula] = Nil): List[Formula] = 
    this match {
      case BinOpForm(hop, f1, f2) if (op == hop) ⇒ 
          //f1.toList(op) ::: f2.toList(op)
	  f1.toList(op, acc, f2 :: open)
      case _ if open.isEmpty ⇒ this :: acc
      case _ ⇒ open.head.toList(op, this :: acc, open.tail)
    }

  lazy val toListAnd = toList(And)
  lazy val toListOr = toList(Or)


  def isComment = isInstanceOf[Atom] && asInstanceOf[Atom].pred == "$comment"

  //TODO- does it also make sense to include ![X]: P(x) as a literal say?
  lazy val isLiteral = this match {
    case Atom(_, _)      ⇒ true
    case Neg(Atom(_, _)) ⇒ true
    case _               ⇒ false
  }

  lazy val isClause = this match {
    case Or(_,_) => this.toList(Or).forall(_.isLiteral)
    case _ => this.isLiteral
  }

  lazy val isNNF: Boolean = this match {
    case Atom(_,_) | Neg(Atom(_,_)) => true
    case And(f1,f2) => f1.isNNF && f2.isNNF
    case Or(f1,f2) => f1.isNNF && f2.isNNF
    case QuantForm(_,_,f) => f.isNNF
    case _ => false
  }

  lazy val isCNF: Boolean = this match {
    case And(_,_) => this.toList(And).forall(_.isClause)
    case Or(_,_) => this.isClause
    case QuantForm(_,_,f) => f.isCNF
    case _ => this.isLiteral
  }

  lazy val toLit = this match {
    case a @ Atom(_, _)      ⇒ { a.toSignedEqn match { case (sign, eqn) ⇒ Lit(sign, eqn) } }
    case Neg(a @ Atom(_, _)) ⇒ { a.toSignedEqn match { case (sign, eqn) ⇒ Lit(!sign, eqn) } }
    case _                   ⇒ throw InternalError("toLit called with a non-Literal: " + this)
  }

  def closure(q: Quantifier) =
    if (vars.isEmpty) this else QuantForm(q, vars.toList, this)

  // For convenience
  // notice that this conceals an update to Sigma for now
  def skolemize = {
    val (resF,newSig) = skolemization.skolemize(this,Sigma)
    Sigma = newSig
    resF
  }

  def skolemizeUnlessIsBG = if (isBG) this else skolemize
  
  def reduceInnermostFG(rules: FormulaRewriteRules) =
    FormulaRewriting.fgRewriting.reduceInnermost(this, rules)
  def reduceInnermost(rules: FormulaRewriteRules) =
    FormulaRewriting.fullRewriting.reduceInnermost(this, rules)
  def reduceOnePass(r: FormulaRewriteRules) = 
    FormulaRewriting.fullRewriting.reduceOnePass(this, r)
  def reduceOuterOnePass(r: FormulaRewriteRules) = 
    FormulaRewriting.fullRewriting.reduceOuterOnePass(this, r)

  // Used frequently, hence a method
  lazy val simplifyCheap = reduceInnermost(elimNegNeg).reduceInnermost(elimTrivial)

  // Only makes sense for PolyAtoms, hence overriden there
  def canonical: Formula = this

  /*
   * Expand all let-expressions (Let, LetFormula) in this.
   */
  def letExpanded: Formula = {

    // RemoveOuterLet: remove one outermost occurrence of a let-expression in a term t.
    // This is done by replacing t[let x=v in s] by t[s], more precisely,
    // returns the triple (rho(x), v, t[rho(s)]) where rho is a renaming sbstitution,
    // which provides enough information for
    // the caller to build an (existantial) quantification.
    // The renaming must be done in order to avoid unintended capturing, of course.

    def removeOuterLetList(ts: List[Term]): Option[(List[(Var, Term)], List[Term])] = {
      ts match {
        case Nil ⇒ None
        case t :: rest ⇒ {
          removeOuterLet(t) map {
            case (bindings, tNew) ⇒ (bindings, tNew :: rest) // Found one
          } orElse {
              removeOuterLetList(rest) map { case (bindings, restNew) ⇒ (bindings, t :: restNew) }
          }
        }
      }
    }

    def removeOuterLet(t: Term): Option[(List[(Var, Term)], Term)] =
      t match {
        case Let(bindings, in) ⇒ {
          // Need to rename the variables in order to avoid unintended capturing later,
          // when the binding is lifted to outer positions
          val letVars = bindings map { _._1 }
          val rho = Term.mkRenaming(letVars)
          Some(bindings map { case(v, t) => (rho(v).asInstanceOf[Var], t) }, rho(in))
        }
        case PFunTerm(op, args) ⇒
          removeOuterLetList(args) match {
            case None                                 ⇒ None
            case Some((bindings, argsNew)) ⇒ Some(bindings, PFunTerm(op, argsNew))
          }
        case _ ⇒ None // variables, constants, etc
      }

    // body of letExpanded
    this match {
      case a @ Atom(pred, args) ⇒
        removeOuterLetList(args) match {
          case None ⇒ a // no (more) let constructs in a
          case Some((bindings, argsNew)) ⇒ {
            val letVars = bindings map { _._1 }
            // variables have already been renamed - no need to rename again
            Exists(letVars,
              And(bindings map { case (v,t) => Equation(v, t).asInstanceOf[Formula] }  reduceRight { And(_, _) },
                Atom(pred, argsNew))).letExpanded
          }
        }
      case LetFormula(bindings, in) ⇒ {
        // can directly convert into a formula
        val letVars = bindings map { _._1 }
        val rho = Term.mkRenaming(letVars)
        Exists(rho(letVars).asInstanceOf[List[Var]],
          And(bindings map { case (v,t) => Equation(rho(v), t).asInstanceOf[Formula] }  reduceRight { And(_, _) },
            rho(in))).letExpanded
      }
      case UnOpForm(op, f)           ⇒ UnOpForm(op, f.letExpanded)
      case BinOpForm(op, f1, f2)     ⇒ BinOpForm(op, f1.letExpanded, f2.letExpanded)
      case QuantForm(q, x, f) ⇒ QuantForm(q, x, f.letExpanded)
    }
  }



}

/**
 * Atoms are used in logical transformations e.g. CNF and parsing.
 * Later, in the main loop the base datastructure is an Eqn and all
 * Atoms are transformed to that (or PolyAtoms).
 * The reason 'pred' is used here instead of a proper operator is a
 * legacy attempt to support polymorphic equation operators.
 * There is a better way now but this method has become ingrained in the
 * design unfortunately.
 */
case class Atom(pred: String, args: List[Term]) extends Formula {

  // Mixin Expression
  lazy val vars = args.vars
  val isQuantifierFree = true
  lazy val symConsts = args.symConsts
  lazy val depth = if (args.isEmpty) 1 else (args map { _.depth }).max + 1
  lazy val atoms = List(this)
  lazy val minBSFGTerms = args.minBSFGTerms
  lazy val maxBSFGTerms = args.maxBSFGTerms
  lazy val sorts = args.sorts
  lazy val operators =
    if (pred == "=") {
      // println("xxx Operators of " + this + ": " + args.operators)
      args.operators
    }
    else {
      Sigma.findOperator(pred, args.sortsOf) match {
        case Some(op) ⇒ {
          val res = args.operators + op
          // println("xxx Operators of " + this + ": " + res)
          res
        }
        case _        ⇒ throw InternalError("predicate symbol is not a defined operator: " + pred)
      }
    }


  def matchers(that: Formula, gammas: List[Subst]) =
    that match {
      case Atom(thatpred, thatargs) ⇒
        if (pred == thatpred && args.length == thatargs.length) // allow varyadic
          Expression.matchers(args, thatargs, gammas)
        else
          List.empty
      case _ ⇒ List.empty
    }

  def applySubst(sigma: Subst) = 
    if (sigma actsOn this) Atom(pred, sigma(args)) else this

  lazy val iteExpanded = {
    // ITE-Expand arguments and build the formula
    // println("*** expand " + this)
    val h = args map { _.iteExpanded }
    var template : Formula = Atom(pred, h map { _._1 })
    var bindings = (h map { _._2 }).flatten
    while (bindings.nonEmpty) {
      val (x, ITE(cond, thenTerm, elseTerm)) = bindings.head
      bindings = bindings.tail
      val thenSubst = Subst(x -> thenTerm)
      val elseSubst = Subst(x -> elseTerm)
      template = And(Implies(cond, thenSubst(template)), Implies(Neg(cond), elseSubst(template)))
    }
    // println("*** result " + template)
    template
  }

  def replace(lhs: FunTerm, rhs: Term) = 
    map( _.replace(lhs, rhs)) 
    //Atom(pred, args map { _.replace(lhs, rhs) })

  def replace(lhs: Atom, rhs: Formula) = {
    val argsReplaced = args map { _.replace(lhs, rhs) }
    lhs match {
      case Atom(thatPred, thatArgs) if pred == thatPred && args.length == thatArgs.length =>
        Expression.matchers(thatArgs, argsReplaced, List(Subst.empty)) match {
          case Nil => throw GeneralError("let-binder: cannot apply definition %s <=> %s to atom %s".format(lhs, rhs, Atom(pred, argsReplaced)))
          case List(sigma) => sigma(rhs)
          case _ => throw InternalError("Unexpected matching result in replace.")
        }
      case _ => Atom(pred, argsReplaced)
    }
  }

  override def toString =
    // Generic equality "=" is not an operator in our signatures, hence check explicity
    this match {
      case Equation(s, t) ⇒ "(" + s.toString + " = " + t.toString + ")"
      case _ ⇒ Sigma.findFormatFn(pred, Arity(args.sortsOf, Signature.OSort)) match {
        case Some(fn) ⇒ fn(args)
        case None     ⇒ pred + args.toMyString("", "(", ",", ")") // Fallback 
      }
    }

  lazy val kind = this match {
    case Equation(s, t) ⇒ s.kind lub t.kind
    case _ ⇒ Sigma.findOperator(pred, args.sortsOf) match {
      case None ⇒ {
        // Last resort - quick hack
        if (pred == "<" || pred == "≤" || pred == "|" || pred == "=" || pred == "≠")
          // treat these as background
          Expression.lub(args)
          else
          throw InternalError("predicate symbol is not a defined operator: " + pred) // was: false
      }
      case Some(op) ⇒ if (op.kind == FG)
        Expression.FG
      else
        // op is a background operator, the lub of the arguments gives the result. 
        // Notice if args.isEmpty we have PureBG
        Expression.lub(args)
    }
  }

  // Convert an Atom to a literal.
  // Assume it is well-sorted.
  // We need to convert it to a *literal*, not an equation, because some atoms in bgtheory
  // might convert to negated atoms (0 != ...) and we must be able to override this definition
  lazy val toSignedEqn = {
    // println("toSignedEqn " + this)
    val res = this match {
      case Equation(s, t) ⇒ { 
        // println("aaa")
        val res = (true, Eqn(s, t))
        // println("bbb")
        res
      }
      case TrueAtom       ⇒ (true, PredEqn(TT))
      case FalseAtom      ⇒ (false, PredEqn(TT))
      case _ ⇒ Sigma.findOperator(pred, args.sortsOf) match {
        case Some(op) ⇒ (true, PredEqn(if (args.isEmpty) SymConst(op) else PFunTerm(op, args)))
        // Because pred is a predicate symbol we know it cannot be a domain element,
        // hence it suffices to build a PFunTerm or a SymConst
        case None     ⇒ throw new InternalError("toEqn called with an undeclared predicate symbol "+pred)
      }
    }
    // println("toSignedEqn " + res)
    res
  }

}

/**
 * Equations *are* atoms (with predicate symbol "=")
 * Equality is polymorphic, and the sort of Eqn in the clausal form later
 * will be determined in the clause normalform transformation later
 */
object Equation {
  def apply(s: Term, t: Term) = new Atom("=", List(s, t))
  def unapply(f: Formula) = f match {
    case Atom("=", List(s, t)) ⇒ Some((s, t))
    case _                     ⇒ None
  }
}

// Some common background atoms

object LessEq {
  def apply(s: Term, t: Term) = Atom("$lesseq", List(s, t))
  def unapply(f: Formula) =
    f match {
      case Atom("$lesseq", List(s, t)) ⇒ Some((s, t))
      case _                           ⇒ None
    }
}

object Less {
  def apply(s: Term, t: Term) = Atom("$less", List(s, t))
  def unapply(f: Formula) =
    f match {
      case Atom("$less", List(s, t)) ⇒ Some((s, t))
      case _                         ⇒ None
    }
  def unapply(e: Eqn) = 
    e match {
      case PredEqn(PFunTerm(op,List(s,t))) if (op.name=="$less") => Some(s,t)
      case _ => None
    }
}

object GreaterEq {
  def apply(s: Term, t: Term) = Atom("$greatereq", List(s, t))
  def unapply(f: Formula) =
    f match {
      case Atom("$greatereq", List(s, t)) ⇒ Some((s, t))
      case _                              ⇒ None
    }
}

object Greater {
  def apply(s: Term, t: Term) = Atom("$greater", List(s, t))
  def unapply(f: Formula) =
    f match {
      case Atom("$greater", List(s, t)) ⇒ Some((s, t))
      case _                            ⇒ None
    }
}

object Divides {
  def apply(s: Term, t: Term) = Atom("$divides", List(s, t))
  def unapply(f: Formula) =
    f match {
      case Atom("$divides", List(s, t)) ⇒ Some((s, t))
      case _                            ⇒ None
    }
}

// Disequations are currently used internally by QE only.
// Handy to introduce them here generally.
/* object DisEquation {
  def apply(s: Term, t: Term) = new Atom("!=", List(s, t))
  def unapply(f: Formula) = f match {
    case Atom("!=", s :: t :: Nil) => Some((s, t))
    case _ => None
  }
}
*/
object Comment {
  def apply(s: String) = Atom("$comment", List(FGConst(""""""" + s + """"""", Signature.ISort)))
}

object TrueAtom extends Atom("$true", List.empty) {
  // Want to make sure application of a substitution preserves this, otherwise toString would be different
  override def applySubst(sigma: Subst) = this
  override def toString = printer.trueSym
}

object FalseAtom extends Atom("$false", List.empty) {
  override def applySubst(sigma: Subst) = this
  override def toString = printer.falseSym
}

/*
 * Logical operators
 */
abstract class Op

abstract class UnOp extends Op {
  override def toString: String
}
object Neg extends UnOp {
  override def toString = printer.negSym
  def apply(f: Formula) = UnOpForm(Neg, f)
  def unapply(g: Formula) =
    g match {
      case UnOpForm(Neg, f) ⇒ Some(f)
      case _                ⇒ None
    }
}

abstract class BinOp extends Op { override def toString: String }

object And extends BinOp {
  override def toString = printer.andSym
  def apply(f1: Formula, f2: Formula) = BinOpForm(And, f1, f2)
  def unapply(g: Formula) =
    g match {
      case BinOpForm(And, f1, f2) ⇒ Some((f1, f2))
      case _                      ⇒ None
    }
}
object Or extends BinOp {
  override def toString = printer.orSym
  def apply(f1: Formula, f2: Formula) = BinOpForm(Or, f1, f2)
  def unapply(g: Formula) =
    g match {
      case BinOpForm(Or, f1, f2) ⇒ Some((f1, f2))
      case _                     ⇒ None
    }
}

object Iff extends BinOp {
  override def toString = printer.iffSym
  def apply(f1: Formula, f2: Formula) = BinOpForm(Iff, f1, f2)
  def unapply(g: Formula) =
    g match {
      case BinOpForm(Iff, f1, f2) ⇒ Some((f1, f2))
      case _                      ⇒ None
    }
}

object IffNot extends BinOp {
  override def toString = printer.iffNotSym
  def apply(f1: Formula, f2: Formula) = BinOpForm(IffNot, f1, f2)
  def unapply(g: Formula) =
    g match {
      case BinOpForm(IffNot, f1, f2) ⇒ Some((f1, f2))
      case _                         ⇒ None
    }
}

object Implies extends BinOp {
  override def toString = printer.impSym
  def apply(f1: Formula, f2: Formula) = BinOpForm(Implies, f1, f2)
  def unapply(g: Formula) =
    g match {
      case BinOpForm(Implies, f1, f2) ⇒ Some((f1, f2))
      case _                          ⇒ None
    }
}

object Implied extends BinOp {
  override def toString = printer.convImpSym
  def apply(f1: Formula, f2: Formula) = BinOpForm(Implied, f1, f2)
  def unapply(g: Formula) =
    g match {
      case BinOpForm(Implied, f1, f2) ⇒ Some((f1, f2))
      case _                          ⇒ None
    }
}

/*
 * Formulas built with logical operators
 */

case class UnOpForm(op: UnOp, f: Formula) extends Formula {
  lazy val vars = f.vars
  lazy val kind = f.kind
  lazy val depth = f.depth+1
  lazy val minBSFGTerms = f.minBSFGTerms
  lazy val maxBSFGTerms = f.maxBSFGTerms

  val isQuantifierFree = f.isQuantifierFree
  lazy val symConsts = f.symConsts
  def matchers(that: Formula, gammas: List[Subst]) =
    that match {
      case UnOpForm(thatop, thatf) ⇒
        if (op == thatop) f.matchers(thatf, gammas) else List()
      case _ ⇒ List.empty
    }

  def applySubst(sigma: Subst) = UnOpForm(op, sigma(f))
  def replace(lhs: FunTerm, rhs: Term) = map( _.replace(lhs,rhs))
    //UnOpForm(op, f.replace(lhs, rhs))
  def replace(lhs: Atom, rhs: Formula) =  UnOpForm(op, f.replace(lhs, rhs))

  lazy val iteExpanded = UnOpForm(op, f.iteExpanded)
  lazy val sorts = f.sorts
  lazy val operators = f.operators
  lazy val atoms = f.atoms
  override def toString = op + f.toString
}

case class ITEForm(cond: Formula, thenForm: Formula, elseForm: Formula) extends Formula {
 
  lazy val kind = Expression.FG
  lazy val isQuantifierFree = cond.isQuantifierFree && thenForm.isQuantifierFree && elseForm.isQuantifierFree
  
  //Mixin Expression- these follow from the local defintion of atoms
  lazy val vars = cond.vars ++ thenForm.vars ++ elseForm.vars
  lazy val symConsts = cond.symConsts ++ thenForm.symConsts ++ elseForm.symConsts
  lazy val sorts = cond.sorts ++ thenForm.sorts ++ elseForm.sorts
  lazy val operators = cond.operators ++ thenForm.operators ++ elseForm.operators 
  lazy val depth = List(cond.depth, thenForm.depth, elseForm.depth).max + 1
  lazy val minBSFGTerms = List(cond, thenForm, elseForm).minBSFGTerms
  lazy val maxBSFGTerms = List(cond, thenForm, elseForm).maxBSFGTerms
  lazy val atoms = cond.atoms ++ thenForm.atoms ++ elseForm.atoms
  
  def matchers(that: Formula, gammas: List[Subst]) =
    that match {
      case ITEForm(thatCond, thatThenForm, thatElseForm) ⇒
        Expression.matchers(List(cond, thenForm, elseForm), 
          List(thatCond, thatThenForm, thatElseForm), gammas)
      case _ ⇒ List.empty
    }

  def applySubst(sigma: Subst) = ITEForm(sigma(cond), sigma(thenForm), sigma(elseForm))
  def replace(lhs: FunTerm, rhs: Term) = map( _.replace(lhs,rhs) )
  //  ITEForm(cond.replace(lhs, rhs), thenForm.replace(lhs, rhs), elseForm.replace(lhs, rhs))
  def replace(lhs: Atom, rhs: Formula) =  
    ITEForm(cond.replace(lhs, rhs), thenForm.replace(lhs, rhs), elseForm.replace(lhs, rhs))

  lazy val iteExpanded = {
    // println("*** expand " + this)
    val condEx = cond.iteExpanded
    val thenEx = thenForm.iteExpanded
    val elseEx = elseForm.iteExpanded
    val res = And(Implies(condEx, thenEx), Implies(Neg(condEx), elseEx))
    // println("*** result " + res)
    res
  }
  
  override def toString = "$ite_f(%s, %s, %s)".format(cond, thenForm, elseForm)
}



case class BinOpForm(op: BinOp, f1: Formula, f2: Formula) extends Formula {
  lazy val vars = f1.vars union f2.vars
  lazy val kind = f1.kind lub f2.kind
  val isQuantifierFree = f1.isQuantifierFree && f2.isQuantifierFree
  lazy val symConsts = f1.symConsts ++ f2.symConsts
  lazy val sorts = f1.sorts ++ f2.sorts
  lazy val operators = f1.operators ++ f2.operators
  lazy val atoms = f1.atoms:::f2.atoms
  lazy val depth = List(f1.depth, f2.depth).max + 1
  lazy val minBSFGTerms = f1.minBSFGTerms union f2.minBSFGTerms
  lazy val maxBSFGTerms = f1.maxBSFGTerms union f2.maxBSFGTerms

  def matchers(that: Formula, gammas: List[Subst]) =
    that match {
      case BinOpForm(thatop, thatf1, thatf2) ⇒
        if (op == thatop)
          Expression.matchers(List(f1, f2), List(thatf1, thatf2), gammas)
        else
          List.empty
      case _ ⇒ List.empty
    }

  lazy val iteExpanded = BinOpForm(op, f1.iteExpanded, f2.iteExpanded)
  
  def applySubst(sigma: Subst) = BinOpForm(op, sigma(f1), sigma(f2))
  def replace(lhs: FunTerm, rhs: Term) = map(_.replace(lhs,rhs))
  def replace(lhs: Atom, rhs: Formula) = BinOpForm(op, f1.replace(lhs, rhs), f2.replace(lhs, rhs))

  override def toString = "(" + f1.toString + " " + op.toString + " " + f2.toString + ")"

}

/*
 * Quantifiers
 */

abstract class Quantifier {
  def dual: Quantifier
  override def toString: String
}

object Forall extends Quantifier {
  override def toString = printer.forallSym
  def dual = Exists
  def apply(xs: List[Var], f: Formula) =
    if (xs.isEmpty) f else QuantForm(Forall, xs, f)
  def unapply(g: Formula) =
    g match {
      case QuantForm(Forall, xs, f) ⇒ Some((xs, f))
      case _                        ⇒ None
    }
}

object Exists extends Quantifier {
  override def toString = printer.existSym
  def dual = Forall
  def apply(xs: List[Var], f: Formula) =
    if (xs.isEmpty) f else QuantForm(Exists, xs, f)
  def unapply(g: Formula) =
    g match {
      case QuantForm(Exists, xs, f) ⇒ Some((xs, f))
      case _                        ⇒ None
    }
}

/**
 * Quantified formulas
 */
case class QuantForm(q: Quantifier, xs: List[Var], f: Formula) extends Formula {
  assume(!xs.isEmpty, { println("Empty list of variables in quantification") })
  
  lazy val kind = f.kind
  val isQuantifierFree = false

  // Mixin Expression
  lazy val vars = f.vars -- xs
  lazy val sorts = f.sorts ++ xs.sorts
  val symConsts = f.symConsts
  lazy val operators = f.operators
  lazy val depth = f.depth + 1
  lazy val minBSFGTerms = f.minBSFGTerms
  lazy val maxBSFGTerms = f.maxBSFGTerms

  lazy val atoms = f.atoms

  def matchers(that: Formula, gammas: List[Subst]) = {
    assert(false, { println("Formula.scala: matchers on quantified formulas not implemented") })
    List.empty
  }

  // As usual, unintended capturing of variables in the range of sigma by quantified
  // variable must be avoided. Renaming away the quantified variable before applying sigma
  // solves the problem.
  def applySubst(sigma: Subst) = {
    val rho = Term.mkRenaming(xs)
    //      QuantForm(q, rho(xs), sigma(rho(f)))
    QuantForm(q, xs map { rho(_).asInstanceOf[Var] }, sigma(rho(f)))
  }
  def replace(lhs: FunTerm, rhs: Term) = {
    val rho = Term.mkRenaming(xs)
    QuantForm(q, xs map { rho(_).asInstanceOf[Var] }, rho(f).replace(lhs, rhs))
  }
  def replace(lhs: Atom, rhs: Formula) = {
    val rho = Term.mkRenaming(xs)
    QuantForm(q, xs map { rho(_).asInstanceOf[Var] }, rho(f).replace(lhs, rhs))
  }

  lazy val iteExpanded = QuantForm(q, xs, f.iteExpanded)

  import util._

  override def toString = printer.quantifierToString(this)
    //"(" + q + " " + (xs map { _.toStringWithSort }).toMyString("", " ", "") + " " + f + ")"

}


/**
 * Let formulas
 */
case class LetFormula(bindings: List[(Var, Term)], body: Formula) extends Formula {
  val (letVars, letTerms) = bindings.unzip
  
  lazy val kind = body.kind

  lazy val sorts = body.sorts ++ letTerms.sorts
  lazy val operators = body.operators ++ letTerms.operators
  val symConsts = body.symConsts ++ letTerms.foldLeft(Set.empty[SymConst])((s,t) => s ++ t.symConsts)
		  /*{ 
		    var s = Set.empty[SymConst]
		    letTerms foreach { s ++= _.symConsts }
		    s
		  }*/

  lazy val depth = body.depth // not correct, ignores bindings
  lazy val vars = body.vars -- letVars
  lazy val minBSFGTerms = letTerms.minBSFGTerms union body.minBSFGTerms
  lazy val maxBSFGTerms = letTerms.maxBSFGTerms union body.minBSFGTerms

  //abstract values defined by Formula
  val isQuantifierFree = false
  lazy val atoms = body.atoms

  def matchers(that: Formula, gammas: List[Subst]) = {
    assert(false, { println("Formula.scala: matchers on LetForm not implemented") })
    List.empty
  }

  // As usual, unintended capturing of variables in the range of sigma by quantified
  // variable must be avoided. Renaming away the quantified variable before applying sigma
  // solves the problem.
  def applySubst(sigma: Subst) = {
    val rho = Term.mkRenaming(letVars)
    //      QuantForm(q, rho(xs), sigma(rho(f)))
    LetFormula(bindings map { case(v, t) => 
      (rho(v).asInstanceOf[Var], sigma(t)) }, sigma(rho(body)))
  }
  def replace(lhs: FunTerm, rhs: Term) = {
    val rho = Term.mkRenaming(letVars)
    //      QuantForm(q, rho(xs), sigma(rho(f)))
    LetFormula(bindings map { case(v, t) => 
      (rho(v).asInstanceOf[Var], t.replace(lhs, rhs)) }, rho(body).replace(lhs, rhs))
  }
  def replace(lhs: Atom, rhs: Formula) = {
    val rho = Term.mkRenaming(letVars)
    //      QuantForm(q, rho(xs), sigma(rho(f)))
    LetFormula(bindings map { case(v, t) => 
      (rho(v).asInstanceOf[Var], t) }, rho(body).replace(lhs, rhs))
  }

  lazy val iteExpanded = {
    // ITE-Expand arguments and build the formula
    val h = letTerms map { _.iteExpanded }
    var template : Formula = LetFormula(letVars zip (h map { _._1 }), body.iteExpanded)
    var bindings = (h map { _._2 }).flatten
    while (bindings.nonEmpty) {
      val (x, ITE(cond, thenTerm, elseTerm)) = bindings.head
      bindings = bindings.tail
      val thenSubst = Subst(x -> thenTerm)
      val elseSubst = Subst(x -> elseTerm)
      template = And(Implies(cond, thenSubst(template)), Implies(Neg(cond), elseSubst(template)))
    }
    template
  }

  override def toString = printer.letFormulaToString(this)

}


object Formula {

  val skfCtr = new util.Counter

}

/**
 * Encapsulates the result of a formula broken into disjuncts, or
 * equivalently, an n-ary or operator.
 * The idea is that you can modify the components of this and it will
 * work out if it is trivial or not in order to avoid the problems with
 * the plain list representation in cooper.
 */
class OrList(val subFs: List[Formula]) 
  extends BinOpForm(Or, subFs.headOption.getOrElse(FalseAtom), 
			(if(subFs.isEmpty) Nil else subFs.tail).toOr) {

  /** Perform cheap simplification */
  private def normalised(fs: List[Formula]) = {
    //TODO- could do a more efficient version
    val r1 = fs.map(_.simplifyCheap)
    if(r1.exists(_==TrueAtom)) List(TrueAtom)
    else r1.filterNot(_==FalseAtom).distinct
  }

  /** The formula as a usual chain of nested Or operators */
  lazy val asStdFormula = subFs.reduceLeft(Or(_,_))

  //TODO, could return a raw formula if a single literal is produced
  def map(g: Formula => Formula): OrList = {
    //apply map but provide early exit if true is produced
    var rest = subFs
    var result = List.empty[Formula]
    while(!rest.isEmpty) {
      val next = rest.head
      rest = rest.tail
      
      g(next).simplifyCheap match {
	case TrueAtom => //early exit
	  return new OrList(List(TrueAtom)) //or something equivalent to this?
	case FalseAtom => ()
	case f => result ::= f
      }
    }
    return new OrList(result.distinct)
  }

  /** Should inspect the formula and absorb Or(_,_) into outer list */
  def flatMap(g: Formula => Formula): Formula = {
    //new OrList(normalised(subFs.flatMap(g(_).subFs)))
    //apply map but provide early exit if true is produced
    var rest = subFs
    var result = List.empty[Formula]
    while(!rest.isEmpty) {
      val next = rest.head
      rest = rest.tail
      
      g(next).simplifyCheap match {
	case TrueAtom => //early exit
	  return TrueAtom
	case FalseAtom => ()
	case f @ Or(_,_) => result = f.toList(Or) ::: result
	case f => result ::= f
      }
    }
    val res = result.distinct
    if(res.tail.isEmpty) return res.head
    else return new OrList(res)
  }

  //def compl = new AndList(subFs.map(Neg(_)))

  def isFlat = 
    subFs.forall({ case Atom(_,_) | Neg(Atom(_,_)) => true; case _ => false })

  //overridden members of Formula/BinOpForm:
  override lazy val kind = subFs map { _.kind } reduceLeft { _ lub _ }

  /** Usually don't need this, since OrLists are simplified during construction */
  override lazy val simplifyCheap = new OrList(normalised(subFs))

  //should inherit this from BinOpForm
/*
  def matchers(that: Formula, gammas: List[Subst]): List[Subst] = 
    asStdFormula.matchers(that,gammas)
*/

  /**
   * Collect all atoms when formula structure doesn't matter, e.g. used a lot
   * in cooper.
   */
  override lazy val atoms = subFs.flatMap(_.atoms).toList

  override val isQuantifierFree = subFs.forall(_.isQuantifierFree)

  //these ones should be overridden so that we always get an OrList back

  /**
   * replace binds all occurrences of f-terms in this to the lhs via a matcher σ
   * and replaces all these occurrences by the corresponding (rhs)σ
   * @param lhs is a flat term over pairwise different variables, something like f(X,Y)
   */
  override def replace(lhs: FunTerm, rhs: Term) = new OrList(subFs.map(_.replace(lhs,rhs)))
/*
  /**
   * Replace an atom with a formula?
   */
  override def replace(lhs: Atom, rhs: Formula): Formula = new OrList(subFs.map(_.replace(lhs,rhs)))

  override def map(m: Term => Term): Formula = new OrList(subFs.map(_.map(m)))

  override def iteExpanded: Formula = this

  override def elimNonLinearMult = new OrList(subFs.map(_.elimNonLinearMult))
  */
}

class AndList(subFs: List[Formula]) {}
