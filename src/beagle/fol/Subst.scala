package beagle.fol

import beagle._
import term._
import datastructures.Eqn

/* There are two operations on substitutions described in Substitution Tree Indexing:
 * join and compose.
 * Let s = {x1 -> s1, ..., xn -> sn} and t = {y1 -> t1, ..., ym -> tm}
 * s compose t := s(t(x)) for all x
 * or {y1 -> s(t1), ... ym -> s(tm)} ∪ {xi -> si | xi ∈ Dom(s)\Dom(t)}
 * s join t := {y1 -> s(t1), ..., ym -> s(tm)} ∪ {xi -> si | xi ∈ Dom(s)\Intro(t)}
 * where Intro(t) is the subset of {t1,...,tm} which are variables.
 */

final class Subst(val env: Map[Var, Term]) {
  // It might be tempting to mixin collection.Map[Var, Term]
  // however apply and + are just too different to make sense

  def isEmpty = env.isEmpty || (env forall { pair => pair._1 == pair._2 })
  def lookup(v: Var): Option[Term] = env.get(v)
  def actsOn(v: Var) = env.contains(v) && env(v)!=v
  def actsOn(vs: List[Var]): Boolean = vs exists { actsOn(_) }
  def actsOn(vs: Set[Var]): Boolean = vs exists { actsOn(_) }
  def actsOn[T](e: Expression[T]): Boolean = actsOn(e.vars)
  // def getEnv = env

  /**
   * Two substitutions are equal if they have the same `env`.
   * This is NOT functional equality, e.g. [X->X,Y->X]!=[Y->X]
   * It may be the case that this does not matter in practise since
   * substitutions of the first form are never made?
   */
/*
  override def equals(any: Any) = any match {
    case that: Subst => 
      (this.env.keys ++ that.env.keys).forall( x => this.env(x)==that.env(x) ) 
    case _ => false
  }
 */

  // The above definition of equals is better, but it is not obvious how
  // to define a hasCode for substitutions, so that two equal substitutions have 
  // the same hashCode
  override def equals(any: Any) = any match {
    case that: Subst => (this.env == that.env)
    case _ => false
  }

  override def hashCode: Int = env.hashCode

  /** @return only the non-trivial values from `env`. */
  def values = env collect { case (x,t) if x!=t => t } toSet

  // Use substitutions as functions:
  // Notice that this works for any Expression
  // def apply[T](es: Array[Expression[T]]) = 
  //   if (isEmpty) es.asInstanceOf[Array[T]] else 
  //     es.map(_.applySubst(this))
  def apply[T](es: List[Expression[T]]) = 
    if (isEmpty) es.asInstanceOf[List[T]] else 
      es.map(_.applySubst(this))
  def apply[T](e: Expression[T]) = 
    if (isEmpty) e.asInstanceOf[T] else 
      e.applySubst(this)

  override def toString = env.mkString("[", ", ", "]")

  /** Extend with one binding - don't check anything */
  def +(xt: (Var, Term)) = new Subst(env + xt)

  /** Composition of substitutions.
   * @return The substitution sigma(this)
   */
  def ++(sigma: Subst) = 
    new Subst((env.mapValues(sigma(_)) // apply sigma to all terms,
	       ++
	       // extend with all bindings from sigma that env does not act on,
	       sigma.env.filterKeys(v => !env.isDefinedAt(v)))
	      // and delete all trivial bindings
	      .filter(vt => vt._1 != vt._2))

  /** Synonym for the above to help readability of code.
   * In the scala function interface, `f compose g` is `f(g(x))`
   * whereas `f andThen g` is `g(f(x))`.
   * So this means "first sigma, then this".
   */
  def compose(sigma: Subst) = sigma ++ this

  /** Remove bindings for a given list of variables */
  def --(xs: List[Var]) = new Subst(env -- xs)

  /** Let this = {x1 -> s1, ..., xn -> sn} and sigma = {y1 -> t1, ..., ym -> tm},
   * then `this join sigma` := {y1 -> this(t1), ..., ym -> this(tm)} ∪ {xi -> si ∈ this | ¬(xi ∈ Intro(t))}
   */
  def join(sigma: Subst): Subst = 
    new Subst((sigma.env.mapValues(this(_)) ++ this.env.filterKeys(!sigma.intro(_))))

  /** Whether this substitution is an injective map from variables to variables. */
  def isRenaming: Boolean = {
    val res = env forall { case (x,y) =>
      // Check that all terms in the codomain are variables, 
      y.isVar &&
        // and that env is injective,
        (!(env exists { v1t1 => (y == v1t1._2) && (x != v1t1._1) }))
    }
    // println(this + " isRenaming = " + res)
    res
    }

  /** @return The set of vars which this properly acts on, i.e. those which are not mapped to themselves */
  lazy val dom: Set[Var] = env collect { case (x,t) if (x!=t) => x } toSet

  /** @return The set of variables which are introduced by this substitution */
  lazy val intro: Set[Var] = env.values.collect({ case x: Var => x }).toSet

  /** Unifiers must be idempotent, but matchers may not be */
  def isIdempotent: Boolean = env forall { case (x,t) => this(t)==t }

  /** Combine two unifiers (typically mgus) into one, simultaneous unifier, if possible */
  def combine(that: Subst): Option[Subst] =
    // Take this and extend it to a unifier for each binding in that.
    // The assert substitution can be used at most once.
    // This is a consequence of how the assert bindings are made.
    try {
      var sigma = this // sigma will be the result
      for (bind <- that.env) {
        // sigma must also be a unifier of the bindings in that,
        // hence sigma is applied to these bindings before unifying them
        val s = sigma(bind._1)
        val t = sigma(bind._2)
        sigma = sigma ++ unification.unify(s, t)
      }
      Some(sigma)
    } catch {
      case unification.UnifyFail => None
    }
}

object Subst {
  def apply(bindings: (Var, Term)*) = new Subst(bindings.toMap)

  val empty = Subst()

  /**
   * Build the cross product of all unifiers in ss1 and ss2
   * by combining their elements
   */
  def combineSubst(ss1: List[Subst], ss2: List[Subst]): List[Subst] =
    for (s1 <- ss1; s2 <- ss2; sigma <- (s1 combine s2)) yield sigma

}

