package beagle.fol.term

import beagle._
import fol._
import datastructures.PMI._
import util._

/** FunTerms which never have args */
abstract class Const extends FunTerm {

  /* Abstract members */

  val args = List.empty
  val weightForKBO = KBO.constWeight // overridden by domain elements, see below
  // Oops, we had:
  // override lazy val operators = Set.empty[Operator]
  // constants don't count as operators. It would make sense though to distinguish between
  // parameters and originally defined symbolic BG constants

  val varsCnt = Map.empty[Var, Int]
  val vars = Set.empty[Var]

  def applySubst(sigma: Subst) = this
  // def normalize = this

  lazy val elimNonLinearMult = this

  def subterms = new Iterator[Term] {
    // delivers just this
    var gotit = false; def next() = { gotit = true; thisconst }; def hasNext = !gotit
  }

  override def replaceAt(pos: Pos, t: Term) = if(pos.isEmpty) t else this

  /* New members */

  val thisconst = this

}

/** Notice that the unapply method here also captures Domain Elements. */
object Const {
  def unapply(t: Term) = t match {
    case s: SymConst   ⇒ Some(s.op)
    case d: DomElem[_] ⇒ Some(d.op)
    case _             ⇒ None
  }
}

/** Symbolic constants, distinguished from domain elements which have a value attached.
 * Therefore this includes both FG and BG constants (parameters) but not domain elements.
 */
case class SymConst(op: Operator) extends Const with VarOrSymConst {
  
  val symConsts = Set(this)

  val depth = 0

  lazy val minBSFGTerms =
    if (op.kind == FG && sort.thyKind == BG) Set[Term](this) 
    else Set.empty[Term]

  // lazy val subTermsWithPos = 
  //   if (this == TT) PMI.empty else List(this -> List.empty)
  // lazy val ops = 
  //   if (this == TT) Set.empty[Operator] else Set(op)

  lazy val termIndex = 
    if (this == TT) TermIndex.empty 
    else TermIndex(op -> List((this, List.empty)))

  //def replaceAt(pos: Pos, t: Term) = if(pos.isEmpty) t else this

  def replace(lhs: FunTerm, rhs: Term) = 
    if (lhs == this) rhs else this

  def replace(lhs: Atom, rhs: Formula) = this

}

/** Applies only to `SymConst`s which have an `FG` operator. */
object FGConst {

  def apply(name: String, sort: Sort) = {
    require(sort.thyKind!=BG, println("FGConst should not be used to introduce BG constants"))
    //TODO- some weird stuff happens here:
    /*val op = Sigma.findOperator(name,Nil) filter {
      _.sort() == sort
    } getOrElse {
      val res = Operator(FG, name, Arity0(sort))
      Sigma+=res
      res
    }
    SymConst(op)*/
    SymConst(Operator(FG, name, Arity0(sort)))
  }

  def apply(op: Operator) = {
    require(op.kind == FG, println("FGConst application with non-FG operator"))
    SymConst(op)
  }

  def unapply(t: Term) = t match {
    case SymConst(op @ Operator(FG, _, _)) ⇒ Some((op))
    case _                                 ⇒ None
  }
}

/**
 * Applies only to `SymConst`s which have a `BG` operator namely parameters or BG skolem constants.
 * This excludes Domain Elements.
 */
object BGConst {

  def apply(name: String, sort: Sort) = {
    require(sort.thyKind == BG, println("BGConst application with non-BG sort"))
    SymConst(Operator(BG, name, Arity0(sort)))
  }

  def apply(op: Operator) = {
    require(op.arity.nrArgs==0 && op.arity.resSort.thyKind == BG, println("BGConst application with non-BG operator"))
    SymConst(op)
  }

  def unapply(t: Term) = t match {
    case SymConst(op @ Operator(BG, _, _)) ⇒ Some((op))
    case _                                 ⇒ None
  }
}

/** Specialised in the individual theories */
abstract class DomElem[T] extends Const {
  /* Abstract members */
  val op: Operator
  val value: T
  val depth = 0

  // val subTermsWithPos = PMI.empty
  val termIndex = TermIndex.empty
  // val ops = Set.empty[Operator]
  //def replaceAt(pos: Pos, t: Term) = null
  def replace(lhs: FunTerm, rhs: Term) = this
  def replace(lhs: Atom, rhs: Formula) = this
  // notice the TPTP sort can be obtained by the sort method
  val symConsts = Set.empty[SymConst]
  val minBSFGTerms = Set.empty[Term]
  override def toString = printer.domElemToString(this)
  
}
