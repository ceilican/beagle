package beagle.fol.term

import beagle._
import fol._
import util._
import datastructures.Eqn
import datastructures.PMI._

/* Function Term subclasses */

/**
 * Has a head operator 'op' and a list of terms `args` which may be empty.
 * Function subclasses of `Term` subclass `FunTerm`
 */
abstract class FunTerm extends Term {
  // Abstract members
  val op: Operator
  val args: List[Term]

  //derived
  lazy val preorder: List[Any] = op :: args.flatMap(_.preorder)

  lazy val maxBSFGTerms: Set[Term] =
    if (this.isBSFG) Set(this)
    else {
      val (bsfg,other) = args.partition(_.isBSFG)
      bsfg.toSet ++ (other.map(_.maxBSFGTerms).flatten.toSet)
      // (bsfg ::: (other.map(_.maxBSFGTerms).flatten)).toSet
    }

  /** Get the (result) sort of a given term t. */
  lazy val sort = op.sort(args.map(_.sort))

  lazy val kind =
    if (op.kind == FG)
      Expression.FG
    else
      // op is a background operator, the lub of the arguments gives the result. 
      // Notice if args.isEmpty we have PureBG
      Expression.lub(args)

  //lazy val isBG = op.kind == BG && (args forall { _.isBG })
  //lazy val isPureBG = op.kind == BG && (args forall { _.isPureBG })
  //lazy val isPureFG = op.kind == FG && (args forall { _.isPureFG })
  lazy val sorts: Set[Type] = if (sort == Signature.OSort) args.sorts else (args.sorts + sort)
  lazy val operators = args.operators + op
  override def toString = op.formatFn(args) 

  /*
   * Whether we have created a definition for this already
   */
  // This is an utterly unreliable definition: the mere presence of rhsName in defineMap doesn't say anything.
  // The defineMap is global, the definition culd have been inserted only after a left split and we're currently in the right split.
/*  def haveDefinition = {
    assume(vars.isEmpty && op.kind == FG && sort.kind == BG, "haveDefinition: need a ground B-sorted term with an FG-operator")
    val rhsName = printer.skolemEltPref+"'" + toString + "'"
    Term.defineMap.isDefinedAt(rhsName)
  }
 */

  /**
   * Return an equation that says that this is equal to some value;
   * Also return the freshly created operator
   */
  def mkDefinition(): (Eqn,Operator) = {
    assume(vars.isEmpty && op.kind == FG && sort.thyKind == BG, 
	   "mkDefinition: need a ground B-sorted term with an FG-operator")

    val rhsName = printer.skolemEltPref + "'" + toString + "'"

    val ctr = Term.defineMap.get(rhsName) getOrElse { 
        // have to invent a new short name (an int) and remember it
        val ctr = Term.defineCtr.next()
        Term.defineMap += ((rhsName, ctr))
        ctr
      }
  
    val rhsShortName = printer.skolemEltPref + ctr
    // val rhs = new SymConst(Operator(sort.kind, "#e_'" + toString + "'", Arity0(sort))) 

    val newOp = new Operator(BG, rhsShortName, Arity0(sort)) {
      override val skolemCtr = Some(ctr)
    }

    (Eqn(this, SymConst(newOp)), newOp)
  }

  def get(p: Pos): Option[Term] = 
    if(p.isEmpty) Option(this)
    else if (args.isEmpty || !args.isDefinedAt(p.head)) None
    else args(p.head).get(p.tail)

}


object FunTerm {
  /** Provides a generic constructor for FunTerms which includes sort-checking
   * and casting of domain elements.
   */
  def apply(op: Operator, args: List[Term] = List()): FunTerm = {
    if(op.arity.nrArgs!=args.length) 
      throw SyntaxError("ill-sorted term: " + PFunTerm(op, args))
    else if(args.isEmpty) 
      SymConst(op)
    else {
      val liftedArgs = (op.arity.argsSorts zip args).map({ 
	case (argSort, v) if (argSort == v.sort) => v
	case (argSort, v) if (argSort.thyKind == BG && v.isDomElem) => {
	  //v lifted
	  bgtheory.castTo(v,argSort)
	}
	case _ => throw SyntaxError("ill-sorted term: " + PFunTerm(op, args))
      })
      PFunTerm(op,liftedArgs)
    }
  }

  /** Sometimes the decomposition comes in handy */
  def unapply(t: Term) = t match {
    case Const(op)          ⇒ Some((op, List.empty))
    case PFunTerm(op, args) ⇒ Some((op, args))
    case _                  ⇒ None
  }
}

/**
 * Other view on FunTerm
 */
object FGFunTerm {
  def apply(op: Operator, args: List[Term]) = {
    require(op.kind == FG, println("FGFunTerm application with non-FG operator"))
    if (args.isEmpty) SymConst(op) else PFunTerm(op, args)
  }

  def unapply(t: Term) = t match {
    case FGConst(op)                             ⇒ Some((op, List.empty))
    case PFunTerm(op @ Operator(FG, _, _), args) ⇒ Some((op, args))
    case _                                       ⇒ None
  }
}

object NonDomElemFunTerm {
  def apply(op: Operator, args: List[Term]) =
    if (args.isEmpty) SymConst(op) 
    else PFunTerm(op, args)

  def unapply(t: Term) = t match {
    case _ if (t==TT)        => None
    case SymConst(op)       ⇒ Some((op, List.empty))
    case PFunTerm(op, args) ⇒ Some((op, args))
    case _                  ⇒ None
  }
}

/** Proper functional terms, i.e. not constants. */
case class PFunTerm(op: Operator, args: List[Term]) extends FunTerm {

  require(!args.isEmpty, println("Attempt to construct a PFunTerm without arguments"))

  // Mixin Expression

  def applySubst(sigma: Subst) = 
    if (sigma actsOn this) PFunTerm(op, sigma(args)) else this

  def replace(lhs: FunTerm, rhs: Term) = {
    val argsReplaced = PFunTerm(op, args map { _.replace(lhs, rhs) })
    lhs matcher argsReplaced match {
      case None => 
        if (lhs.asInstanceOf[FunTerm].op == op && lhs.asInstanceOf[FunTerm].args.length == args.length)
          throw GeneralError("let-binder: cannot apply definition %s = %s to atom %s".format(lhs, rhs, this))
        else argsReplaced
      case Some(sigma) => sigma(rhs)
      //case _ => throw InternalError("Unexpected matching result in replace.")
    }
  }

  /*
   * Eliminate all non-linear multiplications s * t by #nlpp(s, t)
   * This is done during preprocessing.
   */
  lazy val elimNonLinearMult = {
    import bgtheory._

    // Calling simplifyBG here prevents terms of the form e.g 3(4x) or (3+4)x having multiplication
    // replaced.
    // val argsElim = args map { _.elimNonLinearMult }
    val argsElim = args map { _.simplifyBG.elimNonLinearMult }
    lazy val defaultRes = PFunTerm(op, argsElim)

    argsElim match {
      // Giving a stronger guard here doesn't prevent the case above because
      // weakAbstraction can apply to give a nonlinear term.
      case List(l, r) if (!l.isDomElem && !r.isDomElem) =>
        // need to check if op is multiplication; if so replace
        op match {
          case LIA.Product.op => PFunTerm(LIA.NLPPOpInt, argsElim)
          case LRA.Product.op => PFunTerm(LRA.NLPPOpRat, argsElim)
          case LFA.Product.op => PFunTerm(LFA.NLPPOpReal, argsElim)
          case _ => defaultRes
        }
      case _ => defaultRes
    }
  }

  def replace(lhs: Atom, rhs: Formula) =
    PFunTerm(op, args map { _.replace(lhs, rhs) })

  lazy val varsCnt = //Term.varsCnt(args)
    args.foldLeft(Map[Var,Int]())( 
      (varsCnt,subterm) => varsCnt ++ (subterm.varsCnt.transform((x,c) => varsCnt.getOrElse(x,0)+c))
    )
  lazy val vars = args.vars
  lazy val weightForKBO = args.foldLeft(KBO.funWeight)((acc, h) ⇒ acc + h.weightForKBO)
  lazy val symConsts = Term.symConsts(args)

  lazy val minBSFGTerms = {
    val argsBSFGTerms = Term.minBSFGTerms(args)
    if (op.kind == FG && sort.thyKind == BG && argsBSFGTerms.isEmpty)
      // this is the minimal B-sorted FG Term we are looking form
      Set[Term](this)
    else
      argsBSFGTerms
  }

  // lazy val ops = opsUnion(args map { _.ops }) + op
  lazy val termIndex = {
    val argsIndex = liftIndex(args map { _.termIndex })
    argsIndex + (op, List(this ->  List.empty))
  }

  def replaceAt(pos: Pos, t: Term) = 
    if (pos.isEmpty) t 
    else 
      replaceAtList(args, pos, t) map { PFunTerm(op, _) } getOrElse { this }

  def thisterm = this
  /** An iterator over just this */
  def thisiterator = new Iterator[Term] {
    // delivers just this
    var gotit = false; def next() = { gotit = true; thisterm }; def hasNext = !gotit
  }
  def subterms = args.foldLeft[Iterator[Term]](thisiterator)((i: Iterator[Term], t: Term) ⇒ i ++ t.subterms)

  /**
   * The depth of a compound term is the max of its arguments plus 1.
   * That is, we take the tree depth
   */
  lazy val depth = (args map { _.depth }).max + 1
}
