package beagle.fol

/* A naive implementation of substitution trees for indexing */

import term._
import beagle.util.Counter
/* Note that this brings type Pos:=List[Int] into scope */
import beagle.datastructures.PMI._
import unification._

/**Indicator vars are used inside indexed substitutions to make
 * variant finding easier.
 */
trait IndicatorVar
class GenIndVar(i: Int, s: Type) extends GenVar("*",i,s) with IndicatorVar
class AbstIndVar(i: Int, s: Type) extends AbstVar("*",i,s) with IndicatorVar

/** Some extra methods for substitutions 
 * @todo move to the Subst file once stable
 */
object SubstAux {

  /** Most specific generalisation.
   * If there exists substitutions mu, t1, t2 such that
   * t1.mu = s1 and t2.mu = s2 and there is no subst l such that
   * l.mu has these properties then mu is the most specific common
   * generalisation.
   */
  def msg(sigma: Subst, rho: Subst): Option[(Subst, Subst, Subst)] = {
    var mAcc = Subst.empty
    var (s1Acc, s2Acc) = (Subst.empty,Subst.empty)

    for((xi,ti) <- sigma.env) {
      (ti,rho(xi)) match {
	case (_, yi: Var) if(!xi.isAbstVar && yi.isAbstVar) => s1Acc+=(xi -> ti) //unless xi is an abstvar
	//TODO- check the following cases for AbstVar
	case (ti,ri) if (ti == ri) => mAcc += (xi->ti)
	case (ti: FunTerm, ri: FunTerm) if(ti.op!=ri.op)=> {
	  s1Acc += (xi -> ti)
	  s2Acc += (xi -> ri)
	}
	case (ti,ri) =>
	  msg(ti,ri,s1Acc,s2Acc) match {
	    case None => return None
	    case Some((cg,s1Up,s2Up)) => {
	      mAcc += (xi -> cg)
	      s1Acc = s1Up
	      s2Acc = s2Up
	    }
	  }
      }	
    }
    return Option((mAcc,s1Acc,s2Acc))
  }
    
  //TODO- make this sort safe
  //is this even possible in sorted logic?
  //the last rule always applies in any case, but for different sorts it cannot apply
  /**
   * @param s first argument term
   * @param t second argument term
   * @param sg1 accumulator/context substitution for s
   * @param sg2 accumulator/context substitution for t
   * @return A triple containing the generalisation term and substitutions that give
   * `s` and `t` respectively when applied to the generalisation term.
   * If no generalisation exists, returns `None`.
   */
  def msg(s: Term, t: Term, sg1: Subst, sg2: Subst): Option[(Term, Subst, Subst)] = 
    if(s.sort!=t.sort) None
    else (s,t) match {
      //is this correct behaviour?
      //case _ if (s == t) => Option((s,sg1,sg2))
      //recursive case
      case (PFunTerm(op1,arg1), PFunTerm(op2,arg2)) if (op1 == op2) => {
	//traverse the list of arguments, collecting msgs as you go
	arg1.zip(arg2).foldLeft(Option(List[Term](),sg1,sg2))({ 
	  case (None, _) => None
	  case (Some((ts,s1,s2)), (a1,a2)) => 
	    msg(a1,a2,s1,s2) map { case (t,sig1,sig2) => (t::ts,sig1,sig2) }
	}) map { 
	  //then build the generalisation term:
	  case (ts,s1,s2) => (PFunTerm(op1,ts.reverse),s1,s2)
	}
      }
      //other cases
      case (x: Var, y) if (x == y && x.isInstanceOf[IndicatorVar]) => Option((s,sg1,sg2))
      case (x: Var, y) if (!x.isInstanceOf[IndicatorVar] && x.sort == y.sort) => 
	if(x.isGenVar || (y.isPureBG)) //one or the other could be an Abst var
	  Option((x, sg1, sg2 + (x->t)) )
	else //x not an AbstVar and y not a DE or AbstVar
	  None
      case _ => 
	//this first bit could be removed
	sg1.dom.intersect(sg2.dom).find(y => sg1(y)==s && sg2(y)==t) map { 
	  (_, sg1, sg2)
	} orElse { 
	  val xj = GenVar("X",s.sort).fresh //xj is a new aux variable
	  Option((xj, sg1 + (xj -> s), sg2 + (xj -> t)))
	}
    }

  /** Normalise a term using indicator variables.
   * This replaces variables with indicators *_i in a consistent way
   * where variables are sorted by first occurrence in a prefix traversal of
   * the term tree.
   */
  def normalise[T <: Term](t: T): T = {
    //take the set of first occurrences of variables in t
    val renaming = 
      Term.allSubtermsWithPos(t).filter(_._1.isVar)
	.toList
	//sort the positions by lex order
	.sortWith((x,y) => SubstTree.lexGtr(y._2,x._2))
	//then collect first occurrences
	.foldLeft(List[Var]())( (acc,v) => if (!acc.contains(v._1)) acc :+ v._1.asInstanceOf[Var] else acc )
	.zipWithIndex
	.map({
	  case (x: GenVar,i) => (x,new GenIndVar(i,x.sort))
	  case (x: AbstVar,i) => (x,new AbstIndVar(i,x.sort))
	})
	.toMap[Var,Term]
    
    //build a substitution to fresh indicator variables
    (new Subst(renaming))(t).asInstanceOf[T] //ok to cast because substitution is a renaming
  }

  /**Notice that this requires a fixed order on variables, so I just use lex order on name (incl sort?)
   * In order to normalise (x1->t1, ... xn->tn), treat it as if it were a term f(t1,...,tn)
   * and normalise that.
   * @todo this introduce a new operator and arity for every normalisation which is not necessary,
   * instead just rewrite the code above.
   */
  def normalise(s: Subst): Subst = {
    //sort env of subst by variable
    val (xs, ts) = 
      s.env.toList
	.sortWith((x,y) => x._1.name < y._1.name)
	.unzip[Var,Term]

    //make a term f(t1,...,tn)
    val tsNorm = normalise(PFunTerm(Operator(FG,"_",new Arity(ts.map(_.sort),Signature.ISort)),ts)).args
    
    //make subst again
    (new Subst(xs.zip(tsNorm).toMap))
  }

}

case class SubstTree(sigma: Subst, omega: Set[SubstTree]) {

  val isLeaf = omega.isEmpty

  // def getCompat(phi: Subst): Set[Term]
  // def getMatchable(query: Term): Set[Term]
  def merge(other: SubstTree): Set[Subst] = ???

  def isVariantNode(s: Subst, rho: Subst): Boolean = 
    sigma.dom.forall( x => s(rho(sigma(x))) == rho(x) && !s.dom.exists(_.isInstanceOf[IndicatorVar]))
  
  //find a variant node
  def existsVarNode(rho: Subst): Option[Subst] = {
    val tgtVars: List[Term] = sigma.dom.filterNot(x => rho(sigma(x))==rho(x)).toList
    //attempt to make rho(sigma(x)) = rho(x) by finding simultaneous unifier of rho(x),rho(sigma(x))*/
    try {
      val s = unifyList(tgtVars.map(rho(_)), tgtVars.map(x => rho(sigma(x))))
      if(s.dom.exists(_.isInstanceOf[IndicatorVar])) None
      else Option(s)
    } catch {
      case UnifyFail => None
    }
  }

  //x in union(I(sigma_i) set minus union(domain(sigma_i)))
  //for root node sigma.intro - sigma.dom

  def insert(rho: Subst): SubstTree = insert(SubstAux.normalise(rho), rho.dom)._1

  def insert(rho: Subst, ov: Set[Var]): (SubstTree, SubstTree) = 
    if(this.isLeaf) {
      if(sigma.isEmpty) { //insertion in an empty tree
	val m = SubstTree(new Subst(rho.env.filterKeys(ov(_))), Set())
	(m,m) 
      }else { //non-empty
	if(existsVarNode(rho).isDefined) (this,this)
	else { //if nothing is in the variant subs
	  val (mu,s1,s2) = SubstAux.msg(sigma,rho).getOrElse( throw new InternalError("No msg found"))
	  val pRest = new Subst(rho.env.filterKeys(x => ov(x) && !sigma.dom(x)))
	  val m = SubstTree(s2 ++ pRest, Set.empty) //notice that the ++ here is a union
	  (SubstTree(mu,Set(SubstTree(s1,omega),m)),m)
	}
      }
    }else {
      val s = existsVarNode(rho)
      if(s.isEmpty){ //non-leaf, dont recurse
	val (mu,s1,s2) = SubstAux.msg(sigma,rho).getOrElse( throw new InternalError("No msg found"))
	val pRest = new Subst(rho.env.filterKeys(x => ov(x) && !sigma.dom(x)))
	val m = SubstTree(s2 ++ pRest, Set.empty)
	(SubstTree(mu,Set(SubstTree(s1,omega),m)),m)
      }else { //non-leaf, must recurse
	val nPrime: SubstTree = SubstTree.selectSubnode(this,s.get++rho)
	//sigma is the variant node...
	val (mPrime,m) = nPrime.insert(s.get++rho, (ov -- sigma.dom) ++ sigma.intro)
	(SubstTree(sigma,(omega-nPrime)+mPrime),m)
      }  
    }

  //note that delta may not be necessary in this context
  def delete(rho: Subst, delta: SubstTree => Boolean): SubstTree = 
    existsVarNode(rho) match {
      case Some(s) if (this.isLeaf) =>
	return if(delta(this)) SubstTree.empty else this
      case Some(s) => {
	val rest = omega.map(_.delete(s compose rho,delta))
	if(rest.isEmpty) return SubstTree.empty
	else if(rest.tail.isEmpty) return SubstTree(rest.head.sigma join sigma, rest.head.omega)
	else return SubstTree(sigma, rest)
      }
      case None => return this
    }

  lazy val substitutions: Set[Subst] = omega.flatMap( _.substitutions ) + sigma
}

object SubstTree {
  val empty = SubstTree(Subst.empty, Set())

  /* Generic search functions */

  //{s | forall xi in dom(sigma). s(rho(sigma(x))) = rho(x) & s is most general}
  val gen: (SubstTree,Subst) => Set[Subst] = 
    (n: SubstTree, rho: Subst) => {
      val dom = n.sigma.dom.toList      
      matchTo(dom.map(x => rho(n.sigma(x))), 
	      dom.map(x => rho(x)),
	      Map[Var,Term]() 
      ) map { 
	Set(_) 
      } getOrElse { 
	Set[Subst]() 
      }
    }

  //{s | forall xi in dom(sigma). s(rho(sigma(x))) = s(rho(x)) & s is most general}
  val uni: (SubstTree,Subst) => Set[Subst] = 
    (n: SubstTree, rho: Subst) => {
      val dom = n.sigma.dom.toList
      try {
	Set(unification.unifyList(dom.map(x => rho(n.sigma(x))),
			      dom.map(rho(_))))
      } catch {
	case UnifyFail => Set[Subst]()
      }
    }

  val inst: (SubstTree,Subst) => Set[Subst] = 
    (n: SubstTree, rho: Subst) => { 
      uni(n,rho) filterNot { _.dom exists { _.isInstanceOf[IndicatorVar]} }
    }

  def search(n: SubstTree, rho: Subst, f: (SubstTree,Subst) => Set[Subst]): Set[SubstTree] = 
    f(n,rho) flatMap { sigma => 
      n.omega.flatMap(search(_, sigma++rho, f)) + n
    }

  //TODO- this heuristic needs to be improved
  //1- must select a variant subnode if such a node exists
  //2- select a subnode with a non-empty msg
  //3- in any other case return empty
  def selectSubnode(n: SubstTree, rho: Subst): SubstTree = { //st.omega.head
    n.omega.find(_.existsVarNode(rho).isDefined) orElse { 
      n.omega.find(m => SubstAux.msg(m.sigma,rho).isDefined) 
    } getOrElse SubstTree.empty
  }

  /* True if l1 > l2 in the lexicographic order */
  def lexGtr(l1: Pos, l2: Pos): Boolean = {
    var (p,q) = (l1,l2)
    while(!p.isEmpty) {
      if(q.isEmpty || p.head > q.head) return true
      else if(p.head < q.head) return false
      else { p=p.tail;q=q.tail }
    }
    return false //p is empty but q is not
  }

  /** A variable is open at a node if it is introduced by the join of substitutions along
   * the path to the node, and not in the domain of any preceding substitution.
   * Otherwise it is closed.
   * All non-indicator variables are closed at leaf nodes.
   */
  def openForN(x: Var, p: List[SubstTree]): Boolean = {
    //for now joins associate to the left
    val pathDom = p.foldLeft(Set[Var]())( _++_.sigma.dom )
    val pathIntro = p.foldLeft(Set[Var]())( _++_.sigma.intro )
    (pathIntro(x) && !pathDom(x)) //or return a set
  }

  //broken:
  def prettyPrint(n: SubstTree) {
    println("| "+n.sigma)
    var just = Map(0 -> 0)
    var stack = n.omega.toList
    var nextStack = List[SubstTree]()
    do {
      var str = ""
      for((s,i) <- stack.zipWithIndex) {
	nextStack=nextStack++(s.omega.toList)
	just+=(i -> str.length)
	str+=("| "+s.sigma+" ")
      }
      println(str)
      stack = nextStack
      nextStack = List[SubstTree]()
    } while(!stack.isEmpty)
     
  }
  
}

object TestApplications {
  import beagle.datastructures._
  //possibly we should have a subst tree for each sort.

  //given a clause find all demodulators in a set of demodulators
  def findDemods(l: Lit, dm: Set[Eqn]) {
    //build an index over the initial set dm
    val x: Map[Type,Var] = dm.map(s => (s.sort, GenVar("X",s.sort).fresh)).toMap
    val idx = dm.foldLeft(SubstTree.empty)((acc,eqn) => acc.insert(Subst(x(eqn.sort)->eqn.lhs)))
    //{lhs,eqn,->} or {lhs,eqn,->}+{rhs,eqn,<-}

    //for each subterm of l (innermost) look up all terms which have that term as an instance
    def inner(t: Term): Term = {
      if(!x.isDefinedAt(t.sort)) t
      else {
      val res = t match {
	case PFunTerm(f,args) => {
	  val newT = PFunTerm(f,args map (inner(_)))
	  //things which have newT as an instance
	  SubstTree.search(idx, Subst(x(t.sort) -> newT), SubstTree.gen)
	}
	case c: VarOrSymConst => SubstTree.search(idx,Subst(x(c.sort) -> c),SubstTree.gen) //things which have c as an instance e.g. c itself or a variable
      }
      println(s"Searching for $t yields:")
      println(res.flatMap(_.substitutions))
      t
      }
    }

    inner(l.eqn.lhs)
    inner(l.eqn.rhs)
  }
}

/*
case object EmptyTree extends SubstTree {
  lazy val substitutions = Set.empty[Subst]
}
case class SubstNode(sigma: Subst, omega: Set[SubstTree]) extends SubstTree {
  assume(omega.size >= 2)
  lazy val substitutions = omega.flatMap( _.substitutions ) + sigma
}
*/
/*
 * Subject to the conditions
 * 1) a node in the tree is either a leaf node (Sig,Empty) or an inner node such that omega.size >= 2
 * 2) for every path (s0,o0), ..., (sn, on) from the root to a leaf node,
 * a) the variables introduced (i.e. those in the co-domain) in the join of all substitutions are indicator variables
 * b) The domains of the substitutions are pairwise disjoint
 */


