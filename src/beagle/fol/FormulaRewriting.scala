package beagle.fol

import beagle._

/**
 * FormulaRewriting creates an object with methods reduceInnermost and reduceOutermost
 * to apply the rules specified with these methods to a given formula, as long as possible.
 * @param A predicate that determines additional base cases (beyond Atoms)  
 */
class FormulaRewriting(baseCase: (Formula => Boolean) = (_ => false)) {

  /**
   * Apply a given list of FormulaRewriteRules, innermost-first order
   */
  def reduceInnermost(f: Formula, rules: FormulaRewriteRules,depth: Int = 0): Formula = {
    // Rewrite the subformulas of f
    if (baseCase(f)) {
      // stopAtPureBG && f.isPureBG)
      // Treat pure BG formulas as atoms for the purpose of CNF
      f
    } else {
      val h = f match {
        case Atom(_, _) => f
        case UnOpForm(op, f) => 
	  UnOpForm(op, reduceInnermost(f, rules,depth+1))
        case BinOpForm(op, f1, f2) => 
	  BinOpForm(op, reduceInnermost(f1, rules,depth+1),
		      reduceInnermost(f2, rules))
        case QuantForm(q, xs, f) => 
	  QuantForm(q, xs, reduceInnermost(f, rules, depth+1))
      }
      // Rule applicable at top-level?
      if (rules isDefinedAt h) {
	// Apply it, and start over
        reduceInnermost(rules(h), rules, depth+1)
      } else
        // No rule is applicable at top level
        // and as all subformulas are rewritten we can stop
        h
    }
  }

  /** Apply an idempotent transformation, e.g. replacing <=> innermost first */
  def reduceOnePass(f: Formula, r: FormulaRewriteRules): Formula = {
    if(baseCase(f)) return f

    val h =
      f match {
	case Atom(_, _) => f
	case UnOpForm(op, f) => 
	  UnOpForm(op, reduceOnePass(f, r))
	case BinOpForm(op, f1, f2) => 
	  BinOpForm(op, reduceOnePass(f1, r),
		    reduceOnePass(f2, r))
        case QuantForm(q, xs, f) => 
	  QuantForm(q, xs, reduceOnePass(f, r))
      }
    if(r isDefinedAt h) 
      return r(h)
    else 
      return h
  }

/*
 * An iterative translation of the above which avoids StackOverflow error.
 * (Probably contains bugs too)
 * Overall the algorithm is inefficient, e.g. PUZ073+1 seems impossible.
 */
/*
  def reduceInnermost(f: Formula, rules: FormulaRewriteRules, depth: Int = 0): Formula = {
    //println("rewriting "+f)
    var currentF = f;
    //a stack of constructors- and side formulas
    var openF: List[(Option[Formula],Formula => Formula)] = Nil
    
    if(baseCase(f)) return f

    def unwrap() {
      //unwrap current, until base case or atom
      while(!baseCase(currentF) && !currentF.isInstanceOf[Atom]) {
	//println("unwrap: currentF="+currentF)
	currentF match {
          //case Atom(_, _) => f
          case UnOpForm(op, f) => {
	    currentF = f
	    openF ::= (None,{ UnOpForm(op, _) })
	    //UnOpForm(op, reduceInnermost(f, rules,depth+1))
          }case BinOpForm(op, f1, f2) => {
	    currentF = f1
	    openF ::= (Some(f2), { BinOpForm(op, _, f2)}) //TODO- how to search RHS?
	  // BinOpForm(op, reduceInnermost(f1, rules,depth+1),
	  // 	      reduceInnermost(f2, rules))
          }case QuantForm(q, xs, f) => {
	    currentF = f
	    openF ::= (None, { QuantForm(q, xs, _) })
	    //QuantForm(q, xs, reduceInnermost(f, rules, depth+1))
	  }
	}
      }
      //println("unwrap: currentF="+currentF)
    }

    def backtrack() {
      //println("back: currentF="+currentF)
      //assume currentF is baseCase or Atom
      //backtrack (pop stack) until a rule is defined or RHS branch open or stack is empty
      while(!(rules isDefinedAt currentF) && !openF.isEmpty && openF.head._1 == None) {
	currentF = openF.head._2(currentF) //backtrack 1 level
	//println("back: currentF="+currentF)
	openF = openF.tail
      }
      if(rules isDefinedAt currentF) { 
	//println("rule application:"+rules(currentF))
	currentF = rules(currentF)
      }else if(!openF.isEmpty && openF.head._1!=None) {
	//println("right branch "+openF.head._1.get)
	openF.head._2(currentF) match {
	  case BinOpForm(op, f1Closed, f2) => {
	    currentF = f2
	    openF = openF.tail
	    openF ::= (None, { BinOpForm(op, f1Closed, _) } )
	    //unwrap() openF is not empty so this will be done immediately
	  }
	  case _ => //oops
	    throw util.InternalError("Unexpected stack structure in formula rewriting")
	}
      }
      //otherwise openF is empty
    }

    do {
      unwrap() //until atom or basecase
      backtrack() //also applies a rule
    } while(!openF.isEmpty)

    return currentF
  }
*/

  /**
   * @param z what to do for an atom.
   * @param g1 how to fold in the subformula of a unary operator.
   * @param g2 how to fold in subformulae of binary operators.
   */
  def foldNoQuantifiers[A](f: Formula, z: Atom => A)(g1: A => A, g2: (A,A) => A): A = f match {
    case a: Atom => z(a)
    case UnOpForm(op,f1) => g1(foldNoQuantifiers(f1,z)(g1,g2))
    case BinOpForm(op,f1,f2) => g2(foldNoQuantifiers(f1,z)(g1,g2),foldNoQuantifiers(f1,z)(g1,g2))
    case _ => throw new Exception("Assumes no quantifiers!")
  }

  /**
   * Apply a given list of FormulaRewriteRules, outermost-first order
   */
  def reduceOutermost(f: Formula, rules: FormulaRewriteRules): Formula = {

    def hReduceOutermost(f: Formula): Option[Formula] =
      if (baseCase(f)) None
      else if (rules isDefinedAt f)
        // apply it, and this is the result
        Some(rules(f))
      else f match {
        case Atom(_, _) => None
        case UnOpForm(op, g) => {
          hReduceOutermost(g) map { UnOpForm(op,_) } 
        }
        case BinOpForm(op, g1, g2) => {
	  hReduceOutermost(g1) map { BinOpForm(op,_,g2) } orElse { hReduceOutermost(g2) map { BinOpForm(op,g1,_) } }
        }
        case QuantForm(q, xs, g) => {
          hReduceOutermost(g) map { QuantForm(q,xs,_) }
        }
        case x => {
          println("should not get " + x)
          None
        }
      }

    // Body of reduceOutermost
    var h = f
    var hReduced = hReduceOutermost(h)
    while (hReduced != None) {
      h = hReduced.get
      hReduced = hReduceOutermost(h)
    }
    f
  }

  /** Apply rule once at each level of the formula */
  def reduceOuterOnePass(f: Formula, r: FormulaRewriteRules): Formula = {
    if(baseCase(f)) return f
    
    (if(r isDefinedAt f) r(f) else f) match {
      case a: Atom => a
      case UnOpForm(op, g) => UnOpForm(op, reduceOuterOnePass(g,r))
      case BinOpForm(op, g1, g2) => BinOpForm(op, reduceOuterOnePass(g1,r), reduceOuterOnePass(g2,r))
      case QuantForm(q, xs, g) => QuantForm(q, xs, reduceOuterOnePass(g,r))
    }
  }

}

object FormulaRewriting {

  // Useful rewritings: 
  val fullRewriting = new FormulaRewriting
  val fgRewriting = new FormulaRewriting(_.isBG) // Stops at BG formulas
  
}


