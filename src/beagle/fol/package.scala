package beagle

import fol.term._

package object fol {

  import util._

  type FormulaRewriteRules = PartialFunction[Formula, Formula]

  /*
   * Global variables and constants
   */

  // Global signature - will be set explicitly by Main 
  //  var Sigma = bgtheory.LIA.signatureLIA //Signature.signatureInt
  var Sigma = Signature.signatureEmpty
  
  /*
 * Predefined terms  
 */
  val TTOp = Operator(FG, "$$true", Arity0(Signature.OSort))
  val TT = term.SymConst(TTOp) 
  //case object TT extends SymConst(TTOp)

  // Important constants
  /*
  val ZeroInt = DomElemInt(0)
  val OneInt = DomElemInt(1)
  val MinusOneInt = DomElemInt(-1)
  val ZeroRat = DomElemRat(RatInt(0))
  val OneRat = DomElemRat(RatInt(1))
  val MinusOneRat = DomElemRat(RatInt(-1))
  val ZeroReal = DomElemReal(RatInt(0))
  val OneReal = DomElemReal(RatInt(1))
  val MinusOneReal = DomElemReal(RatInt(-1))*/
  val LemmaTerm = term.FGConst("lemma", Signature.ISort) // Used in annotations

  /**
   * Implicit conversions
   */

 case class MyExpressionSeq[T](es: Seq[Expression[T]]) {
    def vars = es.foldLeft(Set.empty[Var])(_ ++ _.vars)
    def bgVars = es.foldLeft(Set.empty[Var])(_ ++ _.bgVars)
    def sorts = es.foldLeft(Set.empty[Type])(_ ++ _.sorts)
    def operators = es.foldLeft(Set.empty[Operator])(_ ++ _.operators)
    def symConsts = es.foldLeft(Set.empty[SymConst])(_ ++ _.symConsts)
    def minBSFGTerms = es.foldLeft(Set.empty[Term]) { _ ++ _.minBSFGTerms }
    def maxBSFGTerms = es.foldLeft(Set.empty[Term]) { _ ++ _.maxBSFGTerms }

    def fresh() = {
      val rho = term.Term.mkRenaming(vars)
      es map { rho(_) }
    }

    def freshGenVars() = {
      val rho = term.Term.mkRenamingIntoGenVars(vars)
      es map { rho(_) }
    }
  }

  implicit def toMyExpressionSeq[T](es: Seq[Expression[T]]) = new MyExpressionSeq(es)

  /*
 * Implicits for term lists
 */
  case class MyTermList(ts: List[Term]) {
    def sortsOf = ts map { _.sort }
  }

  implicit def toMyTermList(ts: List[Term]) = new MyTermList(ts)

  class MyVarList(ts: List[term.Var]) {
    /** Remove vars not in `vs` */
    def restrictTo(vs: Set[term.Var]) =
      ts filter { vs contains _ }

    /** Remove vars which are in `vs` */
    def restrictToNot(vs: Set[term.Var]) =
      ts filterNot { vs contains _ }

  }

  implicit def toMyVarList(ts: List[Var]) = new MyVarList(ts)

  case class MyFormulaIterable(fs: Iterable[Formula]) {
    // def sorts(ts: List[Term]) = ts map { _.sort }
    //  def sorts = ts map { _.sort }
    def toAnd = if (fs.isEmpty) TrueAtom else fs.reduceLeft(And(_, _))
    def toOr = if (fs.isEmpty) FalseAtom else fs.reduceLeft(Or(_, _))
  }

  implicit def toMyFormulaIterable(fs: Iterable[Formula]) = new MyFormulaIterable(fs)

}
