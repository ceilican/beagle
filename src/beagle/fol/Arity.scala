package beagle.fol

import beagle._
import util._

/**
 * Arity: The signature of individual function (and predicate) symbols.
 */
case class Arity(argsSorts: List[Type], resSort: Type) {
  val nrArgs = argsSorts.length

  override def toString = printer.arityToString(this)

  def toRat = {
    def sortToRat(s: Type) = if (s == bgtheory.LFA.RealSort) bgtheory.LRA.RatSort else s
    Arity(argsSorts map { sortToRat(_) }, sortToRat(resSort))
  }
}

object Arity0 {
  def apply(r: Type) = new Arity(List.empty, r)
  def unapply(r: Arity) = r match {
    case Arity(Nil, t) ⇒ Some(t)
    case _ ⇒ None
  }
}
object Arity1 {
  def apply(r: (Type, Type)) = Arity(List(r._1), r._2)
  def unapply(r: Arity) = r match {
    case Arity(List(t1), t) ⇒ Some(t1 -> t)
    case _ ⇒ None
  }
}
object Arity2 {
  def apply(r: ((Type, Type), Type)) = Arity(List(r._1._1, r._1._2), r._2)
  def unapply(r: Arity) = r match {
    case Arity(List(t1, t2), t) ⇒ Some((t1, t2) -> t)
    case _ ⇒ None
  }
}
object Arity3 {
  def apply(r: ((Type, Type, Type), Type)) = Arity(List(r._1._1, r._1._2, r._1._2), r._2)
  def unapply(r: Arity) = r match {
    case Arity(List(t1, t2, t3), t) ⇒ Some((t1, t2, t3) -> t)
    case _ ⇒ None
  }
}
