package beagle.VerySimplex
package object standalone {
  
	import transformations._
	import datastructures._
	import datastructures.Relationship._
	import optalgorithms._
	
	def main(args: Array[String]) {
	  val problems = List(
	  		// ARI621=1 has no integer solution
				(Constraint(Polynomial(List(Monomial("#p_norm"), Monomial("x", -1)), 0), Eq, 0),
	  		 List( Constraint(Polynomial(List(Monomial("x", -2)), 3), Geq, 0),
	  				   Constraint(Polynomial(List(Monomial("x", 2)), -3), Geq, 0) )),
	  		// From PUZ133=2.p:
	      (Constraint(Polynomial(List(Monomial("#p_norm"), Monomial("skf_1", -1), Monomial("skf_2", -1), Monomial("n", -1)), 0), Eq, 0),
	  		 List( Constraint(Polynomial(List(Monomial("skf_1", 1)), -1), Geq, 0),
	  			 	   Constraint(Polynomial(List(Monomial("skf_1", -1), Monomial("n"))), Geq, 0),
	  				   Constraint(Polynomial(List(Monomial("skf_2", -1), Monomial("n"))), Geq, 0) )),
	  	  // Does not have an INT solution / loops forever w/o cuts:
	  	  (Constraint(Polynomial(List(Monomial("#p_norm"), Monomial("x1", -2), Monomial("x2", -3), Monomial("x3", -1)), 0), Eq, 0),
	  		 List( Constraint(Polynomial(List(Monomial("x1", 1), Monomial("x2", 1), Monomial("x3"))), Leq, 40),
	  			 	   Constraint(Polynomial(List(Monomial("x1", 2), Monomial("x2"), Monomial("x3",-1))), Geq, Rational(10,3)),
	  				   Constraint(Polynomial(List(Monomial("x2", -1), Monomial("x3"))), Geq, 10) )),
	      // From "An example of two phase simplex method" paper, optlab.mcmaster.ca:
	  		(Constraint(Polynomial(List(Monomial("#p_norm"), Monomial("x1", -2), Monomial("x2", -3), Monomial("x3", -1)), 0), Eq, 0),
	  		 List( Constraint(Polynomial(List(Monomial("x1", 1), Monomial("x2", 1), Monomial("x3"))), Leq, 40),
	  			 	   Constraint(Polynomial(List(Monomial("x1", 2), Monomial("x2"), Monomial("x3",-1))), Geq, 10),
	  				   Constraint(Polynomial(List(Monomial("x2", -1), Monomial("x3"))), Geq, 10) )),
	  		(Constraint(Polynomial(List(Monomial("#p_norm"), Monomial("x1", -1)), 0), Eq, 0),
	  		 List( Constraint(Polynomial(List(Monomial("x1", 1))), Leq, -1))),
	  		(Constraint(Polynomial(List(Monomial("#p_norm"), Monomial("x1", -1), Monomial("x2", 1)), 0), Eq, 0),
	  		 List( Constraint(Polynomial(List(Monomial("x1", 1), Monomial("x2", -1))), Leq, -1) )),
	  		(Constraint(Polynomial(List(Monomial("#p_norm"), Monomial("x1", -1), Monomial("x2", 1)), 0), Eq, 0),
	  		 List( Constraint(Polynomial(List(Monomial("x1", 1), Monomial("x2", -1))), Leq, 1) )),
	  		(Constraint(Polynomial(List(Monomial("#p_norm"), Monomial("x1", -1), Monomial("x2", 1)), 0), Eq, 0),
	  		 List( Constraint(Polynomial(List(Monomial("x1", 1), Monomial("x2", -1))), Lt, 1) )),
	  		(Constraint(Polynomial(List(Monomial("#p_norm"), Monomial("x1", -1), Monomial("x2", 1)), 0), Eq, 0),
	  		 List( Constraint(Polynomial(List(Monomial("x1", 1), Monomial("x2", -1))), Lt, -1) )),
	  		(Constraint(Polynomial(List(Monomial("#p_norm"), Monomial("x1", -1), Monomial("x2", 1)), 0), Eq, 0),
	  		 List( Constraint(Polynomial(List(Monomial("x1", 1), Monomial("x2", -1))), Lt, -1),
	  				 	 Constraint(Polynomial(List(Monomial("x1", 1))), Lt, 10),
	  		       Constraint(Polynomial(List(Monomial("x2", 1))), Gt, 51) )),
	  	  // From "8 The Two-Phase Simplex Method" paper:
	  		(Constraint(Polynomial(List(Monomial("#p_norm"), Monomial("x1", 6), Monomial("x2", 3)), 0), Eq, 0),
	  		 List( Constraint(Polynomial(List(Monomial("x1", 1), Monomial("x2", 1))), Geq, 1),
	  			 	   Constraint(Polynomial(List(Monomial("x1", 2), Monomial("x2", -1))), Geq, 1),
	  				   Constraint(Polynomial(List(Monomial("x2", 3))), Leq, 2) )),
	  		// Unbounded problem. From http://www.mathstools.com/section/main/Unbounded_Simplex_sample
	  	  (Constraint(Polynomial(List(Monomial("#p_norm"), Monomial("x1", -1), Monomial("x2", -1)), 0), Eq, 0),
	  		 List( Constraint(Polynomial(List(Monomial("x1", -1), Monomial("x2", 1))), Leq, -1),
	  				   Constraint(Polynomial(List(Monomial("x1", -1), Monomial("x2", -1))), Leq, -2) )),
	  		(Constraint(Polynomial(List(Monomial("#p_norm"), Monomial("x1", -1)), 0), Eq, 0),
	  		 List( Constraint(Polynomial(List(Monomial("x1", -1))), Leq, -1) )),
	  		// From de Moura paper:
	  		(Constraint(Polynomial(List(Monomial("#p_norm"), Monomial("x1", -1), Monomial("x2", -1)), 0), Eq, 0),
	  		 List( Constraint(Polynomial(List(Monomial("x1", -3), Monomial("x2", 3))), Leq, -1),
	  			 	   Constraint(Polynomial(List(Monomial("x1", 3), Monomial("x2", -3))), Leq, 2) ))
	  		)
	  
//	  		val result = Rational(-8,3).additiveNormalForm
//	  println("result: " + result.numer + " / " + result.denom)
//	  		exit(0)
	  		
	 // test(problems.reverse.tail.reverse)
	  //test(problems.take(problems.length - 1))
test(problems)
//	 test(problems, 2)
	}

	// If you only want a specific test to run
	def test(problems: List[(Constraint, List[Constraint])], problemNumber: Int): Unit =
	  test(List(problems(problemNumber)))
	
	// Run all test problems
	def test(problems: List[(Constraint, List[Constraint])]): Unit = {
	  problems match {
	    case ((o,c)::Nil) =>
	      val cons = c.map(_.replaceVars)
	      val objFn = o.replaceVars
	      println("***********************************************************************************************************************")
			  println("max " + objFn + ", subject to:")
				cons.foreach(c => println("    " + c))
						
			  println("Starting simplex solver: ")
			  val simplexSolver = new Simplex(cons, objFn)
			  val sol = simplexSolver.solve
	      println("")

			  if (sol.feasible(cons)) print("[OK] Solution found: ");
			  else print("[ERR] No solution! ");
	      println("Values: " + sol.getValues)
	      sol.feasible(cons)
	      // exit(0)
	      // --------------------------------------------------------------------------------------------------------------------------------
	      
	      println("\nStarting branch and bound: ")
	      val bnbSolver = new BnB(cons, objFn)                                                                                                                          
	      if (!bnbSolver.go)
	      	println("[ERR] No solution!");                                                                                                                            
	  		else
  				println("[OK] Solution found: ")
		    
	    case (x::xs) =>
	      test(List(x))
	      test(xs)
	  }
	}

}
