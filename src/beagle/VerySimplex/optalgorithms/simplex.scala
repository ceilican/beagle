package beagle.VerySimplex.optalgorithms

import beagle.VerySimplex._
import datastructures._
import transformations._

// TODO: isUnbounded exists, in case we need to read the results of
// an unbounded matrix differently, and therefore offer an alternative
// feasible-method.  This is, sort of, work in progress.

class SolutionMatrix(val matrix: Matrix, val isUnbounded: Boolean = false)
{
  def feasible(cons: List[Constraint]) = matrix.feasible(cons)
	def getValues = matrix.getValues
}

class Simplex(val cons: List[Constraint], val obj: Constraint, val onIntegers: Boolean = false) 
{
  implicit def matrix2Array(m: Matrix) = m.tabl
  implicit def int2SymNum(i: Int) = SymNum(i)

	// Return which column in the objective function contains the most negative coefficient.
	// 
	// Specifically: 
	// Some(n) is returned where n means "n-th column", or None, if the solution is already optimal.

	private def minColumnIdx(objRow: Array[SymNum]): Option[Int] = {
  	val min = objRow.dropRight(1).min
		if (min >= 0) None
		else Some(objRow.indexOf(min)) 
	}

  // Returns either the smallest positive number, or the least negative.
  
  def leastNegative(n: Array[SymNum]): SymNum = {
    val posN = n.filter(_ >= 0)
    if (posN.length > 0)
      return posN.min
    throw new CustomException("No least negative element found.")
  }
  
	// Returns the index of the row which hosts the pivot element.
  // Returns -1 if no such row can be determined.
  
  private def minRatioRowIdx(tabl: Array[Array[SymNum]], column: Int): Int = {
  	val ratios = tabl.tail.map(row => 
  	  if (row(column) > 0) row.last/row(column)
  	  else SymNum(-1))
  	  
	  try { 
	  	return 1 + ratios.indexOf(leastNegative(ratios))
	  } 
		catch {
	  	case ce: CustomException => 
	  	  // System.err.println("VerySimplex-info: solution is unbounded")
	  	  return -1
	  }
  }
  
	// This method takes the row containing the pivot element, whose value is 1,
	// and another row where the element at the index of the pivot element
	// should be made 0.  (In other words, this is performing Gaussian
	// elimination.)  It returns the new row, where the element at the
	// position of the pivot is now 0.
	
	private def eliminate(dstRow: Array[SymNum], pivotRow: Array[SymNum], pivotIdx: Int): Array[SymNum] = {
		val factor = (dstRow(pivotIdx) * Rational(-1)) / pivotRow(pivotIdx)
		dstRow.zip(pivotRow).map { case (a, b) => factor * b + a }
	}
	
	// This assumes that all constraints, including the objective function
	// have been put into normal form first!
	// 
	// We assume that the first row contains the objective function!

	def solve(): SolutionMatrix = {
		def solve2(tabl: Matrix): SolutionMatrix = {
			// tabl.print
		  minColumnIdx(tabl.head) match {
			  case None => new SolutionMatrix(tabl, false)
			  case Some(pivotColIdx) =>
			    val pivotRowIdx = minRatioRowIdx(tabl, pivotColIdx)
			    if (pivotRowIdx < 0) {
			      return new SolutionMatrix(tabl, true)			      
			    } 
			    val pivotElem   = tabl(pivotRowIdx)(pivotColIdx)
			    val newPivotRow = tabl(pivotRowIdx).map(_ / pivotElem)
			    tabl(pivotRowIdx) = newPivotRow
			    
			    // Gaussian elimination on remaining rows
			    for (i <- 0 to tabl.size - 1) {
			      if (i != pivotRowIdx)
			        tabl(i) = eliminate(tabl(i), newPivotRow, pivotColIdx)
			    }
			    
			    solve2(tabl)
			}
		}
		
		// Computes factor * r1 + r2
		def addRows(factor: Int, r1: Array[SymNum], r2: Array[SymNum]): Array[SymNum] =
			r1.zip(r2).map { case (a, b) => factor * a + b }

		var strictInequalitiesRemoved = rmStrictIneq(cons, onIntegers)
		
    var normalisedCons =
      insertSlackVars((obj :: strictInequalitiesRemoved).map(_.remConstants))  // ******* Objective function is first row *******
		
		var allVars = normalisedCons.flatMap(_.vars).distinct  // All variables in the list of constraints
		  
	  // col is a mapping which is from variables, represented as strings, 
	  // to the respective column index in the matrix that they are associated
	  // to, represented as integer.
		var col = allVars.map(v => (v, allVars.indexOf(v))).toMap
		
		var matrix = toMatrix(normalisedCons, col) 

		// Check if we need two-phase approach
		if (!matrix.feasible(cons)) {
			// System.err.println("VerySimplex-info: initial matrix not feasible. Have to switch to two-phase approach.")
		  
		  // Transform constraints and matrix
		  normalisedCons = addArtificialVars(normalisedCons.tail)
		  normalisedCons = addArtificialObjective(normalisedCons) 
		  
		  allVars = normalisedCons.flatMap(c => c.vars).distinct
		  val artifVars = allVars.filter(v => v.startsWith("#artif_"))
		  col = allVars.map(v => (v, allVars.indexOf(v))).toMap
		  matrix = toMatrix(normalisedCons, col)

		  // println("New matrix:")
		  // matrix.print

		  // We now need to get the artificial variables into the basis.
	    artifVars.foreach { v =>
	      matrix.rows(v).foreach { i =>
	        if (i != 0) {
		        matrix(0) = addRows(-1, matrix(i), matrix(0))
	        }
	      }
	    }

		  // println("Priced out artif. obj. function:")
		  // matrix.print
		  
		  // Now solve Phase I
			val sol = solve2(matrix)
			
			// If matrix infeasible, there is no solution, return
			if (!sol.feasible(cons))
				return sol
			// We can stop right here in case we're not solving integer problems,
		  // to avoid multiple evaluations of the same problem.  We're not interested
			// in optimisation, but in solutions!
			else if (!onIntegers)
			  return sol

			// println("Solution of Phase I:")
			// matrix.print

			// Now, re-add original objective and throw out all things artificial...
			var newCons = sol.matrix.toCons
			
			// First, remove artificial vars as they have to be 0 anyway after Phase I
			newCons = newCons.map(_.removeMonomials("#p_artif" :: artifVars))
			newCons = obj :: newCons
			allVars = newCons.flatMap(c => c.vars).distinct
			col = allVars.map(v => (v, allVars.indexOf(v))).toMap
			matrix = toMatrix(newCons, col)
			
			// println("Removed artif. vars., i.e., matrix for Phase II before price out:")
			// matrix.print
		  
			// Second, price out old objective function, 
		  val objFnVars = obj.removeMonomials(List("#p_norm")).vars
			objFnVars.foreach { v =>
	      val rows = matrix.rows(v).filter(i => i != 0)
	      if (rows.size == 1 && matrix.objFn.vars.contains(v)) {
	        val row = matrix(rows.head)
	        val factor = -(matrix(0)(matrix.col(v)).value.numer / row(matrix.col(v)).value.numer)
	        matrix(0) = addRows(factor, row, matrix(0))
	      }
			}
			
			// println("Matrix for Phase II after price out:")
			// matrix.print
		}
		
		// Solve Phase II (or Phase I, if there wasn't in fact a Phase I before...)
		val sol = solve2(matrix)
		// println("Solution matrix:")
		// sol.matrix.print
		sol
	}
}
