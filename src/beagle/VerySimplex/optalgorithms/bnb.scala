// Branch and bound implementation for integer problems

package beagle.VerySimplex.optalgorithms

import beagle.VerySimplex._
import datastructures._
import Relationship._
import transformations._

case class BnBSolutionException(smth: Matrix) extends Exception
case class BnBNoSolutionException(smth: Matrix) extends Exception

class BnB(private val consOrig: List[Constraint], private val objFn: Constraint)
{  
  // *** Main method to call ***
	def go = relax(consOrig, objFn)
	
	private def bnbConstraints(vName: String, sMatrix: Matrix, gCuts: List[Constraint] = Nil): (List[Constraint], List[Constraint]) = {
	  val valOfVar: SymNum = sMatrix.getValues(vName)
	  
	  if (valOfVar.floor != 0) {
	    val cons1 = sMatrix.toCons ::: gCuts ::: List(Constraint(Polynomial(List(Monomial(vName))), Leq, valOfVar.floor))
	    val cons2 = sMatrix.toCons ::: gCuts ::: List(Constraint(Polynomial(List(Monomial(vName))), Geq, valOfVar.ceil))
	    (cons1, cons2)
	  }
	  else {
	  	val cons1 = sMatrix.toCons ::: gCuts ::: List(Constraint(Polynomial(List(Monomial(vName))), Eq, valOfVar.floor))
	    val cons2 = sMatrix.toCons ::: gCuts ::: List(Constraint(Polynomial(List(Monomial(vName))), Geq, valOfVar.ceil))
	    (cons1, cons2)
	  }
	}
	
	// Assumes that cons is "normalised" as above
	private def relax(cons: List[Constraint], objFn: Constraint): Boolean = {
	  val ss       = new Simplex(cons, objFn, true)
	  val sol      = ss.solve
	  var sMatrix  = sol.matrix	  
	  var fracVars = sMatrix.getValues.keys.filter { v =>
			val varVal = sMatrix.getValues(v)
		  !varVal.isInt // && varVal >= 0 && !(v.startsWith("#slack") || v.startsWith("#artif") || v.startsWith("#p_"))
	  }
	  
	  if (fracVars.isEmpty && sMatrix.feasible(cons)) {
	  	true
	  }
	  else {
	  	if (fracVars.isEmpty || !sMatrix.feasible(cons)) {
	  	  return false
	  	}
 	
	  	// Compute Gomory cuts if any
	  	val gCuts: List[Constraint] =
	  	  if (sol.isUnbounded) {
	  	  	Nil 
	  	  }
	  	  else { 
	  	  	sMatrix.cutableCons.flatMap(_.getCut).distinct // .filter(cutHasBasicVar(_, sMatrix))
	  	    // sMatrix.cutableCons.map(_.getCut).flatten.filter(c => c.vars.filter(v => v.startsWith("#artif")).size == 0)
	  	  }
	  	
	  	if (gCuts.size != 0) {
	  		// gCuts = List(gCuts.head) // We only add one cut per run! I read that you should not add more than one, although I can't see why.
	  	  // println("---------------8<--------------\nGOMORY CUTS: " + gCuts.toString + "\n---------------8<--------------")
	  	}
	  	  
	  	// Only branch on original problem variables
	  	fracVars = fracVars.filter(v => !(v.startsWith("#slack") || v.startsWith("#artif") || v.startsWith("#p_")))
	  	fracVars = fracVars.filter(v => sMatrix.getValues(v) >= 0)
	  	if (fracVars.isEmpty) 
	  		return false

	    // Find most fractional variable to branch on
	  	var prevVar = fracVars.head
	    var prevVal = sMatrix.getValues(prevVar) 
	    for (v <- fracVars.tail) {
	    	val value = sMatrix.getValues(v)
	      val fracSize: SymNum = (value - SymNum(value.toInt)).abs
	      if (fracSize > prevVal) {
	        prevVal = fracSize
	        prevVar = v
	      }
	    }  
	  	
	  	val (prob1, prob2) = bnbConstraints(prevVar, sMatrix, gCuts)
	  	val valOfVar: SymNum = sMatrix.getValues(prevVar)
	  	// println("Branching on " + prevVar + "=" + valOfVar + " in terms of " + valOfVar.floor + " and " + valOfVar.ceil + " (Matrix size: " + sMatrix.size + ")")
	  	relax(prob1, sMatrix.objFn) || relax(prob2, sMatrix.objFn)
	
//	    for (v <- fracVars) {
//	      val (prob1, prob2) = bnbConstraints(v, sMatrix, gCuts)
//	      
//	      val valOfVar: SymNum = sMatrix.getValues(v)
//  	    println("Branching on " + fracVars.head + "=" + valOfVar + " in terms of " + valOfVar.floor + " and " + valOfVar.ceil + " (Matrix size: " + sMatrix.size + ")")
//	      
//  	    if (relax(prob1, sMatrix.objFn) || relax(prob2, sMatrix.objFn))
//	        return true
//	      else
//	      	return false
//	    }
//	  	return false
	  }
	}
}
