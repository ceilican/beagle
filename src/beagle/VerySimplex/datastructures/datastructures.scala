package beagle.VerySimplex.datastructures

import Relationship._

class CustomException(msg: String) extends RuntimeException(msg)

// ***********************************************************
// Rationals
// ***********************************************************

case class Rational(private val top: Int, private val bot: Int) extends Ordered[Rational] 
{
	require(bot != 0)

	def this(n: Int) = this(n, 1)
	
	private def gcd(a: Int, b: Int): Int =
	  if (b == 0) a else gcd(b, a % b)
	  
  private val g = gcd(top.abs, bot.abs)

  val (numer, denom) = if (bot < 0) (-top/g, -bot/g) else (top/g, bot/g)                                                                                                                                                      

  def compare(r: Rational): Int = {
    if (numer * r.denom < r.numer * denom) -1
    else if (numer * r.denom > r.numer * denom) 1
    else 0
  }
	
	def floor: Int = (numer.toFloat / denom.toFloat).floor.toInt
	def ceil: Int  = (numer.toFloat / denom.toFloat).ceil.toInt
	
	def toInt = (numer / denom).toInt
	
	def isInt = denom.abs == 1
	
	// Operations and comparisons on Rational
	def * (r: Rational) = Rational(numer * r.numer, denom * r.denom)
	def / (r: Rational) = Rational(numer * r.denom, denom * r.numer)
	def + (r: Rational) = Rational(numer * r.denom + r.numer * denom, denom * r.denom)
	def - (r: Rational) = Rational(numer * r.denom - r.numer * denom, denom * r.denom)
	def ==(r: Rational) = numer * r.denom == r.numer * denom
	def !=(r: Rational) = !(this == r)
	
	// Int operations and comparisons
	def / (i: Int) = this * Rational(1, i)
	def * (i: Int) = Rational(numer * i, denom)
	def ==(i: Int) = numer == i && denom == 1 || numer == 0 && i == 0
	def !=(i: Int) = !(this == i)
	def < (i: Int) = numer < i * denom
	def > (i: Int) = numer > i * denom
	def <=(i: Int) = this < i || this == i
	def >=(i: Int) = this > i || this == i
	def - (i: Int):Rational = this - Rational(i,1)
	def + (i: Int):Rational = this + Rational(i,1)
	  
	override def toString = 
	  if (numer == 0)
	    "0"
	  else if (denom == 1)
	    numer.toString
	  else
	    numer + "/" + denom
	    
	def toDouble = numer / denom

  // Brings the Rational first in "additive normal form" we need for Gomory cuts,
	// and then leaves out the integral part entirely.
  def additiveNormalForm: Rational = {
	  val intPart  = toDouble.toInt
	  val fracPart = this - intPart
	  
	  if (fracPart >= 0)
	  	fracPart
	  else 
	    Rational(fracPart.denom.abs - fracPart.numer.abs, fracPart.denom.abs)
  }
}

object Rational {
	def apply(x: Int) = new Rational(x, 1)
}

// ***********************************************************
// Symbolic rationals
// ***********************************************************

case class SymNum(val value: Rational, val k: Rational) extends Ordered[SymNum]
{
  def this(x: Int) = this(Rational(x, 1), Rational(0, 1))
  def this(x: Int, y: Int) = this(Rational(x, 1), Rational(y, 1))
  def this(x: Rational) = this(x, Rational(0,1))
 
  // See description of this method on Rational.
  def additiveNormalForm = SymNum(value.additiveNormalForm, k) 
    
  // Comparisons on Rational
	def ==(y: Rational) = value == y && k == 0
	def !=(y: Rational) = !(this == y)
  def > (y: Rational) = value > y || value == y && k > 0
  def < (y: Rational) = value < y || value == y && k < 0
  def >=(y: Rational) = this > y || this == y
  def <=(y: Rational) = this < y || this == y

	def ==(y: Int) = value == y && k == 0
  def !=(y: Int) = !(this == y)
  def > (y: Int) = value > y || value == y && k > 0
  def < (y: Int) = value < y || value == y && k < 0
  def >=(y: Int) = this > y || this == y
  def <=(y: Int) = this < y || this == y

  def abs = if (this < 0) this * SymNum(-1) else this
  
  def toInt = value.toInt
    
  def ceil: Int  = 
    if (!value.isInt)
      value.ceil
    else if (value.isInt && k > 0) 
      value.ceil + 1
    else // if (value.isInt && k <= 0)
      value.ceil
      
  def floor: Int =
    if (!value.isInt)
      value.floor
    else if (value.isInt && k >= 0) 
    	value.floor
    else // if (value.isInt && k < 0)
      value.floor - 1

  def isInt = k == 0 && value.isInt
  
  // Operations on Rational
  def / (y: Rational)  = 
    if (y != 0) SymNum(value / y, k / y)
    else throw new CustomException("Operation / failed: " + this + " / " + y.toString)
	def * (y: Rational)  = SymNum(value * y, k * y)
	  
	// Operations and comparison on SymNum
	def *(y: SymNum): SymNum = 
	  if (y.k == 0) this * y.value
	  else if (k == 0) y * value
	  else throw new CustomException("Operation * failed: "  + this + " * " + y.toString)
	def /(y: SymNum): SymNum = 
	  if (y.k == 0) this / y.value
	  else throw new CustomException("Operation / failed: " + this + " / " + y.toString)
	def +(y: SymNum) = SymNum(value + y.value, k + y.k)
	def -(y: SymNum) = SymNum(value - y.value, k - y.k)
	def ==(y: SymNum) = value == y.value && k == y.k
	def !=(y: SymNum) = !(this == y)
	def compare(y: SymNum): Int = {
    if (value < y.value || value == y.value && k < y.k) -1
    else if (value > y.value || value == y.value && k > y.k) 1
    else 0
	}
  
  override def toString = 
    if (k != 0) "(" + value.toString + ", " + k.toString + "*\u03b4)" else value.toString

  implicit def int2SymNum(i: Int) = new SymNum(i)
} 

object SymNum 
{
	def apply(x: Int) = new SymNum(x)
	def apply(x: Int, y: Int) = new SymNum(x, y)
	def apply(r: Rational, y: Int) = new SymNum(r, Rational(y))
	def apply(r: Rational) = new SymNum(r, Rational(0,1))
}

object SymInt
{
	def unapply(v: SymNum): Option[Int] = if (v.isInt) Some(v.value.numer) else None
}

// ***********************************************************
// Monomial
// ***********************************************************

case class Monomial(val variable: String, val coeff: SymNum) 
{
	def this(v: String) = this(v, SymNum(Rational(1,1)))
	def this(v: String, i: Int) = this(v, SymNum(Rational(i,1)))
  
	def inverse = Monomial(variable, coeff * SymNum(-1))
	
	// Uses the variable mapping and returns the value of the monomial
	def eval(varMap: Map[String, SymNum]) = varMap(variable) * coeff
	
  override def toString = 
    if (coeff == 1) 
      variable.toString  
    else if (coeff == -1) 
      "-" + variable.toString  
    else 
      coeff.toString + variable.toString 
}

object Monomial 
{
	def apply(v: String) = new Monomial(v, SymNum(Rational(1,1)))
	def apply(v: String, x: Int) = new Monomial(v, SymNum(Rational(x,1)))
	def apply(v: String, x: Rational) = new Monomial(v, SymNum(x))
}

// ***********************************************************
// Polynomial
// ***********************************************************

// Given a variable, x, k is really just short for k * x^0.
// So a polynomial is really only a list of monomials, but
// simplex doesn't care for x^0, so we treat k separately.

case class Polynomial(val ms: List[Monomial], val k: SymNum) 
{
  def isConstant = ms.size == 0
  
  // def addMonomial(m: Monomial) = Polynomial(ms ::: List(m), k) // TODO, that should be faster than inserting an item in the front?!
  def addMonomial(m: Monomial) = Polynomial(m :: ms, k) // TODO, that should be faster than inserting an item in the front?!
  def vars = ms.map(m => m.variable)                           // TODO, that should be a distinct list!

  def inverse = Polynomial(ms.map(m => m.inverse), k * SymNum(-1))
  
  // Return coefficient for variable, or 0 if no such variable exists.
  def coeff(variable: String): SymNum = {
    ms.find(_.variable == variable) match {
  	  case None    => SymNum(0)
  	  case Some(m) => m.coeff
  	}
  }
  
  override def toString = {
    if (isConstant)
      k.toString
    else {
    	if (k != 0)
    		ms.mkString(" + ") + " + " + k.toString
    	else
    	  ms.mkString(" + ")
    }
  }

  // Returns the value of the polynomial wrt. variable mapping
  def eval(varMap: Map[String, SymNum]) = {
    if (ms.length > 0)
    	ms.map(_.eval(varMap)).reduceLeft((x,y) => x + y) + k
    else
    	k
  }
  
  def isEmpty = ms.size == 0 && k == 0
      
  // Keeps only the fractional part of the coefficients around.
  // Needed for Gomory cuts.
  def dropIntegers = {
    def drop(ms: List[Monomial]): List[Monomial] = {
      ms match {
        case Nil =>
          Nil
        case m::ms =>
          if (m.coeff.additiveNormalForm != 0)
          	Monomial(m.variable, m.coeff.additiveNormalForm) :: drop(ms)
          else
            drop(ms)
      }
    }

//    def drop(ms: List[Monomial]): List[Monomial] = {
//      ms match {
//        case Nil =>
//          Nil
//        case m::Nil => 
//          if (m.coeff.additiveNormalForm != 0)
//          	List(Monomial(m.variable, m.coeff.additiveNormalForm))
//          else
//            Nil
//        case m::ms =>
//          drop(List(m)) ::: drop(ms)
//      }
//    }

    if (k == 0 && ms.size != 0)
    	Polynomial(drop(ms), k)
    else if (k != 0 && ms.size == 0)
      Polynomial(Nil, k.additiveNormalForm)
    else
    	Polynomial(Nil, k.additiveNormalForm)
  }
  
  // Replaces variable x by x_1 - x_2
  def replaceVars = {
  	val m1 = ms.flatMap(m => 
  	  if (m.variable.startsWith("#p_")) 
  	    List(m) 
  	  else 
  	    List(Monomial(m.variable + "_1", m.coeff), Monomial(m.variable + "_2", SymNum(-1) * m.coeff)))
  	Polynomial(m1, k)
  }
//  def replaceVars = {
//  	val m1 = ms.map(m => 
//  	  if (m.variable.startsWith("#p_")) 
//  	    List(m) 
//  	  else 
//  	    List(Monomial(m.variable + "_1", m.coeff), Monomial(m.variable + "_2", SymNum(-1) * m.coeff))).flatten
//  	Polynomial(m1, k)
//  }
}

object Polynomial 
{
  def apply(i: Int) = new Polynomial(List(), SymNum(i))
  def apply(r: Rational) = new Polynomial(List(), SymNum(r))
  def apply(s: SymNum) = new Polynomial(List(), s)
	def apply(m: List[Monomial], g: Int) = new Polynomial(m, SymNum(g))
	def apply(m: List[Monomial], r: Rational) = new Polynomial(m, SymNum(r))
	def apply(m: List[Monomial]) = new Polynomial(m, SymNum(0))
  //  def unapply(i: Int): Option[Polynomial] = Some(Polynomial(i))
 }

// ***********************************************************
// Constraint
// ***********************************************************

case class Constraint(val a: Polynomial, val rel: Relationship, val b: Polynomial) 
{
  lazy val vars = a.vars ::: b.vars   // TODO, that should be a distinct list!

  // Returns name of slack var
  def slackVar: Option[String] = {
    val sVars = vars.filter(v => v.startsWith("#slack_"))
    if (sVars.length > 0)
      Some(sVars.head)
    else
      None
  }

  // See Polynomial
  def replaceVars = Constraint(a.replaceVars, rel, b.replaceVars)
  
  // Removes all monomials from constraint that contains variable "del"
  def removeMonomials(del: List[String]) =
  	Constraint(Polynomial(a.ms.filter(m => !del.contains(m.variable))), rel, b)
  
  def inverse = rel match {
    case Eq => Constraint(a.inverse, Eq, b.inverse)
    case _  =>      
      throw new CustomException("Cannot invert non-equality constraint: " + this.toString)
  }
    
  // Check if constraint is satisfied by the variable mapping varMap
  def sat(varMap: Map[String, SymNum]): Boolean = {
    rel match {
      case Leq => 
        a.eval(varMap) <= b.eval(varMap)
      case Geq => 
        a.eval(varMap) >= b.eval(varMap)
      case Eq  =>
        a.eval(varMap) == b.eval(varMap)
      case Lt  => 
        a.eval(varMap)  < b.eval(varMap)
      case Gt  => 
        a.eval(varMap)  > b.eval(varMap)
    }
  }
  
  // Returns coefficient for variable (i.e., 0 if no such variable exists).
  def coeff(variable: String): SymNum = {
    (a.coeff(variable), b.coeff(variable)) match {
      case (SymInt(y), x) if (y == 0) => x
      case (x, SymInt(y)) if (y == 0) => x
      case _ => SymNum(0)
    }
  }
  
  // If the RHS of an equality is negative, invert constraint.
  def rmNegRHS: Constraint = {
  	val constant = b.k
    if (b.isConstant && constant < 0)
    	return this.inverse
    this
  }
  
  // Turns the constraint into normal form using a slack variable, slackvar.
  def insertSlackVar(slackvar: String) = {
    if (rel == Leq) {
    	if (a.isConstant)
    		Constraint(b.addMonomial(Monomial(slackvar, -1)), Eq, a).rmNegRHS
    	else if (b.isConstant)
    		Constraint(a.addMonomial(Monomial(slackvar)), Eq, b).rmNegRHS
    	else
    	  throw new CustomException("Normalisation failed due to unexpected constraint format: " + this.toString)
  	}
    else if (rel == Geq) {
    	if (a.isConstant)
    		Constraint(b.addMonomial(Monomial(slackvar)), Eq, a).rmNegRHS
    	else if (b.isConstant)
    		Constraint(a.addMonomial(Monomial(slackvar, -1)), Eq, b).rmNegRHS
    	else
    	  throw new CustomException("Normalisation failed due to unexpected constraint format: " + this.toString)
    }
    else if (rel == Eq)
      this.rmNegRHS
    else
      throw new CustomException("Normalisation failed due to unexpected constraint format: " + this.toString)
  }

  // Puts an inequality like 0 < a - 2 into the form 2 < a, or 0 < -a + 2 into -2 < -a, and so forth.
  // (This is necessary so that slack variables can be added easier.)
  def remConstants = {
    if (a.isConstant)
      Constraint(Polynomial(a.ms, a.k - b.k), rel, Polynomial(b.ms))
    else if (b.isConstant)
      Constraint(Polynomial(a.ms), rel, Polynomial(b.ms, b.k - a.k))
    else 
    	throw new CustomException("Normalisation failed due to unexpected constraint format: " + this.toString)
  }

  // Get a candidate-cut, doesn't check yet, however, whether it's
  // a real one as it's not clear if it contains a basic variable.
  // IMPORTANT!! Assumes that rel = Eq and that RHS is not an integer.
  // Otherwise it makes little sense to call this method.
  def getCut: Option[Constraint] = {
    assume(rel == Eq && b.isConstant && !b.k.isInt)
    val lhs = a.dropIntegers
    if (!lhs.isEmpty)
    	Some(Constraint(lhs, Geq, b.dropIntegers))
    else
      None
  }
  
  override def toString = a.toString + " " + rel + " " + b.toString
}

object Constraint 
{
	def apply(i: Int, r: Relationship, u: Polynomial) = new Constraint(Polynomial(i), r, u)
	def apply(u: Polynomial, r: Relationship, i: Int) = new Constraint(u, r, Polynomial(i))
	def apply(u: Polynomial, r: Relationship, i: Rational) = new Constraint(u, r, Polynomial(List(), i))
}
