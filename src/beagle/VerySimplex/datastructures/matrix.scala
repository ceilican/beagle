package beagle.VerySimplex.datastructures
import scala.collection.mutable.ListBuffer
// tabl is the actual matrix with objective function as first row;
// col is a mapping from variable names to columns.

case class Matrix(var tabl: Array[Array[SymNum]], var col: Map[String, Int])
{
  val vars = col.keys.toList

  // varInCol(2) returns "x1" for example (i.e., the inverse of col)
  
  def varInCol = col.map(_.swap)

  // Returns all rows that contain an entry for variable v
  
  def rows(v: String): List[Int] = {
    var resl: List[Int] = List()
  	for (i <- 0 to tabl.size - 1) {
  	  val row = tabl(i)
  	  if (!(row(col(v)) == SymNum(0)))
  	    resl = i :: resl
  	}
    resl
  }

  implicit def int2SymNum(i: Int) = SymNum(i)

  def head = tabl.head
  def size = tabl.size
  def tail = tabl.tail
  
	// Get the value of variable from solution matrix (or any matrix, really).

	def getVal(variable: String): SymNum = {
		val coeffValuePairs = tabl.map(row => (row(col(variable)), row.last))
		coeffValuePairs.filter{ case (x,y) => x != 0 } 
		match {
	  	case Array((x,y)) => x * y
	  	case _            => 0    // Non-basic var
		}
  }

  // Returns true if the variable has zeros above and below in the matrix
  // which means it could be basic, given that its value is positive and
  // we don't choose another positive value in the same row via some other
  // constraint.
  
	def isPotentiallyBasic(variable: String): Boolean = {
	  val coeffValuePairs = tabl.map(row => (row(col(variable)), row.last))
		coeffValuePairs.filter{ case (x,y) => x != 0 } 
		match {
	  	case Array((x,y)) => true
	  	case _            => false // Non-basic var
		}
	}
	
	def varsInRow(row: Array[SymNum]): List[String] = {
	  var result: List[String] = Nil
	  for (i <- 0 to row.size - 2) {
	    if (row(i) != 0)
	    	result = varInCol(i) :: result
	  }
		result
	}
	
	// Drop columns in matrix that correspond to variable variable
	
	def removeCol(variable: String) = {
	  val key = col(variable)
	  tabl = tabl.map(row => row.take(key) ++ row.takeRight(row.size - (key + 1)))
	  col  = col.filter { case (v, c) => v != variable }
	  col  = col.map { case (v, c) => if (c > key) (v, c - 1) else (v, c) }
	}
	
	// Returns the values of all variables in row row 
	// as a map of variable names to actual values.
	
	def varValues(row: Array[SymNum]): Map[String, SymNum] = {
	  var result: Map[String, SymNum] = Map()
	  for (j <- 0 to row.size - 2) {
	    if (row(j) != 0) {
		    val curVar = varInCol(j)
		    val curVal = getVal(curVar)
		    
		    result = result.+((curVar, curVal))
	
		    if (getVal(curVar) > 0) {
		      val remainingVars = varsInRow(row).filter(v => v != curVar)
		      remainingVars.foreach(v => result = result.+((v, SymNum(0))))
		      return result
		    }
	    }
	  }
	  result
	}
	
  // Get the actual solutions as a map.

	def getValues: Map[String, SymNum] = {
		def mergeVals(m1: Map[String, SymNum], m2: Map[String, SymNum]) = {
			m1 ++ m2.map { case (k,v) => k -> (if (v < 0 && m1.getOrElse(k, SymNum(0)) > 0) m1(k) else v) }   
	  }
          
    var result: Map[String, SymNum] = Map()
    tabl.map { row => 
        result = mergeVals(result, varValues(row))
    }
    result
  }

	def feasible(cons: List[Constraint]): Boolean = {
	  cons match {
	    case c::Nil =>
	    	c.sat(getValues) 
	    	  // && (!getValues.keys.exists(v => (!(v.startsWith("#p_"))) && getValues(v) < 0))
	    case x::xs => feasible(List(x)) && feasible(xs)
	  }
	}

	private def row2Constraint(row: Array[SymNum]): Constraint = {
	  var mons = new ListBuffer[Monomial]
  	for (i <- 0 to row.size - 2) {
  	  if (row(i) != 0)
  	  	mons.insert(0, Monomial(varInCol(i), row(i)))
  	}
    Constraint(Polynomial(mons.toList), Relationship.Eq, Polynomial(row(row.size - 1)))
	}
	
//	private def row2Constraint(row: Array[SymNum]): Constraint = {
//	  var mons: List[Monomial] = Nil
//  	for (i <- 0 to row.size - 2) {
//  	  if (row(i) != 0)
//  	  	mons = Monomial(varInCol(i), row(i)) :: mons
//  	}
//    Constraint(Polynomial(mons), Relationship.Eq, Polynomial(row(row.size - 1)))
//	}

	// Convert matrix to list of constraints.
	// IMPORTANT: Returns *not* the objective function. 
	
	def toCons: List[Constraint] = tabl.tail.map(row2Constraint(_)).toList
	
	def objFn = row2Constraint(tabl.head)
	
	// Returns a list of constraints for which one can construct Gomory cuts
	
	def cutableCons: List[Constraint] =
	  toCons.filter { _ match {
	    case Constraint(_, _, Polynomial(Nil, k)) =>
	    	if (!k.isInt && k > 0) true
	    	else false
	    case _ => false
	  }}
	
	def print: Unit = {
          return
	  if (tabl.size > 3) {
	    println(" <Matrix too large to be printed: " + tabl.size + ">")
	    return
	  }
	  val spaces = "          "
	  for (i <- 0 to tabl(0).size - 1) {
	  	if (i <= tabl(0).size - 2) {
	  		val tab = spaces.drop(varInCol(i).length)
		    System.out.print(varInCol(i) + tab)
	  	}
	    else
	    	System.out.print("RHS")
	  }
	  println("")
	  for (row <- tabl) {
	  	for (i <- 0 to row.size - 1) {
	  		val tab = spaces.drop(row(i).toString.length)
	  		if (i <= tabl(0).size - 2)
	  			System.out.print(row(i) + tab)
	  	  else
	  	    System.out.print(row(i))
	  	}
	  	println("")
	  }
	}
}

object Matrix
{
	def apply() = new Matrix(Array(Array()), Map())
}
