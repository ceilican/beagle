package beagle.VerySimplex
import scala.Array.canBuildFrom
import scala.collection.mutable.ListBuffer

package object transformations {

  import datastructures._
  import datastructures.Relationship._

  /* ************************************************************************************************************
   * How to call these methods / transform constraints into normal form:
   * (cs are our constraints, objectiveFn a special constraint, marking the objective function)
   * 
   * 1. rmStrictIneq(cs)       // Rewrites strict inequalities
   * 2. (objectiveFn :: cs).map(c => c.remConstants)  
   *                           // Adds objective to front, and puts constants all on one side of the inequalities
   * 3. insertSlackVars(cs)    // Turns inequalities into equalities using slack variables
	 * 4. toMatrix
   * ************************************************************************************************************ */

  // Turn strict inequalities into non-strict ones using symbolic data type SymNum 
  // (Do this before any other normalisation.)
  
  def rmStrictIneq(cs: List[Constraint], forInt: Boolean = false): List[Constraint] = {
  	cs match {
  	  case Nil   => List()
  	  case Constraint(a, r, b)::Nil =>
			  r match {
			    case Lt => 
			      if (a.isConstant) {
			        if (forInt == true)
			        	List(Constraint(Polynomial(a.ms, a.k.value + 1), Leq, b))
			      	else
			        	List(Constraint(Polynomial(a.ms, SymNum(a.k.value, 1)), Leq, b))			        
			      }
			      else if (b.isConstant) {
			        if (forInt == true)
			        	List(Constraint(a, Leq, Polynomial(b.ms, b.k.value - 1)))
			        else
			        	List(Constraint(a, Leq, Polynomial(b.ms, SymNum(b.k.value, -1))))
			      }
			      else
			      	throw new CustomException("Removing strict inequalties failed unexpected constraint format: " + Constraint(a, r, b).toString)
			    case Leq =>	List(Constraint(a, r, b))
			    case Geq =>	List(Constraint(a, r, b))
			    case Eq  => List(Constraint(a, r, b))
			    case Gt  => 
			      if (a.isConstant) {
			        if (forInt == true)
			        	List(Constraint(Polynomial(a.ms, a.k.value + 1), Geq, b))
			        else
			        	List(Constraint(Polynomial(a.ms, SymNum(a.k.value, 1)), Geq, b))
			      }
			      else if (b.isConstant) {
			        if (forInt == true)
			        	List(Constraint(a, Geq, Polynomial(b.ms, b.k.value + 1)))
			        else
			        	List(Constraint(a, Geq, Polynomial(b.ms, SymNum(b.k.value, 1))))
			      }
			      else
			      	throw new CustomException("Removing strict inequalties failed unexpected constraint format: " + Constraint(a, r, b).toString)
			    case _   =>	throw new CustomException("IMPLEMENT ME: transformation of " + r)			      
			  }
			case x::xs => rmStrictIneq(List(x), forInt) ::: rmStrictIneq(xs, forInt)
  	}
  }
  
  // Brings list of constraint to a form that uses slack variables.
  
  def insertSlackVars(cs: List[Constraint]): List[Constraint] = {
    val slacks = cs.map(_.slackVar match {
      case Some(sv) => Some(sv.split("_")(1).toInt)
      case None => None
    }).flatten
    
    var counter = 0
    if (slacks.length > 0)
      counter = slacks.max + 1

    var newCs = new ListBuffer[Constraint]()
    for (c <- cs) {
      newCs += c.insertSlackVar("#slack_" + counter.toString)
      counter += 1
    }
    newCs.toList
  }
  
  // Assumes that cs has been normalised prior to that 
  // and that obj function is row 0, i.e., on top.
  // 
  // col is a mapping which is from variables, represented as strings, 
  // to the respective column index in the matrix that they are associated
  // to, represented as integer.
  
  def toMatrix(css: List[Constraint], col: Map[String, Int]): Matrix = {
    def convert(cs: List[Constraint]): Array[Array[SymNum]] = {
	  	cs match {
	  	  case Nil    => Array(Array())
	  	  case c::Nil => 
	  	    var result = Array.fill[SymNum](col.size + 1)(SymNum(0))
	  	    
	  	    for (v <- c.vars)
	  	      result(col(v)) = c.coeff(v)
	  	    
	  	    if (c.a.isConstant)
	  	    	result(result.size - 1) = c.a.k
	  	    else if (c.b.isConstant)
	  	    	result(result.size - 1) = c.b.k
	  	    else
	  	      throw new CustomException("Normalisation failed due to unexpected constraint format: " + this.toString)
	  	    
  	    	Array(result)
	  	  case c::cr  =>
	  	    convert(List(c)) ++ convert(cr)
	  	}
  	}
  	
    Matrix(convert(css), col)
  }

  // We assume that constraints are normalised already, otherwise
  // it makes no sense to add artificial variables, really.
  
  def addArtificialVars(cons: List[Constraint]): List[Constraint] = {
  	def addVar(p: Polynomial, newVar: String) =
  	  p match { case Polynomial(mons, k) => Polynomial(Monomial(newVar)::mons, k) }

  	def doAllConstraints(css: List[Constraint], i: Int): List[Constraint] = {
	    css match {
	  	  case Nil => List()
	  	  case Constraint(a, Eq, b)::Nil => {
	  	    val cons = Constraint(a, Eq, b)
	  	    cons.slackVar match {
	  	      case Some(slackVar) => 
	  	        // Assumption is that b is merely a constant, not a full blown polynomial.
	  	        // And we add an artif. var only if the sign of the slack var and b
	  	        // is opposite...
	  	        if (a.coeff(slackVar) > 0 && b.k < 0 || a.coeff(slackVar) < 0 && b.k >= 0)
	  	        	List(Constraint(addVar(a, "#artif_" + i), Eq, b))
	  	        else
	  	          List(cons)
	  	      // ...or if there is no slack var. at all.
	  	      case None =>
	  	        List(Constraint(addVar(a, "#artif_" + i), Eq, b))
	  	    }
	  	  }
	  	  case x::xs => 
	  	    doAllConstraints(List(x), i) ::: doAllConstraints(xs, i+1)
	  	  case _ =>
	  	  	throw new CustomException("Adding of artificial variables failed (did you normalise your constraints?) due to unexpected constraints format: " + cons.toString)
	  	}
  	}
  	// We assume the old objective is the head, so we remove it first!
  	// (You should not add artificial variables to the objective, in fact, you can completely ignore it.)
  	if (cons.size == 0) List() // (shouldn't happen, but...)
  	else doAllConstraints(cons, 0) 
  }
  
  def addArtificialObjective(cons: List[Constraint]): List[Constraint] = {
    val allVars   = cons.map(c => c.vars).flatten.distinct
    val artifVars = allVars.filter(v => v.startsWith("#artif_"))
    val monoms    = artifVars.map(v => Monomial(v, 1))
    val newObj    = Constraint(Polynomial(Monomial("#p_artif") :: monoms), Eq, 0)
    newObj :: cons
  }
  
  def cutHasBasicVar(cut: Constraint, m: Matrix): Boolean = {
    cut match {
      case Constraint(Polynomial(ms, k), _, _) =>
        ms.exists { 
          case Monomial(v,c) => m.isPotentiallyBasic(v) 
        } 
//        || 
//        ms.exists { m => 
//        	val v = m.variable
//          !(v.startsWith("#slack") || v.startsWith("#artif") || v.startsWith("#p_")) 
//        }
    }
  }
}
