package beagle.VerySimplex.datastructures

object Relationship extends Enumeration {
	type Relationship = Value
	val Lt  = Value("<")
	val Gt  = Value(">")
	val Leq = Value("≤")
	val Geq = Value("≥")
	val Eq  = Value("=")
	val Neq = Value("≠")
}
