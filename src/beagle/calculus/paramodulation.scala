package beagle.calculus

import beagle._
import datastructures._
import fol._
import term._
import util._
import PMI._
import collection.mutable.ListBuffer

/** Contains methods implementing paramodulation; used exclusively in derivation rules. */
object paramodulation {

  /**
   * The main method for superposition.
   * @param from      The clause which has the (positive) literal used for rewriting
   * @param fromPos   The position (as an index) within from of the literal used for rewriting
   * @param lhs       The LHS term of the rewriting equation
   * @param rhs       The RHS term of the rewriting equation
   * @param isOrdered Whether LHS is greater than RHS
   * @param into      The clause which is being paramodulated into
   * @param pos       The position (as a PMI position) within into of the target term.
   * @param intoTerm  The actual term referred to by pos.
   */
  private def sup(from: Clause, fromPos: Int, lhs: Term, rhs: Term, isOrdered: Boolean, into: Clause, pos: Pos, intoTerm: Term) = {

    // Superposition into a literal into from an equation lhs=rhs, poor man indexing version, in all possible ways using
    // lhs=rhs from left to right
    def sup(into: Lit, pos: Pos) = {

      def sup(into: Eqn, pos: Pos) = {

        // the from-term lhs is implicit in top-para and sup below
        def topPara: Option[(Term, Subst)] =
          // println("top para: " + from + " into " + into)
          for(sigma <- (intoTerm mgu lhs);
              fromLhsSigma = sigma(lhs);
              // Check that the instantiated term is not pure BG.
              // Need that, because otherwise x+1 = c into 1+1 = d would be possible.
              // If sigma(e1) is ordered than the second disjunct holds true,
              // so we do not need to check it explicitly
              if (!fromLhsSigma.isPureBG && (isOrdered || !(sigma(rhs) geq fromLhsSigma)))) yield {
                (rhs, sigma)
            }

        // Body of sup into equation
        val eqnPos :: posInEqn = pos

        eqnPos match {
          case 0 => {
            topPara flatMap { case (intoTermRes, sigma) =>
                // superposition: don't paramodulate into smaller sides, check it:
                // the first test is redundant with the second but cheaper
                if (!into.isOrdered && (sigma(into.rhs) geq sigma(into.lhs)))
                  None
                else Some((Eqn(into.lhs.replaceAt(posInEqn, intoTermRes), into.rhs), sigma))
            }
          }

          case 1 => {
            if (into.isOrdered)
              None // don't paramodulate into smaller sides
            else
              topPara flatMap { case (intoTermRes, sigma) =>
                  // superposition: don't paramodulate into smaller sides, check it:
                  // the first test is redundant with the second but cheaper
                  if (sigma(into.lhs) geq sigma(into.rhs))
                    None
                  else Some((Eqn(into.lhs, into.rhs.replaceAt(posInEqn, intoTermRes)), sigma))
              }
          }
        }
      }

      // Body of sup into literals
      for((eqnRes, sigma) <- sup(into.eqn, pos);
	  //cheaply avoid building trivial clauses
	  //this also catches paramodulations on the same position in the same clause
	  if(!into.isPositive || !eqnRes.isTrivial)) yield
	(Lit(into.isPositive, eqnRes), sigma)

    }

    // Body of sup into clause
    val litPos :: posInLit = pos
    val intoLit = into(litPos);

    if (!(into.iEligible contains litPos))
      None
    else if (intoLit.isPredLit && intoLit.isPositive && rhs == TT)
      // rhs is TT means we have a predicative equation - trivial
      None
    else //same as below but transcribed to a for comprehesion
      for {
	(intoLitRes, sigma) <- sup(intoLit, posInLit)
	if (!into.iEligibleIsMax || sigma(into).iIsMaximalIn(litPos))
        if (sigma(from).iIsStrictlyMaximalIn(fromPos))
      } yield {
	//TODO- how to include info from both at an interface level?
	(into.modified(lits = sigma(into.lits.replaceNth(litPos, intoLitRes) ::: from.lits.removeNth(fromPos)),
		       idxRelevant = into.idxRelevant ++ from.idxRelevant,
		       age = math.max(into.age, from.age) + 1, 
		       parents = List(from.id, into.id)
		     ).abstr, 
	 sigma)
      }
      
  /* sup(intoLit, posInLit) match {
        case None => None
        case Some((intoLitRes, sigma)) => {
          if (into.iEligibleIsMax && !sigma(into).iIsMaximalIn(litPos))
            None
          else if (!sigma(from).iIsStrictlyMaximalIn(fromPos))
            None
          else
            Some((Clause(sigma(into.lits.replaceNth(litPos, intoLitRes) ::: from.lits.removeNth(fromPos)),
              into.idxRelevant ++ from.idxRelevant,
              math.max(into.age, from.age) + 1, List(from.id, into.id)).abstr, sigma))
        }
      }*/
  }

  /**
   * Expand an equation to (l,r,ordered). 
   * @return a list of all relevant orientations, taking into account whether
   * either term of the eqn is pure BG.
   */
  private def makeEquationsForSup(eqn: Eqn): List[(Term, Term, Boolean)] = {
    var res = List.empty[(Term, Term, Boolean)]
    if (!eqn.lhs.isPureBG) res ::= (eqn.lhs, eqn.rhs, eqn.isOrdered)
    if (!eqn.rhs.isPureBG && !eqn.isOrdered) res ::= (eqn.rhs, eqn.lhs, false)
    res
  }

  // This is the version that instantiates the rhs if it is a variable
  def makeEquationsForSupPartInst(eqn: Eqn): List[(Term, Term, Boolean, Subst)] = {
    var res = List.empty[(Term, Term, Boolean, Subst)]

    def partInstToRes(lhs: Term, rhs: Term) {
      assume(rhs.isVar)
      for {
	templ <- Sigma.templates(rhs.sort)
        newRhs = templ.fresh()
        gamma = Subst(rhs.asInstanceOf[Var] -> newRhs)
        newLhs = gamma(lhs)
      } {
        newLhs compare newRhs match {
          case Ordering.Greater => res ::= (newLhs, newRhs, true, gamma)
          case Ordering.Equal => ()
          case Ordering.Unknown =>  {
            newRhs compare lhs match {
              case Ordering.Greater => () // Wrong orientation - ignore
              // case Ordering.Equal => ??? // impossible case
              case Ordering.Unknown => res ::= (newLhs, newRhs, false, gamma)
            }
          }
        }
      }
    }

    if (!util.flags.partinst.value) {
      if (!eqn.lhs.isPureBG) res ::= (eqn.lhs, eqn.rhs, eqn.isOrdered, Subst.empty)
      if (!eqn.rhs.isPureBG && !eqn.isOrdered) res ::= (eqn.rhs, eqn.lhs, false, Subst.empty)
    } else {
      // Do partial instantiation, if appropriate
      if (!eqn.lhs.isPureBG) {
        if (eqn.rhs.isVar && eqn.rhs.isFG && !eqn.isOrdered) 
          partInstToRes(eqn.lhs, eqn.rhs)
        else 
          // rhs is not a FG variable or eqn is ordered
          res ::= (eqn.lhs, eqn.rhs, eqn.isOrdered, Subst.empty)
      }
      if (!eqn.rhs.isPureBG && !eqn.isOrdered) {
        if (eqn.lhs.isVar && eqn.lhs.isFG)
          partInstToRes(eqn.rhs, eqn.lhs)
        else 
          // lhs is not a FG variable
          res ::= (eqn.rhs, eqn.lhs, false, Subst.empty)
      }
    }
    res
  }
  


  /** Assumes clauses are variable disjoint */
  def supFromClauses(from: ListClauseSet, variant: Clause, into: Clause) = {
    var res = ListBuffer[Clause]()
    for (
      fromCl <- from.clauses;
      // eligible literals are never pure BG, hence no need to test that
      fromPos ← fromCl.iEligible intersect fromCl.iPosLits;
      fromLit = fromCl(fromPos) ) {
      for (
        (lhs, rhs, isOrdered) <- makeEquationsForSup(fromLit.eqn);
        (intoTerm, pos) <- variant.negLitsSubTermsWithPosForSup(lhs);
        (intoClRes, sigma) ← sup(fromCl, fromPos, lhs, rhs, isOrdered, variant, pos, intoTerm)
      ) {
        util.stats.nrInfPara += 1
        val resCl = intoClRes // if (intoClRes.weight < into.weight) intoClRes.modified(age = into.age) else intoClRes
        util.reporter.onPara(fromCl, into, resCl, sigma)
        res += resCl
      }
      // Superposition into positive literals, possibly partially instantiates
      for (
        (lhs, rhs, isOrdered, gamma) <- makeEquationsForSupPartInst(fromLit.eqn);
        (intoTerm, pos) <- variant.posLitsSubTermsWithPosForSup(lhs);
        (intoClRes, sigma) ← sup(gamma(fromCl), fromPos, lhs, rhs, isOrdered, variant, pos, intoTerm)
      ) {
        util.stats.nrInfPara += 1
        val resCl = intoClRes // if (intoClRes.weight < into.weight) intoClRes.modified(age = into.age) else intoClRes
        util.reporter.onPara(fromCl, into, resCl, sigma)
        res += resCl
      }
      // Superposition into negative literals, never partially instantiates
   
    }
    res
  }

/*
  def supFromClausesOld(from: ListClauseSet, into: Clause) = {
    for (
      fromCl <- from.clauses;
      // eligible literals are never pure BG, hence no need to test that
      fromPos ← fromCl.iEligible intersect fromCl.iPosLits;
      fromLit = fromCl(fromPos);
      // Try both orientations of equations
      (lhs, rhs, isOrdered, gamma) <- makeEquationsForSupPartInst(fromLit.eqn);
      (intoTerm, pos) <- into.subTermsWithPosForSup(lhs);
      // Non-effective pre-test:
      // if ((intoTerm mgu lhs) != None);
      (intoClRes, sigma) ← sup(gamma(fromCl), fromPos, lhs, rhs, isOrdered, into, pos, intoTerm)
    ) yield {
      util.stats.nrInfPara += 1
      util.reporter.onPara(gamma(fromCl), into, intoClRes, sigma)
      intoClRes
    }
  }
 */

  def supIntoClauses(from: Clause, variant: Clause, into: ListClauseSet) = {
    var res = ListBuffer[Clause]()
    // Assume clauses are variable disjoint
    for {
      // eligible literals are never pure BG, hence no need to test that
      fromPos ← variant.iEligible intersect variant.iPosLits
      fromLit = variant(fromPos)
      intoCl <- into.clauses
    } {
      for {
        // Superposition into positive literals, possibly partial instantiation
        (lhs, rhs, isOrdered) ← makeEquationsForSup(fromLit.eqn)
        (intoTerm, pos) ← intoCl.negLitsSubTermsWithPosForSup(lhs)
        (intoClRes, sigma) ← sup(variant, fromPos, lhs, rhs, isOrdered, intoCl, pos, intoTerm)
      }  {
        // println("***")
        // println("from: " + from)
        // println("into: " + intoCl)
        // println("sup:  sup(%s, %s, %s, %s, %s, %s, %s, %s)".format(gamma(from), fromPos, lhs, rhs, isOrdered, intoCl, pos, intoTerm))
        // println("res:  " + intoClRes)
        util.stats.nrInfPara += 1
        val resCl = intoClRes // if (intoClRes.weight < intoCl.weight) intoClRes.modified(age = intoCl.age) else intoClRes
        util.reporter.onPara(from, intoCl, resCl, sigma)
        res += resCl
      }

      for {
        // Superposition into positive literals, possibly partial instantiation
        (lhs, rhs, isOrdered, gamma) <- makeEquationsForSupPartInst(fromLit.eqn)
        (intoTerm, pos) <- intoCl.posLitsSubTermsWithPosForSup(lhs)
        (intoClRes, sigma) ← sup(gamma(variant), fromPos, lhs, rhs, isOrdered, intoCl, pos, intoTerm)
      }  {
        // println("***")
        // println("from: " + from)
        // println("into: " + intoCl)
        // println("sup:  sup(%s, %s, %s, %s, %s, %s, %s, %s)".format(gamma(from), fromPos, lhs, rhs, isOrdered, intoCl, pos, intoTerm))
        // println("res:  " + intoClRes)
        util.stats.nrInfPara += 1
        val resCl = intoClRes // if (intoClRes.weight < intoCl.weight) intoClRes.modified(age = intoCl.age) else intoClRes
        util.reporter.onPara(from, intoCl, resCl, sigma)
        res += resCl
      }
      // Superposition into negative literals, never partial instantiating
    }
    res
  }
/*
  def supIntoClausesOld(from: Clause, into: ListClauseSet) = {
    // Assume clauses are variable disjoint
    for (
      // eligible literals are never pure BG, hence no need to test that
      fromPos ← from.iEligible intersect from.iPosLits;
      fromLit = from(fromPos);
      // Try both orientations of equations
      (lhs, rhs, isOrdered, gamma) <- makeEquationsForSupPartInst(fromLit.eqn);
      intoCl <- into.clauses;
      (intoTerm, pos) <- intoCl.subTermsWithPosForSup(lhs);
      (intoClRes, sigma) ← sup(gamma(from), fromPos, lhs, rhs, isOrdered, intoCl, pos, intoTerm)
    ) yield {
      // println("***")
      // println("from: " + from)
      // println("into: " + intoCl)
      // println("sup:  sup(%s, %s, %s, %s, %s, %s, %s, %s)".format(gamma(from), fromPos, lhs, rhs, isOrdered, intoCl, pos, intoTerm))
      // println("res:  " + intoClRes)
      util.stats.nrInfPara += 1
      util.reporter.onPara(gamma(from), intoCl, intoClRes, sigma)
      intoClRes
    }
  }
 */
}
