package beagle.calculus

import beagle._
import datastructures._
import fol._
import term._
import util._
import proofoutput._
import scala.util.control.Breaks

/** Methods for simplification of clauses.
 * The entry point (includes all simplifications) is `reduceWithStatus`.
 */
object simplification {

  /** Demodulate a clause `cl` exhaustively with the given positive unit clauses `posUnits`.
   * @note The result needs abstraction
   * @todo perhaps this could work better if it returned an Option type?
   */
  def demodulate(cl: Clause, posUnits: Iterable[Clause]): (Clause, Boolean) = {

    if (flags.nodemod.value) return (cl, false)

    // assume posUnits are all ordered
    var idx = Set.empty[Int] // The indices of the clauses used for demodulation.
    
    var touchedLit = false // whether some literal was touched by simplification
    var demodMaxAge = 0 

    var unitsUsed = List.empty[Clause]

    var clDemod = cl.unabstrCautious // The currently demodulized version of cl

    def makeEquationsForDemod(eqn: Eqn) = {
      var res = List.empty[(Term, Term, Boolean)]
      if (!eqn.isOrdered && !eqn.rhs.isVar) res ::= (eqn.rhs, eqn.lhs, false)
      if (!eqn.lhs.isVar) res ::= (eqn.lhs, eqn.rhs, eqn.isOrdered)
      res
    }

    def demodulate(l: Lit): Lit = {
      var k = l // the copy of l we work on stepwisely
      var done = false
      val breakInner = new Breaks
      do { 
        stats.simpDemod.tried

        breakInner.breakable {

          // todo: this for-loop could be rather inefficient by always expanding all iteration variables,
          // which can be a lot of overhead, in particular when the inner break is triggered early.
          // Some experiments seem to show, however, that this does not make a big difference
          for {
	    u <- posUnits
            (lhs, rhs, isOrdered) <- makeEquationsForDemod(u(0).eqn)
            (s, pos) <- k.subTermsWithPosForSup(lhs)
            sigma <- lhs matcher s
            eRhsInst = sigma(rhs)
            if isOrdered || (s gtr eRhsInst)
          } {
            val kDemod = k.replaceAt(pos, eRhsInst)
            // Various conditions to check if we're ok to proceed,
            // the nontrivial one is whether demodulation with e gives a smaller clause than cl
            if ( // preprocessing ||
              pos.tail.nonEmpty || // paramodulating into a proper subterm of the lhs or rhs, or
              !k.isPositive || // or a negative literal, or
              kDemod.isTrivialPos || // a trivial literal; clauses with trivial positive literals will be removed anyway
              (k.eqn.isOrdered && pos.head == 1) || // demodulating the rhs of an ordered equation
                                                    // Coming here means we are demodulating the top-level of the (potentially) bigger side of k.
                                                    // In this case we to make sure explictly that the demodulator is smaller than the clause to be demodulated.
                                                    // For example, demodulating a->b into a->c with a > b > c would violate this, as a->b > a->c.
              (clDemod.length > 1) || // Relax the just said: experimental
              (cl.lits exists { _ gtr Lit(true, Eqn(s, eRhsInst)) })) { // Lit(true, Eqn(s, eRhsInst)) is the instantiated demodulator
              idx ++= u.idxRelevant
              unitsUsed::=u
            
	      stats.simpDemod.succeed
              // println("demodulation of %s with instance %s → %s yields %s".format(k, s, eRhsInst, kDemod))
              k = kDemod // Update k with current result
              touchedLit = true
              demodMaxAge = math.max(demodMaxAge, u.age)
              if (k.isTrivialPos)
                return Lit.TrueLit
              else if (k.isTrivialNeg)
                return Lit.FalseLit
              else 
                breakInner.break() // Continue with outer loop
            }
          }

          import bgtheory._

	  /**
          See if there is non-linear multiplication we can eliminate.
          Doing this explictly is better than via general demodulation for two reasons: 
          (1) it may take a long time until a demodulator like '#nlpp'(20000, X) = $product(20000, X)
              hase been generated, and
          (2) Only demodulator like '#nlpp'(10, X) = $product(10, X) are generated but not their
              symmetric forms '#nlpp'(10, X) = $product(10, X). 
          The code below avoids both issues.
	  */
          for ((s, pos) <- k.termIndex(LFA.NLPPOpReal) ++ k.termIndex(LRA.NLPPOpRat) ++ k.termIndex(LIA.NLPPOpInt) ) { 
            // All occurrences of non-linear multiplication
            val demodRes =
              s match {
                case PFunTerm(LIA.NLPPOpInt, List(l:LIA.DomElemInt, r)) => Some(LIA.Product(l, r))
                case PFunTerm(LIA.NLPPOpInt, List(l, r:LIA.DomElemInt)) => Some(LIA.Product(l, r))
                case PFunTerm(LRA.NLPPOpRat, List(l:LRA.DomElemRat, r)) => Some(LRA.Product(l, r))
                case PFunTerm(LRA.NLPPOpRat, List(l, r:LRA.DomElemRat)) => Some(LRA.Product(l, r))
                case PFunTerm(LFA.NLPPOpReal, List(l:LFA.DomElemReal, r)) => Some(LFA.Product(l, r))
                case PFunTerm(LFA.NLPPOpReal, List(l, r:LFA.DomElemReal)) => Some(LFA.Product(l, r))
                case _ => None
              }
            if (demodRes != None) {
              val rhsInst = demodRes.get
              lazy val kDemod = k.replaceAt(pos, rhsInst)
              // Conditions to check if we're ok to proceed,
              // the nontrivial one is whether demodulation with e gives a smaller clause than cl
              if (pos.tail.nonEmpty || // paramodulating into a proper subterm of the lhs or rhs, or
                !k.isPositive || // or a negative literal, or
                (k.eqn.isOrdered && pos.head == 1) || // demodulating the rhs of an ordered equation
                                                    // Coming here means we are demodulating the top-level of the (potentially) bigger side of k.
                                                    // Only in this case we to make sure explictly that the demodulator is smaller than the clause to be demodulated.
                                                    // For example, demodulating a->b into a->c with a > b > c would violate this, as a->b > a->c.
              (cl.lits exists { _ gtr Lit(true, Eqn(s, rhsInst)) })) {
	        stats.simpDemod.succeed
                // println("demodulation of %s with instance %s → %s yields %s".format(k, s, rhsInst, kDemod))
                k = kDemod // Update k with current result
                touchedLit = true
                if (k.isTrivialPos)
                  return Lit.TrueLit
                else if (k.isTrivialNeg)
                  return Lit.FalseLit
                else
                  breakInner.break() // Continue with outer loop
              }
            }
          }
          // After both for loops
          // Coming here means that no rule has applied
          done = true
        } // breakInner
      } while (!done)
      k // final result
    }

    // Body of demodulation for clauses

    // println("Unit clauses for demodulation of %s:".format(cl.toString()))
    // posUnits foreach { cl => println(cl) }

    var someTouched = false
    do {
      touchedLit = false
      val clDemodResLits = clDemod.lits map { demodulate(_) }
      if (touchedLit) {
        someTouched = true
        clDemod = Clause(clDemodResLits, clDemod.idxRelevant ++ idx, math.max(cl.age, demodMaxAge)+0).unabstrCautious
        // clDemod = Clause(clDemodResLits, clDemod.idxRelevant ++ idx, math.max(cl.age, demodMaxAge)+1).unabstr
        // Clause(hclResLits, hcl.idxRelevant ++ idx, cl.age+1).simplifyCheap match {
        //   case Nil => hcl = Clause(List(Lit.TrueLit), hcl.idxRelevant ++ idx, cl.age+1) // todo: delete clause straight away
        //   case List(c) => hcl = c
        // }
        // println("Success: " + hcl)
      }
    } while (touchedLit)

    if (someTouched) {
      // Try 26/5/2014: never increase the age
      val res = (clDemod, true) //  else math.max(cl.age, demodMaxAge)+0
      reporter.detailedSimp("demod", cl, res._1, unitsUsed)
      res
    } else
      (cl, false)

/*
    val h = cl.unabstr.lits map { demodulate(_) }
    if (touchedLit) {
      // Try 26/5/2014: never increase the age
      val res = (Clause(h, cl.idxRelevant ++ idx, cl.age+1), true) //  else math.max(cl.age, demodMaxAge)+0
      reporter.detailedSimp("demod", cl, res._1, unitsUsed)
      res
    } else
      (cl, false)
 */

  }

  /** Simplify a Clause by removing positive literals that are subsumed by some
   * negative unit clause in `negUnits`.
   * No need to remove negative literals, as this is done by simplification.
   * The clause recieves a new ID only if it is simplified by `negUnits`.
   * @return the simplified clause and a boolean representing whether the clause
   * was simplified or not.
   */
  def negUnitSimp(cl: Clause, negUnits: Iterable[Clause]): (Clause, Boolean) = {
    var touched = false
    var idx = Set.empty[Int]
    var resLits = List.empty[Lit]
    
    var unitsUsed = List.empty[Clause]
    stats.simpNegUnit.tried

    for (l ← cl.lits)
      if (l.isPositive){
        negUnits find { 
          u => (u(0).eqn matcher l.eqn).nonEmpty
        } match {
	  case Some(u) => {
            idx ++= u.idxRelevant
            unitsUsed::=u
            touched = true
          }
	  case None => resLits ::= l 
	}
      }else resLits ::= l

    if (touched) {
      //stats.nrSimpNegUnit += 1
      stats.simpNegUnit.succeed
      //val res = (Clause(resLits.reverse, cl.idxRelevant ++ idx, cl.age + 1), true)
      val res = (cl.modified(lits = resLits.reverse, 
			     idxRelevant = cl.idxRelevant ++ idx, 
			     age = cl.age + 0),
		 true)
      reporter.detailedSimp("neg_unit", cl, res._1, unitsUsed)
      res
    } else (cl, false)

  }

  /** Cheap simplifications: removal of trivial negative equations,
   * tautology detection, unabstraction, removal of extraneous BG variables by QE.
   * @return an abstracted, simplified clause.
   * @todo simplifyCheap is rather expensive, on DAT043 same time as overall inferencing
   * @todo record trivial negative equation deletion
   * @see [[beagle.datastructures.Clause.simplifyBG]]
   */
  def simplifyCheap(cl: Clause): List[Clause] = {

/*
    def simplifyBG(cl: Clause) = {
      val safeOnly = !(flags.bgsimp.value == "aggressive")
      assume(!flags.stdabst.value, "Can't do unabstraction when standard abstraction is in use ")
      if (safeOnly) 
         bgtheory.solver.simplify(cl.unabstr, safeOnly) 
      else
        cl.canonical
    }


    def simplifyBG(cl: Clause) = {
      val safeOnly = !(flags.bgsimp.value == "aggressive")
      assume(!flags.stdabst.value, "Can't do unabstraction when standard abstraction is in use ")
      // resCl is maintained as an unabstracted clause
      var resCl = cl.unabstr
      var done = false
      do {
        val (h, touched) = bgtheory.solver.simplify(resCl, safeOnly)
        if (touched) {
          // This may lead to new unabstractions
          resCl = h.unabstr
        } else
          done = true
      } while (!done)
      resCl
    }
 */

    // todo: simplifyCheap is rather expensive, on DAT043 same time as overall inferencing
    var cl1 = cl.unabstrCautious
    var cl1old: Clause = null
    do {
      cl1old = cl1
      cl1 = cl1.simplifyBG.unabstrCautious
    } while (cl1.length < cl1old.length) 

    // Removal of trivial negative equations
    // TODO- record trivial negative unit deletion
    // The ident number of the result is the same as the given clause
    // val cl2 = Clause(cl1.id, cl1.lits.toListSet filterNot { _.isTrivialNeg }, cl1.idxRelevant, cl1.age)
    // val cl2 = cl1.modified( lits = cl1.lits.toListSet filterNot { _.isTrivialNeg } )
    // removal of trivial negative literals and duplicates is part of unabstraction now.
    
    if (cl1.isTautology) {
      //stats.nrSimpTaut += 1
      List.empty
    } else
      List(cl1.abstr)
  }

  /** Reduce the clause `cl` by all clauses in the given clauses `cls`
   * This includes rewriting by negative unit clauses.
   * @todo need arithmetic simplification at this stage,
   * i.e. (n < m) & (m < t) => (n < t) more than that, we need to ensure that n is not negative
   * or consider absolute values.
   * @note The result is also cheaply simplified, but only if the clause was touched
   * @return A pair consisting of the clauses resulting from simplification against `cls`
   * and a boolean which is true if `cl` was simplified.
   */
  def reduceWithStatus(cl: Clause, cls: ClauseSet): (Iterable[Clause], Boolean) = {
    Timer.reduction.start()
    // Changed back to unabstrAggressive in 0.9.7
    // val units = cls map { _.unabstrAggressive } filter { _.isUnitClause }
    // val units = cls map { _.unabstrCautious } filter { _.isUnitClause }
    // val units = cls filter { _.isUnitClause }
    // val units = cls.unitClauses


    // println("Unit clauses for demodulation of %s:".format(cl.toString()))
    // units foreach { cl => println(cl) }
    // println("---------------------------------------")

    Timer.demod.start()
    // val (cl1, touched1) = demodulate(cl, units filter { cl ⇒ cl(0).isPositive && cl(0).eqn.isOrdered }, false)
    // Use late check for orderedness (after instantiation)
    val (cl1, touched1) = demodulate(cl, cls.unitClauses filter { _(0).isPositive })
    Timer.demod.stop()

    // After demodulation the clause may need abstraction, and cheap simplification could apply
    val cl3 = if (touched1) cl1.abstr else cl1 // cl1.simplifyCheap

    // if (cl2.isEmpty) {
    //   Timer.reduction.stop()
    //   return (List.empty, true)
    // }

    // Otherwise cl2 is a singleton
    // val List(cl3) = cl2

    val (cl4, touched4) = negUnitSimp(cl3, cls.unitClauses filter { !_(0).isPositive })

    Timer.reduction.stop()

    Timer.subsumption.start()
    val subRes = cls.clauses exists { _ subsumes cl3 }
    Timer.subsumption.stop()

    if (subRes) {
      //stats.nrSimpSubsume += 1
      return (List.empty, true)
    } else if (touched1 || touched4)
      return (cl4.simplifyCheap, true)
  else
      // Have not touched the clause at all
      return (List(cl), false)
  }

  /* Experimental */

  import bgtheory.LIA._
  /** Allow unit clauses (n < t) to subsume (m < t)
   * 3 < t => n < t if n <= 3
   * t < 3 => n > t if n >= 3
   * Assumes that all literals are normalised to <= 
   * @return true if cl1 subsumes cl2 arithmetically.
   */
  def ariSubsumes(cl1: Iterable[Clause], cl2: Clause): Boolean = {
    
    //Given (a > b) && (c > d)
    //true if (a>b) |= (c>d)
    // i.e. when (c.abs >= a.abs) && b matches d
    // or if (b.abs >= d.abs) && a matches c
    def case1(a: Term, b: Term, c: Term, d: Term): Boolean = (a,b,c,d) match {
      case (DomElemInt(aN), bTerm, DomElemInt(cN), dTerm) =>
	((cN.abs >= aN.abs) && !(bTerm matcher dTerm).isEmpty)
      case (aTerm, DomElemInt(bN), cTerm, DomElemInt(dN)) =>
	((bN.abs >= dN.abs) && !(aTerm matcher cTerm).isEmpty)
      case _ => false
    }

    //Given (a >= b) && (c > d)
    //true if (a >= b) |= (c > d) 
    // i.e. when (c.abs > a.abs) && b matches d
    // or if (b.abs > d.abs) && a matches c
    def case2(a: Term, b: Term, c: Term, d: Term): Boolean = (a,b,c,d) match {
      case (DomElemInt(aN), bTerm, DomElemInt(cN), dTerm) =>
	((cN.abs > aN.abs) && !(bTerm matcher dTerm).isEmpty)
      case (aTerm, DomElemInt(bN), cTerm, DomElemInt(dN)) =>
	((bN.abs > dN.abs) && !(aTerm matcher cTerm).isEmpty)
      case _ => false
    }

    //one of the clauses in cl1 subsumes cl2
    cl2.lits.head match {
      //-(a <= b) --> (a > b)
      case Lit(false, LessEqEqn(a,b)) => {
	cl1 exists { cl => cl.lits.head match {
	  //-(c <= d) --> (c > d)
	  case Lit(false, LessEqEqn(c,d)) if(case1(c,d,a,b)) =>
	    {reporter.debug(cl.lits.head+" subsumed "+cl2.lits.head); true}
	  //-(c<d) --> (d >= c)
	  case Lit(false, LessEqn(c,d)) if(case2(d,c,a,b)) =>
	    {reporter.debug(cl.lits.head+" subsumed "+cl2.lits.head); true}
	  //(d > c)
	  case Lit(true, LessEqn(c,d)) if(case1(d,c,a,b)) =>
	    {reporter.debug(cl.lits.head+" subsumed "+cl2.lits.head); true}
	  case _ => false
	}}
      }
      //-(a<b) --> (b >= a)
      case Lit(false, LessEqn(a,b)) => {
	cl1 exists { cl => cl.lits.head match {
	  //-(c <= d) --> (c > d)
	  case Lit(false, LessEqEqn(c,d)) if(case2(c,d,b,a)) =>
	    {reporter.debug(cl.lits.head+" subsumed "+cl2.lits.head); true}
	  //-(c<d) --> (d >= c)
	  case Lit(false, LessEqn(c,d)) if(case1(d,c,b,a)) =>
	    {reporter.debug(cl.lits.head+" subsumed "+cl2.lits.head); true}
	  //(d > c)
	  case Lit(true, LessEqn(c,d)) if(case2(d,c,b,a)) =>
	    {reporter.debug(cl.lits.head+" subsumed "+cl2.lits.head); true}
	  case _ => false
	}}
      }
      //(b > a)
      case Lit(true, LessEqn(a,b)) => {
	cl1 exists { cl => cl.lits.head match {
	  //-(c <= d) --> (c > d)
	  case Lit(false, LessEqEqn(c,d)) if(case1(c,d,b,a)) =>
	     {reporter.debug(cl.lits.head+" subsumed "+cl2.lits.head); true}
	  //-(c<d) --> (d >= c)
	  case Lit(false, LessEqn(c,d)) if(case2(d,c,b,a)) =>
	    {reporter.debug(cl.lits.head+" subsumed "+cl2.lits.head); true}
	  //(d > c)
	  case Lit(true, LessEqn(c,d)) if(case1(d,c,b,a)) =>
	    {reporter.debug(cl.lits.head+" subsumed "+cl2.lits.head); true}
	  case _ => false
	}}
      }
      case _ => false
    }
  }

}
