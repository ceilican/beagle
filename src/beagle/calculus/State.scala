package beagle.calculus

import beagle._
import datastructures._
import util._
import derivationrules._
import simplification._
import fol._
import term._
import collection.mutable.ArrayBuffer
import bgtheory._
import proofoutput._ // Proof output

//case object FAIL extends Exception
case object TIMEOUT extends Exception

/**
 * @param given, the given clauses, expected to be already cheaply simplified
 * @param sos, old clauses to start with (typically lemmas)
 */
class State(given: List[Clause], sos: List[Clause]) {

  case class CLOSE(empty: Clause) extends Exception
  case object CONTINUE extends Exception

  /** The unprocessed clauses, those awaiting inference */
  var neu = new PQClauseSet 
  var old = new ListClauseSet 
  // var posUnits = new ListClauseSet 
  // var negUnits = new ListClauseSet
  
  // The clause sets manipulated:
  // var neu = new PQClauseSet(None) // The unprocessed clauses, those awaiting inference

  // var old = new ListClauseSet(None) // The processed clauses, those that are closed under inferences

  var decisionLevels = ArrayBuffer.empty[decisionLevelInfo]

  Term.resetDefineMap()
  
  old.add(sos) // could do interreduction

  /**
   * The current values for the above clause sets, used for backtracking.
   */
  class StateInfo(val neu: PQClauseSet, val old: ListClauseSet)
  // class StateInfo(val neu: ListClauseSet, val old: ListClauseSet)

  /**
   * Provides the information needed to backtrack to a decision level.
   * @param rightClausesFn A function that takes relevant indices to make a right conclusion
   * for the split inference that created that decision level.
   * @param stateInfo Holds the clause sets as they were before the left split.
   */
  case class decisionLevelInfo(rightClausesFn: (Set[Int] ⇒ List[Clause]), stateInfo: StateInfo)

  /**
   * Add a clause to neu.
   * @param throwCLOSEOnEmpty Throw CLOSE exception when detecting an empty clause in
   * the clause set after simplification.
   * @throws CLOSE if throwCLOSEOnEmpty is set and an empty clause is detected after
   * simplification.
   */
  def addToNeu(cl: Clause, throwCLOSEOnEmpty: Boolean = true, simplify: Boolean = true) {
    
    reporter.onClauseAdded(cl)
    
    val clSimp = 
      if (simplify) {
        // Do full reduction before adding the clause
        cl.reduceWithStatus(old) match {
          case (clRed, true) => clRed // reduction applied, includes cheap simplification
          case (clRed, false) => clRed flatMap { _.simplifyCheap }
        }
      } else List(cl)

    if (throwCLOSEOnEmpty)
      (clSimp find { _.isEmpty }) match {
        case None     ⇒ ()
        case Some(cl) ⇒ {
          throw CLOSE(cl)
        }
      }
    // No, do this when a clause is added to old
    // if (flags.simpneu.value && (clSimp forall { cl => cl.isUnitClause && cl(0).isFlat && cl(0).isFG })) {
    //   neu.reduce(new ImmutableListClauseSet(clSimp))
    // }

    neu.add(clSimp)
  }

  /**
   * Applying the single premise derivation rules Equality Resolution and
   * Equality Factoring on a new clause.
   * @param cl a clause which is not pure background.
   */
  def singlePremiseInferences(cl: Clause) = {
    assume(!cl.isPureBG)
    Timer.inferencing.start()

    var infResults = List.empty[Clause]

    // Simplification done later
    for (newCl ← infRef(cl)) {
      reporter.onInference(cl, newCl, "Ref")
      infResults ::= newCl
    }

    for (newCl ← infFact(cl)) {
      reporter.onInference(cl, newCl, "Fact")
      infResults ::= newCl
    }

    Timer.inferencing.stop()
    infResults
  }

  def clausalInferences(cs: ListClauseSet, cl: Clause) : Iterable[Clause] = {
    assume(!cl.isPureBG)
    Timer.inferencing.start()
    println("from non-fresh: " + cl)
    val clFresh = cl.fresh
    val res = infSupIntoClauses(cl, clFresh, cs) ++ infSupFromClauses(cs, clFresh, cl) ++ infChaining(cl, clFresh, cs)
    Timer.inferencing.stop()
    res
  }

  /**
   * Main procedure.
   * Modified to return the empty clause used in closing instead of FAIL.
   * @param timeout Time allowed just for this call.
   * @throws TIMEOUT if `timeout` is exceeded.
   * @return Left(emptyClause) or Right(this) respective representing the empty
   * clause used to close or the final state if saturation is achieved.
   */
  def derive(timeout: Double): Either[Clause,State] = {

    Timer.mainLoop.start()

    val war = flags.weightAgeRatio.value

    reporter.onProofStart(given)
    
    given foreach { neu.addAndInterreduce(_) }
    
    /** How many lightest clauses have been selected in a row already */
    var lightestCnt = 0
    // var timeleft = timeout

    while (!neu.isEmpty) try {
      // println("time main loop: " + Timer.mainLoop.elapsedSecs)
      // println("time left: " + timeleft)
      // timeleft = timeleft - Timer.mainLoop.elapsedSecs
      //thrown from here
      if (Timer.mainLoop.sinceStarted() > timeout) throw TIMEOUT

      //if (debug) show()
      reporter.onNewState(old, neu)
      
      // Select a clause from neu, as specified by the flag weight-age ratio
      val sel = if (lightestCnt < war) {
        lightestCnt += 1
        neu.removeLightest()
      } else {
        lightestCnt = 0
        neu.removeOldest()
      }

      // See if selected can be deleted because one of the parents was removed from old
      // Not effective - can as well forget it
/*
      sel.parents match {
        case None => () // No information about sel 
        case Some(parents) =>
          if (parents forall { parentId => old.clauses exists { _.id == parentId } })
            stats.simpOrphanDel.tried // OK, all parents clauses are still in old
          else {
            //stats.nrOrphanDel += 1
	    stats.simpOrphanDel.succeed
            // println("xxx delete clause " + sel + " because a parent is redundant")
            reporter.onClauseDeleted(sel)
            throw CONTINUE
          }
      }
 */

      val h1 = sel.reduce(old)
      // reduce does not do cheap simplification, precisely if sel has not been touched.
      // But then sel has been cheaply simplified before it was put into neu

      // invariant: clauses in neu are cheaply simplified, hence so is sel,
      // and reduce preserves cheap simplification

      println("State sel.id: " + sel.id)
      
      if (h1.isEmpty) { 
        reporter.onClauseDeleted(sel)
        throw CONTINUE
      }

      // Otherwise we have a single clause
      val List(selRed) = h1

      // Save call to ~ if not necessary because we do not report the result anyway.
      if (reporter.isDebugReporter && !(selRed ~ sel)) reporter.onSimplified(sel, selRed)
      if (selRed.isEmpty) throw CLOSE(selRed)

      println("State selectedClause selRed.id: " + selRed.id)
      reporter.selectedClause(selRed, decisionLevels.length)

      // Try Define
      val h2 = infDefine(selRed)
      if (h2 != None) {
        val Some((selRedNew, defClauses)) = h2
        reporter.onDefine(selRed, defClauses, selRedNew)
        defClauses foreach { addToNeu(_) }
        addToNeu(selRedNew) // infDefine could be applicable to selRedNew again,
        // hence it goes into neu
        throw CONTINUE
      }

      // Try Split
      val h3 = infSplit(selRed, decisionLevels.length)
      if (h3 != None) {
        val Some((leftClauses, rightClausesFn)) = h3
        // println("xx split %s into %s and %s".format(selRed, leftClause, rightClausesFn))
        reporter.onSplit(selRed, leftClauses, decisionLevels.length)
        
        // Make the decision point
        decisionLevels += decisionLevelInfo(rightClausesFn, new StateInfo(neu = neu.klone(), old = old.klone()))
        
        leftClauses foreach { addToNeu(_) }
        throw CONTINUE
      }

      // Define does not apply, try Inst
      // Currently defunct
      /*
      val h4 = infInst(selRed,old)
      if (h4 != None) {
        val Some(c) = h4

        reporter.onInst(selRed, c)
        c foreach { addToNeu(_) }
        throw CONTINUE
      }
       */

      // We are adding selRed to old and close under inferences. 
      // Also invoke the BG solver if appropriate

      if (flags.nobred.value) {
        println(flags.nobred.value)
        old.add(selRed)
      }
      else {
        println(flags.nobred.value)
        println("State_selRed.id: " + selRed.id)
        val newFromOld = old.addAndBackwardReduce(selRed)
        println("NewFromOld: " + newFromOld)
        newFromOld foreach { cl =>
          println("cl id: " + cl.id)
          addToNeu(cl)
        }
      }
      //println("calling onClauseAdded for: " + selRed.id)
      //reporter.onClauseAdded(selRed)

      if (selRed.isBG) {
        // In case the solver needs unit clauses need to add the solver clauses that are not unit clauses (or not empty)
        // to neu, so that these can be split on in the next round.
        // asSolverClauses provides us readily with these sets:
        val (inOld, newfromBGClauses) = selRed.asSolverClauses.get
        // newfromBGClauses is already simplified cheap
        // println("xx " + selRed + " partitioned " + (inOld, newfromBGClauses))

        if (!inOld.isEmpty) {
          // Might have the empty clause now, asSolverCauses might generate it
          // by simplification
          (inOld find { _.isEmpty }) match {
            case None => ()
            case Some(cl) => {
              throw { 
                CLOSE(cl)
              }
            }
          }

          // Have some new clauses the solver understands, hence check close
          val BGClauses = old.clauses filter { _.isBG } flatMap { _.asSolverClauses.get._1  }
          // println("xx " + hBGClauses)

	  stats.nrBGConsistencyChecks += 1;
          // println("XXXXX " + solver.name)
          solver.check(BGClauses) match {
            case UNSAT(core) => {
	      // idxRel are the relevant ids of the BGclauses considered
	      val idxRel =
	        if (util.flags.noMuc.value || util.flags.split.value == "off")
                  // all clauses are relevant
                  BGClauses.foldLeft(Set.empty[Int]) { (acc,cl) => acc++cl.idxRelevant }
                else {
                  // Get the ids of the core clauses:
                  val idxCore = core match {
                    case None => { 
                      //heuristic for running MUC, use the naive approach
                      val unsatCore = solver.minUnsatCoreClauses(BGClauses)                      
                      reporter.onBGInconsistent(selRed, unsatCore)
                      unsatCore map {_.id}
                    }
                    case Some(coreIdx) => coreIdx
                  }
                  // Build the union of the relevant indexes of the core clauses
                  var res = Set.empty[Int]
                  for (cl <- BGClauses) {
                    if (idxCore contains cl.id)
                      res ++= cl.idxRelevant
                  }
                  res
                }
        
	      stats.nrInfClose += 1;
	      //use context information from last added BG clause to construct empty clause
              throw CLOSE(selRed.modified(lits=List(), idxRelevant=idxRel))
            }
            case SAT | UNKNOWN => ()
          }
        }

        newfromBGClauses foreach { cl =>
          addToNeu(cl, simplify = false)
          //reporter.onClauseAdded(cl) // this is being done inside addToNeu now
        }
      }

      if (!selRed.isPureBG) {
        singlePremiseInferences(selRed) foreach { addToNeu(_) }
        clausalInferences(old, selRed) foreach { addToNeu(_) }
      }

/*      if (flags.simpneu.value && selRed.isUnitClause && selRed(0).isFlat) {
//        neu.reduce(new ImmutableListClauseSet(List(selRed)))
        val oldFlat = new ImmutableListClauseSet(old.clauses filter { cl => cl.isUnitClause && cl(0).isFlat })
        neu.reduce(oldFlat)
      }
 */

      // def useForSimpNeu(cl: Clause) = cl.isUnitClause && cl(0).isFlat && !cl(0).isNLPP
      def useForSimpNeu(cl: Clause) = cl.isUnitClause && cl.isGround

      if (flags.simpneu.value && useForSimpNeu(selRed)) {
        // Reduction of neu. However neu is potentially big, and so reduce is too expensive,
        // hence reduce only in a cheap way.
        // val oldFlat = new ImmutableListClauseSet(old.clauses filter { useForSimpNeu(_) })
        // neu.reduce(oldFlat, selRed)
        neu.reduce(selRed)
      }
      // if (flags.simpneu.value && selRed.isUnitClause && selRed(0).isFlat) {
      //   neu.reduce(old, cheap = true)
      // }

    } catch {
      case CONTINUE ⇒ ()
      case CLOSE(emptyCl) ⇒ {
	val idxRelevant = emptyCl.idxRelevant
        // logvClause(Clause(List.empty, idxRelevant, 0))
        reporter.onClose(decisionLevels.length,emptyCl)
	if (idxRelevant.isEmpty) {
          Timer.mainLoop.stop()
	  //throw FAIL
	  return Left(emptyCl)
        } else {
          val backtrackLevel = idxRelevant.max
          val blInfo = decisionLevels(backtrackLevel)
          decisionLevels.reduceToSize(backtrackLevel) // shorten stack

          val preservedClauses = 
            if (flags.keep.value) {
              (old.clauses
               // ++ neu.clauses
              ) filter { cl =>
              (cl.idxRelevant.isEmpty // || cl.idxRelevant.max < backtrackLevel
              ) &&
                !(blInfo.stateInfo.neu.clauses exists { _ == cl }) &&
                !(blInfo.stateInfo.old.clauses exists { _ == cl })
              }
            } else List()
          old = blInfo.stateInfo.old
          neu = blInfo.stateInfo.neu
          neu.add(preservedClauses)
          //new
	  //solverBGClauses = (old.clauses filter { cl ⇒ cl.isBG && solver.canClose(cl) }).toList

          //if (verbose || debug) log("=== Backtrack to level " + backtrackLevel + " ===")
          
          val rightClauses = blInfo.rightClausesFn(idxRelevant - backtrackLevel) 
          //logd("Right clauses are:")
          // Do the right split
          rightClauses foreach { cl ⇒
            //logd(cl)
            addToNeu(cl, throwCLOSEOnEmpty = false)
          }
          reporter.onBacktrack(backtrackLevel,rightClauses,old,neu)
          
          //logd("\nState is now:")
          //if (debug) show()
        }
      }
    }
    println()
    Timer.mainLoop.stop()
    Right(this)
  }
}

