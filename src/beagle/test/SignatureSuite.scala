package beagle.test

import org.scalatest._

import beagle._
import datastructures._
import fol._
import term._
import util._
import bgtheory.LIA._
import Signature._

/**
* Some tests for class Signature.
*/
class SignatureSuite extends FunSuite with GivenWhenThen {

  info("Testing Signature.+(op: Operator): Signature")

  //def +(op: Operator): Signature
  test("Signature.+(op: Operator) normal behaviour") {
    val s = signatureEmpty+IntSort
    val f1 = Operator(FG,"f",Arity1(ISort -> ISort))
    val f2 = Operator(BG, "c", Arity0(IntSort))
    Given(s"New operators $f1, $f2")
    When("Adding operators")
    val s1 = (s+f1)+f2
    Then("Operators appear in signature.operators")
    assert(s1.operators(f1))
    assert(s1.operators(f2))
    Then("Operators appear in signature.fgOperators/bgOperators")
    assert(s1.fgOperators(f1) && !s1.bgOperators(f1))
    assert(!s1.fgOperators(f2) && s1.bgOperators(f2))
    Then("Operators can be found by signature.findOperator")
    assert(s1.findOperator(f1.name)===Some(f1))
    assert(s1.findOperator(f2.name)===Some(f2))
  }

  test("Signature.+(op: Operator) adding duplicates") {
    val s = signatureEmpty+IntSort
    val f1 = Operator(FG,"f",Arity1(ISort -> ISort))
    val f2 = Operator(BG, "c", Arity0(IntSort))
    Given(s"New operators $f1, $f2")
    When("Adding duplicates")
    val s1 = (s+f1)+f2
    Then("Signature does not change")
    assert(s1+f1 === s1, "Signature changed on adding duplicate op")
    assert(s1+f2 === s1, "Signature changed on adding duplicate op")
  }

  test("Signature.+(op: Operator) overloading operators by kind is not allowed") {
    val s = signatureEmpty+IntSort
    val f1 = Operator(FG,"f",Arity1(IntSort -> IntSort))
    val f2 = Operator(BG, "f", Arity1(IntSort -> IntSort))
    Given(s"New operators $f1, $f2, which have same name & arity but different kinds")
    When("Adding to signature")
    Then("SyntaxError is thrown")
    intercept[SyntaxError] {
      val s1 = (s+f1)+f2
    }
  }

  test("Signature.+(op: Operator) overloading operators by arity") {
    val f1 = Operator(FG, "f", Arity1(ISort -> ISort))
    val f2 = Operator(FG, "f", Arity2((ISort,ISort) -> ISort))
    Given(s"New operators $f1, $f2, which have same name & kind but different arity")
    When("Adding to signature")
    val s1 = (signatureEmpty+f1)+f2
    Then("Operators appear in signature.operators")
    assert(s1.operators(f1))
    assert(s1.operators(f2))
    Then("Operators appear in signature.fgOperators/bgOperators")
    assert(s1.fgOperators(f1) && !s1.bgOperators(f1))
    assert(s1.fgOperators(f2) && !s1.bgOperators(f2))
    Then("findOperator can identify the correct operator by arg sort")
    assert(s1.findOperator(f1.name, List(ISort))===Some(f1))
    assert(s1.findOperator(f2.name, List(ISort,ISort))===Some(f2))
  }

  info("Testing Signature.+(sort: Sort): Signature")
  //def +(sort: Sort): Signature
  val newSort1 = FGSort("s1")
  val newSort2 = BGSort("s2")

  test("Signature.+(sort:Sort)- sorts are correctly added") {
    Given(s"New sorts $newSort1, $newSort2")
    When("Adding to empty signature")
    val s1 = (Signature.signatureEmpty+newSort1)+newSort2
    Then("The new sorts appear in the signature.sorts")
    assert(s1.sorts(newSort1),"sort(0) is not in Signature.sorts")
    assert(s1.sorts(newSort2),"sort(1) is not in Signature.sorts")
    Then("The new sorts appear in the signature.bgSorts/fgSorts and not vice versa")
    assert(s1.fgSorts(newSort1) && !s1.bgSorts(newSort1), "sort(0) is not identified by kind")
    assert(s1.bgSorts(newSort2) && !s1.fgSorts(newSort2), "sort(1) is not identified by kind")
    Then("findSort can find the new sorts")
    assert(s1.findSort(newSort1.name)===Some(newSort1),"Signature.findSort does not find the sort")
    assert(s1.findSort(newSort2.name)===Some(newSort2),"Signature.findSort does not find the sort")
  }

  test("Signature.+(sort: Sort)- no exception when adding an existing sort") {
    Given(s"New sorts $newSort1, $newSort2")
    When("Adding to empty signature")
    val s1 = (Signature.signatureEmpty+newSort1)+newSort2
    Then("Can add both again with no exceptions thrown and no change to the signature")
    assert(s1+newSort1 === s1, "Signature changed on adding duplicate sort")
    assert(s1+newSort2 === s1, "Signature changed on adding duplicate sort")
    Then("Can add multiple sorts with the same name and kind")
    assert((s1+newSort1)+FGSort("s1") === s1, "Signature changed on adding duplicate sort")
  }
/*
  test("Signature.+(sort: Sort)- exception when adding a duplicate sort") {
    val newSort1 = FGSort("s1")
    val newSort2 = FGSort("s1")
    Given("Two sorts with the same name")
    When("Adding to a signature")
    Then("SyntaxError is thrown")
    intercept[SyntaxError] {
      (signatureEmpty+newSort1)+newSort2
    }
  }
*/

/*

  info("Testing Signature.eqOperators: Map[Sort, Operator]")

  test("Signature.eqOperators contains equality operators for a new sort") {
    Given("New sorts "+List(newSort1,newSort2))
    When("Adding to empty signature")
    val s1 = (Signature.signatureEmpty+newSort1)+newSort2
    Then(s"eqOperators contains equality operators for $newSort1")
    assert(s1.eqOperators.isDefinedAt(newSort1),s"eqOperators($newSort1) is undefined")
    Then(s"eqOperators contains equality operators for $newSort2")
    assert(s1.eqOperators.isDefinedAt(newSort2),s"eqOperators($newSort2) is undefined")
  }
*/

}
