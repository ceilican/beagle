package beagle.test

import org.scalatest._
import beagle._
import fol._
import term._
import finite._
import bgtheory.LIA._
import datastructures._

class FiniteSuite extends FunSuite with GivenWhenThen {

  /*
   * Adds new definitions for the BSFG terms in c at exception points given in pi. 
   * Assumes that the value sets of pi are subsets of the delta sets in c.
   * Assumes that when not defined, the value of pi is the empty set.
   */
  //def finiteTransform(c: FQClause, pi: Map[Var,Domain[DomElemInt]]): Set[FQClause]
  
  def properFQClause(fqc: FQClause): Boolean = {
    fqc.clause.unabstrAggressive.vars subsetOf fqc.delta.keys.toSet
  }

  val x = AbstVar("X",IntSort)
  val y = AbstVar("Y",IntSort)
  val z = AbstVar("Z",IntSort)

  Sigma=addStandardOperators(Sigma)
  bgtheory.setSolver("cooper-clauses")

  //non BSFG clauses
  val c1 = FQClause(Map(),
		    Clause(List(LessEq(x,DomElemInt(9)).toLit,Less(DomElemInt(4),x).toLit)))
	    .simplifyCheap
	    .head

  val f = Operator(FG,"f",Arity1(IntSort->IntSort))
  Sigma+=f

  //clause with nested operators
  val c_2_1 = FQClause(Map(x -> new RangeExcept(0,10)) ,
		       Clause(List(Eqn(PFunTerm(f,List(PFunTerm(f,List(x)))),x).toLit, Eqn(x,DomElemInt(3)).toLit)))

  /* ----beginning of test cases------------------ */
  info("Tests for FiniteSearch.finiteTransform")
  
  test("1: Clauses with no BSFG terms are untouched") {
    assert(FiniteSearch.finiteTransform(TrueFQClause, Map(x -> SingletonDom(1)))===Set(TrueFQClause))
    assert(FiniteSearch.finiteTransform(c1, Map(x -> SingletonDom(1)))===Set(c1))
  }

  //test("simple ground version"){}

  //test("new definitions for BSFG terms in c at points in pi are added"){}

  //each point of the original deltas has a definition
  //output either has no BSFG terms or is a flat definition
  
  test("2: Nested definitions are properly handled"){
    val res_2_1 = FiniteSearch.finiteTransform(c_2_1,Map(x -> SingletonDom(2)))
    Given("f(f(x))=x | x=3 <- x in [0,10]")
    //should get: f(2)=a, f(a)=b, b=2 | 2=3, f(X) =a2 <- X!=2, f(a2)=b2, b2=2 | X=3 <- X!=2
    When("applying FiniteSearch.finiteTransform with x->[2]")

    Then("each of the produced clauses is properly finitely quantified")
    assert(res_2_1 forall { properFQClause(_) }, 
	   s"Some clause in $res_2_1 was not properly finitely quantified")

    Then("6 definitions are produced")
    assert(res_2_1.size === 6,
	   s"$res_2_1 did not contain the correct number of clauses")

    Then("there are 4 definitions for f")
    assert(res_2_1.filter(_.operators(f)).size === 4)
  }

  test("3: Where pi is an empty Map") {
    Given(c_2_1.toString)
    When("Applying FiniteSearch.finiteTransform with Map()")

    Then("TrueFQClause should be unchanged")
    assert(FiniteSearch.finiteTransform(TrueFQClause, Map())===Set(TrueFQClause))

    Then(s"$c_2_1 produces 3 default definitions")
    assert(FiniteSearch.finiteTransform(c_2_1, Map()).size===3)
    //so f(X)=a, f(a)=b, b=X | X=3 <- Sx
  }

  //test("Assume values of pi are subsets of the deltas in c"){}
  //what happens if they are not?

  /* --------------Testing FQClauseSet- nonFQCl------------------------- */
  info("Tests for FQClauseSet")

  //simple bsfg clause: Y=3 <- dX
  val c4_1 = FQClause(Map(x -> new RangeExcept(0,10)) ,
		       Clause(List(Eqn(y,DomElemInt(3)).toLit),Set(),0))

  //simple bsfg clause: f3=4 <- dX
  val c4_2 = FQClause(Map(x -> new RangeExcept(0,10)) ,
		       Clause(List(Eqn(PFunTerm(f,List(DomElemInt(3))),DomElemInt(4)).toLit),Set(),0))

  //simple bsfg clause: fX=X <- dX
  val c4_3 = FQClause(Map(x -> new RangeExcept(0,10)) ,
		       Clause(List(Eqn(PFunTerm(f,List(x)),x).toLit),Set(),0))
  
  test("4: FQClauseSet.nonFQCl"){
    //Empty => Empty
    val empty = new FQClauseSet(Set())
    assert(empty.nonFQCl == Set.empty[Clause], "An empty FQClauseSet has no non-FQ clauses")

    // 3=X <- Y => 3=X
    val test1 = new FQClauseSet(Set(c4_1))
    assert(test1.nonFQCl === Set(c4_1.clause))

    // f(c)=d <- Y => f(c)=d
    val test2 = new FQClauseSet(Set(c4_2))
    assert(test2.nonFQCl === Set(c4_2.clause))

    // cl => cl
    val test4 = new FQClauseSet(Set(c1))
    assert(test4.nonFQCl === Set(c1.clause))

    // cl, f(X)=X <- X => cl
    val test5 = new FQClauseSet(Set(c1,c4_3))
    assert(test5.nonFQCl === Set(c1.clause))
  }

  info("Tests for FQClause")

  //test sneaky non-idempotent subs example
  //applying [x->y,y->z] to C[x,y,z] where dx & dy & dz = empty should not be trivial.
  //because then it would not properly match another clause
  test("Non-idempotent substitutions are properly applied"){
    val p = Operator(FG,"p",Arity3((IntSort,IntSort,IntSort)->Signature.OSort))

    Given("a clause P(x,y,z) <- x in [0,1], y in [1,2], z in [2,3]")
    val cl = FQClause(Map(x -> new RangeDom(0,2),
			   y -> new RangeDom(1,3),
			   z -> new RangeDom(2,4)),
		       Clause(List(PredEqn(PFunTerm(p,List(x,y,z))).toLit)))
    
    val s1 = Subst(x->y,y->z)
    When(s"applying $s1")

    val expect = FQClause(Map(y -> new RangeDom(0,2),
    			      z -> new RangeDom(2,3)),
    			  Clause(List(PredEqn(PFunTerm(p,List(y,z,z))).toLit)))

    Then(s"get $expect")
    assert(s1(cl)===expect)

    val s2 = Subst(x->DomElemInt(0),y->x)
    When(s"applying $s2")

    val expect2 = FQClause(Map(x -> new RangeDom(1,3),
    			       z -> new RangeDom(2,4)),
    			  Clause(List(PredEqn(PFunTerm(p,List(DomElemInt(0),x,z))).toLit)))

    Then("get P(0,x,z) <- x in [1,2], z in [2,3]")
    assert(s2(cl)===expect2)
  }

  info("Tests for Domain")
    //include Empty, SingletonDom, RangeDom, SetDom, RangeExcept
    //both normal constructors and companion object constructors

  test("Equality between different representations of {}") {

    val cases = List(
      EmptyIntDomain,
      RangeDom(0,0),
      new RangeDom(0,0),
      SetDom(Set()),
      new SetDom(Set()),
      RangeExcept(0,0,Set()),       //where lb==ub
      RangeExcept(0,5,Set(0,1,2,3,4))          //where except==[lb,ub-1]
    )
    
    for(c1 <- cases;
	c2 <- cases) {
      assert(c1.equals(c2), s"$c1 : ${c1.getClass} did not equal $c2 : ${c2.getClass}")
      //assert(c2.equals(c1), s"$c2 : ${c2.getClass} did not equal $c1 : ${c1.getClass}")
    }
  }

  test("Equality between different representations of {2}") {

    val cases = List(
      SingletonDom(2),
      new SingletonDom(2),
      SetDom(Set(DomElemInt(2))),
      new SetDom(Set(DomElemInt(2))),
      RangeDom(2,3),
      new RangeDom(2,3),
      RangeExcept(2,3,Set()),
      RangeExcept(0,4,Set(0,1,3))
    )

    for(c1 <- cases;
	c2 <- cases) {
      assert(c1.equals(c2), s"$c1 : ${c1.getClass} did not equal $c2 : ${c2.getClass}")
    }
  }

  test("Equality between different representations of [2,3,4]") {

    val deSet = Set(2,3,4).map(DomElemInt(_))

    val cases1 = List(
      SetDom(deSet),
      new SetDom(deSet),
      RangeDom(2,5),
      new RangeDom(2,5),
      RangeExcept(2,5,Set()),
      RangeExcept(0,5,Set(0,1))
    )
    
    for(c1 <- cases1;
	c2 <- cases1) {
      assert(c1.equals(c2), s"$c1 : ${c1.getClass} did not equal $c2 : ${c2.getClass}")
    }
  }

  test("Equality between different representations of {3,5,7}") {

    val deSet2 = Set(3,5,7).map(DomElemInt(_))

    val cases2 = List(
      SetDom(deSet2),
      new SetDom(deSet2),
      RangeExcept(2,8,Set(2,4,6)),
      RangeExcept(3,8,Set(4,6))
    )

    for(c1 <- cases2;
	c2 <- cases2) {
      assert(c1.equals(c2), s"$c1 : ${c1.getClass} did not equal $c2 : ${c2.getClass}")
    }

  }

}
