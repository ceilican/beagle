package beagle.test

import org.scalacheck._
import beagle._
import fol._
import term._
import bgtheory.LIA._
import Signature.ISort

object TermGenerators {
  import Gen._
  import Arbitrary.arbitrary

  /** Generate an arbitrary sort. For now just one of IntSort or ISort
   * @todo add other sorts
   */
  val sortGen = oneOf(const(IntSort),const(Signature.ISort))

  //TODO- terms can be overloaded, it's rare but possible...

  /* ---- Operators------------------- */

  /** Create a foreground operator which has `s` as a result sort. */
  def fgOpGen(s: Type): Gen[Operator] = 
    for {
      pre <- oneOf(const("f"),const("g"),const("h"))
      index <- choose(0,100)
      name = pre+index
      n <- choose(1,7)
      args <- Gen.containerOfN[List,Type](n,sortGen)
      //make n choices for arg. sort and 1 for res sort
    } yield Operator(FG,name,Arity(args,s))

  /** Create an FG operator with an arbitrary sort */
  val fgOpGen: Gen[Operator] = for { res <- sortGen; op <- fgOpGen(res) } yield op

  /** Includes FG ops, Sum, Difference and UMinus for ints */
  implicit def arbOperator: Arbitrary[Operator] = 
    Arbitrary(oneOf(fgOpGen,const(Sum.op),const(UMinus.op),const(Difference.op)))

  /* ----Vars------------------------- */

  /** Note that there is a 1/100 chance of generating an existing GenVar */
  val genVarGen = for {n <- choose(0,100)} yield GenVar("x_"+n)

  /** Note that there is a 1/100 chance of generating an existing AbstVar */
  val abstVarGen = for {n <- choose(0,100)} yield AbstVar("x_"+n,IntSort)

  def varGen(s: Type) = s match {
    case IntSort => abstVarGen
    case ISort => genVarGen
  }

  /** Yields either an abstraction variable or general variable */
  implicit def arbVar: Arbitrary[Var] = 
    Arbitrary(oneOf(genVarGen,abstVarGen))

  /* ----Consts----------------------- */

  //include BG and FG
  val bgConstGen = for {n <- choose(0,100)} yield BGConst("c_"+n,IntSort)
  val fgConstGen = for {n <- choose(0,100)} yield FGConst("d_"+n,ISort)

  //TODO- should this include negative ints, or can they be accessed using UMinus?
  val domElemGen = for {n <- choose(0,1000)} yield DomElemInt(n)

  def constGen(s: Type) = s match {
    case IntSort => oneOf(bgConstGen,domElemGen)
    case ISort => fgConstGen
  }

  /** Can return None if `sig` has no constants */
  def constGen(s: Type, sig: Signature): Gen[Term] = s match {
    case IntSort => oneOf(bgConstGen,domElemGen)
    case ISort => 
      for { 
	op <- oneOf(sig.fgOperators.filter(op => op.sort()==s && op.arity.nrArgs==0).toSeq) 
      } yield FGConst(op)
  }

  implicit def arbConst : Arbitrary[Const] = 
    Arbitrary(oneOf(bgConstGen,fgConstGen,domElemGen))

  /* ----Terms------------------------- */

  def funTermGen(s: Type): Gen[FunTerm] =
    for {
      op <- fgOpGen(s)
      args = (op.arity.argsSorts.map(termGen(_).sample.get))
    } yield PFunTerm(op,args)

  def funTermGen(s: Type, sig: Signature): Gen[Term] = 
    for {
      op <- oneOf(sig.operators.filter(_.sort()==s).toSeq)
      args = (op.arity.argsSorts.map(termGen(_,sig).sample.get))
    } yield 
      if(!args.isEmpty) PFunTerm(op,args)
      else SymConst(op)

  //PFunTerm generator
  val funTermGen: Gen[FunTerm] = 
    for { s <- sortGen; t <- funTermGen(s) } yield t

  def termGen(s: Type): Gen[Term] = Gen.frequency(
    (1, funTermGen(s)),
    (4, constGen(s)),
    (5, varGen(s))
  )

  /** Non-failing, i.e. if no operators of desired sort/arity just return a var */
  def termGen(s: Type, sig: Signature): Gen[Term] = s match {
    case IntSort => //we assume this always has a full LIA signature, so ok
      Gen.frequency(
	(1, funTermGen(s, sig)),
	(4, constGen(s, sig)),
	(5, varGen(s))
      )
    case ISort => { //otherwise check existence of constants/function operators
      val ops = sig.operators.filter(_.sort()==s)
      if(ops.isEmpty) varGen(s)
      else {
	var genList: List[(Int,Gen[Term])] = List((5, varGen(ISort)))
	if(ops.exists(_.arity.nrArgs==0)) 
	  genList = (4,constGen(ISort,sig)) :: genList
	if(ops.exists(_.arity.nrArgs>0)) 
	  genList = (1,funTermGen(ISort,sig)) :: genList
	Gen.frequency(genList.toSeq:_*)
      }
    }
  }

  /** Generate a random term over a given signature. */
  def termGen(sig: Signature): Gen[Term] = 
    for { s <- sortGen
	 t <- termGen(s,sig) } yield t
  
  implicit def arbTerm: Arbitrary[Term] =
    Arbitrary(oneOf(arbitrary[Var],arbitrary[Const],funTermGen))

  /* ----Signature----------------------- */

  val sigGen = for {
      fgOps <- Gen.containerOf[List,Operator](fgOpGen)
    } yield (new Signature(Set(IntSort),fgOps.toSet ++ LIAOperators))

  /** Generate a random signature, including LIA operators */
  implicit def arbSig: Arbitrary[Signature] = 
    Arbitrary(sigGen)

  /* ----Substitutions-------------------- */

  /**This can produce maps which are not valid substitutions e.g. [x->y, y->x]
   * or [x->f(y), y->4], also non-injective substitutions and non-reduced substitutions,
   * i.e. they could contain [x->x].
   */
  val substGen: Gen[Subst] = 
    for {
      xs <- Gen.containerOf[List,Var](arbitrary[Var])
      ts <- Gen.containerOf[List,Term](arbitrary[Term])
      coDom = (ts.distinct ++ xs).toSeq //some can be identity
      //generate a map by, for each variable choosing a random image
      m <- Gen.listOf[(Var,Term)](for {x <- oneOf(xs); t <- oneOf(coDom) } yield (x -> t))
    } yield new Subst(m.toMap)

  implicit def arbSubst : Arbitrary[Subst] = Arbitrary(substGen)

}
