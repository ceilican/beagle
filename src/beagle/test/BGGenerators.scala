package beagle.test

import org.scalacheck._
import beagle._
import fol._
import term._
import bgtheory.LIA._


object PolyGenerators {
  import Gen._
  import Arbitrary.arbitrary

  /* Basic types: var, const and both */

  implicit def arbBGVar : Arbitrary[AbstVar] = 
    Arbitrary(for {n <- choose(0,100)} yield AbstVar("x_"+n,IntSort))
  
  implicit def arbBGConst : Arbitrary[SymConst] = 
    Arbitrary(for {n <- choose(0,100)} yield BGConst("c_"+n,IntSort))
  
  implicit def arbVarOrSymConst: Arbitrary[VarOrSymConst] = 
    Arbitrary(oneOf(arbitrary[AbstVar],arbitrary[SymConst]))

  /**
   * Generate an arbitrary negative, positive or zero monomial for
   * the variable x
   */
  def monoForX(x: VarOrSymConst): Gen[Monomial] = 
    for { c <- choose(-1000,1000)} yield Monomial(c,x)

  val monomialGen = 
    for {
      x <- arbitrary[VarOrSymConst]
      m <- monoForX(x)
    } yield m

  //TODO- how can we test the case where an x-monomial occurs in the list ms twice?
  val polyGen =
    for { 
      ms <- Gen.containerOf[List,Monomial](monomialGen)
      k <- choose(-1000,1000)
    } yield Polynomial(ms,k,Nil)

  implicit def arbPoly: Arbitrary[Polynomial] = Arbitrary(polyGen)
  implicit def arbMon: Arbitrary[Monomial] = Arbitrary(monomialGen)

}

object BGFormulaGenerator {
  import Gen._
  import Arbitrary.arbitrary
  import PolyGenerators._

  val genZeroEQPoly: Gen[Atom] = for(p <- arbitrary[Polynomial]) yield ZeroEQPoly(p)
  val genZeroLTPoly: Gen[Atom] = for(p <- arbitrary[Polynomial]) yield ZeroLTPoly(p)
  val genZeroNEPoly: Gen[Atom] = for(p <- arbitrary[Polynomial]) yield ZeroNEPoly(p)

  val genAtom = 
    oneOf(genZeroEQPoly,
	  genZeroLTPoly,
	  genZeroNEPoly,
	  const(TrueAtom),
	  const(FalseAtom))

  //TODO- this only generates NNF
  val genNeg = for(f <- genAtom) yield Neg(f)

  val genAnd = for(f1 <- genFormula; f2 <- genFormula) yield And(f1,f2)
  
  val genOr = for(f1 <- genFormula; f2 <- genFormula) yield Or(f1,f2)

  def genFormula: Gen[Formula] = Gen.frequency(
    (1, genNeg),
    (4, genAnd),
    (1, genOr),
    (14, genAtom.asInstanceOf[Gen[Formula]])
  )

  //implicit def arbAtom: Arbitrary[Atom] = Arbitrary(genAtom)

  implicit def arbFormula: Arbitrary[Formula] = Arbitrary(genFormula)

}
