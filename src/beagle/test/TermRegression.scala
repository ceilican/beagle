package beagle.test

import org.scalatest._

import beagle._
import fol._
import term._
import datastructures._
import PMI._
import bgtheory.LIA._

/**
* Tests which specify the current behaviour of Term, useful to
* spot if anything has changed.
*/
class TermRegression extends FunSuite with GivenWhenThen {
  //consider only the major leaf classes in Term, i.e. PFunTerm, Const and GenVar/AbstVar
  info("GenVar")
  
  //case class constructor
  test("GenVar case class constructor- concrete value members") {
    val gv = GenVar("X",5,Signature.ISort)

    //def freshVar() = GenVar(name, Term.variantCtr.next(), sort)
    val gv_fresh = gv.freshVar
    assert(gv_fresh.isInstanceOf[GenVar])
    assert(gv_fresh.name===gv.name)
    assert(gv_fresh.sort===gv.sort)
    assert(gv_fresh.index!=gv.index)

    //def freshGenVar() = freshVar()
    assert(gv.freshGenVar!=gv)

    //val kind = if (sort.kind == BG) Expression.ImpureBG else Expression.FG
    val gvInt = GenVar("X",0,IntSort)
    assert(gvInt.kind === Expression.ImpureBG)
    assert(gv.kind === Expression.FG)

    //concrete value members inherited from var, also applies to AbstVar
    assert(gv.termIndex === TermIndex.empty)
    assert(gv.depth === 0)
    assert(gv.varsCnt === Map(gv -> 1))
    assert(gv.vars === Set(gv))
    assert(gv.weightForKBO === KBO.varWeight)
    assert(gv.symConsts === Set.empty[SymConst])
    assert(gv.minBSFGTerms === Set.empty[Term])
    assert(gv.maxBSFGTerms === Set.empty[Term])
    assert(gv.sorts === Set(Signature.ISort))
    assert(gv.operators === Set.empty[Operator])

    assert(gv.preorder === List(gv))

    assert(gv.thisvar === gv)
    assert(gv.subterms.toList === List(gv))

    assert(gv.iteExpanded === (gv, List.empty))
    assert(gv.elimNonLinearMult === gv)
  }

  //version 1
  test("GenVar companion object apply(String)") {
    val gv1 = GenVar("Y")
    assert(gv1.index === 0)
    assert(gv1.sort === Signature.ISort)
  }

  //version 2
  test("GenVar companion object apply(String,Sort)") {
    val gv2 = GenVar("Z",IntSort)
    assert(gv2.index === 0)
  }

  info("AbstVar")

  test("AbstVar case class constructor- concrete value members") {
    val v = AbstVar("X",5,IntSort)

    //def freshVar() = AbstVar(name, Term.variantCtr.next(), sort)
    val v_fresh = v.freshVar
    assert(v_fresh.isInstanceOf[AbstVar])
    assert(v_fresh.name===v.name)
    assert(v_fresh.sort===v.sort)
    assert(v_fresh.index!=v.index)

    //def freshGenVar() = freshVar()
    val v_fresh_g = v.freshGenVar
    assert(v_fresh_g.isInstanceOf[GenVar])
    assert(v_fresh_g.name===v.name)
    assert(v_fresh_g.sort===v.sort)
    assert(v_fresh_g.index!=v.index)

    assert(v.kind === Expression.PureBG)

    //concrete value members inherited from var, also applies to AbstVar
    assert(v.termIndex === TermIndex.empty)
    assert(v.depth === 0)
    assert(v.varsCnt === Map(v -> 1))
    assert(v.vars === Set(v))
    assert(v.weightForKBO === KBO.varWeight)
    assert(v.symConsts === Set.empty[SymConst])
    assert(v.minBSFGTerms === Set.empty[Term])
    assert(v.maxBSFGTerms === Set.empty[Term])
    assert(v.sorts === Set(IntSort))
    assert(v.operators === Set.empty[Operator])

    assert(v.preorder === List(v))

    assert(v.thisvar === v)
    assert(v.subterms.toList === List(v))

    assert(v.iteExpanded === (v, List.empty))
    assert(v.elimNonLinearMult === v)
    
    intercept[java.lang.AssertionError] {
      AbstVar("X",0,Signature.ISort)
    }
  }

  //version 1
  test("AbstVar companion object apply(String,Sort)") {
    val gv2 = AbstVar("Z",IntSort)
    assert(gv2.index === 0)
  }
}
