package beagle.test

import org.scalatest._

import beagle._
import datastructures._
import fol._
import term._
import bgtheory.LIA._

/**
* Some tests for clauses
*/
class ClauseSuite extends FunSuite with GivenWhenThen {

  info("Tests for Clause methods which return new Clauses")

  /* Initial datastructures */
  val empty1 = Clause(List(),Set(),0)
  val empty2 = Clause(List(),Set(1,2,3),3)

  val x = GenVar("X")
  val y = GenVar("Y")
  val z = GenVar("Z")
  // val w = AbstVar("W",IntSort)
  // val w2 = AbstVar("W_2", IntSort)

  val c = FGConst("c",Signature.ISort)
  val d = FGConst("d",Signature.ISort)

  Sigma = addStandardOperators(Signature.signatureEmpty)
  Sigma+=c.op
  Sigma+=d.op

  /* The clauses we will use for testing */
  val c1 = Clause(List(Eqn(x,y).toLit),Set(),0)
  val c2 = Clause(List(Eqn(x,y).toLit,Eqn(z,y).toLit),Set(1),1)
  val c3 = Clause(List(Eqn(c,d).toLit),Set(1),0)

  val all = List(empty1,empty2,c1,c2,c3)

  info("Basic test clauses:\n"+all.mkString("\n")+"\n")

  /* init bgsolver to use for simplifyBG */
  bgtheory.setSolver("cooper-clauses")

  info("Testing Clause.applySubst and ID")

  test("Clause.applySubst preserves ID if sigma does not apply") {
    val s1 = Subst(z->y)
    Given(s"sigma = $s1 applied to clause $c1")
    
    Then("Clause.id remains the same.")
    assert(s1(c1).id === c1.id)
  }

  test("Clause.applySubst yields different ID if sigma modifies clause") {
    val s2 = Subst(x->y)
    Given(s"sigma = $s2 applied to clause $c1")

    Then("Clause.id is different.")
    assert(s2(c1).id != c1.id, "ID is SAME for different clauses")
  }

  info("Testing Clause.fresh() and Clause.freshGenVars()")

  test("Clause.fresh always preserves ID"){
    for(c <- all)
      assert(c.fresh.id == c.id, "For "+c+" ID is different after fresh()")
  }

  test("Clause.fresh returns a clause with non-intersecting variable set"){
    for(c <- all;
	cf = c.fresh)
      assert( !(cf.vars exists {c.vars(_)}), s"$c and $cf share a variable")
  }

  test("Clause.freshGenVars always preserves ID"){
    for(c <- all)
      assert(c.freshGenVars.id == c.id, "For "+c+" ID is different after fresh()")
  }

  test("Clause.modified"){
    //if id is not set- same?
    //if id is set
  }

  info("Testing Clause.replaceAt(pos: Pos, t: Term): Clause")

  test("replace at empty position should not be possible") {
    val someTerms = List[Term](x,y,z,c,d)
    Given(s"Any clause and terms $someTerms")
    When("replacing with position = Nil")
    Then("Always get InternalError")
    for(c <- all;
	t <- someTerms) {
      intercept[util.InternalError] {
	c.replaceAt(Nil, t) 
	info(s"replaceAt with empty position succeeded for $c")
      }
    }
  }

  test("replace at literal position should not be possible") {
    val someTerms = List[Term](x,y,z,c,d)
    Given(s"Any clause and terms $someTerms")
    When("replacing with position = List(1)")
    Then("Always get InternalError")
    for(c <- all;
	t <- someTerms) {
      intercept[util.InternalError] {
	c.replaceAt(List(1), t) 
	info(s"replaceAt with literal position succeeded for $c")
      }
    }
  }

  //This should be tested in Term.replaceAt...
  /*

  test("replace at non-existing position should not be possible") {}

  test("replace with ill-sorted term should not be possible") {
    val d = BGConst("d",IntSort)
    val cl = List(c1,c2,c3)
    Given(s"A BG sorted constant and pure FG clauses $cl")
    When("Calling replaceAt, anywhere")
    Then("Always get None")
    for(c <- cl;
	p <- c.positions)
      assert(c.replaceAt(p,d) === None)
  }
  */

  test("if replacement is possible, new ID"){
    
  }

  test("If replacement is not possible, same ID") {
    //cases:
    //bad position
    //bad sort
  }

  test("Clause.simplifyBG: ID is different if the clause is simplified"){

    val cSimp = Clause(List(Eqn(DomElemInt(3),DomElemInt(4)).toLit),Set(),0)
    Given(s"A BG clause: $cSimp")

    val cs2 = cSimp.simplifyBG
    When(s"simplified by BGSolver, get: $cs2")

    Then("The new clause should have a different ID")
    assert(cs2.id != cSimp.id,s"$cSimp simplifies to $cs2, but IDs match.")
  }

  test("Clause.simplifyBG: ID is preserved if the clause is not simplified") {
    Given("Clauses which do not contain BG literals")
    assert(all forall (_.lits forall (!_.isBG)))

    When("simplifyBG is called")

    Then("The clause ID remains the same")
    for(c <- all)
      assert(c.id == c.simplifyBG.id, s"$c is ${c.simplifyBG} but the ID is different.")
  }

  test("Clause.simplifyCheap- ID is different when simplification is applied"){
    val cSimp = Clause(List(Eqn(x,y).toLit,Eqn(z,z).toLit.compl),Set(),0)
    Given(s"A clause $cSimp which can be simplified by simplifyCheap")
    val cs2 = cSimp.simplifyCheap
    When(s"Applying simplify cheap, get $cs2")
    Then("None of these clauses should have the same ID")
    for(c <- cs2) 
      assert(cSimp.id != c.id,s"$cSimp simplifies to $c, but IDs match.")
  }
  
  test("Clause.simplifyCheap preserves ID when not applied") {
    for(c <- all;
	c2 <- c.simplifyCheap)
      assert(c.id == c2.id, s"$c is ${c2} but the ID is different.")
  }

  test("sko():") {
    Given("Ground clauses")
    val cl = all.filter(_.isGround)
    When("Calling Clause.sko()")
    Then("The clause ID is preserved")
    for(c <- cl)
      assert(c.sko.id == c.id, "Non-skolemised clause has different ID")
  }
  
  test("sko(): ID different if skolemisation is possible") {
    Given("Non-ground clauses")
    val cl = all.filterNot(_.isGround)
    When("Calling Clause.sko()")
    Then("The clause ID is different")
    for(c <- cl)
      assert(c.sko.id != c.id, "New skolem clause has same ID as parent")
  }

  //abst applies either stdAbstract or weakAbstract
  //test those instead
  /*
  test("ID preserved in clause.abstr"){
    //using flags.stdabst=true should have res.id=in.id
    //otherwise same id again
    //use clauses where abstraction does and does not apply
  }
  */

  //is abstr/unabstr ever applied here?
  test("Clause.stdAbstract preserves ID"){
    for(c <- all)
      assert(c.stdAbstract.id == c.id)
  }

  test("Clause.unabstr preserves ID"){
    for(c <- all)
      assert(c.unabstrCautious.id == c.id)
  }

  test("Clause.unabstrAggressive preserves ID"){
    for(c <- all)
      assert(c.unabstrAggressive.id == c.id)
  }

  test("Clause.weakAbstract preserves ID"){
    for(c <- all)
      assert(c.weakAbstract.id == c.id)
  }

  test("Clause.geq is not sensitive to rearrangement of lits") {
    Given("Any clause")
    When("Permuting the literals")
    Then("Clause.geq is true in both directions")
    for(c <- all;
	l2 <- c.lits.permutations;
	c2 = c.modified(lits=l2)) {
	  assert((c geq c2) && (c2 geq c), s"$c was not geq than $c2 although they are the same")
	}
  }

  info("Clause.subsume bug")
  test("p(X,X) does not subsume p(X,Y) v p(Y,X)") {
    val p = Operator(FG,"p",Arity2((Signature.ISort,Signature.ISort) -> Signature.OSort))
    Sigma+=p

    val c1 = Clause(List(PredEqn(PFunTerm(p,List(x,x))).toLit))
    val c2 = Clause(List(PredEqn(PFunTerm(p,List(x,y))).toLit,
			 PredEqn(PFunTerm(p,List(y,x))).toLit))

    assert(!c1.subsumes(c2))
  }

  info("simplifyCheap")
  test("Simple tautology") {
    val c = Clause(List(Neg(Less(DomElemInt(0),DomElemInt(0))).toLit))
    Given("~(0<0)")
    Then("simplifyCheap yields True")
    assert(c.simplifyCheap.head==Clause(List(Lit.TrueLit)))
  }

}
