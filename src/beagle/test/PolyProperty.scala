package beagle.test

import org.scalacheck._
import beagle._
import fol._
import term._
import bgtheory.LIA._

object CooperProperties extends Properties("Cooper"){
  import org.scalacheck.Prop.{forAll, BooleanOperators}
  import PolyGenerators._
  import BGFormulaGenerator._

  property("elimEquation") = forAll { (f: Formula, x: VarOrSymConst) => {
    val f2 = cooper.normalize(f)
    ((f2 contains x) ==> (cooper.elimEquation(And(ZeroEQPoly(Polynomial(Monomial(1,x)::Nil,0,Nil)),f2),x) != None)) &&
    ((!(f2 contains x)) ==> (cooper.elimEquation(f2,x) == None))
  }}
}

object PolySpecification extends Properties("Polynomial"){
  import org.scalacheck.Prop.{forAll, BooleanOperators}
  import PolyGenerators._

  /* Not tested */
  //def +(n: Int): Polynomial
  //def -(n: Int): Polynomial
  //def /(n: Int): Polynomial

  /* TODO */
  //Add an improper monomial, while preserving the invariant
  //def +(im: ImproperMonomial): Polynomial

  //def *(n: Int): Polynomial

  // def -(p: Polynomial): Polynomial
  //def -(im: ImproperMonomial): Polynomial
  //def -(m: Monomial): Polynomial

  //Divide by the gcd of the coefficients
  //lazy val factorize: Polynomial
  //lazy val gcdCoeffs: Int

  // Replace the variable x in this by p.
  // def replace(x: VarOrSymConst, p: Polynomial): Polynomial

  // lazy val separate: (Polynomial, Polynomial)
  // lazy val toTerm: Term

  // lazy val unitCoeffVarOrSymConsts: Set[VarOrSymConst]
  // The set of variables or symbolic constants in this with a unit coefficient

  // lazy val varOrSymConsts: Set[VarOrSymConst]
  // The variables or symbolic constants in this polynomial

  /* Tests */

  //def +(p: Polynomial): Polynomial
  property("sum(polynomial)") = forAll { (p1: Polynomial, p2: Polynomial) => {
    val res = (p1+p2)
    ("(p1+p2) was "+res) |:
    ("constant has sum of both constants" |: res.k == p1.k+p2.k) &&
    ("variables occur just once" |: res.ms.map(_.x) == res.ms.map(_.x).distinct) &&
    ("all variables of both polys occur, unless |c1|==|c2|" |:
      (p1.varOrSymConsts++p2.varOrSymConsts).forall(x => res.varOrSymConsts(x) || p2.getCoeff(x).abs==p1.getCoeff(x).abs)
    )
  }}

  //Add a monomial, while preserving the invariant
  //def +(m: Monomial): Polynomial
  property("sum merges monomials") = forAll { (p: Polynomial, m: Monomial) => 
    (p+m).ms.filter(_.x==m.x) match {
      case List(Monomial(d,_)) if (d == m.c && !p.varOrSymConsts(m.x)) => true
      case List(Monomial(d,_)) if (d == (m.c + p.getCoeff(m.x))) => true
      case List() if(p.getCoeff(m.x) == -m.c) => true
      case _ => false
    }					   }

  property("sum preserves monomial order") = forAll { (p: Polynomial, m: Monomial) =>
    (p+m).ms.map(_.x).filterNot(_==m.x) == p.ms.map(_.x).filterNot(_==m.x)
						   }

  // Push a monomial as the first monomial into this.
  // def +:(m: Monomial): Polynomial
  property("+:(m: Monomial)") = forAll { (p: Polynomial, m: Monomial) =>
    ("When p contains an x-monomial" |: (p.varOrSymConsts(m.x)) ==> {
      val m2 @ Monomial(c,_) = p.+:(m).ms.head
      ("head of p+:m is "+m2) |:
	("p+:cx has (c+cp)x as head)" |: ((c == (m.c + p.getCoeff(m.x))))) &&
	("there is no other x monomial in the tail" |: !p.ms.tail.map( _.x ).contains(m.x))
    }) &&
    ("p does not contain x so the head is exactly m" |: p.+:(m).ms.head == m )
  }

  // lazy val withPositiveCoeff: Polynomial
  // Make the coefficient of the variable arranged for positive.
  property("withPositiveCoeff") = forAll { p: Polynomial => {
    val res = p.withPositiveCoeff
    ("The coefficient of the head monomial must be > 0" |: ((!p.ms.isEmpty) ==> res.getCoeff(p.ms.head.x) > 0))
  }}

  // Make the monomial with x the leftmost monomial, if x occurs in this
  // def rearrange(x: VarOrSymConst): Polynomial
  property("rearrange") = forAll { (p: Polynomial) => 
    ("p is unchanged if no monomials"                 |: ((p.varOrSymConsts.isEmpty) ==> (p.rearrange(AbstVar("x",IntSort)) == p))) ||
    ( !p.varOrSymConsts.isEmpty ==> { val x = p.varOrSymConsts.head
      ( s"When p has $x-monomial"                        |:  
	((s"after a rearrange the head is an $x-monomial"|: p.rearrange(x).ms.head.x == x ) &&
         (s"the tail does not contain $x"                |: p.rearrange(x).ms.tail.find(_.x==x).isEmpty)))
    })
  }

}
