package beagle.test

import org.scalacheck._
import beagle._
import fol._
import term._
import bgtheory.LIA._

object TermSpecification extends Properties("Term functions") {
  import TermGenerators._
  import DSGenerators._
  import org.scalacheck.Prop.{forAll, BooleanOperators}

  Sigma = addStandardOperators(Signature.signatureEmpty)
  bgtheory.setSolver("cooper-clauses")

  property("allSubtermsWithPos") = forAll { (t: Term) => {
    val posList = Term.allSubtermsWithPos(t)
    ("Every subterm pair must produce that subterm using get" |: Prop.all( posList.map({ case (s,p) =>
      (( s"The position $p should be non-empty" |: t.get(p).isDefined ) &&
       ( s"The term at $p in $t should be $s" |: t.get(p).get == s ))
    }).toSeq:_*)) &&
    ( "All variables are listed" |: Prop.all( t.vars.map( x => {
      ( s"$x must occur in $posList" |: posList.exists(_._1 == x) )
    }).toSeq:_*) )
  }}

  property("replaceSubterm") = forAll { (s: Term, t: Term) => {
    (s"Replacing subterm u (!=t) in s[u] means" |: Prop.all( s.subterms.map( u => {
      val s2 = s.replaceSubterm(u,t)
      ( s"$u should be replaced in the subterms list of $s2 by $t" |: (!(s2.subterms.contains(u)) || t.subterms.contains(u)) ) &&
      ( s"The positions occupied by $u are now occupied by $t" |: {
	Term.allSubtermsWithPos(s).forall({case (term,pos) => (term!=u) || s2.get(pos)==Some(t)})
     }) &&
      ( "Replacing head position means s==t" |: s.replaceSubterm(s,t)==t )
    }).toSeq:_*)) 
  }}

  property("replaceAt") = forAll { (s: Term, t: Term) => {
    ( s"Replacing any position in $s with $t" |: Prop.all( s.positions.map(pos => {
      val st = s.replaceAt(pos,t)
      (s"The subterm at $pos in $st is $t" |: st.get(pos)==Some(t) ) &&
      (s"Every other position in $st is the same" |: s.positions.forall(p2 => ( pos.indexOfSlice(p2)==0 || p2.indexOfSlice(pos)==0 || s.get(p2)==st.get(p2)) ))
    }).toSeq:_*)) && 
    ("Replacing at an undefined position does nothing" |: {
      val badPos = List.fill(s.depth+1)(0)
      s.replaceAt(badPos,t)==s
    })
  }}

}
