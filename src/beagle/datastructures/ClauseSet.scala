package beagle.datastructures

import beagle._
import util._
import calculus._
import fol._
import term._
import collection.mutable.ListBuffer
import collection.mutable.PriorityQueue

// import collection.mutable.{ Set => MSet }

/**
 * Thrown if BG clauses are inconsistent.
 */
case object BGClauseExtensionFail extends Exception


abstract class ClauseSet {
  type Collection <: Iterable[Clause]

  // Abstract members
  val clauses: Collection // All clauses in the clause set, use mutable collections
  def addToClauses(cl: Clause): Unit // Destructively add a clause to clauses
  def klone(): ClauseSet // Shallow copy  
  
  val maxWeight = if (flags.weightBound.value == 0) 20000 else flags.weightBound.value

  // Derived notions
  def add(cl: Clause) {
    // Check if clause is too heavy or got a variant already
    // if (cl.weight > maxWeight || (clauses exists { (_ ~ cl) })) return
    // if (cl.weight > maxWeight || (clauses exists { (_.lits == cl.lits) })) return
    if (cl.weight > maxWeight) return
    addToClauses(cl)
  }

  def add(cls: Iterable[Clause]) {
    cls foreach { add(_) }
  }

  def size = clauses.size
  def isEmpty = clauses.isEmpty

  // lazy val unitClauses = clauses filter { _.isUnitClause }
  // Can't use lazy val, because clauses is a mutable data structure
  def unitClauses: Iterable[Clause]

  def show() {
    clauses foreach { println(_) }
  }

}

import PMI._

class ListClauseSet extends ClauseSet with PMI[ListClauseSet] {

  type Collection = ListBuffer[Clause]

  val clauses = ListBuffer.empty[Clause]

  def apply(i: Int) = clauses(i)

  private var termIndexOpt:Option[TermIndex] = None
  // // Mixin PMI
  def termIndex = {
    if (termIndexOpt == None)
      // Have to build it from scratch
      termIndexOpt = Some(liftIndex(clauses.toList map { _.termIndex }))
    termIndexOpt.get
  }

  private var unitClausesOpt: Option[ListBuffer[Clause]] = None
  private def buildUnitClauses() {
    if (unitClausesOpt == None)
      unitClausesOpt = Some(clauses filter { _.isUnitClause })
  }

  def unitClauses = {
    buildUnitClauses()
    unitClausesOpt.get
  }


  def replaceAt(pos: Pos, t: Term) = {
    val clPos :: inClPos = pos
    if (clauses(clPos).isUnitClause) unitClausesOpt = None
    // Destructive!
    clauses(clPos) = clauses(clPos).replaceAt(inClPos, t)
    if (clauses(clPos).isUnitClause) unitClausesOpt = None
    termIndexOpt = None
    this
  }


  def addToClauses(cl: Clause) {
    cl +=: clauses // This prepends cl to clauses
    // todo: could also append to clauses and re-use index
    termIndexOpt = None
    if (cl.isUnitClause) {
      unitClauses += cl // This works!
    }
  }

  // Interreduce the clauses.
  // Tacitly assume clauses was interreduced before the last clause was added.
  // Hence can start with last clause added 
  // Nothing is done when the empty clause is generated. That is, 
  // the caller need to check that.
  def interreduce() {
    termIndexOpt = None
    unitClausesOpt = None

    if (clauses.isEmpty) return

    val open = ListBuffer(clauses.head) // The list of clauses to be added to clauses
    clauses.trimStart(1)
    while (!open.isEmpty) {
      val next = open.head // chose any
      open.trimStart(1)
      // reduce next using the clauses in closed
      next.reduce(new ImmutableListClauseSet(clauses)) match {
        case Nil => () // Clause is deleted
        case List(nextRed) => {
          // We reduce clauses by nextRed and put those that have been simplified
          // into open.
          val oldClauses = clauses.clone() // Rebuild clauses from a copy
          clauses.clear()
          val nextRedCls = new ImmutableListClauseSet(List(nextRed))
          for (cl <- oldClauses) {
            val (clRed, touched) = cl.reduceWithStatus(nextRedCls)
            clRed match {
              case Nil => () // deleted
              case List(clRed) => 
                if (touched) 
                  open += clRed 
                else
                  // todo: Do we need test for variantship? No, reduce covers that
                  clauses += clRed
            }
          }
          // We need to put the reduced version of next into clauses
          clauses += nextRed
        }
      }
    }
  }

  /*
   * Add a clause cl to clauses, thereby keeping only those
   *  that are not reducible by cl and returning the
   * reduced versions (by cl)
   */
  def addAndBackwardReduce(cl: Clause): List[Clause] = {
    val rest = clauses.clone() // The clauses to be reduced
    var res = List.empty[Clause] // The resulting clauses, those that are simplified
    // Go through rest and separate away the clauses that are reducible by last
    clauses.clear()
    clauses += cl
    val clCls = new ImmutableListClauseSet(List(cl))
    for (into <- rest) {
	stats.simpBackRed.tried
        val (intoRed, touched) = into.reduceWithStatus(clCls)
        intoRed match {
          case Nil => () // deleted
          case List(intoRed) =>
            if (touched) { //  cannot do that: && !intoRed.isPureBG
              res ::= intoRed
              //stats.nrBackRed += 1
	      stats.simpBackRed.succeed
            }
            else
              clauses += intoRed // not touched - can keep in clauses
        }
      }
    termIndexOpt = None
    unitClausesOpt = None
    res
  }


  def klone() = {
    val h = clauses
    val res = new ListClauseSet {
      override val clauses = h.clone()
    }
    res
  }

  def removeLightest(): Clause = {
    require(!clauses.isEmpty, { println("removeLightest: clause set is empty") })
    // println("*** size "  + clauses.size)
    termIndexOpt = None
    var (best, iBest) = (clauses.head, 0)
    for (
      (tails, iTail) ← clauses.tail.tails zip Iterator.from(1);
      if !tails.isEmpty;
      next = tails.head
    ) if (next.isPureBG) {
      clauses.remove(iBest)
      return best
    } else if (next.weight < best.weight) {
      best = next
      iBest = iTail
    }
    // println("*** iBest "  + iBest)
    clauses.remove(iBest)
    if (best.isUnitClause) unitClausesOpt = None
    // best.delete()
    return best
  }
}



object ListClauseSet {

  def apply(cls: Clause*) = {
    val cs = new ListClauseSet
    cls foreach { cs.addToClauses(_) }
    cs
  }

  def apply(cls: ListBuffer[Clause]) = new ListClauseSet {
    override val clauses = cls
  }
}


class ImmutableListClauseSet(cls: Iterable[Clause]) extends ClauseSet {

  type Collection = List[Clause]

  val clauses = cls.toList

  def addToClauses(cl: Clause) {
    throw InternalError("ImmutableListClauseSet.addToClauses() should never be called")
  }

  def klone() = {
    throw InternalError("ImmutableListClauseSet.klone() should never be called")
  }

  lazy val unitClauses = clauses filter { _.isUnitClause }

}



/*
 * Clause sets implemented as priority queues
 */

class PQClauseSet extends ClauseSet {

  object WeightOrdering extends scala.math.Ordering[Clause] {

/*
    def priority(cl: Clause): Int = {
      // The more preferred the lower the result
      if (cl.isUnitClause && cl.lits(0).isFlat && !cl.lits(0).isNLPP) return 0
      return 1
      // if (cl.isUnitClause) return 1
      // if (cl.isUnitClause && cl.isPureBG) return 1
      // if (cl.isPureBG) return 2
      // if (cl.isBG) return 2
      // if (cl.isFlat) return 3
    }

 */
    def compare(a: Clause, b: Clause) = {
      // val (pa, pb) = (priority(a), priority(b))
      // if (pa < pb) 1
      // else if (pa > pb) -1
      // else 
        if (a.weight < b.weight) 1
      else if (b.weight < a.weight) -1
      else if (a.age < b.age) 1 // prefer older clauses
      else if (a.age > b.age) -1
      else if (a.idxRelevant.size < b.idxRelevant.size) 1
      else if (b.idxRelevant.size < a.idxRelevant.size) -1
      else 0
    }
  }

  type Collection = PriorityQueue[Clause]

  val clauses = PriorityQueue.empty[Clause](WeightOrdering)

  private var unitClausesOpt: Option[ListBuffer[Clause]] = None
  private def buildUnitClauses() {
    if (unitClausesOpt == None) {
      var cls = ListBuffer.empty[Clause]
      clauses foreach { cl =>
        if (cl.isUnitClause) cls += cl
      }
      unitClausesOpt = Some(cls)
    }
  }

  def unitClauses = {
    buildUnitClauses()
    unitClausesOpt.get
  }

  def addToClauses(cl: Clause) {
    clauses += cl
    if (cl.isUnitClause) {
      unitClauses += cl
    }
  }

  def klone() = {
    val h = clauses
    val res = new PQClauseSet {
      override val clauses = h.clone()
    }
    res
  }

  def removeLightest() = { 
    val res = clauses.dequeue()
    // res.delete()
    if (res.isUnitClause)
      unitClausesOpt = None
    res
  }

  def removeOldest() = { 
  // Select an oldest clause, and among these a lightest one
    require(!clauses.isEmpty, "removeOldest: clause set is empty")
    
    var best = clauses.dequeue() 
    var kept = List.empty[Clause] // All clauses but the removed one  
    
    // We remove the clauses one by one, inspecting if we've got a better one,
    // thereby remembering those to be kept
    while (!clauses.isEmpty) {
      val next = clauses.dequeue()
      if (next.age < best.age || (next.age == best.age && next.weight < best.weight)) {
        // found a better best: save the current best and make the new best the current best
        kept ::= best
        best = next
      } else 
        // best preserves, next is top be kept
        kept ::= next
    }
    // clauses is empty at this point, still need to push kept into clauses
    clauses ++= kept
    // best.delete()
    if (best.isUnitClause)
      unitClausesOpt = None
    best
  }

  // Add a clause cl to clauses and perform full interreduction.
  // Assumes that cl is already cheaply reduced
  def addAndInterreduce(cl: Clause) {

    val open = ListBuffer(cl) // The list of clauses to be added to clauses
    while (!open.isEmpty) {
      val next = open.head // chose any
      open.trimStart(1)
      // reduce next using the clauses in closed
      next.reduce(new ImmutableListClauseSet(clauses)) match {
        case Nil => () // Clause is deleted
        case List(nextRed) => {
          // We reduce clauses by nextRed and put those that have been simplified
          // into open.
          val oldClauses = clauses.clone() // Rebuild clauses from a copy
          clauses.clear()
          val nextRedCls = new ImmutableListClauseSet(List(nextRed))
          for (cl <- oldClauses) {
            val (clRed, touched) = cl.reduceWithStatus(nextRedCls)
            clRed match {
              case Nil => () // deleted
              case List(clRed) => 
                if (touched) 
                  open += clRed 
                else
                  // todo: Do we need test for variantship? No, reduce covers that
                  clauses += clRed
            }
          }
          // We need to put the reduced version of next into clauses
          clauses += nextRed
        }
      }
    }
    unitClausesOpt = None
  }


  /**
    * Reduce all clauses by the given clause set
    */
  def reduce(newCl: Clause) {
    val newClCset = new ImmutableListClauseSet(List(newCl))
    val oldClauses = clauses.toList
    clauses.clear()
    oldClauses foreach {
      cl => {
        clauses ++=
        (cl.reduceWithStatus(newClCset) match {
          case (clRed, true) => {
            // println("*** Reduce with %s\n    %s\n    %s".format(newCl, cl, clRed))
            val h = clRed flatMap { _.simplifyCheap }
            if (h forall {
              c => (// (c.length < cl.length) || 
                (c.idxRelevant subsetOf cl.idxRelevant))
            }) h
            else
              List(cl)
          }
          case (_, false) => List(cl)
        })
      }
    }
    unitClausesOpt = None
  }

  def reduce(cls: ClauseSet, newCl: Clause) {
    val newClCset = new ImmutableListClauseSet(List(newCl))
    val oldClauses = clauses.toList
    clauses.clear()
    oldClauses foreach {
      cl => {
        clauses ++=
        (cl.reduceWithStatus(newClCset) match {
          case (clRed, true) => {
            // println("*** Reduce with %s\n    %s\n    %s".format(newCl, cl, clRed))
            // clRed flatMap { _.simplifyCheap }
            val h = clRed flatMap { _.reduce(cls) } flatMap { _.simplifyCheap }
            if (h forall {
              c => ((c.length < cl.length) && (c.idxRelevant subsetOf cl.idxRelevant))
            }) h
            else
              List(cl)
          }
          case (_, false) => List(cl)
        })
      }
    }
    unitClausesOpt = None
  }

}


