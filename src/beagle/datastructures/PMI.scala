package beagle.datastructures

import beagle._
import fol.Operator
import fol.term._

/**
 * Poor Man's Indexing.
 * "Indexing" the terms occurring in a container data structure E (like Clause, Lit, Eqn, Term) 
 */
trait PMI[E] {

  import PMI._

  /*
   * Abstract members
   */

  def termIndex: TermIndex

  /** Replace the expression at the given position with t. */
  def replaceAt(pos: Pos, t: Term): E
  
  // Derived methods

  /** Get from termIndex all Terms with their positions that can be the into-term of a superposition inference with the lhs term t */
  def subTermsWithPosForSup(t: Term): Iterable[(Term, Pos)] = 
    t match {
      case x:AbstVar => List.empty
      // Superposition where the from-term is an abstraction variable is never neccessary:
      // If the into-subterm is pure BG then the superposition rule forbids the inference.
      // If the into-subterm is not pure BG then there is no simple restricted unifier
      case x:GenVar => termIndex.subTermsWithPos filter { case (s, _) => s.sort == x.sort }
        // for (tPoss <- termIndex.values;
        //   (s, pos) <- tPoss;
        //   if (s.sort == x.sort))
        // yield (s, pos)
      case t:FunTerm => termIndex(t.op)
      }

  /** @return an `Iterable` that contains a sequence of `(Term,Pos)` pairs,
   * including all non-variable (improper) subterms of this.
   */
  def subTermsWithPos = termIndex.subTermsWithPos

  def subTermsViaIndex = termIndex.subtermsViaIndex

  /** @return The set of valid positions in this expression. */
  def positions = termIndex.positions

  /** Get all subterms which satisfy some predicate */
  def filter(f: Term => Boolean): Iterable[Term] = 
    termIndex.subtermsViaIndex.filter(f)

  /** @return The position of the first subterm that satisfies `p`. */
  def findPos(p: Term => Boolean): Option[Pos] = 
    termIndex.subTermsWithPos find { case (term,pos) => p(term) } map {_._2}
  
}

object PMI {

  type Pos = List[Int]

  /**
   * @param m A map from an operator symbol to a list of occurrences (i.e. subterm and position) within
   * some parent term.
   */
  class TermIndex(val m: Map[Operator, List[(Term, Pos)]]) extends Iterable[(Operator, List[(Term, Pos)])] {

    /** Lookup `op` in the index */
    def apply(op: Operator) = m.getOrElse(op, List.empty[(Term, Pos)])

    /** Add a new entry to the index, existing values are kept. */
    def +(op: Operator, tPoss: List[(Term, Pos)]) = 
      new TermIndex(m.updated(op, m.getOrElse(op, List.empty[(Term, Pos)]) ::: tPoss))

    lazy val values = m.values

    // This must not be a lazy val!
    // OR use a stream? When is this used?
    def iterator = m.iterator

    /** Notice that this does *NOT* include variables! */
    lazy val subTermsWithPos: Iterable[(Term,Pos)] = m.values flatMap { _.toIterable }

    /** Lifts `subterms` to `Lit` and `Clause` */
    lazy val subtermsViaIndex: Iterable[Term] = subTermsWithPos map { _._1 }

    /** A list of valid positions in this */
    lazy val positions:List[Pos] = 
      (for(list <- m.values;
	   (t,p) <- list) yield p).toList.distinct

    override def equals(that: Any) = that match {
      case that:TermIndex => 
	(that canEqual this) && m == that.m
      case _ => false
  }

  override def canEqual(other: Any): Boolean = other.isInstanceOf[TermIndex]

  override def hashCode: Int = m.hashCode

  }

  object TermIndex {
    val empty = new TermIndex(Map.empty)
    def apply(m: (Operator, List[(Term, Pos)])*) =
      new TermIndex(m.toMap)
  }

  /** Used to derive an index inside a container which can have multiple indexes */
  def liftIndex(l: List[TermIndex]): TermIndex = {
    var res = TermIndex.empty
    for ((i, index) <- (l.indices zip l);
         (op, tPoss) <- index)
      res += (op, tPoss map { case (t, pos) => (t, i :: pos) })
    res
  }

  // Put in at the given position pos in a list l of terms a new term t 
/*  def replaceAt[E <: Expression[E], F <: SubEx[E, F]](l: List[F], pos: Pos, t: E): List[F] = {
    val p :: ps = pos // p is the index into the list l, in which we replace
    (for ((i, el) <- l.indices zip l) yield
            if (i == p) el.replaceAt(ps, t) else el).toList
  }
 */

//  def opsUnion(l: List[Set[Operator]]) = l.foldLeft(Set.empty[Operator]) { _ ++ _ }

  /**
   * Put a new term `t` at the given position `pos` in a list `l` of terms.
   * The head of `pos` is the index of the term to replace inside of.
   * @return None if the index is greater than the length of the list or the
   * position does not exist in the term (TODO).
   */
  def replaceAtList[E <: PMI[E]](l: List[E], pos: Pos, t: Term): Option[List[E]] = {
    assert(!pos.isEmpty, "replaceAtList called with empty position- expect this to be done at outer level")
    // p is the index into the list l, in which we replace
    val p :: ps = pos 
    /*(for ((s, i) <- l.zipWithIndex) yield
            if (i == p) s.replaceAt(ps, t) else s).toList*/
    if (p >= l.length) None
    else {
      val newElt = l(p).replaceAt(ps, t)
      Option(l.updated(p, newElt))
    }
    //std usage: if(pos isEmpty) t else replaceAtList(l,pos,t) ...
  }

}
 
