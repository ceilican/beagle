package beagle.datastructures

import beagle._
import fol._
import fol.term._
import util._
import calculus._
import Ordering._
import cnfconversion._
import PMI._ 

/**
 * @param idxRelevant
 * @param age Tracks number of rules applied to this clause
 * @param parents the ids of clauses used to derive this, or None if these are not tracked.
 * Currently this is used by ProofOutput and a (commented out) simplification rule in State.
 */
case class Clause(
  lits: List[Lit],
  idxRelevant: Set[Int],
  age: Int,
  parents: List[Int] = List()) extends Expression[Clause] with PMI[Clause] {

  /** An identifier for the clause which ignores simplification. */
  val id = Clause.idCtr.next()
  
  println("creating: " + id)
  println(this)
  if (id > 24) {
    1+1
  }
  //println(id+" created: "+lits.mkString(", ")+": "+parents.mkString(", "))

  
  /** When indexing is on we do not physically remove a clause, instead we flag it only as deleted */
  //var deleted = false

  def isEmpty = lits.isEmpty

  def apply(i: Int) = lits(i)
  lazy val length = lits.length
  /** Max depth of any clause literal.*/
  lazy val depth = if (lits.isEmpty) 0 else (lits map { _.depth }).max

  // Mixin Expression:
  lazy val vars = lits.vars

  def applySubst(sigma: Subst) = 
    if (sigma actsOn this) Clause(sigma(lits), idxRelevant, age) else this

  // mgus don't need to be defined - dummy
  def mgus(that: Clause) = throw new InternalError("mgus for clauses are not defined")
  def matchers(that: Clause, gammas: List[Subst]) =
    if (length != that.length)
      List.empty
    else
      Expression.matchers(lits, that.lits, gammas)

  override def matcher(that: Clause) = throw new InternalError("using matcher for Clause is ill-defined")

  // Mixin PMI
  // lazy val subTermsWithPos = lift(lits map { _.subTermsWithPos })
  lazy val termIndex = liftIndex(lits map { _.termIndex })

  /**
    * Get all positions in positive clause literals that can be an into-term for superposition
    */
  def posLitsSubTermsWithPosForSup(t: Term): Iterable[(Term, Pos)] = {
    iPosLits flatMap {
      i => lits(i).subTermsWithPosForSup(t) map {
        case (s, pos) => (s, i :: pos)
      }
    }
  }

  /**
    * Get all positions in negative clause literals that can be an into-term for superposition
    */
  def negLitsSubTermsWithPosForSup(t: Term): Iterable[(Term, Pos)] = {
    iNegLits flatMap {
      i => lits(i).subTermsWithPosForSup(t) map {
        case (s, pos) => (s, i :: pos)
      }
    }
  }

  /**
   * Replace a term `t` at position `pos`.
   * The head of `pos` gives the index of the literal to replace inside.
   * Since we cannot replace a `Lit` with a `Term` then this should be checked
   * for at this level.
   * @return a new Clause with the terms appropriately replaced. If the term position
   * does not exist, it simply returns `this`.
   * @throws InternalError if the replacement will be invalid. Non-existing positions
   * do not throw this.
   */
  def replaceAt(pos: Pos, t: Term) = 
    if (pos.isEmpty) 
      throw InternalError(s"Attempt to replace a Clause $this with a Term $t in replaceAt")
    else if (pos.tail.isEmpty) 
      throw InternalError(s"Attempt to replace a Lit with a Term $t in replaceAt")
    else {
      //Clause(PMI.replaceAtList(lits, pos, t), idxRelevant, age)
      PMI.replaceAtList(lits, pos, t) map { l =>
	this.modified(lits=l)
      } getOrElse {
	this
      }
    }

  // New stuff. todo: tidy up
  lazy val toFormula = (lits map { _.toLiteral }).toOr.closure(Forall)
  lazy val (iPureBGLits, iNonPureBGLits) = 0 until length partition { lits(_).isPureBG }
  lazy val (iBGLits, iNonBGLits) = 0 until length partition { lits(_).isBG }
  // lazy val iPureFGLits = iNonPureBGLits filter { lits(_).isPureFG }
  lazy val nonPureBGLits = (iNonPureBGLits map { lits(_) }).toList
  lazy val pureBGLits = (iPureBGLits map { lits(_) }).toList
  // lazy val pureFGLits = (iPureFGLits map { lits(_) }).toList

  lazy val nonBGLits = (iNonBGLits map { lits(_) }).toList
  lazy val BGLits = (iBGLits map { lits(_) }).toList

  lazy val kind = Expression.lub(lits)

  // The B-sorted FG functional subterms occuring in literals in this.
  // These are the ones to be eliminated

  lazy val minBSFGTerms = lits.minBSFGTerms
  lazy val maxBSFGTerms = lits.maxBSFGTerms
  lazy val symConsts = lits.symConsts

  lazy val sorts = lits.sorts
  lazy val operators = lits.operators

  /**
   * True if it contains no BG operators at all, used to short circuit abstraction.
   * Can't use Expression.Kind because FG kind does not guarantee no BG operators.
   */
  //lazy val isPureFG = symConsts.isEmpty && !operators.exists(_.kind==BG)

  lazy val isSolverClause = isBG && bgtheory.solver.isSolverClause(this)

  /**
   * If this is a background clause then compute a set of ground clauses
   * that is equivalent to it over the background domain.
   * All literals are solver literals.
   * @return the set of ground clauses partitioned into a set that can directly be passed to the
   * bg solver and a set that cannot (because the solver needs unit clauses) or None
   * if this is not possible.
   */
  lazy val asSolverClauses: Option[(List[Clause], List[Clause])] = 
    if (isBG) Some(bgtheory.solver.asSolverClauses(this)) else None

  override def equals(that: Any) = {
    that match {
      // Notice: id is excluded from equality. Is this the right thing?
      //JOSH- sometimes (BGSimp etc) can return an unchanged clause which has a new ID...
      case cl: Clause ⇒ (this canEqual that) && age == cl.age && lits == cl.lits && idxRelevant == cl.idxRelevant
      case _ ⇒ false
    }
  }

  def canEqual(other: Any): Boolean = other.isInstanceOf[Clause]

  override def hashCode: Int = 
    41 * ( 41 * (41 + age) + lits.hashCode) + idxRelevant.hashCode

  lazy val isUnitClause = lits match { case _ :: Nil => true; case _ => false }
  lazy val isPosUnitClause = lits match { case Lit(true, _) :: Nil => true; case _ => false }
  lazy val isNegUnitClause = lits match { case Lit(false, _) :: Nil => true; case _ => false }
  lazy val isFlat = lits forall { _.isFlat }

  // For pure FG clauses don't need to look at age, as there are only finitely many 
  // different clauses (modulo renaming) for a given depth/length bound
  // val weight = List(if (depth <= 2) 0 else depth + 1, length, if (!isBG) 0 else age).max
  // val weight = if (isPureBG) 0 else List(depth+1, length, if (lde < 10) 0 else age-2).max
  // As used in paper:
  // val weight = if (isPureBG) 0 else List(depth + 1, length, age - 2).max
  // lazy val weight = lits.foldLeft(0)((acc, l) =>  acc + (if (l.isTrivial) 0 else l.weight))
  lazy val weight = lits.foldLeft(0)(_ + _.weight)

  // NO - it's all properly set up now
  // val weight = List(if (depth <= 2) 0 else depth + 1, length, 0).max
  // val weight = math.max(depth, length) // Exclude pure BG Lits?

  // def idxBelowLevel(i: Int) = idxRelevant.isEmpty || idxRelevant.max < i

  /*
   * The positive, negative and maximal literals. The pure BG literals are ignored.
   * todo: check if this is the right thing.
   */

  //this stores the result of previous iIsMaximalIn calls
  private var maxLitCache = Set.empty[Int]

  /** Whether the literal at i is maximal in this clause */
  def iIsMaximalIn(i: Int): Boolean = {
    if(maxLitCache(i)) {
      return true
    }

    //val res = (!((0 until length) exists { j ⇒ j != i && (lits(j) gtr lits(i)) }))
    var j = 0
    while(j < length) {
      if(j!=i && lits(j).gtr(lits(i))) return false
      else j+=1 //continue
    }
    // println(this + " isMaximalIn " + lits + " = " + res)
    maxLitCache+=i
    return true
  }

  //TODO- can we do something better for retained clauses, i.e. those in old?
  private var strMaxLitCache = Set.empty[Int]
  def iIsStrictlyMaximalIn(i: Int): Boolean = {
    
    if(strMaxLitCache(i)) return true
    
    // val res = (!((0 until length) exists { j ⇒ j != i && (lits(j) geq lits(i)) }))
    var j = 0
    while(j < length) {
      if(j!=i && lits(j).geq(lits(i))) return false
      else j+=1 //continue
    }
    // println(this + " isStrictly MaximalIn " + lits + " = " + res)
    return true
  }

  lazy val (iPosLits, iNegLits) = (0 until length) partition { lits(_).isPositive }
  def isPositive = iNegLits.isEmpty
  def isNegative = iPosLits.isEmpty
  def hasPositive = !iPosLits.isEmpty
  def hasNegative = !iNegLits.isEmpty

  /**
   * extended BGT-fragment: all background-sorted FG-terms are ground.
   */
  lazy val isGBTClause = {
    def h: Boolean = {
      for (
        l ← lits;
        t ← List(l.eqn.lhs, l.eqn.rhs);
        s ← t.subterms
      ) {
        if (s.isBSFG && !s.isGround)
          return false // found offending term
      }
    return true
    }
    h
  }

/*
  def isGBTClause: Boolean = {
    for (
      l ← lits;
      t ← List(l.eqn.lhs, l.eqn.rhs);
      s ← t.subterms
    ) {
      if (!s.isVar && s.sort.kind == BG && !s.isGround)
        return false // found offending term
    }
    return true
  }
 */
  // Selection function for inferences. 
  // We can select negative FG literals

  /**
   * Iterator over maximal non-pure BG literals in clauses,
   * those that are subject to inferences
   */
  lazy val iMaximal = iNonPureBGLits filter { iIsMaximalIn(_) }

  /**
   * Indices of literals eligible for inferences.
   * Do one of: </br>
   * 1) Choose the first maximal negative FG literal, or </br>
   * 2) if negative selection is required: select any negative FG literal, or </br>
   * 3) if that is not possible simply take the first maximal literal. </br>
   *
   * `iEligibleIsMax` is true if an arbitrary maximal literal was chosen.
   * @see [[beagle.util.Flags.negSelection]]
   * @note negSelection defaults to false.
   */
  lazy val (iEligible: IndexedSeq[Int], iEligibleIsMax: Boolean) =
    if (flags.negSelection.value) {
      // Try to find a maximal selected negative literal
      (iMaximal find { i ⇒ !lits(i).isPositive && lits(i).isFG }) match {
        case Some(i) ⇒  // got it
          (Vector(i), false)
        case None ⇒
          // Next best thing: Select any negative literal
          (iNegLits find { i ⇒ lits(i).isFG }) match {
            case Some(i) ⇒ 
              (Vector(i), false)
            case None ⇒ 
              // No selection of negative literals possible
              (iMaximal, true)
          }
      }
    } else {
      // Some heuristics here:
      if (iMaximal.length == 1)
        (iMaximal, true)
      else {
        // val (iNegFG, iOther) = iMaximal partition { i ⇒ !lits(i).isPositive && lits(i).isFG }
        // println("xxx %s : %d maximal, %d negative literals".format(this, iMaximal.length, iNegFG.length))
        // Select a negative literal if possible
        (iMaximal find { i ⇒ !lits(i).isPositive && lits(i).isFG }) match {
          case Some(i) ⇒  // got it
            (i to i, false)
          case None => 
            (iMaximal, true)
        }
      }
    }

/*
  lazy val iEligible: IndexedSeq[Int] =
    if (flags.negSelection.value)
      // Selection of negative literals on
      // But select a negative literal only if it is a FG literal
      (iNegLits find { i ⇒ lits(i).isFG }) match {
        case Some(i) ⇒ i to i
        case None ⇒ iMaximal
      }
    else // no selection of negative literals
      // if one of the negative literals is maximal we implicitly select it,
      // so that we have to work on only that 
      (iMaximal find { i ⇒ !lits(i).isPositive && lits(i).isFG }) match {
        case Some(i) ⇒ i to i
        case None ⇒ iMaximal // all maximal literals are positive
      }
 */

  /**
   * Whether this is a definition for the given term `t`, after unabstraction.
   * Only positive unit clauses can be definitions.
   * If this is a unit then we need to analyse the equation `e` in the sole literal
   * and see if it defines the given `t`.
   * E.g. f(x) = x+x is a definition for f(1).
   */
/*  def isDefinitionFor(t: Term): Boolean =
    if (unabstrAggressive.isUnitClause && unabstrAggressive(0).isPositive) {
      // Only positive unit clauses can be definitions.
      // We need to analyse the equation e in this unit clause and see if it defines t 
      val e = unabstrAggressive(0).eqn
      e.rhs.isBG && e.lhs >>~ t // E.g. f(x) = x+x is a definition for f(1)
    } else
      false

  /**
   * True if this clause is a unit literal defining a term.
   * A definition can only contain one BSFG term.
   * Equivalent to isDefinintionFor(some t).
   */
  def isDefinition: Boolean =
    if (unabstrAggressive.minBSFGTerms.size == 1) { // A definition can contain only one B-sorted FG Term  
      (isDefinitionFor(unabstrAggressive.minBSFGTerms.head) || 
       ((!isGround) && finite.FQClause.isFQDefinition(this)))
      //specialised for FQClauses which have the form f(X)=a v B1 v B2 v ... Bi is pureBG
    } else false
 */

  def isDefinition =
    // Assume cl is unabstracted
    unabstrAggressive.lits match {
      case List(Lit(true, Eqn(lhs, rhs))) =>
        lhs.isMinBSFGTerm && rhs.isPureBG
      case _ => false
    }

  def split: Option[(List[Lit], List[Lit])] = {
    //special cases can be short-circuited
    if (this.isUnitClause || this.isEmpty) 
      return None
    // else if (this.isGround) 
    //   return Some(List(this.lits.head),this.lits.tail)
    
    case class Elem(vars: Set[Var], indices: List[Lit])
    var partitions = List.empty[Elem]

    //can ignore ground lits as they are always separate elements
    var gndLits = List.empty[Elem]
    
    for (l <- lits) {
      if (l.isGround) 
        gndLits ::= Elem(Set.empty, List(l))
      else {
        var varsx = l.vars
        var ix = List(l)
        var newpartitions = List.empty[Elem]
        for (el ← partitions)
          if (el.vars.exists(varsx(_))) {
            varsx ++= el.vars
            ix :::= el.indices
          } else
            newpartitions ::= el
        partitions = Elem(varsx, ix) :: newpartitions
      }
    }
    // If pure BG clauses are not to be split we try to find a maximal subclause
    // of the gndLits
    if (util.flags.split.value == "nopurebgc") {
      val (pureBG, other) = gndLits partition {
        case Elem(_, List(l)) => l.isPureBG
      }
      if (pureBG.nonEmpty && (other.nonEmpty || partitions.nonEmpty)) {
        val right = pureBG flatMap(_.indices)
        val left = (other ::: partitions).flatMap(_.indices) 
        // println("*** split: %s -- %s".format(pureBG, other ::: partitions))
        return Some((left, right))
      }
    }

    // add ground lit components back
    partitions :::= gndLits

    if (partitions.length <= 1)
      return None
    else {
      /*      print("*** Clause " + this + " is splittable into ")
      partitions foreach { p => print(p + " -- ") }
      println()
*/
      val left = partitions.head.indices
      val right = partitions.tail.flatMap(_.indices)
      return Some((left, right))
    }

  }

  /** Instantiate literal with index i */
  def instantiate(i: Int): List[Clause] = {
    def mkDomSubsts(xs: List[Var]): List[Subst] =
      xs match {
        case Nil ⇒ List(Subst.empty)
        case x :: rest ⇒
          // Build the cross product
          for (
            gamma ← mkDomSubsts(rest);
            t ← Sigma.domain(x.sort).fresh()
          ) yield gamma + (x -> t)
      }
    for (gamma ← mkDomSubsts(lits(i).vars.toList filter { Sigma.fdSorts contains _.sort })) yield gamma(this)
  }

  /** Very simplistic checks.*/
  def isTautology: Boolean = {
    stats.simpTaut.tried

    if (lits exists { _.isTrivialPos }) {
      stats.simpTaut.succeed
      return true
    }

    for(i1 <- iPosLits;
    	l1 = lits(i1);
    	i2 <- iNegLits)
      if (l1.eqn == lits(i2).eqn) {
        stats.simpTaut.succeed;
        return true
      }

    // Rather expensive
    // if (!bgc.isConsistentWith(c.lits)) return true
    return false
  }

  /**
   * Remove equations of the form x != t if x does not occur in t and
   * level = 0 and t is domain element or a variable of the same kind as x, or
   * level = 1 and t is a BG term, or
   * level = 2 
   * Preserves the id.
   */
  private def unabstract(cautious: Boolean) = {
    var CRes = List.empty[Lit]
    var sigma = Subst.empty // The substitution we are building from the disequations of the above form
    var touched = false
    for (l ← lits)
      // Invariant: sigma has been applied to CRes   
      l match {
        case Lit(true, _) => CRes ::= sigma(l)  // nothing to remove; maintain the invariant
        case Lit(false, Eqn(lhs, rhs)) if lhs == rhs =>
          // Removal of trivial negative equations
          touched = true // doing nothing means removing the literal
        case Lit(false, e) => {
          val esigma = sigma(e)
          val delta = // The new substitution
            esigma.asVarTermPair match {
              case Some((x : Var, y: GenVar)) => Subst(y -> x) // To make sure that abstraction variables are not replaced by general variables
              // case Some((x : Var, y: AbstVar)) => Subst(x -> y)
                // Preferred:
              case Some((x : Var, t: Term)) if t.isPureBG && !x.occursIn(t) => Subst(x -> t)
              // case Some((x : Var, t: Term)) if cautious && t.isDomElem => Subst(x -> t)
              // case Some((x : GenVar, t: Term)) if t.isBG && !x.occursIn(t) => Subst(x -> t)
                // Generalizes this:
              // case Some((x : Var, d: DomElem[_])) => Subst(x -> d)
                // The generalization is much better, in partiuclar in concert with Define,
                // cf PUZ133=2.p, but unfortunately not covered by the redundancy criterion
               case Some((x : GenVar, t: Term)) if !cautious && !x.occursIn(t) => Subst(x -> t)
                // brave:
              // case Some((x : Var, t: Term)) if !x.occursIn(t) => Subst(x -> t)
              case _ => Subst.empty // Not a variable-term pair or cautious or occurs-check problem
            }
          if (delta.isEmpty)
            CRes ::= Lit(false, esigma)
          else {
            sigma ++= delta
            CRes = delta(CRes) // remove disequation and maintain invariant
            touched = true
          }
        }
      } 
    //(this.modified(lits=CRes.reverse.toListSet), sigma)
    if (touched) 
      //Clause(CRes.reverse, idxRelevant, age)
      (this.modified(lits = CRes.reverse.toListSet), sigma)
    else (this, Subst.empty) // avoid building the clause
  }

  lazy val unabstrCautious = unabstract(cautious = true)._1
  lazy val unabstractAggressive = unabstract(cautious = false)
  lazy val unabstrAggressive = unabstractAggressive._1

  /** Proper subsumption or variants.*/
  def subsumes(that: Clause): Boolean = {
    stats.simpSubsume.tried
    /**
     * Attempt to extend gamma to a substitution such that for each literal in lits2
     * there is a literal in lits1 for which this substitution is a matcher.
     */
    def buildClauseMatching(gamma: Subst, lits1: List[Lit], lits2: List[Lit]): Option[Subst] = {
      //println(s"bcm\ngamma=$gamma\nl1=$lits1\nl2=$lits2")
      lits1 match {
        case Nil ⇒ return Some(gamma)
        case l :: remainingLits ⇒ {
          for (
            k ← lits2;
            g ← l.matchers(k, List(gamma)) // if g.isSimpleFor(bgVars)
          ) {
            // Must remove k from lits2 otherwise mapping from lits1 to lits2 might be non-injective
            // causing p(x,y \/ p(y,x) to subsume p(z,z) . Clearly this would be incomplete
            // as the latter is a factor of the former, but of course factors are generally needed
            buildClauseMatching(g, remainingLits, lits2 filterNot { _ == k }) match {
              case Some(res) ⇒ return Some(res)
              case None ⇒ () // try next literal/matcher
            }
          }
          // After loop now, i.e. all possibilities exhausted
	  //println("No match")
          return None
        }
      }
    }

    if (bgtheory.solver.subsumes(this, that))
      return true
    // Cheap pretests below might not apply to the theory case

    if ((length > that.length) ||
      (depth > that.depth) ||
      (hasNegative && that.isPositive) ||
      (hasPositive && that.isNegative) ||
      !(operators subsetOf that.operators))
      return false // cheap pretests apply

    if (buildClauseMatching(Subst.empty, lits, that.lits) != None) {
      reporter.onClauseSubsumed(this, that)
      stats.simpSubsume.succeed
      return true
    }

    return false
  }

  
  import fol.Ordering._

  /**
   * Comparing two clauses according to the multiset ordering
   * Currently needed only for diagnostic purposes, too see if
   * simplification really gave a smaller clause
   */
  def compare(that: Clause): OrderingResult = {

    // How many times does lit occur in lits ?
    def count(lit: Lit, lits: List[Lit]) = lits count { _ == lit }

    def mso_equ(lits1: List[Lit], lits2: List[Lit]): Boolean =
      (lits1 ::: lits2).toSet forall { l => count(l, lits1) == count(l, lits2) }

    def mso_gtr(lits1: List[Lit], lits2: List[Lit]): Boolean = {
      // Straight from the definition 
      lits2 forall { y ⇒
        !(count(y, lits1) < count(y, lits2)) ||
          (lits1 exists { x ⇒ (x gtr y) && count(x, lits1) > count(x, lits2) })
      }
    }

    // Body of Compare
    if (mso_equ(this.lits, that.lits))
      Equal
    else if (mso_gtr(this.lits, that.lits))
      Greater
    else if (mso_gtr(that.lits, this.lits))
      Less
    else
      Unknown
  }

  // Use cases
  def gtr(that: Clause) = (this compare that) == Greater
  def geq(that: Clause) = List(Greater, Equal) contains (this compare that)

  override def toString = printer.clauseToString(this) 

  // Convenience methods
  def reduce(cls: ClauseSet) = reduceWithStatus(cls)._1
  def reduceWithStatus(cls: ClauseSet) = 
        simplification.reduceWithStatus(this, cls)

  // Includes call of simplifiedBG below
  /** @see [[beagle.calculus.simplification.simplifyCheap]] */
  def simplifyCheap: Iterable[Clause] = 
    simplification.simplifyCheap(this)
  
  /** @see [[beagle.bgtheory.solver.simplify]] */
  lazy val simplifyBG = 
    bgtheory.solver.simplify(this)

  /** Make a copy with fresh variables but preserve clause ID */
  override def fresh() = //Clause(id, lits.fresh().toList, idxRelevant, age)
    this.copy(lits=this.lits.fresh().toList)

  /** Make a copy with fresh variables, also replacing AbstVars with GenVars and preserving clause ID */
  override def freshGenVars() = //Clause(id, lits.freshGenVars().toList, idxRelevant, age)
    this.copy(lits=lits.freshGenVars().toList)

  /** @see [[beagle.datastructures.purification.weakAbstractClause]] */
  lazy val weakAbstract = purification.weakAbstractClause(this)

  /** @see [[beagle.datastructures.purification.stdAbstractClause]] */
  lazy val stdAbstract = purification.stdAbstractClause(this)

  // lazy val canonical = Clause(lits map { _.canonical }, idxRelevant, age)

  /**
   * Notice that the default action is to preserve the current values of
   * the clause, but we want to mask the action of generating new IDs so
   * we use a boolean for id instead of passing an actual value.
   * @todo Are there cases where ID needs to be set?
   * 
   * It is a good idea to use this instead of Clause(lits,...)
   * or new Clause(...) to construct new clauses.
   * This allows the current clause to have some say in what the
   * state of the new clause is which has two benefits:
   * 1) Extending clauses to be labelled/constrained clauses is easier
   * because only one method needs to be overridden to propagate labels.
   * 2) Later we can use this to store state which may not change or change in
   * a simple way, e.g. keeping a PMI index after a call to fresh().
   */
  def modified(lits: List[Lit] = this.lits, 
               idxRelevant: Set[Int]=this.idxRelevant,
               age: Int=this.age,
               parents: List[Int] = this.parents): Clause = {
    println("modifying from: " + this.id )
    val c = Clause(lits, idxRelevant, age, parents)
    val newid = c.id
    c
  }
     

  /** Combine contextual information from two clauses as a result of a two
   * premise inference, i.e. chaining or superposition.
   * The idea is that subclasses of `Clause` which make use of constraints can
   * override this in a sensible way in order to enable inheritance of constraints from
   * both premises of an inference.
   * @todo It might be sensible to eventually have constrained clauses as the superclass
   * and clauses with no constraints as a special case-- the end result will be less intricate
   * but it will require some engineering to get there.
   * @param that The clause to combine with.
   * @param newLits The literals to use in the clause.
   * @param sigma Optionally the mgu of the inference.
   * @return an abstracted clause where the relevant indexes are merged,
   * age is updated to max + 1 and parents are recorded.
   */
  def combine(that: Clause, newLits: List[Lit], sigma: Subst = Subst.empty): Clause =
    Clause(sigma(newLits),
	   this.idxRelevant ++ that.idxRelevant,
	   math.max(this.age, that.age) + 1,
	   List(this.id, that.id)).abstr

 /** Do weak abstraction unless standard abstraction is used; which does not require
   * doing anything more as standard abstraction is preserved by the inference rules.
   */
  def abstr = {
    util.Timer.abs.start()
    val res =
      if (flags.stdabst.value) stdAbstract
      else weakAbstract
    util.Timer.abs.stop()
    res
  }

  /** Get the indexes of literals which can chain with the given literal.
   * Currently this just uses the index as a pre-computed filter.
   * Could do something clever and check the inner terms to remove non-unifiable
   * instances, but it is unclear that this would be faster.
   */
  def chainableLits(l: Lit): List[Int] =
    this.termIndex(l.eqn.lhs.opOption.get) //this is the $less op if used correctly
      .map({ case (l,pos) => pos.head }) //project the head, since each position has the form List(i,0)
}

object Clause {

  val idCtr = new util.Counter()

  def apply(fromLits: List[Lit]): Clause = Clause(fromLits,Set(),0)
/*  
  /** Normal constructor. */
  def apply(id: Int, fromLits: List[Lit], idxRelevant: Set[Int], age: Int) =
    new Clause(id, fromLits, idxRelevant, age)

  /** New clause with unique ID. */
  def apply(fromLits: List[Lit], idxRelevant: Set[Int], age: Int) =
    new Clause(idCtr.next(), fromLits, idxRelevant, age)

  /** Clause with unique ID and parents as given. */
  def apply(fromLits: List[Lit], idxRelevant: Set[Int], age: Int, hparents: List[Int]) =
    new Clause(idCtr.next(), fromLits, idxRelevant, age) {
      override val parents = Some(hparents)
    }

  def apply(fromLits: List[Lit], parents: Option[List[Int]] = None) =
    new Clause(idCtr.next(), fromLits, Set(), 0, parents)
  
  /** Normal constructor. */
  def apply(id: Int, fromLits: List[Lit], idxRelevant: Set[Int], age: Int, parents: Option[List[Int]] = None) =
    new Clause(id, fromLits, idxRelevant, age, parents)

  /** New clause with unique ID. */
  def apply(fromLits: List[Lit], idxRelevant: Set[Int], age: Int, parents: Option[List[Int]] = None) =
    new Clause(idCtr.next(), fromLits, idxRelevant, age, parents)
 */
}

