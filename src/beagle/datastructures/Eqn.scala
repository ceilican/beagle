package beagle.datastructures

import beagle._
import fol._
import term._
import calculus._
import Signature._
import PMI._
import util._

/**
 * Equations, those that make up clauses, as opposed to atomic formulas.
 * Represent equations in an ordered way, if possible.
 * `isOrdered` is true iff one side is greater than the other
 * in this case `lhs` holds the greater side.
 * All non-equational atoms (i.e. predicates) are always ordered.
 */
final class Eqn(s: Term, t: Term) extends Expression[Eqn] with PMI[Eqn] {

  // println("Making an equation Eqn(%s, %s)".format(s, t))

  /*
   * The two sides of an equation are accessed as lhs and rhs, respectively
   * @todo what should happen if a precedence hasn't been given?
   */
  val (lhs, rhs, isOrdered) = {
    import Ordering._
    // println("start compare %s\n              %s".format(s, t))
    val h = (s compare t)
    // println("result = " + h)
    h match {
      // Only cases Equal, Greater and Unknown are possible
      case Equal => (s, t, false)
      case Greater => (s, t, true)
      case Unknown => {
        // println("    again compare %s\n              %s".format(s, t))
        val h = (t compare s)
        // println("    result = " + h)
        h match {
          // Case Equal is not possible, it would have been caught above
          case Greater => (t, s, true)
          case Unknown => 
            if (t.toString > s.toString)
              // this gives us a "canonical" ordering of equations,
              // so that checking equality modulo symmetry can be done by using
              // the oriented versions in this ordering only
              (t, s, false)
            else
              (s, t, false)
	  case _ => throw InternalError(s"Unexpected ordering result $h for an equation $t=$s")
        }
      }
      case _ => throw InternalError(s"Unexpected ordering result $h for an equation $s=$t")
    }
  }
 
  if (isOrdered) stats.nrOrderedLits += 1
      
  lazy val sort = 
    lhs.sort.intersectWith(rhs.sort).getOrElse(
      throw new Exception("Equation "+this+" with non-intersecting sorts "+List(lhs.sort,rhs.sort)))
    
  // lazy val isBG = if (rhs == TT) lhs.isBG else lhs.isBG && rhs.isBG
  lazy val kind = if (isPredEqn) lhs.kind else lhs.kind lub rhs.kind
  lazy val sorts = lhs.sorts ++ rhs.sorts
  lazy val operators = (lhs.operators ++ rhs.operators) - TTOp

  lazy val minBSFGTerms = lhs.minBSFGTerms union rhs.minBSFGTerms
  lazy val maxBSFGTerms = lhs.minBSFGTerms union rhs.maxBSFGTerms
  lazy val symConsts = lhs.symConsts union rhs.symConsts

  // lazy val ops = lhs.ops ++ rhs.ops
  // lazy val subTermsWithPos = lift(List(lhs.subTermsWithPos, rhs.subTermsWithPos))
  lazy val termIndex = liftIndex(List(lhs.termIndex, rhs.termIndex))

  def replaceAt(pos: Pos, t: Term) = 
    pos match {
      case 0 :: lhsPos => Eqn(lhs.replaceAt(lhsPos, t), rhs)
      case 1 :: rhsPos => Eqn(lhs, rhs.replaceAt(rhsPos, t))
      case _ => throw InternalError("Bad position specified for replaceAt: "+pos)
    }

  /** Whether this is a predicate equation, that is, a non-equational
   * atom `A` encoded as an equation `A=true`.
   */
  val isPredEqn = rhs == TT
  def isNLPP = lhs.isNLPPTerm || rhs.isNLPPTerm

  /** Get the two subterms in the orientation specified.
   * @param orientation 0 for `(left, right)` and 1 for `(right, left)`.
   * @note this is wrt the term ordering not the order the arguments were given to the `Eqn`
   * constructor.
   * @throws IllegalArgumentException if orientation is not 0 or 1.
   */
  @deprecated(message="redundant- just specify eqn.lhs or eqn.rhs as you need", since="0.9")
  def getTermTuple(orientation:Int) : (Term, Term) = {
    if (orientation!=0 && orientation!=1) 
      throw new IllegalArgumentException("Equation position may only be 1 (left) or 0 (right)")

    orientation match {case 0 => (lhs, rhs)
                       case 1 => (rhs, lhs)}
  }

  /** @return a list of the top non-logical terms.
   * If a (non-logical) equation the terms are in descending order.
   * Usecase: to retrieve the subterms of a predicate `p(x,y)` without a full
   * structural match like {{{ case PredEqn(PFunTerm(q,args)) if q == p => ... }}}
   */
  lazy val getPredicateArgs: List[Term] = 
    if(isPredEqn) lhs.asInstanceOf[FunTerm].args
    else lhs :: rhs :: Nil

  lazy val asVarTermPair = (lhs, rhs) match {
    case (x: Var, t) ⇒ Some(x, t)
    case (t, x: Var) ⇒ Some(x, t)
    case _           ⇒ None
  }

  /*
 * Mixin Expression
 */

  lazy val vars = lhs.vars union rhs.vars

  def applySubst(sigma: Subst) = Eqn(sigma(lhs), sigma(rhs))

  def mgus(that: Eqn) =
    if (isOrdered && that.isOrdered) // This includes the non-equational case
      this.toTerm mgus that.toTerm // which is either the empty list or a singleton
    else // at least one equation is unordered - need to consider both cases
      (this.toTerm mgus that.toTerm) :::
        (this.toTermRev mgus that.toTerm)
  // TODO: we could possibly check that both are most general
  // otherwise return only the more general one
  // println("mgus(" + this + ", " + that + ") = " + result)
        
  def matchers(that: Eqn, gammas: List[Subst]) =
    if (isOrdered && that.isOrdered)
      this.toTerm.matchers(that.toTerm, gammas)
    else
      (this.toTerm.matchers(that.toTerm, gammas)) :::
        (this.toTermRev.matchers(that.toTerm, gammas))

  lazy val weight = {
      // Pure BG terms never play an an active role in inferences and at top-level can be considered
      // as constants for the purpose of determining the weight 
    // def weighted(t: Term) = if (t.isPureBG) 0 else t.weight      
    // no - cf NUM860=1.p
    def weighted(t: Term) = t.weight      
    // if (isPredEqn) weighted(lhs) else weighted(lhs) + weighted(rhs)
    // experimental - not good for GEG
    if (isPredEqn) weighted(lhs) else weighted(lhs) + weighted(rhs)
    } 

  /* 
   * Abstract members
   */
  lazy val depth = math.max(lhs.depth, rhs.depth)

  /*
   * Some abbreviations
   */
  lazy val isTrivial = lhs == rhs 

  // Convert an equation into a Formula

  lazy val toTerm = PFunTerm(Sigma.eqOperators(sort), List(lhs, rhs))
  lazy val toTermRev = PFunTerm(Sigma.eqOperators(sort), List(rhs, lhs))

  override def equals(other: Any) = other match {
    case that: Eqn ⇒ (lhs == that.lhs && rhs == that.rhs)
    case _: Any    ⇒ false
  }

  override def hashCode: Int = 
    41 * (41 + lhs.hashCode) + rhs.hashCode

  lazy val isFlat = lhs.isFlat && rhs.isFlat

  /**Swap the two sides of a binary operator application if sig is negative.
   * A convenience method used when multiplying by a negative number in arithmetic simplification
   */
  def swapIfNeg(sig: Int) = 
    if (sig < 0) {
      val PredEqn(PFunTerm(op, List(lhs, rhs))) = this
      PredEqn(PFunTerm(op, List(rhs, lhs)))
    } else this


  override def toString = util.printer.eqnToString(this)

  def toAtom =
    if (isTrivial)
      TrueAtom
    else this match {
      case PredEqn(FunTerm(op, args)) ⇒ Atom(op.name, args)
      case _                          ⇒ Equation(lhs, rhs)
    }
  
  def toLit = Lit(true,this)

  /** Apply a function to both sides of the Eqn */
  def map(f: Term => Term): Eqn = new Eqn(f(s),f(t))

}

/** Companion object to `Eqn` */
object Eqn {
  def apply(s: Term, t: Term) = new Eqn(s, t)
  def unapply(e: Eqn) = Option((e.lhs, e.rhs))
}

/** Encode a predicate term as an equation.
 * @example If `p` is a predicate (its sort is `OSort`) then
 * {{{ PredEqn(p(x,y)) = Eqn(p(x,y),TT) }}} where `TT` is the dedicate true constant.
 * To match this we could use: {{{ case PredEqn(t) => () // t = PFunTerm(p,List(x,y)) }}}
 */
object PredEqn {
  def apply(s: Term) = new Eqn(s, TT)
  def unapply(e: Eqn) = if (e.rhs == TT) Option((e.lhs)) else None

}
