package beagle.cnfconversion

import beagle._
import fol._
import term._
import rules._

object standard {

  /**
   * Convert a formula to CNF, i.e. a list of list of Literals
   * @param full if true then pure BG formulas are also subject to CNF conversion,
   * which may introduce Skolem functions.
   * It may often be better to have full false, so that these formulas are treated as atoms by the
   * CNF conversion and treated by QE later
   */
  def toCNF(f: Formula, full: Boolean): (List[List[Formula]], Set[Operator]) = {
    
     //println("***" + f + " --- " + full)

    //accumulates skolem operators
    var newOps = Set.empty[Operator]
    //both full and non-full share these steps initially:
    val h1 = f.
	reduceOnePass(elimIffNot).
	reduceOnePass(elimIff).
	reduceOnePass(elimImpl).
	reduceInnermost(pushdownNeg).
	reduceOnePass(elimNegNeg)
	// reduceInnermost(elimIffNot).
	// reduceInnermost(elimIff).
	// reduceInnermost(elimImpl).
	// reduceInnermost(pushdownNeg).
	// reduceInnermost(elimNegNeg)

    // h1 is a conjunction of formulas, even if a singleton.
    // Convert each element separately into CNF, which can be much more efficient.
    // In particular we save unneccessary pulling out quanifiers. A good example is SWV998=1
    //println("*** h = " + h1)

    // Do skolemisation of outer existential if possible
    // useful in case the formula has an existential quantifier over a conjunction of
    // universally quantified inner formulas which generate skolem functions
    // in this case the h.toList(And) optimisation is blocked and we get massive skolem functions
    // eg. PUZ001+2.p yields a 17 place skolem function
    val h2 = 
      h1 match {
          case g @ Exists(xs, f) if(full || !g.isBG) => {
	    // Peter 10/6/2014 - do *not* Skolemize if this is a BG formula (in the non-full case)
            // Rationale: cooper will treat it
	    val sigma = 
	      xs.foldLeft(Subst.empty)( (acc,x) => { 
		val (binding, op) = skolemization.mkSkoSubst(Nil, x)
		newOps += op
		Sigma += op
		acc+binding
	      } )
            sigma(f)
          }
          case x => x
        }

 // clumsy:
    val res = if (full) {
      val f2 = h2.toListAnd flatMap { f => {/*_. 
			     // Only now quantifiers can be pulled out, as arrows have been eliminated
			     reduceInnermost(pulloutQuants).
			     skolemize.
			     matrix.*/
	      val g = f.reduceInnermost(pushNegThruQuant)
	      fastSkolem(g).
			     // Only universal quantifiers in front now - remove
			     // The matrix now is made from Neg, And, and Or,
			     // convert to CNF
			     reduceInnermost(pushdownNeg).
			     reduceOnePass(elimNegNeg).
			     reduceOnePass(elimTrivial).
			     // reduceInnermost(pushdownOr).
			     // reduceInnermost(flattenAndOr) . 
			   //   reduceInnermost(elimTrivial).
			     toListAnd 
			    }} //map { _.toListOr }
      //println("skolems done: "+f2)
      fastCNF(f2.map(_.toListOr))
    } else {
      //println("non-optimised case")
      val f2 = h2.toListAnd flatMap { _. 
			       // Only now quantifiers can be pulled out, as arrows have been eliminated
			       reduceInnermostFG(pulloutQuants).
			       skolemizeUnlessIsBG. // Skolemization is done in two ways: QE on variables appearing in BG only, the others in toDNF
			       matrix.
			       // Only universal quantifiers in front now - remove
			       // The matrix now is made from Neg, And, and Or,
			       // convert to CNF
			       reduceInnermostFG(pushdownNeg).
			       reduceInnermostFG(elimNegNeg).
			     //   reduceInnermostFG(pushdownOr).
			     //   reduceInnermostFG(flattenAndOr).
			     //   reduceInnermostFG(elimTrivial).
			       toListAnd
			     } //map { _.toListOr }
      fastCNF(f2.map(_.toListOr))
    }
    (res, newOps)
  }

  /** Quickly convert a QF NNF formula (and-list of or-lists) to CNF.
   * Effectively implements the rules pushDownAndOr and flattenAndOr
   * @todo built-in check for trivial formulas
   */
  @annotation.tailrec
  def fastCNF(cls: List[List[Formula]], done: List[List[Formula]] = Nil): List[List[Formula]] = {
    //TODO- we only need the first open clause so change this to a while loop that stops
    //when we have an open clause.
    val (clsDone, clsOpen) = cls partition { _.forall(_.isLiteral) }
    if(clsOpen.isEmpty) return (clsDone ::: done)

    //take the first subformula of a disjunction that is not a literal
    val cl = clsOpen.head
    val (lits, binops) = cl partition { _.isLiteral }
    //it must be a conjunction
    val conj: List[Formula] = binops.head.toListAnd
    //distribute its conjuncts over the remaining subformulas
    val prod = conj map { c =>  (c :: binops.tail ::: lits) flatMap { _.toListOr } } 
    fastCNF(prod ::: clsOpen.tail, clsDone ::: done)
    //TODO- the insertions for ::: are expensive- is there a better way?
  }

  //negs pushed in, assume all quantifiers are flat ie ExEy => Exy.
  //qList is the open universal quantifiers on this branch
  def fastSkolem(f: Formula, qList: Set[Var] = Set()): Formula = {
    //println(qList.mkString("{",",","}")+ ": "+ f)
    
    f match {
      case _ if (f.isQuantifierFree) => f
      case Forall(xs, Forall(ys, g)) => 
	fastSkolem(Forall(xs ::: ys, g), qList)
      case Forall(xs, g) => {
	val rho = Term.mkRenaming(xs)
	val xs2 = rho(xs).asInstanceOf[List[Var]].toSet
	(for(fi <- rho(g).toListAnd) yield {
	  val relevant = xs2 intersect (fi.vars)
	  fastSkolem(fi, qList ++ relevant)
	}) reduceLeft (And(_,_))
      }
      case Exists(xs, Exists(ys, g)) => 
	fastSkolem(Exists(xs ::: ys, g), qList)
      case Exists(xs, g) => {
	//skolemize wrt open universals
	//replace xs with new skolem functions of the open universals
	var subst = Subst.empty
	for(y <- xs) {
	  val (s, skoOp) = skolemization.mkSkoSubst(qList.toList, y)
	  Sigma += skoOp
	  subst += s
	}
	fastSkolem(subst(g), qList)
      }
      case UnOpForm(op, g) => UnOpForm(op, fastSkolem(g, qList))
      case BinOpForm(op, g1, g2) => 
	BinOpForm(op, fastSkolem(g1, qList), fastSkolem(g2, qList))
    }
  }
}
