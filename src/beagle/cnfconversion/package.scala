package beagle

package object cnfconversion {

  import beagle._
  import util._
  import fol._
  import term._
  import datastructures._
  import bgtheory._

  import cnfconversion.rules._

  /** Used to transform constraints into DNF */
  def toDNF(f: Formula): List[List[Formula]] =
    f match {
      //skip some simple cases to which reduction rules never apply
      //usually get these by passing an empty set of BG lits, hence might not
      //be needed for toCNF
      case Neg(TrueAtom) => List(List(FalseAtom))
      case Neg(FalseAtom) => List(List(TrueAtom))
      case Neg(Atom(_, _)) => List(List(f))
      case Atom(_, _) => List(List(f))
      //the usual case
      case _ =>
        f.
          reduceInnermost(elimIffNot).
          reduceInnermost(elimIff).
          reduceInnermost(elimImpl).
          // Only now quantifiers can be pulled out, as arrows have been eliminated
          reduceInnermost(pulloutQuants).
          skolemize.
          matrix.
          // Only universal quantifiers in front now - remove
          // The matrix now is made from Neg, And, and Or,
          // convert to CNF
          reduceInnermost(pushdownNeg).
          reduceInnermost(elimNegNeg).
          reduceInnermost(pushdownAnd).
          reduceInnermost(flattenAndOr).
          reduceInnermost(elimTrivial).
          toListOr map { _.toListAnd }
    }

  /** Transform a single formula into CNF.
   * Note that it also can modify the global signature by introducing skolem constants.
   * Depends on flags.cnfConversion to choose between optimized and standard.
   * @param full if set then pure BG formulas are also transformed by CNF,
   * the alternative is to use QE to eliminate them.
   * @return a list representing a conjunction of clauses,
   * represented as lists of formulas- which should be atoms
   * or negated atoms.
   */
  def toCNF(f: Formula, full: Boolean, sig: Signature): List[List[Formula]] = {
    Timer.cnfConversion.start()

    val (res, newSig) = 
      if (flags.cnfConversion.value == "optimized")
	optimized.toCNF(f, full)
      else
	standard.toCNF(f, full)
    newSig foreach { Sigma += _ }
    Timer.cnfConversion.stop()

    res
  }

  /**
   * Eliminate all occurrences of greater/greatereq in terms of less/lesseq.
   * Works for both integers and rationals.
   */

  // Not used anymore. Use simplification rules instead.
/*
  def normalizeIneq(l: Formula): Formula = l match {
      // case Neg(Less(t1, t2)) ⇒ LessEq(t2, t1)
      // case Neg(LessEq(t1, t2)) ⇒ Less(t2, t1)
      case Neg(Greater(t1, t2)) ⇒ LessEq(t1, t2)
      case Neg(GreaterEq(t1, t2)) ⇒ Less(t1, t2)
      case Greater(t1, t2) ⇒ Less(t2, t1)
      case GreaterEq(t1, t2) ⇒ LessEq(t2, t1)
      case _ ⇒ l
  }
 */

  /**
   * Convert a "clause", as a list of literals into clauses. Because of QE more than one clause may result.
   * Also use the PreprocessingSolver to simplify arithmetic literals, possibly over multiple arithmetic sorts.
   * The resulting clauses are abstracted.
   * @param canLIAQE Indicates whether LIA QE can be applied to the BG-part of the literals.
   */
  private def literalListToClauses(literalList: List[Formula], canLIAQE: Boolean) = {
    // println("toClauses: " + literalList)
    val (fgs, bgs) = purification.purifyFormulas(literalList)
    // println("fgs = " + fgs + " bgs = " + bgs)
    // Negate the bgs for eliminating the (now) existentially quantified extra variables
    val bgsNeg =
      if (!canLIAQE) {
        // Don't bother to do QE
        // If !canLIAQE we assume that literalList is obtained by full clause
        // normalform transformation, i.e., all explicit quantifiers have been eliminated
        Neg(bgs.toOr)
      } else {
        val extraVars = bgs.bgVars -- fgs.bgVars
        LIA.LIASolverClauses.QE(Exists(extraVars.toList, Neg(bgs.toOr)))
      }
    // Negate the result again and join with the fgs 
    toDNF(bgsNeg) map { 
      conj => Clause( fgs ::: (conj map { _.toLit.compl }) )
		.unabstrAggressive
		.simplifyBG
		.unabstrAggressive 
    }
  }

  /**
   * Convert each formula to clausal form, which possibly extends the signature
   * @param forceGenVars whether to replace all abstraction variables (introduced by default)
   * with general variables.
   * @param haveLIA false for Lemma files, otherwise set in preprocessing whenever set contains LIA sort
   * also used to indicate pure LIA formulas parsed as lemmas.
   * If set has the effect of preventing full CNF transform and allowing LIA-QE in literalListToClauses
   */
  def formulasToClauses(fs: Seq[Formula], forceGenvars: Boolean, haveLIA: Boolean): List[Clause] = {
    // Lemma annotated clauses have ordinary variables, that's the whole difference
    (for (
      hf ← fs;
      f = hf.elimNonLinearMult.iteExpanded.letExpanded;
      // canLIAQE = false; 
      // canLIAQE = LIA.isLIA(f); //f.isLIA;

      literalList <- toCNF(f, full = !haveLIA, Sigma);
      cl ← literalListToClauses(literalList, canLIAQE = haveLIA)
    ) yield {
      if (forceGenvars || hf.annotation == Some(LemmaTerm))
        cl.freshGenVars()
      else
        cl
        }).toList
      .reverse // To preserve the order in which clauses are listed
  }

}

