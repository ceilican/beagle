
package beagle.bgtheory.LFA

import beagle._
import fol._
import term._
import datastructures._
import util._
import Rat._
import Polynomial._
import Signature._

object fourierMotzkin {

  /**
   * LRAAtom(op, p) stands for the PredEqn 0 op p . op is typically less or lesseq
   */
  case class LRAAtom(op: Operator, p: Polynomial) {
    def toEqn = PredEqn(PFunTerm(op, List(ZeroReal, p.toTerm)))
    def rearrange(x: VarOrSymConst): LRAAtom = LRAAtom(op, p.rearrange(x))
  }

  def toLRAAtom(l: Lit) =
    // assume l consists of an inequation.
    // Disequations and equations cannot be converted to single LRA atoms and have to be eliminated beforehand
    l match {
      case Lit(true, LessEqn(s, t))    ⇒ LRAAtom(LessEqn.op, (toPolynomial(t) - toPolynomial(s)))
      case Lit(true, LessEqEqn(s, t))  ⇒ LRAAtom(LessEqEqn.op, (toPolynomial(t) - toPolynomial(s)))
      case Lit(false, LessEqn(s, t))   ⇒ LRAAtom(LessEqEqn.op, (toPolynomial(s) - toPolynomial(t)))
      case Lit(false, LessEqEqn(s, t)) ⇒ LRAAtom(LessEqn.op, (toPolynomial(s) - toPolynomial(t)))
    }

  /** Eliminate x from inEqns, giving a new list of inequations
   * Assume inEqns consists of positive < and <= literals only, connected by 'and'
   */
  def QEInEqns(inEqns: List[LRAAtom], x: VarOrSymConst): List[LRAAtom] = {
    // nlits is a list of and-connected atoms, each containing the variable x
    // By subtracting the polynomials the variable x possibly disappears. Cf. x<x 
    // Rearrangement and the following code must deal with that case
    def newop(op1: Operator, op2: Operator) = (op1, op2) match {
      case (LessEqEqn.op, LessEqEqn.op) ⇒ LessEqEqn.op
      case (_, _)                     ⇒ LessEqn.op
    }
    var withoutx = List.empty[LRAAtom]
    for (
      a1s ← inEqns.tails;
      if !a1s.isEmpty;
      a1 = a1s.head
    ) a1 match {
      case LRAAtom(_, Polynomial(Nil, _)) ⇒ withoutx ::= a1
      case LRAAtom(_, Polynomial(Monomial(_, y) :: _, _)) if y != x ⇒
        withoutx ::= a1 // rearrange(x) did not bring x into target position,
      // which means that a1 does not contain x at all
      case LRAAtom(a1op, Polynomial(Monomial(c1, _) :: p1, k1)) ⇒
        // The variable must be x
        for (a2 ← a1s.tail) {
          a2 match {
            case LRAAtom(a2op, Polynomial(Monomial(c2, y) :: p2, k2)) if y == x ⇒
              (c1.sign, c2.sign) match {
                case (-1, 1) ⇒
                  // a1  =  -c1x ~< p1 + k1
                  // a2  =  -(p2 + k2) ~< c2x
                  // where -c1 and c2 are positive
                  // we form
                  // x ~< (p1 + k1)/-c1
                  // (p2 + k2)/-c2 ~< x
                  // and get the result
                  // (p2 + k2)/-c2 ~< (p1 + k1)/-c1
                  // normalized as LRAAtom
                  // 0 ~< (p1 + k1)/-c1 - (p2 + k2)/-c2 
                  withoutx ::= LRAAtom(newop(a1op, a2op),
                    (Polynomial(p1, k1) / (RatInt(-1) * c1)) -
                      (Polynomial(p2, k2) / (RatInt(-1) * c2)))
                case (1, -1) ⇒
                  // a1  =  -(p1 + k1) ~< c1x 
                  // a2  =  -c2x ~< (p2 + k2)
                  // where c1 and -c2 are positive
                  // we form
                  // (p1 + k1)/-c1 ~< x
                  // x ~< (p2 + k2)/-c2
                  // and get the result
                  // (p1 + k1)/-c1 ~< (p2 + k2)/-c2
                  // normalized as LRAAtom
                  // 0 ~< (p2 + k2)/-c2 - (p1 + k1)/-c1 
                  withoutx ::= LRAAtom(newop(a1op, a2op),
                    (Polynomial(p2, k2) / (RatInt(-1) * c2)) -
                      (Polynomial(p1, k1) / (RatInt(-1) * c1)))
                case (_, _) ⇒ {} // nothing to do - a2 does not contain x or x appears with the same sign
              }
          }
        }
    }
    withoutx
  }

  // We may assume has already been unabstracted, as QE is called from bgtheory.QE
  def QE(cl: Clause): List[Clause] = {
    assume(isLFA(cl), "LFA QE: can deal with LFA clauses only: " + cl)

    // Eliminate the variable x from the clause cl
    def eliminateOne(cl: Clause, x: Var): List[Clause] = {

      // Eliminate all disequations from an or-list of literals
      def elimDisequations(lits: List[Lit]) =
        lits flatMap {
          case l @ Lit(_, PredEqn(_))   ⇒ List(l)
          case l @ Lit(true, Eqn(_, _)) ⇒ List(l)
          case Lit(false, Eqn(s, t))    ⇒ List(Lit(true, LessEqn(s, t)), Lit(true, LessEqn(t, s)))
        }

      // Eliminate all positive equations from the relevant literals
      // This will cause exponential blow-up 
      def elimPosEqns(lits: List[Lit]): List[List[Lit]] =
        lits match {
          case Nil                                 ⇒ List(lits)
          case (l @ Lit(_, PredEqn(_))) :: rest    ⇒ (elimPosEqns(rest) map { l :: _ })
          case (l @ Lit(false, Eqn(_, _))) :: rest ⇒ (elimPosEqns(rest) map { l :: _ })
          case Lit(true, Eqn(s, t)) :: rest ⇒ {
            val restRes = elimPosEqns(rest)
            (restRes map { Lit(true, LessEqEqn(s, t)) :: _ }) :::
              (restRes map { Lit(true, LessEqEqn(t, s)) :: _ })
          }
        }

      // Body of EliminateOne
      // Separate those literals that contain x from those that don't.
      // The latter can be ignored for QE and only needs to be picked up at the end
      val (relLits, remLits) = cl.lits partition { _.vars contains x }
      val relLitsNoDis = elimDisequations(relLits)

      val relLitsNoDisNoPosEqnsList = elimPosEqns(relLitsNoDis)
      // relLitsNoDisNoPosEqnsList is a conjunction of disjuncions without equality.

      val res = relLitsNoDisNoPosEqnsList map { lits ⇒
        {
          // Need to negate (and implicitly connect by and) the or-ed relevant inequations
          // before calling QEInEqns
          val litsForQE = lits map { l ⇒ toLRAAtom(l.compl).rearrange(x) }
          val qeRes = QEInEqns(litsForQE, x)
          // Finally negate qeRes again and append the remainder from above to get the result clause
          Clause((qeRes map { a ⇒ Lit(false, a.toEqn) }) ::: remLits, cl.idxRelevant, cl.age)
        }
      }
      // println("EliminateOne(" + cl + "," + x + ") = " + res)
      res
    }

    // Body of QE
    // Assume unabstr has already been applied to cl
    var open = List(cl)
    var closed = List.empty[Clause]

    while (!open.isEmpty) {
      val next = open.head
      open = open.tail
      if (next.vars.isEmpty)
        // Nothing to do, except elimination of disequations
        closed ::= next
      else
        open :::= eliminateOne(next, next.vars.head) flatMap { _.simplifyCheap }
    }

    // println("QE for LRA on " + cl + " gives:")
    // closed foreach { println(_) }
    // println("\n")
    stats.nrQECalls += 1;
    closed
  }

}
