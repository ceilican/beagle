package beagle.bgtheory.LRA

import beagle.VerySimplex._
import beagle.VerySimplex.datastructures._
import beagle.util.Rat
import beagle.fol._
import term._

object VSConversions 
{
	// Variable to VerySimplex-Variable (i.e., string)
  def varToVSVar(v: VarOrSymConst) = v.toString
  
  // Rational to VerySimplex-Rational
  def ratToVSRat(r: Rat): datastructures.Rational = { 
    datastructures.Rational(r.numer, r.denom)
  }
  
  // Monomial to VerySimplex-Monomial
  def monToVSMon(m: Monomial): datastructures.Monomial = {
    m match {
      case Monomial(c, x) =>
	datastructures.Monomial(varToVSVar(x), datastructures.SymNum(ratToVSRat(c)))
    }
  }
  
  // Polynomial to VerySimplex-Polynomial
  def polyToVSPoly(p: Polynomial): datastructures.Polynomial = {
    p match {
      case Polynomial(ms, k) =>
	datastructures.Polynomial(ms.map(m => monToVSMon(m)), SymNum(ratToVSRat(k)))
    }
  }
  
  // SimplexAtom to VerySimplex-Constraint
  def SAToConstraint(a: SimplexAtom): datastructures.Constraint = {
    a match {
      case SimplexAtom(rel, r) =>  // That's short for `0 rel r'.
  	if (rel == "<")
  	  datastructures.Constraint(datastructures.Polynomial(List(),0), Relationship.Lt, polyToVSPoly(r))
  	else
  	  datastructures.Constraint(datastructures.Polynomial(List(),0), Relationship.Leq, polyToVSPoly(r))
    }
  }

  // Generates a generic objective function for a list of constraints
  def genObjectiveFn(vars: List[Constraint]): Constraint = {
    val varIDs    = vars.flatMap(_.vars).distinct
    val monomials = varIDs.map(v => datastructures.Monomial(v, -1))
    Constraint(datastructures.Polynomial(monomials ::: List(datastructures.Monomial("#p_norm"))),
      Relationship.Eq,
      datastructures.Polynomial(List(), 0))
  }
}
