package beagle.bgtheory.LRA

/**
 * Linear Rational Arithmetic
 * Provides methods for quantifier elimination, consistency checking and simplification.
 */

import beagle._
import fol._
import term._
import datastructures._
import util._
import bgtheory._

/**
 * Meant as an interface to Andi's simplex solver
 */
case class SimplexAtom(pred: String, rhs: Polynomial) {
  // pred is either "<" or "≤", stands for "0 < rhs" or "0 ≤ rhs" 
  override def toString = "0 " + pred + " " + rhs
}

object LRASolver extends Solver {
  val name = "fm-rat"
  def subsumes(cl1: Clause, cl2: Clause) = false
  // def hasSolverSignature(cl: Clause) = cl.isLRA
  def QE(cl: Clause) = fourierMotzkin.QE(cl)
  def hasSolverLiterals(cl: Clause) = {
    // the LRA solver does not accept disequations
    // We may assume all greater and greatereq literals have been removed already
    // but other negated inequations are ok
    assume(isLRA(cl) && cl.isGround, "clause " + cl)
    def isSolverLiteral(l: Lit) = l match {
      case Lit(_, PredEqn(_))    ⇒ true
      case Lit(true, Eqn(_, _))  ⇒ true // can expand internally into two inequations 
      case Lit(false, Eqn(_, _)) ⇒ false
    }
    cl.lits forall { isSolverLiteral(_) }
  }

  def toSolverLiterals(cl: Clause) = {
    val resLits = cl.lits flatMap {
      case l @ Lit(_, PredEqn(_))   ⇒ List(l)
      // case Lit(false, LessEqEqn(s, t))    ⇒ List(Lit(true, LessEqn(t, s)))
      // case Lit(false, LessEqn(s, t))      ⇒ List(Lit(true, LessEqEqn(t, s)))
      // case l @ Lit(true, LessEqEqn(_, _)) ⇒ List(l)
      // case l @ Lit(true, LessEqn(_, _))   ⇒ List(l)
      case l @ Lit(true, Eqn(_, _)) ⇒ List(l)
      case Lit(false, Eqn(s, t))    ⇒ List(Lit(true, LessEqn(s, t)), Lit(true, LessEqn(t, s)))
    }
    Clause(resLits, cl.idxRelevant, cl.age)
  }

  def check(cls: Iterable[Clause]): SolverResult = {
    // cls foreach { println(_) }
    assume(cls forall { _.isUnitClause })

    def isInvalid(a: SimplexAtom) =
      (a.pred, a.rhs) match {
        case ("≤", Polynomial(Nil, k))  ⇒ !(RatInt(0) <= k)
        case ("<", Polynomial(Nil, k))  ⇒ !(RatInt(0) < k)
        case ("=", Polynomial(Nil, k))  ⇒ !(RatInt(0) == k)
        case ("!=", Polynomial(Nil, k)) ⇒ !(RatInt(0) != k)
        case (_, _)                     ⇒ false
      }

    def isValid(a: SimplexAtom) =
      (a.pred, a.rhs) match {
        case ("≤", Polynomial(Nil, k))  ⇒ (RatInt(0) <= k)
        case ("<", Polynomial(Nil, k))  ⇒ (RatInt(0) < k)
        case ("=", Polynomial(Nil, k))  ⇒ (RatInt(0) == k)
        case ("!=", Polynomial(Nil, k)) ⇒ (RatInt(0) != k)
        case (_, _)                     ⇒ false
      }

    /*
    case class UNSAT(a: SimplexAtom) extends Exception
    // Evaluate ground SimplexAtoms
    def checkTrivial(a: SimplexAtom): List[SimplexAtom] =
      (a.pred, a.rhs) match {
        case ("≤", Polynomial(Nil, k)) ⇒ if (RatInt(0) <= k) List.empty else throw UNSAT(a)
        case ("<", Polynomial(Nil, k)) ⇒ if (RatInt(0) < k) List.empty else throw UNSAT(a)
        case (_, _)                    ⇒ List(a)
      }
*/
    def toSimplexAtoms(l: Lit) = {
      import Polynomial._
      (l match {
        // disequations have been eliminated beforehand
        case Lit(true, LessEqn(s, t))    ⇒ List(SimplexAtom("<", toPolynomial(t) - toPolynomial(s)))
        case Lit(true, LessEqEqn(s, t))  ⇒ List(SimplexAtom("≤", toPolynomial(t) - toPolynomial(s)))
        case Lit(false, LessEqn(s, t))   ⇒ List(SimplexAtom("≤", toPolynomial(s) - toPolynomial(t)))
        case Lit(false, LessEqEqn(s, t)) ⇒ List(SimplexAtom("<", toPolynomial(s) - toPolynomial(t)))
        case Lit(true, Eqn(s, t)) ⇒ {
          val (sp, tp) = (toPolynomial(s), toPolynomial(t))
          List(SimplexAtom("≤", tp - sp), SimplexAtom("≤", sp - tp))
        }
      }) filterNot { isValid(_) }
    }

    /*
       * Eliminate all variables that have infinitely large or small solutions, if any
       */
    def elimUnboundVars(atoms: Iterable[SimplexAtom]) = {

      def elim(atoms: Iterable[SimplexAtom], x: VarOrSymConst): Iterable[SimplexAtom] = {
        // Eliminate all SimplexAtoms from atoms containing x, provided this preserves satisfiability
        // because either arbitrarily small or large solutions wrt x exist 

        // We go through the atoms one after the other and decide whether we're looking for positive coefficients only
        // or negative coefficients only as we go
        var res = List.empty[SimplexAtom]
        var sign = 0 // 0 stands for "not yet determined"
        for (
          a ← atoms;
          p = a.rhs
        ) (p.ms find { _.x == x }) match {
          case Some(Monomial(c, _)) ⇒
            // got a monomial with the variable x
            (c.signum, sign) match {
              case (csign, 0) ⇒
                // sign was undetermined, fix it now and remove a
                sign = csign
              case (-1, -1) | (1, 1) ⇒
                // sign of x in this atom is consistent with previous ones, remove a
                ()
              case (-1, 1) | (1, -1) ⇒
                // conflict in signs, cannot eliminate 
                return atoms
            }
          case None ⇒
            // x dos not occur in p
            res ::= a
        }
        // if (sign != 0) println("Eliminated " + x + " because of unbounded solutions")
        res.reverse
      }

      val varOrSymConsts = atoms.foldLeft(Set.empty[VarOrSymConst])(_ ++ _.rhs.varOrSymConsts)
      var res = atoms
      for (x ← varOrSymConsts) res = elim(res, x)
      res
    }

    //    try {
    //    val atoms = cls flatMap { cl ⇒ toSimplexAtoms(cl(0)) } flatMap { checkTrivial(_) }
    val atoms = cls map { cl ⇒ toSimplexAtoms(cl(0)) } flatten // also filters out valid ones

    if (atoms exists { a ⇒ isInvalid(a) }) return UNSAT(None)
    val atomsUnboundElim = elimUnboundVars(atoms)
    if (atomsUnboundElim.isEmpty) return SAT

    // println("About to call simplex on these literals:")
    // atomsUnboundElim foreach { println(_) }

    val rawCons = atomsUnboundElim.map(a ⇒ VSConversions.SAToConstraint(a)).toList // todo: get rid of this toList
    val cons = rawCons.map(_.replaceVars)

    // println("Which VerySimplex will see as:")
    // cons foreach { println(_) }

    val objFunction = VSConversions.genObjectiveFn(rawCons).replaceVars
    val simplexSolver = new VerySimplex.optalgorithms.Simplex(cons, objFunction)
    val sol = simplexSolver.solve

    // print("VerySimplex says: ")
    if (sol.feasible(cons)) {
      // println("*** Solution found. *** " + sol.getValues);
      return SAT
    } else {
      // println("*** No solution. ***");
      return UNSAT(None)
    }
    //    } catch {
    //case UNSAT(a) ⇒ {
    //        println("Constraint is trivially unsatisfiable, got " + a)
    //return false
    //}
    //}
  }

  val needsUnitClauses = true

  val simpRulesTermSafe: List[SimpRule[Term]] = List(

    new SimpRule[Term]("evalSum1", {
      case Sum(i1: DomElemRat, i2: DomElemRat) ⇒ DomElemRat(i1.value + i2.value)
      case t                                   ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalProduct", {
      case Product(i1: DomElemRat, i2: DomElemRat) ⇒ DomElemRat(i1.value * i2.value)
      case _                                       ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalDifference1", {
      case Difference(i1: DomElemRat, i2: DomElemRat) ⇒ DomElemRat(i1.value - i2.value)
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalQuotient", {
      case Quotient(i1: DomElemRat, i2: DomElemRat) ⇒ DomElemRat(i1.value / i2.value)
      case _                                        ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalUMinus", {
      case UMinus(i1: DomElemRat) ⇒ DomElemRat(i1.value * -1)
      case _                      ⇒ throw NotApplicable
    }),

    //This includes the LFA version of operators
    new SimpRule[Term]("evalToX", {
      case PFunTerm(ToIntOpRat, List(i: DomElemRat)) ⇒ 
	LIA.DomElemInt( math.floor(i.value.toDouble).toInt )
      case PFunTerm(ToRatOpRat, List(s)) if s.sort == LRA.RatSort ⇒ s
      case PFunTerm(ToRealOpRat, List(i: DomElemRat)) ⇒ LFA.DomElemReal(i.value)
      case _                      ⇒ throw NotApplicable
    }),

    //used to recover from moving NL product to the foreground, if possible.
    new SimpRule[Term]("replaceNLPP", {
      case PFunTerm(NLPPOpRat, List(n: DomElemRat, x)) ⇒ Product(n,x)
      case PFunTerm(NLPPOpRat, List(x, n: DomElemRat)) ⇒ Product(x,n)
      case _                      ⇒ throw NotApplicable
    })
  )

  val simpRulesTermUnsafe: List[SimpRule[Term]] = List(

    new SimpRule[Term]("orientSum", {
      case Sum(t: Term, i: DomElemRat) if !t.isDomElem ⇒ Sum(i, t)
      case _ ⇒ throw NotApplicable
    }),
    new SimpRule[Term]("orientDifference", {
      case Difference(t: Term, i: DomElemRat) if !t.isDomElem ⇒ Sum(DomElemRat(i.value.uminus), t)
      case _ ⇒ throw NotApplicable
    }),
    new SimpRule[Term]("orientProduct", {
      case Product(t: Term, i: DomElemRat) if !t.isDomElem ⇒ Product(i, t)
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalSum2", {
      case Sum(i1: DomElemRat, Sum(i2: DomElemRat, t)) ⇒ Sum(DomElemRat(i1.value + i2.value), t)
      case Sum(i1: DomElemRat, Difference(i2: DomElemRat, t)) ⇒ Difference(DomElemRat(i1.value + i2.value), t)
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("assoc", {
      case Sum(Sum(s: Term, t: Term), u: Term) ⇒ Sum(s, Sum(t, u))
      case Sum(Difference(s: Term, t: Term), u: Term) ⇒ Sum(s, Difference(u, t))
      case Difference(Sum(s: Term, t: Term), u: Term) ⇒ Sum(s, Difference(t, u))
      case Difference(Difference(s: Term, t: Term), u: Term) ⇒ Difference(s, Sum(t, u))
      case Sum(s: Term, Sum(i: DomElemRat, t)) if !s.isDomElem ⇒ Sum(i, Sum(s, t))
      case Sum(s: Term, Difference(i: DomElemRat, t)) if !s.isDomElem ⇒ Sum(i, Difference(s, t))
      case Difference(s: Term, Sum(i: DomElemRat, t)) if !s.isDomElem ⇒ Sum(UMinus(i), Difference(s, t))
      case Difference(s: Term, Difference(i: DomElemRat, t)) if !s.isDomElem ⇒ Sum(UMinus(i), Sum(s, t))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalDifference2", {
      case Difference(i1: DomElemRat, Difference(i2: DomElemRat, t)) ⇒ Sum(DomElemRat(i1.value - i2.value), t)
      case Difference(i1: DomElemRat, Sum(i2: DomElemRat, t)) ⇒ Difference(DomElemRat(i1.value - i2.value), t)
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Term]("evalNeutral", {
      case Difference(t: Term, ZeroRat) ⇒ t
      case Sum(ZeroRat, t: Term)        ⇒ t
      case Product(OneRat, t: Term)     ⇒ t
      case Quotient(t: Term, OneRat)    ⇒ t
      case _                            ⇒ throw NotApplicable
    }))

  // Rules to simplify literals
  val simpRulesLitSafe: List[SimpRule[Lit]] = List(

    // This rule gets rid of all >-literals
    new SimpRule[Lit]("NormalizeInequality", {
      case Lit(isPos, GreaterEqn(d1, d2)) ⇒ Lit(isPos, LessEqn(d2, d1))
      case Lit(isPos, GreaterEqEqn(d1, d2)) ⇒ Lit(isPos, LessEqEqn(d2, d1))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("evalLess", {
      case Lit(true, LessEqn(d1: DomElemRat, d2: DomElemRat)) ⇒
        if (d1.value < d2.value) Lit.TrueLit else Lit.FalseLit
      case Lit(false, LessEqn(d1: DomElemRat, d2: DomElemRat)) ⇒
        if (!(d1.value < d2.value)) Lit.TrueLit else Lit.FalseLit
      case Lit(isPos, LessEqn(t1, t2)) if (t1 == t2) =>
	if (isPos) Lit.FalseLit else Lit.TrueLit
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("evalLessEq", {
      case Lit(true, LessEqEqn(d1: DomElemRat, d2: DomElemRat)) ⇒
        if (d1.value <= d2.value) Lit.TrueLit else Lit.FalseLit
      case Lit(false, LessEqEqn(d1: DomElemRat, d2: DomElemRat)) ⇒
        if (!(d1.value <= d2.value)) Lit.TrueLit else Lit.FalseLit
      case Lit(sign, LessEqEqn(t1, t2)) if (t1 == t2) =>
	if (sign) Lit.TrueLit else Lit.FalseLit
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("evalEqual", {
      case Lit(true, Eqn(d1: DomElemRat, d2: DomElemRat)) ⇒
        if (d1.value == d2.value) Lit.TrueLit else Lit.FalseLit
      case Lit(false, Eqn(d1: DomElemRat, d2: DomElemRat)) ⇒
        if (d1.value != d2.value) Lit.TrueLit else Lit.FalseLit
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("evalIsX", {
      case Lit(t, PredEqn(PFunTerm(IsIntOpRat, List(d: DomElemRat)))) ⇒
	    if (d.value.isRatInt == t) Lit.TrueLit else Lit.FalseLit
      case Lit(t, PredEqn(PFunTerm(IsRatOpRat, List(s)))) if s.sort == RatSort ⇒
	    if (t) Lit.TrueLit else Lit.FalseLit
      case Lit(t, PredEqn(PFunTerm(IsRealOpRat, List(s)))) if s.sort == RatSort ⇒
	    if (t) Lit.TrueLit else Lit.FalseLit
      case _ ⇒ throw NotApplicable
    })
  )

  val simpRulesLitUnsafe: List[SimpRule[Lit]] = List(

    // Incomplete
    new SimpRule[Lit]("sumDiffEqual1", {
      case Lit(isPos, Eqn(Sum(d1: DomElemRat, t), d2: DomElemRat)) ⇒ Lit(isPos, Eqn(t, DomElemRat(d2.value - d1.value)))
      case Lit(isPos, Eqn(Difference(d1: DomElemRat, t), d2: DomElemRat)) ⇒ Lit(isPos, Eqn(t, DomElemRat(d1.value - d2.value)))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("sumDiffEqual2", {
      case Lit(isPos, Eqn(Sum(d1: DomElemRat, t1), Sum(d2: DomElemRat, t2))) ⇒ Lit(isPos, Eqn(t1, Sum(DomElemRat(d2.value - d1.value), t2)))
      case Lit(isPos, Eqn(Difference(d1: DomElemRat, t1), Difference(d2: DomElemRat, t2))) ⇒ Lit(isPos, Eqn(t1, Sum(DomElemRat(d1.value - d2.value), t2)))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("factorEqual1", {
      case Lit(isPos, Eqn(Product(d1: DomElemRat, t), d2: DomElemRat)) if (d1.value != 0) ⇒ Lit(isPos, Eqn(t, DomElemRat(d2.value / d1.value)))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("factorEqual2", {
      case Lit(isPos, Eqn(Product(d1: DomElemRat, t1), Product(d2: DomElemRat, t2))) if (d1.value != 0) ⇒ Lit(isPos, Eqn(t1, Product(DomElemRat(d2.value / d1.value), t2)))
      case Lit(isPos, Eqn(Product(d1: DomElemRat, t1), Product(d2: DomElemRat, t2))) if (d2.value != 0) ⇒ Lit(isPos, Eqn(Product(DomElemRat(d1.value / d2.value), t1), t2))
      case _ ⇒ throw NotApplicable
    }),

    // Incomplete
    new SimpRule[Lit]("factorLessEq", {
      case Lit(isPos, LessEqEqn(Product(d1: DomElemRat, t), d2: DomElemRat)) if (d1.value != 0) ⇒ Lit(isPos, LessEqEqn(t, DomElemRat(d2.value / d1.value)).swapIfNeg(d1.value.signum))
      case Lit(isPos, LessEqEqn(d2: DomElemRat, Product(d1: DomElemRat, t))) if (d1.value != 0) ⇒ Lit(isPos, LessEqEqn(DomElemRat(d2.value / d1.value), t).swapIfNeg(d1.value.signum))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("factorLess", {
      case Lit(isPos, LessEqn(Product(d1: DomElemRat, t), d2: DomElemRat)) if (d1.value != 0) ⇒ Lit(isPos, LessEqn(t, DomElemRat(d2.value / d1.value)).swapIfNeg(d1.value.signum))
      case Lit(isPos, LessEqn(d2: DomElemRat, Product(d1: DomElemRat, t))) if (d1.value != 0) ⇒ Lit(isPos, LessEqn(DomElemRat(d2.value / d1.value), t).swapIfNeg(d1.value.signum))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("sumLessEq", {
      case Lit(isPos, LessEqEqn(Sum(d1: DomElemRat, t), d2: DomElemRat)) ⇒ Lit(isPos, LessEqEqn(t, DomElemRat(d2.value - d1.value)))
      case Lit(isPos, LessEqEqn(d2: DomElemRat, Sum(d1: DomElemRat, t))) ⇒ Lit(isPos, LessEqEqn(DomElemRat(d2.value - d1.value), t))
      case _ ⇒ throw NotApplicable
    }),

    new SimpRule[Lit]("sumLess", {
      case Lit(isPos, LessEqn(Sum(d1: DomElemRat, t), d2: DomElemRat)) ⇒ Lit(isPos, LessEqn(t, DomElemRat(d2.value - d1.value)))
      case Lit(isPos, LessEqn(d2: DomElemRat, Sum(d1: DomElemRat, t))) ⇒ Lit(isPos, LessEqn(DomElemRat(d2.value - d1.value), t))
      case _ ⇒ throw NotApplicable
    }) // todo: similar rules for difference
    )
}

