package beagle.bgtheory

import beagle._
import fol._
import term._
import fol.Signature._
import datastructures._
import util.RatInt

package object LRA {

  object RatSort extends BGSort("$rat") { override def toString = "ℚ" }

  //x-theory tests
  val IsIntOpRat = new IsIntOp(RatSort)
  val IsRatOpRat = new IsRatOp(RatSort)
  val IsRealOpRat = new IsRealOp(RatSort)
  
  val ToIntOpRat = new ToIntOp(RatSort)
  val ToRatOpRat = new ToRatOp(RatSort)
  val ToRealOpRat = new ToRealOp(RatSort)

  // Notoce these operators are not yet added to LRAOperators
  val QuotientEOpRat = new QuotientEOp(RatSort)
  val QuotientTOpRat = new QuotientTOp(RatSort)
  val QuotientFOpRat = new QuotientFOp(RatSort)
  val RemainderEOpRat = new RemainderEOp(RatSort)
  val RemainderTOpRat = new RemainderTOp(RatSort)
  val RemainderFOpRat = new RemainderFOp(RatSort)

  val NLPPOpRat = new NLPPOp(RatSort)

  val LRAOperators = Set(Sum.op, UMinus.op, GreaterEqn.op, LessEqn.op, Difference.op, 
			 Product.op, Quotient.op, LessEqEqn.op, GreaterEqEqn.op, 
			 IsIntOpRat, IsRatOpRat, IsRealOpRat,
			 ToIntOpRat, ToRatOpRat, ToRealOpRat,
			 NLPPOpRat)
  

  def addStandardOperators(s: Signature) = {
    var res = s
    res += RatSort
    LRAOperators foreach { res += _ }
    res
  }

  val ZeroRat = DomElemRat(RatInt(0))
  val OneRat = DomElemRat(RatInt(1))
  val MinusOneRat = DomElemRat(RatInt(-1))

  RatSort.someElement = ZeroRat

  def isLRA(e: Expression[_]) =
    e.isBG && (e.sorts subsetOf Set(RatSort)) // && (e.operators subsetOf LRAOperators)

  // For simplicity treat reals as rationals. The "platonic" reals are different to the "platonic" rationals, of course,
  // but the TPTP reals are IEEE floating point numbers. So, treating them as "unlimited precision" rationals is not more incorrect
  // than treating them as platonic reals.

  def toRat(cl: Clause) = {

    def toRat(l: Lit) = {

      def toRat(t: Term): Term = {
        import bgtheory.LFA._
        import bgtheory.LIA._

        def sortToRat(s: Type) = if (s == RealSort) RatSort else s

        t match {
          case AbstVar(name, index, s) ⇒ if (s == RealSort) AbstVar(name, index, RatSort) else t
          case GenVar(name, index, s)  ⇒ if (s == RealSort) GenVar(name, index, RatSort) else t
          case DomElemReal(r)          ⇒ DomElemRat(r)
          case DomElemInt(_)           ⇒ t
          case DomElemRat(_)           ⇒ t
          case NonDomElemFunTerm(Operator(kind, name, Arity(argsSorts, resSort)), args) ⇒
            NonDomElemFunTerm(Operator(kind, name, Arity((argsSorts map { sortToRat(_) }), sortToRat(resSort))), args map { toRat(_) })
          case TT => TT
        }
      }
      Lit(l.isPositive, Eqn(toRat(l.eqn.lhs), toRat(l.eqn.rhs)))
    }

    Clause(cl.lits map { toRat(_) }, cl.idxRelevant, cl.age)
  }
}
