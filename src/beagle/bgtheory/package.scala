package beagle

import fol.Sort
import fol.DefinedOp
import fol.Operator
import fol.FG
import fol.BG
import fol.Arity1
import fol.Arity2
import fol.Signature
import fol.term._

import datastructures._
import util._

package object bgtheory {

  /**
   * All the solvers we have.
   */
  private val solvers: List[Solver] = List(
    PreprocessingSolver,
    LIA.LIASolverClauses,
    LIA.LIASolverUnits,
    LIA.BNBSolverUnits,
    LIA.BNBSolverClauses,
    LIA.Z3Solver,
    LIA.CVC4Solver,
    LRA.LRASolver,
    LFA.LFASolver, 
    EmptySolver)

  /**
   * The solver we are using, set in main.scala depending on the -format flag
   */
  var solver: Solver = null // EmptySolver

  def setSolver(n: String) {
    (solvers find { _.name == n }) match {
      case Some(s) => {
	solver = s
	s.init()
      }
      case None => throw CmdlineError("no such solver: " + n)
    }
  }

  import LFA.RealSort, LRA.RatSort, LIA.IntSort, fol.Type
  /** Give the least upper bound for a pair of BG sorts for casting.
   * Possibly irrelevant if all theory sorts are cast to the LUB at parsing/pre-processing.
   * Currently: Reals > Rat > Int
   */
  def lubSort(s1: Type, s2: Type): Sort = {
    if(s1 == LFA.RealSort || s2 == LFA.RealSort) LFA.RealSort
    else if (s1 == LRA.RatSort || s2 == LRA.RatSort) LRA.RatSort
    else LIA.IntSort
  }

  def castTo(d: Term, s: Type) = (d,s) match {
    case (_,_) if (d.sort == s) => d
    case (d: LIA.DomElemInt, LRA.RatSort) => LRA.DomElemRat(util.Rat(d.value,1))
    case (d: LIA.DomElemInt, LFA.RealSort) => LFA.DomElemReal(util.Rat(d.value,1))
    case (d: LRA.DomElemRat, LFA.RealSort) => LFA.DomElemReal(d.value)
    case _ => throw new Error("Illegal cast from "+d+":"+d.sort+" to "+s)
  }

  /** Thrown by some solvers if they encounter a nonlinear term */
  case class NonLinearTermFail(t: Term) extends Exception

  /** Thrown by some solvers if they encounter a term of a sort they cannot handle */
  case class IllSortedTermFail(t: Term) extends Exception

  def toSolverResult(isConsistent: Boolean) =
    if (isConsistent) SAT else UNSAT(None)
}

// Some simple operator generator classes
// All operators have to be FG. They are only dealt with by simplification, over fully ground instantiated terms
class ToIntOp(sort: Sort) extends DefinedOp(FG, "$to_int", Arity1(sort -> bgtheory.LIA.IntSort))
class ToRatOp(sort: Sort) extends DefinedOp(FG, "$to_rat", Arity1(sort -> bgtheory.LRA.RatSort))
class ToRealOp(sort: Sort) extends DefinedOp(FG, "$to_real", Arity1(sort -> bgtheory.LFA.RealSort))

class IsIntOp(sort: Sort) extends DefinedOp(FG, "$is_int", Arity1(sort -> Signature.OSort))
class IsRatOp(sort: Sort) extends DefinedOp(FG, "$is_rat", Arity1(sort -> Signature.OSort))
class IsRealOp(sort: Sort) extends DefinedOp(FG, "$is_real", Arity1(sort -> Signature.OSort))

class QuotientEOp(sort: Sort) extends DefinedOp(FG, "$quotient_e", Arity2((sort, sort) -> bgtheory.LIA.IntSort))
class QuotientTOp(sort: Sort) extends DefinedOp(FG, "$quotient_t", Arity2((sort, sort) -> bgtheory.LIA.IntSort))
class QuotientFOp(sort: Sort) extends DefinedOp(FG, "$quotient_f", Arity2((sort, sort) -> bgtheory.LIA.IntSort))

class RemainderEOp(sort: Sort) extends DefinedOp(FG, "$remainder_e", Arity2((sort, sort) -> bgtheory.LIA.IntSort))
class RemainderTOp(sort: Sort) extends DefinedOp(FG, "$remainder_t", Arity2((sort, sort) -> bgtheory.LIA.IntSort))
class RemainderFOp(sort: Sort) extends DefinedOp(FG, "$remainder_f", Arity2((sort, sort) -> bgtheory.LIA.IntSort))


/**
 * This is a FG operator used to replace $product in case a non-linear term is found.
 * It will be eliminated with simplification rules if the term ever becomes linear again.
 * For now leave it as an Operator as it needs to be introduced into the signature when
 * printing in TFF mode.
 * TODO- perhaps give this a nicer print function?
 */
class NLPPOp(sort: Sort) extends Operator(FG, "#nlpp", Arity2((sort,sort) -> sort))

/**
 * An example of how a polymorphic implementation of simplification rules would look.
 * evalQuotient simplifies any combination of $int, $real, $rat domain element arguments
 * and gives the most general result sort (though it could be left as an $int in this case).
 *
 * Notice that this cannot be used fully since the signature of the operators constrains
 * the domain elements to be of the same type. 
 */
object QuotientTSimpRules {
  import bgtheory._

  def domElemToDouble(d: DomElem[_]): Double = d match {
    case LIA.DomElemInt(v) => v.toDouble
    case LRA.DomElemRat(v) => v.toDouble
    case LFA.DomElemReal(v) => v.toDouble
  }
  
  val safe = List(
    new SimpRule[Term]("evalquotient_trunc", {
      case PFunTerm(qOp: QuotientTOp, List(i1: DomElem[_], i2: DomElem[_])) => {
	//attempt to make this ad-hoc polymorphic, so that result has the supremum sort
	val resSort = bgtheory.lubSort(i1.sort,i2.sort)
	val (i1Val,i2Val) = (domElemToDouble(i1),domElemToDouble(i2))

	//quotient_t is the truncation of the real division i1/i2
	if(i2Val==0) 
	  throw SyntaxError(s"Zero divisor: quotient_t($i1,$i2)")
	else {
	  val res = (i1Val / i2Val).toInt
	  resSort match {
	    case LIA.IntSort => LIA.DomElemInt(res)
	    case LRA.RatSort => LRA.DomElemRat(util.Rat(res,1))
	    case LFA.RealSort => LFA.DomElemReal(util.Rat(res,1))
	  }
	}
      }
      case _                      ⇒ throw NotApplicable
    }))

  //val unsafe = ???
}

