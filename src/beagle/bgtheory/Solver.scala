package beagle.bgtheory

import beagle._
import fol._
import fol.Signature._
import datastructures._
import util._

/**
 * UnhandledBGClause exception is thrown when QE is attempted on a background
 * clause for which QE is not defined.
 */
case class UnhandledBGClause(cl: Clause, msg: String) extends Exception
case class UnhandledBGFormula(f: Formula, msg: String) extends Exception

sealed abstract class SolverResult
case object SAT extends SolverResult
case class UNSAT(core: Option[List[Int]]) extends SolverResult
case object UNKNOWN extends SolverResult


/**
 * Solver is a collection of methods that
 * a solver for a given background theory must/should have.
 * Now that solver extends Simplification it has state, namely
 * which bgsimp level is currently set.
 */
abstract class Solver extends Simplification {
  /** Used to refer to the solver from command line.*/
  val name: String

  /** @return True if the given clause has a signature accepted by this solver*/
  // Only needed later, for combined theories
  // def hasSolverSignature(cl: Clause): Boolean

  /**
   *  Quantifier elimination
   *  @param cl A clause for which hasSolverSignature holds true.
   *  We also assume unabst has been applied to cl.
   */
  def QE(cl: Clause): List[Clause]

  /**
   *  Quantifier elimination
   *  @param cl A clause (for which hasSolverSignature does not neccessarily hold true).
   *  Eliminates all extraneous BG variables.
   *  We also assume unabst has been applied to cl.
   *  Used for optimizations only, hence is allowed to be implemented as the identity
   */

  /*
  def QEGeneral(cl: Clause): List[Clause]
   // Not used any more. If there is a pressing need could define it in terms of QE on formulas like this:
  def QEGeneral(cl: Clause): List[Clause] = {
    val extraVars = cl.BGLits.bgVars -- cl.nonBGLits.bgVars
    if (extraVars.isEmpty)
      List(cl)
    else {
      val f = QE(Exists(extraVars.toList, (cl.BGLits map { _.compl.toLiteral }).toAnd))
      val res = 
        for (conj ← cnfconversion.toDNF(f))
           yield Clause(cl.nonBGLits ::: (conj map { _.toLit.compl }), cl.idxRelevant, cl.age)
      // println("QEGeneral: given: " + cl)
      // res foreach { cl => println("QEGeneral: result: " + cl) }
      res
    }
  }
   */

  /**
   * Some solvers accept only literals from a sub-signature.
   * This tests whether the given clause is a member of that
   * signature.
   * @param cl Must be a ground clause.
   */
  def hasSolverLiterals(cl: Clause): Boolean

  /**
   * Converts into clauses of the sub-signature which the solver accepts.
   * @param cl Must be a ground clause.
   */
  def toSolverLiterals(cl: Clause): Clause

  /**
   * True if check only accepts unit clauses as input.
   */
  val needsUnitClauses: Boolean

  /**
   * check if a given set of clauses is consistent.
   * It is applied only to clauses for which isSolverClause holds true. 
   *  In particular these clauses are ground.
   * @return (true, None) if the given list of clauses is consistent.
   * @return (false, unsatCore) if inconsistent and unsatCore is an unsatisfiable core.
   */
  def check(cls: Iterable[Clause]): SolverResult

  /**
   * Naive approach to finding a minimal unsatisfiable subset of clauses,
   * used if isConsistent does not return one.
   * So List() => isConsistent==true, otherwise isConsistent!=true
   * @return a subset of clauses such that this set of clauses is unsat
   * but every proper subset is sat.
   */
    def minUnsatCoreClauses(cls: Iterable[Clause]): List[Clause] = {
    //TODO- could have timed cooper calls to avoid the situation where a few
    //clauses cause unsatisfiability, but the remainder takes a long time to
    //prove sat.
    if (check(cls) == SAT)
      List()
    else {
      util.Timer.muc.start()
      var i = 0
      var cs = cls.toList //the working clause set

      while(i < cs.size) {
        if (check(cs.removeNth(i)) == UNSAT(None)) {
          cs = cs.removeNth(i)
          i = 0
        } else i+=1
      }
      //if you get to here, no more removals possible

      util.Timer.muc.stop()

      reporter.debug("*** min unsat. core of:")
      reporter.debug(cls map { _.id })
      reporter.debug("is "+ (cs map { _.id }) )
      
      cs.toList
    }
  }
  
  /**
   * Naive approach to finding a minimal unsatisfiable subset of clauses,
   * used if isConsistent does not return one.
   * So List() => isConsistent==true, otherwise isConsistent!=true
   * @return a subset of clause ids such that this set of clauses is unsat
   * but every proper subset is sat.
   */
  //def minUnsatCore(cls: Iterable[Clause]): List[Int] = {
  //  minUnsatCoreClauses(cls) map {_.id}
  //}

  /**
   * Theory simplification rules for terms.
   * Two kinds: those that are "safe", i.e. always preserve sufficient completeness
   *  and those that are "unsafe" which can possibly destroy sufficient completeness
   *  The unsafe ones do not need to include the safe ones, for aggressive simplification
   *  the two lists are concatenated.
   */
  /*val simpRulesTermSafe: List[SimpRule[Term]]
  val simpRulesTermUnsafe: List[SimpRule[Term]]*/
  /** Theory simplification rules for literals.*/
/*  val simpRulesLitSafe: List[SimpRule[Lit]]
  val simpRulesLitUnsafe: List[SimpRule[Lit]]*/

  // Use cases:
  /**
   * @param cl Must be a background clause.
   * @return True if cl can be sent to the BG reasoner.
   */
  def isSolverClause(cl: Clause) = {
    assume(cl.isBG)
    // assume(bgtheory.solver.hasSolverSignature(cl)) 
    cl.isGround &&
      (!needsUnitClauses || cl.isUnitClause) &&
      hasSolverLiterals(cl)
  }

  /**
   * Given a background clause cl, compute a set of ground clauses
   * that is equivalent to cl over the background domain.
   * Moreover all literals are solver literals.
   * Return the set of ground clauses partitioned into a set that can directly be passed to the
   * bg solver and a set that cannot (because the solver needs unit clauses)
   */
  def asSolverClauses(cl: Clause): (List[Clause], List[Clause]) = {
    assume(cl.isBG, "asSolverClauses applied to non-background clause")
    // println(" xx asSolverClauses " + cl)
    if (isSolverClause(cl))
      (List(cl), List.empty)
    else {
      // assume(hasSolverSignature(cl), "QE applied to clause over wrong signature")
      val hcl = cl.unabstrAggressive 
      // Eliminate the variables, if there are any
      val groundCls = if (hcl.isGround) List(hcl) else QE(hcl)
      // println(" xx " + solver.name + " QE on " + hcl)
      // println(" xx QE result " + groundCls)

      // Convert the ground clauses into clauses over the proper sub-signature
      val withSolverLiterals = groundCls map {
        c ⇒ (if (hasSolverLiterals(c)) c else toSolverLiterals(c))
      }

      if (needsUnitClauses)
        (withSolverLiterals partition { _.length <= 1 })
      else
        (withSolverLiterals, List.empty)
    }
  }

}

/** A solver that can be used during preprocessing
  * It can only do evaluation of ground terms, including conversions like to_int, to_rat etc.
  * This way, clauses over multiple theories may collapse into clauses over one theory.
 */
object PreprocessingSolver extends Solver {
  val name = "PreProcSolver"
  // def hasSolverSignature(cl: Clause) = false
  def subsumes(cl1: Clause, cl2: Clause) = false
  def QE(cl: Clause) = List(cl)
  // def QE(f: Formula, params: Iterable[SymConst]) = f
  def hasSolverLiterals(cl: Clause) = true
  def toSolverLiterals(cl: Clause) = cl
  def check(cls: Iterable[Clause]) = UNKNOWN

  val needsUnitClauses = false

  val simpRulesTermSafe = 
    LRA.LRASolver.simpRulesTermSafe :::
    LFA.LFASolver.simpRulesTermSafe :::
    LIA.LIASolverClauses.simpRulesTermSafe 
  val simpRulesTermUnsafe = List.empty
  val simpRulesLitSafe = 
    LRA.LRASolver.simpRulesLitSafe :::
    LFA.LFASolver.simpRulesLitSafe :::
    LIA.LIASolverClauses.simpRulesLitSafe 
  val simpRulesLitUnsafe = List.empty
}

object EmptySolver extends Solver {
  val name = "empty"
  // def hasSolverSignature(cl: Clause) = false
  def subsumes(cl1: Clause, cl2: Clause) = false
  def QE(cl: Clause) = List(cl)
  // def QE(f: Formula, params: Iterable[SymConst]) = f
  def hasSolverLiterals(cl: Clause) = true
  def toSolverLiterals(cl: Clause) = cl
  def check(cls: Iterable[Clause]) = UNKNOWN
  val needsUnitClauses = false

  val simpRulesTermSafe = List.empty
  val simpRulesTermUnsafe = List.empty
  val simpRulesLitSafe = List.empty
  val simpRulesLitUnsafe = List.empty
}

