package beagle.bgtheory.LIA

import beagle._
import fol._
import term._
import datastructures._
import util._

sealed abstract class PolyAtom(override val pred: String, val n: Int, val p: Polynomial) extends Atom(pred, List(DomElemInt(n), p.toTerm)) {
  override lazy val isBG = true 
  def rearrange(x: VarOrSymConst): PolyAtom
  def replace(x: VarOrSymConst, p: Polynomial): Atom
  override def toString = "(" + n + " " + pred + " " + p + ")"

  /** Whether this is equivalent to an atom of the form t <= ax.
   * Note that `t` need not be ground and `a` need not be a unit.
   */
  def isLowerBoundFor(x: VarOrSymConst): Boolean = false
  /** Whether this is equivalent to an atom of the form ax <= t.
   * Note that `t` need not be ground and `a` need not be a unit.
   */
  def isUpperBoundFor(x: VarOrSymConst): Boolean = false

}


object PolyAtom {

/*  
 def toSignedEqnInner(sign: Boolean, p: Polynomial): (Boolean, Eqn) = {
    var msSoFar = List.empty[Monomial]
    var msRest = p.ms
    // try to find a monomial with a unit coefficient
    while (!msRest.isEmpty) {
      msRest.head match {
        // todo: allow for symbolic constants
        case Monomial(1, x) if x.isVar => return (sign, Eqn(x.asVar, (Polynomial(msSoFar ::: msRest.tail, p.k, p.ims) * -1).toTerm))
        case Monomial(-1, x) if x.isVar => return (sign, Eqn(x.asVar, Polynomial(msSoFar ::: msRest.tail, p.k, p.ims).toTerm))
        case _ => { msSoFar ::= msRest.head; msRest = msRest.tail }
      }
    }
    // nothing found
    return (sign, Eqn(ZeroInt, p.toTerm))
  }
 */

  /** Helper method to convert ZeroEQPoly atom and ZeroNEPoly atoms into signed equations. 
    */
  def toSignedEqnInner(sign: Boolean, hp: Polynomial): (Boolean, Eqn) = {

    def h(lhs: Polynomial, rhs: Polynomial) = {
      val (finalLhs, finalRhs) = 
        if (rhs.k < 0)
          (lhs - rhs.k, rhs - rhs.k)
        else if (lhs.k < 0)
          (lhs - lhs.k, rhs - lhs.k)
        else
          (lhs, rhs)
      (sign, Eqn(finalLhs.toTerm, finalRhs.toTerm))
    }

    // First search p.ims
    val p = hp.factorize
    if (!p.ims.isEmpty) {
      var bestIm = p.ims.head
      var bestImIndex = 0
      for ((im, index) <- (p.ims.tail zip (1 until p.ims.length)))
        // See if im is better than what we've got so far
        if (sign) {
          // We target ordered equations
          if (im.hasUnitCoeff) {
            if (!bestIm.hasUnitCoeff) {
              bestIm = im // prefer unit coefficient ims
              bestImIndex = index
            }
            else if (im.t gtr bestIm.t) {
              bestIm = im // prefer bigger terms, for orientation
              bestImIndex = index
            }
          } 
        } else {
          // sign is negative, target orientable equations
          if (im.t gtr bestIm.t) {
            bestIm = im // prefer bigger terms, for orientation
            bestImIndex = index
          }
        }
      // Build the result
      val newp = Polynomial(p.ms, p.k, p.ims.removeNth(bestImIndex))
      val bestp = Polynomial(List(), 0, List(bestIm))
      val (lhs, rhs) = 
        if (bestIm.c < 0) (bestp * -1, newp) else (bestp, (newp * -1))
      // h(lhs, rhs)
      (sign, Eqn(lhs.toTerm, rhs.toTerm))

    } else {
      // p.ims is empty
      // Basically the same search as above
      var bestM = p.ms.head
      var bestMIndex = 0
      for ((m, index) <- (p.ms.tail zip (1 until p.ms.length)))
        // See if im is better than what we've got so far
        if (sign) {
          // We target ordered equations
          if (m.hasUnitCoeff) {
            if (!bestM.hasUnitCoeff) {
              bestM = m // prefer unit coefficient ims
              bestMIndex = index
            }
            else if (m.x.toTerm gtr bestM.x.toTerm) {
              bestM = m // prefer bigger terms, for orientation
              bestMIndex = index
            }
          } 
        } else {
          // sign is negative, target orientable equations
          if (m.x.toTerm gtr bestM.x.toTerm) {
            bestM = m // prefer bigger terms, for orientation
            bestMIndex = index
          }
        }
      // Build the result
      // Build the result
      val newp = Polynomial(p.ms.removeNth(bestMIndex), p.k, p.ims)
      val bestp = Polynomial(List(bestM), 0, List())
      val (lhs, rhs) = 
        if (bestM.c < 0) (bestp * -1, newp) else (bestp, (newp * -1))
      // h(lhs, rhs)
      (sign, Eqn(lhs.toTerm, rhs.toTerm))
      // Improve: the case when p.k is 0
     // (sign, Eqn(DomElemInt(- p.k), (p - p.k).toTerm))
    }

  }

  def unapply(f: Formula) = f match { 
    case pa:PolyAtom => Some((pa.pred, pa.n, pa.p))
    case _ => None
  }

  // The following method is used by Lit.canonical
  def canConvertToPolyAtom(l: Lit) = 
    l match {
      case Lit(_, LessEqEqn(_, _)) |
           Lit(_, LessEqn(_, _)) |
           Lit(_, GreaterEqEqn(_, _)) |
           Lit(_, GreaterEqn(_, _)) => true
      case Lit(_, Eqn(s, _)) if s.sort == IntSort => true
      case _ => false
    }

  def toPolyAtom(l: Lit) = {
    //this should be ok, assuming canConvertToPolyAtom is called before
    val List(sPoly,tPoly) = 
      l.getPredicateArgs map { Polynomial.toPolynomial(_, allowImproper = true) }

    val res = l match { 
      // s < t ≡ 0 < t-s
      case Lit(true, LessEqn(s, t)) ⇒ ZeroLTPoly(tPoly - sPoly)
      // s ≤ t ≡ 0 ≤ t-s ≡ 0 < (t+1)-s
      case Lit(true, LessEqEqn(s, t)) ⇒ ZeroLTPoly((tPoly + 1) - sPoly)
      // s > t ≡ t < s ≡ 0 < s-t
      case Lit(true, GreaterEqn(s, t)) ⇒ ZeroLTPoly(sPoly - tPoly)
      // s ≥ t ≡ t ≤ s ≡ 0 ≤ s-t ≡ 0 < (s+1)-t
      case Lit(true, GreaterEqEqn(s, t)) ⇒ ZeroLTPoly((sPoly + 1) - tPoly)
      // s = t ≡ 0 = t-s
      case Lit(true, Eqn(s, t)) ⇒ ZeroEQPoly(tPoly - sPoly)

      // ¬(s < t) ≡ s ≥ t ≡ 0 < (s+1)-t
      case Lit(false, LessEqn(s, t)) ⇒ ZeroLTPoly((sPoly + 1) - tPoly)
      // ¬(s ≤ t) ≡ s > t ≡ 0 < s-t
      case Lit(false, LessEqEqn(s, t)) ⇒ ZeroLTPoly(sPoly - tPoly)
        // ¬(s > t) ≡ s ≤ t ≡ 0 < (t+1)-s
      case Lit(false, GreaterEqn(s, t)) ⇒ ZeroLTPoly((tPoly + 1) - sPoly)
        // ¬(s ≥ t) ≡ s < t ≡ 0 < t-s
      case Lit(false, GreaterEqEqn(s, t)) ⇒ ZeroLTPoly(tPoly - sPoly)
      // s ≠ t ≡ 0 ≠ t-s
      case Lit(false, Eqn(s, t)) ⇒ ZeroNEPoly(tPoly - sPoly)
    }
    // println("  toPolyAtom: res = " + res)
    res
  }

  /** Monic polynomials can be converted to Int => Boolean expressions and evaluated */
  def toExpr(pa: PolyAtom): Option[Int => Boolean] = pa match {
    case ZeroLTPoly(p) => p.expr map { _ andThen { 0 < _ } }
    case ZeroEQPoly(p) => p.expr map { _ andThen { 0 == _ } }
    case ZeroNEPoly(p) => p.expr map { _ andThen { 0 != _ } }
    case DividesPolyAtom(n,p) => p.expr map { _ andThen { _%n == 0 } }
  }

}


/*
 * Various classes of PolyAtoms.
 * Assume the polynomials are canonical, i.e. no monomial over the same variable occurs twice
 */

class ZeroLTPoly(p: Polynomial) extends PolyAtom("<", 0, p) {
  // override def applySubst(sigma: Subst) = ZeroLTPoly(p.applySubst(sigma))
  def rearrange(x: VarOrSymConst) = new ZeroLTPoly(p.rearrange(x))
  def replace(x: VarOrSymConst, q: Polynomial) = ZeroLTPoly(p.replace(x, q))

  // override def equals(that: Any) = 
  //   that match { 
  //     case that: ZeroLTPoly => (this canEqual that) && super.equals(that)
  //     case _ => false
  //   }
  // override def canEqual(other: Any): Boolean = other.isInstanceOf[ZeroLTPoly]
  // override def hashCode: Int = p.hashCode

 override lazy val toSignedEqn = {
   val pMinus1 = p-1


   def h(p: Polynomial) = {
    // Strategy is to remove as many negation signs as possible
    val (msNegCoeff, msPosCoeff) = p.ms partition { _.c < 0 }
    val (imsNegCoeff, imsPosCoeff) = p.ims partition { _.c < 0 }
    // This is not good for the GEG's:
    // if (p.k == 1) {
    //   val negs = (Polynomial(msNegCoeff, 0, imsNegCoeff) * -1).toTerm
    //   val poss = Polynomial(msPosCoeff, 0, imsPosCoeff).toTerm
    //   (false, LessEqn(poss, negs))
    //   // (true, LessEqEqn(negs, poss))
    // } else
    {
      val hk = if ((p.k < 0) || (msNegCoeff.isEmpty && imsNegCoeff.isEmpty)) -p.k else 0
      val negs = (Polynomial(msNegCoeff, -hk, imsNegCoeff) * -1)
      val poss = Polynomial(msPosCoeff, p.k+hk, imsPosCoeff)
      // if (poss.k == 1)
      //   (false, LessEqn((poss - 1).toTerm, negs.toTerm))
      // else
        (false, LessEqEqn(poss.toTerm, negs.toTerm))
      // (true, LessEqn(negs, poss))
    }
    // {
    //   val negs = Polynomial(msNegCoeff, 0, imsNegCoeff)
    //   val poss = Polynomial(msPosCoeff, 0, imsPosCoeff)
    //   (false, LessEqn(poss.toTerm, ((negs * -1) + (-p.k + 1)).toTerm))
    //   // (true, LessEqn(negs, poss))
    // }
   }

   if (p.gcdCoeffs >= pMinus1.gcdCoeffs) 
     h(p.factorize)
   else
     h(pMinus1.factorize + 1)
  }

  override def isLowerBoundFor(x: VarOrSymConst) = p.getCoeff(x) > 0
  override def isUpperBoundFor(x: VarOrSymConst) = p.getCoeff(x) < 0
}

class ZeroEQPoly(p: Polynomial) extends PolyAtom("=", 0, p) {
  // override def applySubst(sigma: Subst) = ZeroEQPoly(p.applySubst(sigma))
  def rearrange(x: VarOrSymConst) = new ZeroEQPoly(p.rearrange(x))
  def replace(x: VarOrSymConst, q: Polynomial) = ZeroEQPoly(p.replace(x, q))

  // override def equals(that: Any) = 
  //   that match { 
  //     case that: ZeroEQPoly => (this canEqual that) && super.equals(that) && p == that.p 
  //     case _ => false
  //   }
  // override def canEqual(other: Any): Boolean = other.isInstanceOf[ZeroEQPoly]
  // override def hashCode: Int = p.hashCode

  // Could use the generic method of Atom for conversion to signed equation but this is better
  // as it re-arranges for an equation between a variable and a term that can be eliminated
  override lazy val toSignedEqn = PolyAtom.toSignedEqnInner(true, p)
}

class ZeroNEPoly(p: Polynomial) extends PolyAtom("≠", 0, p) {
  // override def applySubst(sigma: Subst) = ZeroNEPoly(p.applySubst(sigma))
  def rearrange(x: VarOrSymConst) = new ZeroNEPoly(p.rearrange(x))
  def replace(x: VarOrSymConst, q: Polynomial) = ZeroNEPoly(p.replace(x, q))

  // override def equals(that: Any) = 
  //   that match { 
  //     case that: ZeroNEPoly => (this canEqual that) && super.equals(that) && p == that.p 
  //     case _ => false
  //   }
  // override def canEqual(other: Any): Boolean = other.isInstanceOf[ZeroNEPoly]
  // override def hashCode: Int = p.hashCode

  override lazy val toSignedEqn = PolyAtom.toSignedEqnInner(false, p)
}

class DividesPolyAtom(n: Int, p: Polynomial) extends PolyAtom("|", n, p) {
  // override def applySubst(sigma: Subst) = util.log(DividesPolyAtom(n, p.applySubst(sigma)))
  def rearrange(x: VarOrSymConst) = new DividesPolyAtom(n, p.rearrange(x))
  def replace(x: VarOrSymConst, q: Polynomial) = DividesPolyAtom(n, p.replace(x, q))
  // override def equals(that: Any) = 
  //   that match { 
  //     case that: DividesPolyAtom => (this canEqual that) && n == that.n && p == that.p && super.equals(that)
  //     case _ => false
  //   }
  // override def canEqual(other: Any): Boolean = other.isInstanceOf[DividesPolyAtom]
  // override def hashCode: Int = 41 * n + p.hashCode

  override lazy val toSignedEqn = (true, DividesEqn(DomElemInt(n), p.toTerm))
}

// The apply methods in the following objects include factorization and recognition of trivial cases

// ZeroLTPoly(p) stands for 0 < p
object ZeroLTPoly {
  def apply(p: Polynomial) = 
    // Check for trivial cases first
    p match {
      case Polynomial(Nil, k, Nil) =>
        if (k > 0) TrueAtom else FalseAtom
      // Try to factorize as much as possible
      case p @ Polynomial(_, 0, _) => new ZeroLTPoly(p.factorize)
      case p @ Polynomial(_, 1, _) => new ZeroLTPoly((p - 1).factorize + 1)
      case p @ Polynomial(_, -1, _) => new ZeroLTPoly((p - 1).factorize + 1)
      case p @ Polynomial(_, k, _) => {
        // try both:
        val h1 = p.factorize
        val h2 = (p - 1).factorize + 1
        var best = p
        if (math.abs(h1.k) < math.abs(best.k)) best = h1
        if (math.abs(h2.k) < math.abs(best.k)) best = h2
        new ZeroLTPoly(best)
      }
    }

/*    p match {
      case Polynomial(Nil, k, Nil) =>
        if (k > 0) TrueAtom else FalseAtom
      case p @ Polynomial(List(Monomial(c,x)), k, Nil) => {
          val d = math.abs(c)
          if (d > 1) 
            new ZeroLTPoly(Polynomial(List(Monomial(c/d,x)), k/d, Nil)) 
          else new ZeroLTPoly(p)
        }
      case Polynomial(_, 1, _) =>
        new ZeroLTPoly((p - 1).factorize + 1)
      case p => new ZeroLTPoly(p.factorize)
    }
 */

  def unapply(f: Formula) = 
    f match {
      case PolyAtom("<", 0, p) => Some(p)
      case _ => None
    }
}

/**
 * Canonical representation of a positive equation: s=t becomes 0=s-t
 */
object ZeroEQPoly {
  def apply(p: Polynomial) = {
    // Check for trivial cases first 
    p match {
      case Polynomial(Nil, k, Nil) =>
        if (k == 0) TrueAtom else FalseAtom
      case p => new ZeroEQPoly(p.factorize)
    }
  }

  def unapply(f: Formula) =
    f match {
      case PolyAtom("=", 0, p) => Some(p)
      case _ => None
    }
}

object ZeroNEPoly {
  def apply(p: Polynomial) = {
    // Check for trivial cases first 
    p match {
      case Polynomial(Nil, k, Nil) =>
        if (k == 0) FalseAtom else TrueAtom
      case p => new ZeroNEPoly(p.factorize)
    }
  }

  def unapply(f: Formula) =
    f match {
      case PolyAtom("≠", 0, p) => Some(p)
      case _ => None
    }
}

object DividesPolyAtom {
  def apply(n: Int, p: Polynomial): Atom = {
    assume(p.ims.isEmpty)
    p match {
      case Polynomial(Nil, k, Nil) =>
        // Constant case
        if ((k % n) == 0) TrueAtom else FalseAtom
      case p => {
        // Undecided case. Make sure n is positive and as small as possible
        val g = gcd(math.abs(n), p.gcdCoeffs)
        // Normalize n and p by dividing by g
        val (normn, normp) = if (n < 0) (n / -g, p / -g) else (n / g, p / g)
        if (normn == 1) TrueAtom else new DividesPolyAtom(normn, normp)
      }
    }
  }

  def unapply(f: Formula) =
    f match {
      case PolyAtom("|", n, p) if p.ims.isEmpty => Some((n, p))
      case _ => None
    }
}

