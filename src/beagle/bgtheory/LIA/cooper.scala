package beagle.bgtheory.LIA

/**
 * Functions implementing Cooper quantifier elimination for LIA.
 * TODO- better simplification for terms/lits
 */
object cooper {

  import beagle._
  import fol._
  import fol.term._
  import util._
  import datastructures.Clause

  /**
   * Turn f into an equivalent formula such that all polynomials contain x only with a unit coefficient.
   * Assumes that f has been rearranged for x
   *
   * given 0 < cx + p and assume c|lcm, and let d = lcm/c
   * return 0 < x + dp
   *
   * For divisibility constraints transform
   * n | cx + p
   * into (normalized version of)
   * dn | x + dp
   *
   * For equational constraints transform
   *  0 = cx + p
   * into
   *  0 =  x + dp
   * Because d is positive iff k is, dp will always have the right sign
   * (and x has a positive unit coefficient)
   *
   * Disequations: similarly
   */
  def unitCoeffs(f: Formula, x: VarOrSymConst) = {    
    // Thanks to embedding the following functions into unitCoeffs, the variable x can be left implicit 

    /** @return the lcm of all coefficients of x in f */
    // def coeffsLcm(f: Formula): Int =
    //   f match {
    //     case UnOpForm(op, g) ⇒ coeffsLcm(g)
    //     case BinOpForm(op, f1, f2) ⇒ lcm(coeffsLcm(f1), coeffsLcm(f2))
    //     case PolyAtom(_, _, Polynomial(Monomial(c, y) :: _, k)) if (y == x) ⇒ math.abs(c)
    //     case _ ⇒ 1
    //   }

    /**
     * @return the lcm of all coefficients of x in f
     * Getting the coefficients then applying lcm in list form
     * is much faster for large formulas
     */
    def coeffsLcm(f: Formula): Int = {
      //extract a list of polyAtom coeffs of x
      def getCoeffs(g: Formula, x: VarOrSymConst, acc: List[Int] = List(), rest: List[Formula] = List()): List[Int] = {
    	g match {
          case UnOpForm(_, g) ⇒ getCoeffs(g,x,acc,rest)
          case BinOpForm(_, f1, f2) ⇒ getCoeffs(f1,x,acc,f2::rest)
          case PolyAtom(_, _, Polynomial(Monomial(c, y) :: _, k, Nil)) if (y == x) ⇒ {
	    val lcmList = c.abs :: acc
	    if(rest.isEmpty) lcmList
	    else getCoeffs(rest.head,x,lcmList,rest.tail)
	  }
          case _ ⇒ {
	    val lcmList = 1 :: acc
	    if(rest.isEmpty) lcmList
	    else getCoeffs(rest.head,x,lcmList,rest.tail)
	  }
    	}
      }

      lcm(getCoeffs(f,x))
      //todo- what if you get int overflow here
      //- could use BigInt, or could represent lcm as products
    }

    val xcoeffslcm = coeffsLcm(f)

    /** Give all occurences of x in f a unit coefficient */
    def h(f: Formula): Formula =
      f.mapAtom({
        // The case for divides is a bit special because the result requires canonical form,
        // which is obtained by calling Divides; also could simplify to True or False
        case DividesPolyAtom(n, Polynomial(Monomial(c, y) :: ms, k, Nil)) if (y == x) ⇒ {
          val d = xcoeffslcm / c
          // gives the new monomial a unit coefficient
          DividesPolyAtom(d * n, Monomial(1, x) +: (Polynomial(ms, k, Nil) * d)) // d*n could be negative, but not a problem
        }
        case ZeroLTPoly(Polynomial(Monomial(c, y) :: ms, k, Nil)) if (y == x) ⇒ {
          val d = xcoeffslcm / c
          if (d < 0)
            // Must multiply by -d because need a positive number to multiply an equality  
            ZeroLTPoly(Monomial(-1, x) +: (Polynomial(ms, k, Nil) * -d))
          else
            ZeroLTPoly(Monomial(1, x) +: (Polynomial(ms, k, Nil) * d))
        }
        // EQ or NE case - no problem with multiplying by a possibly negative d
        case ZeroEQPoly(Polynomial(Monomial(c, y) :: ms, k, Nil)) if (y == x) ⇒ {
          val d = xcoeffslcm / c
          ZeroEQPoly(Monomial(1, x) +: (Polynomial(ms, k, Nil) * d))
        }
        case ZeroNEPoly(Polynomial(Monomial(c, y) :: ms, k, Nil)) if (y == x) ⇒ {
          val d = xcoeffslcm / c
          ZeroNEPoly(Monomial(1, x) +: (Polynomial(ms, k, Nil) * d))
        }
        case g ⇒ g
      })

    if (xcoeffslcm == 1)
      h(f) // no need for the divisibility constraint
    else
      And(DividesPolyAtom(xcoeffslcm, Polynomial(Monomial(1, x) :: Nil, 0, Nil)), h(f))

  }

  /**
   * Rearrange the polynomials in f so that each starts with the x-monomial,
   * if it has one.
   */
  def rearrange(f: Formula, x: VarOrSymConst): Formula = 
    f.mapAtom({ case p:PolyAtom => p.rearrange(x); case a => a })

  /**
   * The minus infinity formula obtained from f
   */
  def infty(f: Formula, x: VarOrSymConst): Formula =
    f.mapAtom({
      case ZeroLTPoly(Polynomial(Monomial(c, y) :: _, _, Nil)) if (y == x) ⇒ {
        assume(c == 1 || c == -1, "monomial with unit coefficient expected")
        c match {
          case 1  ⇒ FalseAtom
          case -1 ⇒ TrueAtom
        }
      }
      case ZeroEQPoly(Polynomial(Monomial(1, y) :: _, _, Nil)) if (y == x) ⇒
        FalseAtom
      // xxx - check this
      case ZeroNEPoly(Polynomial(Monomial(1, y) :: _, _, Nil)) if (y == x) ⇒
         TrueAtom
      // Divides case
      case f ⇒ f
    })

  /**
   * Dual to the minus infinity transform.
   * Only difference is that the ZeroLT cases are reversed.
   * Divisibility atoms unchanged so # instances the same.
   */
  def posInfty(f: Formula, x: VarOrSymConst): Formula =
    f.mapAtom({
      case ZeroLTPoly(Polynomial(Monomial(c, y) :: _, _, _)) if (y == x) ⇒ {
        assume(c == 1 || c == -1, "monomial with unit coefficient expected")
        c match {
          case 1  ⇒ TrueAtom
          case -1 ⇒ FalseAtom
        }
      }
      case ZeroEQPoly(Polynomial(Monomial(1, y) :: _, _, _)) if (y == x) ⇒
        FalseAtom
      case ZeroNEPoly(Polynomial(Monomial(1, y) :: _, _, _)) if (y == x) ⇒
        TrueAtom
      case f => f
    })

  /** @return The LCM of the constants of the divisibility atoms in f that contain x */
  def divisibilityConstsLcm(f: Formula, x: VarOrSymConst): Int = 
    f.atoms.foldLeft(1)((acc,a) => a match {
      case DividesPolyAtom(n, Polynomial(Monomial(_, y) :: _, _, _)) ⇒ if (y == x) lcm(n,acc) else acc // We know n is positive
      case _ ⇒ acc
    })

  def replaceAtom(f: Formula, replaceF: (Atom) => Atom): Formula = f.mapAtom(replaceF)

  /**
   * Assume that f has been rearranged for x.
   * @return The set of lower bound polynomials of x in f.
   */
  def lowerBounds(f: Formula, x: VarOrSymConst): Set[Polynomial] =
    f.atoms.foldLeft(Set.empty[Polynomial])((acc, a) => (a match {
      case ZeroLTPoly(Polynomial(Monomial(c, y) :: ms, k, Nil)) if (y == x) ⇒ {
        assume(c == 1 || c == -1, println("lowerBounds: monomial with non-unary factor encountered"))
        c match {
          case 1  ⇒ Set(Polynomial(ms, k, Nil) * -1)
          case -1 ⇒ Set.empty[Polynomial]
        }
      }
      case ZeroEQPoly(Polynomial(Monomial(c, y) :: ms, k, Nil)) if (y == x) ⇒ {
        assume(c == 1, println("lowerBounds: monomial with non-unary factor encountered"))
        Set((Polynomial(ms, k, Nil) + 1) * -1)
      }
      case ZeroNEPoly(Polynomial(Monomial(c, y) :: ms, k, Nil)) if (y == x) ⇒ {
        assume(c == 1, println("lowerBounds: monomial with non-unary factor encountered"))
        Set(Polynomial(ms, k, Nil) * -1)
      }
      case _ ⇒ Set.empty[Polynomial]
    }) union acc)

  /**
   * Dual 'A'-set to the lower bounds set above. Used with positive infinity transform.
   * Also assumes that f has been rearranged for x.
   */
  def upperBounds(f: Formula, x: VarOrSymConst): Set[Polynomial] =
    f.atoms.foldLeft(Set.empty[Polynomial])((acc, a) => (a match {
      case ZeroLTPoly(Polynomial(Monomial(c, y) :: ms, k, _)) if (y == x) ⇒ {
        assume(c == 1 || c == -1, println("upperBounds: monomial with non-unary factor encountered"))
        c match {
          case 1  ⇒ Set.empty[Polynomial]
          case -1 ⇒ Set(Polynomial(ms, k, List.empty))
        }
      }
      case ZeroEQPoly(Polynomial(Monomial(c, y) :: ms, k, _)) if (y == x) ⇒ {
        assume(c == 1, println("upperBounds: monomial with non-unary factor encountered"))
        Set((Polynomial(ms, k, List.empty) * -1)+1)
      }
      case ZeroNEPoly(Polynomial(Monomial(c, y) :: ms, k, _)) if (y == x) ⇒ {
        assume(c == 1, println("upperBounds: monomial with non-unary factor encountered"))
        Set(Polynomial(ms, k, List.empty) * -1)
      }
      case _ ⇒ Set.empty[Polynomial]
    }) union acc)

  /**
   * Replace all occurences of the variable x in f with the polynomial p.
   * @return f[p].
   */
  def replace(f: Formula, x: VarOrSymConst, p: Polynomial): Formula =
    f match {
      case UnOpForm(op, g)       ⇒ UnOpForm(op, replace(g, x, p))
      case BinOpForm(op, f1, f2) ⇒ BinOpForm(op, replace(f1, x, p), replace(f2, x, p))
      // All these cases explictly because after replacement could get TrueAtom or FalseAtom
      // case a @ PolyAtom(_, _, _) ⇒ a.asInstanceOf[PolyAtom].replace(x, p)
      // More concise, but does it work?
      case a: PolyAtom           ⇒ a.replace(x, p)
      case _                     ⇒ f
    }

  /**
   * Eliminate the variable x from f because it is either unbounded below or unbounded above.
   * @return None if this does not succeed, i.e. if we cannot determine if f has a solution
   * iff f has arbitrary large/small solutions wrt x.
   */
  def elimUnbound(f: Formula, x: VarOrSymConst): Option[Formula] = {
    var sign = 0 // 0 stands for "not yet determined"

    def h(f: Formula): Option[Formula] =
      f match {
        case UnOpForm(op, g) ⇒ // this can only be a negated divisibility constraint
	  h(g) map { UnOpForm(op, _) } //ie return Some(f) if h(g) is defined
        case BinOpForm(op, f1, f2) ⇒ {
          /*val f1Res = h(f1)
          val f2Res = h(f2)
          BinOpForm(op, f1Res, f2Res)
	  */
	  h(f1) flatMap { r1 => h(f2) map { BinOpForm(op,r1,_)} }
        }
        case DividesPolyAtom(_, Polynomial(Monomial(_, y) :: _, _, Nil)) ⇒
          //if (y == x) None else Some(f)
	  //todo: can we do something better?
	  //yes, divides constraints do not prevent x from growing very large/very small
	  //same reasoning applies as in thm 5.7 in Harrison
	  Some(f)
        case p @ ZeroLTPoly(Polynomial(Monomial(c, y) :: _, _, Nil)) ⇒
          if (y == x)
            (c.signum, sign) match {
              case (csign, 0) ⇒ {
                // sign was undetermined, fix it now and remove a
                sign = csign
                Some(TrueAtom)
              }
              case (-1, -1) | (1, 1) ⇒
                // sign of x in this atom is consistent with previous ones
                Some(TrueAtom)
              case (-1, 1) | (1, -1) ⇒
                // conflict in signs, cannot eliminate
		None
            }
          else Some(f)
        case ZeroEQPoly(Polynomial(Monomial(_, y) :: _, _, Nil)) ⇒
          if (y == x) None else Some(f)
        case ZeroNEPoly(Polynomial(Monomial(_, y) :: _, _, Nil)) ⇒
        // never a problem 
          Some(if (y == x) TrueAtom else f)
        case _ ⇒ Some(f) //throw InternalError("unboundElim unexpected case: " + f)
      }

    h(f)
  }

  /**
   * Eliminate x from f because it occurs in some equation in f
   * Works best on conjunctions of atoms.
   * Expects that PolyAtoms of f have been rearranged for x.
   * No simplification is done here, so successful results will include at
   * least one T atom as a conjunct.
   * @return None if x *cannot* be eliminated, or Some(f2) where f2 is
   * f with x eliminated.
   */
  def elimEquation(f: Formula, x: VarOrSymConst): Option[Formula] = {
    for (h ← f.toListAnd)
      h match {
        case ZeroEQPoly(Polynomial(Monomial(1, y) :: r, k, Nil)) if (y == x) ⇒ {
	  val b = Polynomial(r, k, Nil) * -1
	  reporter.debugCooper(s"*** binding $x to $b")
          return Some(replace(f, x, b))
	}
        case ZeroEQPoly(Polynomial(Monomial(-1, y) :: r, k, Nil)) if (y == x) ⇒ {
	  val b = Polynomial(r, k, Nil)
	  reporter.debugCooper(s"*** binding $x to $b")
          return Some(replace(f, x, b))
	}
        case _ ⇒ () // continue
      }

    // nothing found
    return None
  }

  /** Remove inequalities which are implied by smaller inequalities.
   * Unlike other elim* methods it cannot fully eliminate x from a formula.
   */
  def elimSubsumption(f: Formula, x: VarOrSymConst): Option[Formula] = {
    var res = List.empty[Formula]
    var bestPos: Option[Formula] = None
    var bestNeg: Option[Formula] = None
    var success = false
    for (h ← f.toListAnd)
      h match {
        case p @ ZeroLTPoly(Polynomial(List(Monomial(1, y)), k, Nil)) if (y == x) ⇒ {
          bestPos match {
            case None => bestPos = Some(p)
            case Some(ZeroLTPoly(Polynomial(_, kBest, _))) =>
              if (k < kBest) {
                // p is better and subsume current best
                bestPos = Some(p)
                success = true
              }
              else {
                // old best is at least as good as p and subsumes p
                success = true
              }
          }
        }
        case p @ ZeroLTPoly(Polynomial(List(Monomial(-1, y)), k, Nil)) if (y == x) ⇒ {
          bestNeg match {
            case None => bestNeg = Some(p)
            case Some(ZeroLTPoly(Polynomial(_, kBest, _))) =>
              if (k < kBest) {
                // p is better and subsume current best
                bestNeg = Some(p)
                success = true
              }
              else {
                // old best is at least as good as p and subsumes p
                success = true
              }
          }
        }
        case _ ⇒ res ::= h // irrelevant - continue
      }

    (bestNeg, bestPos) match {
      // test for inconsistencies
      case (Some(ZeroLTPoly(Polynomial(_, kNeg, _))),
          Some(ZeroLTPoly(Polynomial(_, kPos, _)))) if (!(kNeg + kPos > 1)) => Some(FalseAtom)
      case (_, _) => 
        if (success) {
          // don't forget to add best
          if (bestPos != None) res ::= bestPos.get
          if (bestNeg != None) res ::= bestNeg.get
          Some(res.reverse.toAnd)
        }
        else
          None
    }
  }

  /**
   * Eliminate a given variable x from f in a cheap way.
   * If f contains x, then rearrange f for x and then
   * try `elimEquation`; `elimSubsumption` and `elimUnbound`
   * in order to simplify.
   */
  def elimCheap(f: Formula, x: VarOrSymConst): Formula = {
    if (f contains x) {

      reporter.debugCooper("elimCheap: try to eliminate " + x + " from " + f)
      val g = rearrange(f, x)
      reporter.debugCooper("elimCheap: rearranged: " + g)

      reporter.debugCooper("elimCheap: try eliminating " + x + " as part of an equation")
      elimEquation(g,x) orElse {
	reporter.debugCooper("elimCheap: try eliminating " + x + " because of unbound solutions")
        // Must try elimination by Unbound first, because subsumption does not remove *all* occurrences of x 
	elimUnbound(g, x)
      } orElse {
	reporter.debugCooper("elimCheap: try eliminating some occurrences of " + x + " by subsumption")
	elimSubsumption(g,x)
      } map { h1 => {
	  reporter.debugCooper("elimCheap: success, obtained " + h1)
	  h1.simplifyCheap
        }
      } getOrElse { f } // if still None return f
    }
    else f
  }

  /**
   * Assumes that g is a quantifier-free formula with normalized atoms, not already simplified.
   * leaves a result that is supposed to be simplified by the caller
   * (Rationale: simplification is best done on the context, i.e. the formula that contains g)
   */
  // todo: update according to elimMany above
  //now this is integrated with eliminateMany

  def elimManyDefer(g: Formula, xs: Iterable[VarOrSymConst]) = {
    var res = g
    for(x <- xs) res = elimCheap(res.simplifyCheap, x)

    var params = Map[Var,Int]()

    for (x ← xs;
	 if(res contains x)) {
	   eliminate(res.simplifyCheap, x, mustDefer=true) match {
	     case Left((r,j,d)) => {
	       res=r
	       params+=(j->d)
	     }
	     case Right(TrueAtom) => res=TrueAtom
	   }
	   reporter.debugCooper("Elimination of " + x + " from " + g + " gives " + res)
	   res = elimCheap(res.simplifyCheap, x)
	 }

    val remainingParams = params.filterKeys(res.vars(_))

    reporter.debugCooper("cooper: need to instantiate with "+remainingParams)
    if(remainingParams.isEmpty) res
    else {
      for((j,d) <- remainingParams) {
	res=rearrange(res,j)
	res=(for(v <- 1 to d) yield {
	  replace(res,j,Polynomial(List.empty,v,List.empty))
	}).toList.toOr.simplifyCheap
	//println(s"after inst $j from 1 to $d, we have $res")
      }
      res
    }
  }

  //case object TooManyInstances extends Exception
  private val NEVER_DEFER = true //Set this to true to disable defer!

  /**
   * Eliminate the variable x from g, return an equivalent formula.
   * Can either instantiate or defer if the number of instances will be large.
   * The hope with defer is that a later formula will falsify the clause set before instantiation.
   * @param mustDefer always defer instantiation, used to recover action of -cooper-defer flag.
   * @return Right(f) when x is eliminated in g in the usual way by instantiation or
   * Left(f,j,d) where f is the result of instatiating with the bounds in the max/min formula
   * and the infinity with a new variable j which must be instantiated over [1,d].
   */
  def eliminate(g: Formula, x: VarOrSymConst, mustDefer: Boolean = false): Either[(Formula,Var,Int),Formula] = {

    reporter.debugCooper("cooper: eliminate " + x + " from " + g)

    val f = rearrange(g, x)
    reporter.debugCooper("cooper: rearranged: " + f)
    
    val h = unitCoeffs(f, x)
    reporter.debugCooper("cooper: elimination formula (has unit coefficients): " + h)

    val hInfty = infty(h, x).simplifyCheap
    reporter.debugCooper("cooper: -infinity formula: " + hInfty)

    val hPosInf = posInfty(h, x).simplifyCheap
    reporter.debugCooper("cooper: +infinity formula: " + hPosInf)

    if(hInfty==TrueAtom || hPosInf==TrueAtom) 
      return Right(TrueAtom)

    val B = lowerBounds(h, x)
    reporter.debugCooper("cooper: lower bounds: " + B)

    val uB = upperBounds(h,x)
    reporter.debugCooper("cooper: upper bounds: " + uB)

    //test which is 'simpler'
    //****** TO DISABLE POS TRANSFORM, JUST SET THIS TO FALSE
    val usePosTransform = 
      (uB.size < B.size)

    if(usePosTransform) reporter.debugCooper("cooper: using positive transform!")

    val D = divisibilityConstsLcm(h, x)
    reporter.debugCooper("cooper: lcm of divisibility constraints: " + D)
    
    val bounds = if(usePosTransform) uB else B
    val inftyF = if(usePosTransform) hPosInf else hInfty

    if(D > 500 && (mustDefer || !NEVER_DEFER)) { //then just inst with bounds and return
      reporter.debugCooper("**Too many instances, using deferred cooper")
      val jvar = AbstVar("J_"+x,IntSort)

      //when using pos infty transform replace with b-j not b+j
      val j = if (usePosTransform) Monomial(-1,jvar) else Monomial(1,jvar)

      val or1: Formula = 
	(replace(inftyF,x,Polynomial(List(j),0, List.empty)) :: 
	 (for(b <- bounds) yield { replace(h,x,b+j)}).toList).toOr

      Left((or1,jvar,D))
    } else { //normal version

    case object TRUEDISJUNCTION extends Exception
    try {
      //***TODO- could try both +infty and -infty looking for TRUEDISJUNCTION
      //is it better to do this or to just choose one?

      reporter.debugCooper(s"Instances of ${if(usePosTransform) '+' else '-'}infinity formula:")
      val resInfty = {
	if(!inftyF.contains(x)) reporter.debugCooper(List(inftyF))
	else
	  for (j ← 1 to D;
	       hInftyj = replace(inftyF, x, Polynomial(List.empty, j, List.empty)).simplifyCheap;
	       if(hInftyj!=FalseAtom)) yield {
		 
		 if (hInftyj == TrueAtom) {
		   reporter.debugCooper(s"*** j=$j satisfies $inftyF")
		   throw TRUEDISJUNCTION
		 }
		 else {
		   reporter.debugCooper(s"j=$j: $hInftyj")
		 }
		 hInftyj
	       }
      }
      
      reporter.debugCooper("\nInstances of elimination formula:")
      val resBounds: Seq[Formula] = 
	if(!h.contains(x)) { 
	  reporter.debugCooper(h)
	  h :: Nil 
	}else {
          val replaceSign: (Int) => Int = if(usePosTransform) (x) => -x else (x) => x
	  
	  if (D > 500) {
	    //a placeholder for x, just like in the deferred version above.
	    val j = 
	      if (usePosTransform) Monomial(-1,AbstVar("J_"+x,IntSort)) 
	      else Monomial(1,AbstVar("J_"+x,IntSort))
	    for (b ← bounds.toSeq;
		 hInner = replace(h, x, b + j);
		 vals = (1 to D);
		 v <- exprFilter(hInner,x,vals)) yield {
              val hMax = replace(hInner, x, Polynomial.Const(v)).simplifyCheap
              reporter.debugCooper(hMax)
              if (hMax == TrueAtom) throw TRUEDISJUNCTION
              hMax
            }
	  }else {  
            for (j ← 1 to D;
		 b ← bounds) yield {
              //note b-j not b+j here
              // It is better performance-wise to not use debug(replace(...))
              val hMax = replace(h, x, b + replaceSign(j)).simplifyCheap
              reporter.debugCooper(hMax)
              if (hMax == TrueAtom) throw TRUEDISJUNCTION
              hMax
            }
	}
      }

      val res = (resInfty ++ resBounds).toOr
      Right(res)

    } catch {
      case TRUEDISJUNCTION ⇒ return Right(TrueAtom)
    }
    }
  }

  /** Quickly eliminate trivial instances of the elimination formula by converting it
   * to a scala expression where possible (i.e. when it contains as a subformula a conjunction of monic poly atoms)
   * and evaluating it.
   * @returns a sequence representing only non-trivial values to instantiate.
   */
  private def exprFilter(f: Formula, x: VarOrSymConst, vals: Seq[Int]): Seq[Int] = { 
    //select eligible literals to check
    val polyLits = f.toList(And) collect {  
      case sf: PolyAtom => PolyAtom.toExpr(sf)
    }

    val polyExprs: List[Int => Boolean] = polyLits collect { case Some(exp) => exp }

    //this function combines all of the PolyAtom expression via &&
    val check: Int => Boolean = (y: Int) => {
      polyExprs.foldLeft(true)( _ && _(y) )
    }

    val res = vals.filter(v => check.apply(v)).toSeq
    reporter.debugCooper(s"exprFilter reduced instances from ${vals.length} to ${res.length}")
    res
  }

  /**
   * Given a conjunction, use the equivalence c < x & x < d == c < d + 1
   * to reduce.
   * Assume every literal of conj contains x.
   */
  def elimShadows(conj: List[Formula], x: VarOrSymConst): List[Formula] = {
    //elimCheap usually gets it first
    if(conj.isEmpty || conj.tail.isEmpty) return conj

    //ineqs are all the inequalities in conj, some might not include x
    val (ineqs,rem) = conj.partition({ 
      case ZeroLTPoly(p) if(p.varOrSymConsts(x)) => true
      case _ => false
    })
    //ineqsR- only those inequalities containing x and rearranged for x
    val ineqsR: List[PolyAtom] = ineqs map { _.asInstanceOf[PolyAtom].rearrange(x) }

    //polyatom is a lower bound iff x coeff is > 0
    val (lb,ub) = ineqsR.partition( _.isLowerBoundFor(x) )

    if(lb.isEmpty || ub.isEmpty) return conj

    val upperAllUnit = ub.forall( _.p.getCoeff(x).abs == 1 )
    val lowerAllUnit = lb.forall( _.p.getCoeff(x).abs == 1 )
    //only if all PolyAtoms with x are inequalities
    //val canReplace = !rem.exists( _.contains(x) )
    //todo- replacement disabled due to performance hit on GEG025
    val canReplace = false

    reporter.debugCooper(s"elimShadows: lb polys are $lb")
    reporter.debugCooper(s"elimShadows: ub polys are $ub")

    /**
     * Given lower bound (b < ax) and upper bound (cx < d)
     * cb < cax < ad <=> ad - cb > 1 <=> ad -cb -1 > 0
     * then then real shadow is cb < ad - 1
     * By transitivity (b < ax) & (cx < d) => cb < ad - 1
     * but not the other way, e.g. 1 < 3x & 5x < 3 == False but 5.1 < 3.3 - 1 = 5 < 8
     * If we have an upper bound with c=1 and *all* lower bounds have a=1 then the
     * upper bound can be replaced with the shadow, but not the upper bounds.
     * If all x coeffs are 1 then the lower bounds can be replaced too.
     * Hence we store the equivalent shadows just in case we can replace later.
     * @return all real shadows possibly including non-equivalent ones.
     */
    def realShadow(lb: Polynomial, ub: Polynomial): Atom = {
      //lb = (b < ax)
      //as a poly l=ax + b + k, so really we get l = (b+k) < ax
      val (Monomial(a,_),negB) = lb.uncons.get
      //ub = (cx < d)
      val (Monomial(negC,_),d) = ub.uncons.get
      val ad = d * a
      val cb = negB * negC
      ZeroLTPoly(ad - cb - 1)
    }

    //store the equivalent atoms in here
    var replacements = Map[PolyAtom,List[Formula]]()

    //conjunction of all resolutions of lower + upper bounds
    val allBoundsShadow: List[Formula] = 
      if(canReplace && upperAllUnit) {
	//we can replace any lower bound with a unit coeff

	//if all lower bounds are unit then they can be removed too.
	if(lowerAllUnit) ub.foreach( u => replacements+=(u->List()) )

	(for(l <- lb) yield {
	  val rs = ub.map( u => realShadow(l.p,u.p) )
	  if(l.p.getCoeff(x).abs==1) {
	    //replace with the conjunction of shadows made with all upper bounds
	    replacements += (l -> rs)
	  }
	  rs.toAnd
	})
      }else if(canReplace && lowerAllUnit) {
	//we can replace any upper bound with a unit coeff
	(for(u <- ub) yield {
	  val rs = lb.map( l => realShadow(l.p,u.p) )
	  if(u.p.getCoeff(x).abs==1) {
	    //replace with the conjunction of shadows made with all upper bounds
	    replacements += (u -> rs)
	  }
	  rs.toAnd
	})
      }else {
	//cannot replace either, so just check for unsat
	  (for(l <- lb;
	       u <- ub) yield {
	    realShadow(l.p,u.p)
	  })
      }

    if(allBoundsShadow.toAnd.simplifyCheap == FalseAtom) {
      reporter.debugCooper("elimShadows: real shadow was unsat so formula is false!")
      return List(FalseAtom)
    } else if(!replacements.isEmpty) {
      reporter.debugCooper("elimShadows: replacements will be: "+replacements)
      //replace in conj...
      val res = conj.flatMap({ 
	case b: ZeroLTPoly => replacements.getOrElse(b.rearrange(x),List(b))
	case f => List(f)
      })
      
      reporter.debugCooper("elimShadows: simplified to: "+res)
      return res
    } else {
      return conj //not simplified
    }
  }

  /**
   * Assumes that g is a quantifier-free formula with normalized atoms (i.e. PolyAtoms), not already simplified.
   * leaves a result that is supposed to be simplified by the caller
   * (Rationale: simplification is best done on the context, i.e. the formula that contains g)
   * Multiplies out disjunctions resulting from instantiation.
   * Invariant: xs ++ instantiations.keys should contain all of the Vars + SymConsts of the formula?
   * @param instantiations are outstanding instantiations from previous recursive calls.
   */
  def eliminateMany(g: Formula, xs: Iterable[VarOrSymConst], instantiate: Map[Var,Int] = Map()): Formula = {
    var toInstantiate = instantiate
    var resOr = List.empty[Formula]

    for (conj <- g.toListOr) {
        // first we apply eliminate cheap to conj
        var conjRes: List[Formula] = conj.toListAnd
        for (x ← xs) {
          // partition conjRes into two parts, one that contains x and one that doesn't
          val (conjX, conjNoX) = conjRes partition { _ contains x }
          if (conjX.nonEmpty) {
            val conjXtoAnd: Formula = conjX.toAnd.simplifyCheap
            var conjXRes: Formula = elimCheap(conjXtoAnd, x).simplifyCheap

	    conjXRes = elimShadows(conjXRes.toListAnd, x).toAnd.simplifyCheap
            conjXRes match {
              case TrueAtom => conjRes = conjNoX
              case FalseAtom => conjRes = List(FalseAtom)
              case conjXRes =>
                // must be a conjunction
                conjRes = conjXRes.toListAnd ::: conjNoX
            }
          }
        }

        // conjRes is simplifiedCheap but may contain False
        if (conjRes contains FalseAtom)
            conjRes = List(FalseAtom)
        else 
          conjRes = conjRes filterNot { _ == TrueAtom }

        for (x ← xs) {
          // partition conjRes into two parts, one that contains x and one that doesn't
          val (conjX, conjNoX) = conjRes partition { _ contains x }
          //var conjXRes: Formula = null
          if (conjX.nonEmpty) {
            // need to go the expensive way
	    val conjXRes = 
              eliminate(conjX.toAnd, x) match {
		case Left((f, v, d)) => { toInstantiate+=(v -> d); f }
		case Right(f) => f
	      }

            conjXRes.simplifyCheap match {
              case TrueAtom => conjRes = conjNoX
              case FalseAtom => conjRes = List(FalseAtom)
              case conjXRes @ Or(_, _) =>
		//TODO- deferred instantiation wont work properly if done this way, i.e. you wont get the full benefit,
		//Idea: you could just add everything back into the formula and do tail recursion...
                // Peter 3/2/2015: Why do we need to append conjNoX ?
                // conjRes = eliminateMany((conjXRes.toListOr map { el => (el :: conjNoX).toAnd }).toOr, xs).simplifyCheap.toListAnd ::: conjNoX
                conjRes = eliminateMany((conjXRes.toListOr map { el => (el :: conjNoX).toAnd }).toOr, xs, toInstantiate).simplifyCheap.toListAnd
              case conjXRes =>
                // must be a conjunction
                conjRes = conjXRes.toListAnd ::: conjNoX
            }
          }
        }
        // After all variables eliminated from conjRes
        val h = conjRes.toAnd
        if (h == TrueAtom) {
	  reporter.debugCooper("Elimination of " + xs + " from " + g + " gives " + TrueAtom)
	  return TrueAtom
	}
        resOr ::= conjRes.toAnd
      }

      var res = resOr.toOr

      val remainingParams = toInstantiate.filterKeys(res contains _)

      reporter.debugCooper("cooper: need to instantiate with "+remainingParams)
      // Josh: some changes here (removed the "if") - please check
      for ((j,d) <- remainingParams) {
	res = rearrange(res,j)
	res = (for (v <- 1 to d) yield {
	  replace(res,j,Polynomial(List.empty,v,List.empty))
	}).toList.toOr.simplifyCheap
	// println(s"after inst $j from 1 to $d, we have $res")
      }
      reporter.debugCooper("Elimination of " + xs + " from " + g + " gives " + res)
      res
  }

  /**
   * Assume f is a quantifer-free BG formula and contains only the connectives And, Or and Neg. 
   * The result is an equivalent formula in nnf over the connectives And, Or and Neg (the latter in a limited way)
   * All (dis)equations and inequalities will be arranged suitable for Cooper using Polynomials
   * Furthermore, elimTrivial has been applied. 
   */
  def toPolyAtomFormula(f: Formula): Formula = {

    import Polynomial._ // for "toPolynomial"

      def h(f: Formula): Formula =
        f match {
          case TrueAtom        ⇒ TrueAtom
          case FalseAtom       ⇒ FalseAtom
          case Less(s, t)      ⇒ ZeroLTPoly(toPolynomial(t, false) - toPolynomial(s, false))
          case LessEq(s, t)    ⇒ h(Less(s, Sum(t, OneInt))) // s < t+1
          case GreaterEq(s, t) ⇒ h(Less(t, Sum(s, OneInt)))
          case Greater(s, t)   ⇒ h(Less(t, s))
          case Divides(s, t) ⇒ s match {
            case s: DomElemInt ⇒ DividesPolyAtom(s.value, toPolynomial(t, false))
            case _             ⇒ throw InternalError("divides atom with non-integer argument: " + f)
          }
          // s = t => 0 = t - s
          case Equation(s, t)                  ⇒ ZeroEQPoly(toPolynomial(t, false) - toPolynomial(s, false))
          case a @ PolyAtom(_, _, _)           ⇒ a // assume polyatoms have been brought into canonical form before
          case And(f1, f2)                     ⇒ And(h(f1), h(f2))
          case Or(f1, f2)                      ⇒ Or(h(f1), h(f2))
          // case Iff(f1, f2) => h(And(Implies(f1, f2), Implies(f2, f1)))
          // case IffNot(f1, f2) => h(Neg(Iff(f1, f2)))
          // case Implies(f1, f2) => h(Or(Neg(f1), f2))
          // case Implied(f1, f2) => h(Implies(f2, f1))

        case Neg(TrueAtom)                   ⇒ FalseAtom
        case Neg(FalseAtom)                  ⇒ TrueAtom
        case Neg(Less(s, t))                 ⇒ h(GreaterEq(s, t))
        case Neg(LessEq(s, t))               ⇒ h(Greater(s, t))
        case Neg(Greater(s, t))              ⇒ h(LessEq(s, t))
        case Neg(GreaterEq(s, t))            ⇒ h(Less(s, t))
        case Neg(Divides(s, t))              ⇒ Neg(h(Divides(s, t)))
        case Neg(Equation(s, t))             ⇒ ZeroNEPoly(toPolynomial(t, false) - toPolynomial(s, false))
        case Neg(ZeroEQPoly(p))              ⇒ ZeroNEPoly(p)
        case Neg(ZeroNEPoly(p))              ⇒ ZeroEQPoly(p)
        case Neg(ZeroLTPoly(p))              ⇒ ZeroLTPoly((p * -1) + 1)
        // Divisibility predicates will occur only positively.
        // But at the time his called, Divides atoms should not
        // occur anyway in f
        case nd @ Neg(DividesPolyAtom(n, p)) ⇒ nd
        case Neg(And(f1, f2))                ⇒ h(Or(Neg(f1), Neg(f2)))
        case Neg(Or(f1, f2))                 ⇒ h(And(Neg(f1), Neg(f2)))
        // case Neg(Iff(f1, f2)) => h(IffNot(f1, f2))
        // case Neg(IffNot(f1, f2)) => h(Iff(f1, f2))
        // case Neg(Implies(f1, f2)) => h(And(f1, Neg(f2)))
        // case Neg(Implied(f1, f2)) => h(Neg(Implies(f2, f1)))

        case Neg(Neg(f))                     ⇒ h(f)
      }

    // Body of normalize
    h(f). //reduceInnermost(elimNegNeg, false).
    reduceInnermost(cnfconversion.rules.elimTrivial)
  }

 /**
  * Eliminate all explicit existential quantifiers from f and also the given parameters params.
  * Assume f has been normalized for quantifier elimination,
  * i.e. no universal quantifiers, and the boolean connectives
  * are Or, And, and Neg. 
  */
  def cooper(f: Formula, params: Iterable[SymConst]) = {

    /** Eliminate all explicit quantifiers, innermost first */
    def eliminateAll(f: Formula): Formula =
      if (f.isQuantifierFree)
        f
      else f match {
        // Cannot have an atom, because atoms are quantifier-free
        case UnOpForm(op, g)       ⇒ UnOpForm(op, eliminateAll(g))
        case BinOpForm(op, f1, f2) ⇒ BinOpForm(op, eliminateAll(f1), eliminateAll(f2))
        case Exists(xs, g) if(flags.cooperDefer.value)  ⇒ elimManyDefer(toPolyAtomFormula(eliminateAll(g)), xs)
	case Exists(xs, g)         ⇒ eliminateMany(toPolyAtomFormula(eliminateAll(g)), xs)
      }

    // First eliminate all explicit quantifiers, then the given parameters
    val r1 = eliminateAll(f)
    val r2 = toPolyAtomFormula(r1)
    val r3 = if(flags.cooperDefer.value) elimManyDefer(r2,params) else eliminateMany(r2,params)
    //val r3 = 
   
    stats.nrQECalls += 1;
    r3.simplifyCheap

  }

  var isConsistentCnt = 0

  /**
   * The usual isConsistent specialised for lists of *ground* clauses.
   */
  def isConsistent(cls: Iterable[Clause]): Boolean = {
    reporter.debugCooper(s"cooper.isConsistent (ground clauses) called on $cls")
    isConsistentCnt += 1
    util.Timer.cooperCalls.start()
    val f1 = cls.map(cl => toPolyAtomFormula(cl.toFormula)).toAnd
    //roughly cooper is:
    val f2 = eliminateMany(f1, f1.symConsts)
    util.Timer.cooperCalls.stop()
    val res = f2.simplifyCheap
    // We need to evaluate f2 to get the conclusive result
    res != FalseAtom
  }

  /** Cooper's algorithm requires a certain normal, achieved this way: */
  def normalize(f: Formula) = {
    import cnfconversion.rules._

    val elimUniv: FormulaRewriteRules = {
      case Forall(x, g) ⇒ Neg(Exists(x, Neg(g)))
    }
    f.reduceInnermost(elimUniv).
    reduceInnermost(elimIffNot).
    reduceInnermost(elimIff).
    reduceInnermost(elimImpl).
    reduceInnermost(pushdownNegNoQuant). // May not introduce universal quantifiers
    simplifyCheap
  }

  /**
   * This is the entry point to the Cooper algorithm.
   * Removes from f all *explicit* quantifiers and the also the given parameters.
   * Parameter forced, if true, enforces always quantifier elimination,
   * even if f is quantifier-free and contains no parameters.
   * The result in theis case will always be TrueAtom or FalseAtom,
   * so that QE acts as a decision procedure.
   */
  def QE(f: Formula, forced: Boolean, params: Iterable[SymConst] = List.empty) = {
    reporter.debugCooper(s"cooper.QE called on $f")
    if (!forced && f.isQuantifierFree && params.isEmpty) {
      // println(" QE: leave untouched: %s, normalized %s".format(f, normalize(f)))
      f
    }
    else
    {
      util.Timer.cooperCalls.start()
      // println("QE " + f)
      util.Timer.normForCooper.start()
      val h = normalize(f)
      util.Timer.normForCooper.stop()
      // println("normalized: " + h)
      val res = cooper(h, params)
      reporter.debugCooper("cooper: final QE result "+res)
      util.Timer.cooperCalls.stop()
      // println("QE of formula " + f + " and params " + params + " is " + res)
      res
    }
  }

}
