package beagle

import parser._
import fol._
import term._
import bgtheory._

/**
 * Exposes basic beagle functions in a clean way in order to
 * simplify the reuse of beagle in other projects.
 *
 * There are two main ways to construct a formula to run beagle on:
 * 1) From a parser; either `TPTPParser` or `SMTLibParser` using the
 * `parseSMT` and `parseTPTP` entry points here.
 * 2) Manually using a `FormulaContext` object here.
 * `FormulaContext` objects carry signature information and construct new
 * operators where needed.
 */
object API {
  //formula construction
  //for simplicity these will build a signature as we go
  //perhaps this could be passed along with the formulas built?

  //there should also be an option to fix either a signature or precedence
  //OO-style: FormulaContext object, init with a precedence use as a factory for terms/formulas
  trait WithContext { 
    val fc: FormulaContext 
  }
  case class TermFC[+T <: Term](t: T, fc: FormulaContext) extends WithContext
  case class FormFC(f: Formula, fc: FormulaContext) extends WithContext {
    /** Existential closure */
    def existClosure = FormFC(f.closure(Exists),fc)
    /** Universal closure */
    def uniClosure = FormFC(f.closure(Forall),fc)
  }

  //functional-style: SimpleSig type rides along with constructors, result is expression+sig
  //merging ones also merge signatures and throw an exception if operators are renamed


  /* Some default signatures to use to initialise FormulaContext */

  def LIASig = { 
    println("Note: you can refer to the Int Sort with '$int'")
    LIA.addStandardOperators(Signature.signatureEmpty)
  }
  def LRASig = {
    println("Note: you can refer to Rat Sort with '$rat'")
    LRA.addStandardOperators(Signature.signatureEmpty)
  }
  def LFASig = {
    println("Note: you can refer to Real Sort with '$real'")
    LFA.addStandardOperators(Signature.signatureEmpty)
  }

  /**
   * A `FormulaContext` allows you to create terms and formulas with respect to
   * an implicit signature.</br>
   * The signature is constructed on the fly, only ensuring that no ambiguous operators
   * are created.
   * For example, starting with an empty signature if we call {{{ context.term('f', X) }}}
   * then `context.sig` after will contain the operator f with arity 1.
   * If we then try {{{ context.term('f',List(X,X)) }}} we should expect an exception.
   * @param initSig The initial signature to use when constructing terms is the union of signatures
   * given here. Note that you don't need to include the empty signature on the list.
   * @param prec A precedence with which to sort terms with.
   * We expect that any future signatures should respect the initial precedence.
   */
  class FormulaContext(val initSig: List[Signature] = Nil, val prec: List[String] = List()) {
    //sig must be a var, otherwise you could have the case that subterms of a formula refer
    //to different signatures in a formula context.
    var sig: Signature = 
      if(initSig.isEmpty) Signature.signatureEmpty
      else if(initSig.tail.isEmpty) //signatureEmpty is already a subset of this.
	initSig.head
      else
	initSig.foldLeft(Option(Signature.signatureEmpty))( 
	  (acc, next) => acc.flatMap( _.merge(next) )
	) getOrElse {
	  throw new util.SyntaxError("Initial signatures could not be merged")
	}

    //union of sorts and operators unless there is a conflict
    def merge(that: FormulaContext): FormulaContext = 
      (this.sig merge that.sig) map { s => 
	new FormulaContext(s :: Nil)
      } getOrElse {
	throw new util.SyntaxError("Signatures could not be merged")
      }

    /** A pair of FormulaContexts are equal iff they wrap the same signature */
    override def equals(other: Any) = other match {
      case fc: FormulaContext if(fc.sig == this.sig) => true
      case _ => false
    }

    //for now all given sorts are assumed to be FG
    private def getOrUpdateSort(sName: String): Type =
      if(sName.isEmpty) Signature.ISort
      else {
	sig.findSort(sName) getOrElse {
	  val res = new FGSort(sName)
	  sig += res
	  res
	}
      }

    /** The main way to add a new operator to an implicit signature */
    private def getOrUpdateOp(op: String, a: Arity): Operator =
      sig.findOperator(op, a.argsSorts) filterNot { 
	//if you have an existing op with same arg sorts but different result sort, do not return it
	//TODO- shouldn't this be impossible?
	_.sort() != a.resSort
      } getOrElse {
	//how do you construct BG constants?
	//only a BG operator if it already exists, so FG here
	val res = Operator(FG, op, a)
	sig += res
	res
      }

    /* term construction- possibly extending sig */

    /**
     * Produce a new variable relative to the given signature.
     * Notice that for now all constructed variables are GenVars.
     * @param name The name for the new variable
     * @param sort The sort of the new variable.
     * If the given sort does not already exist in the signature,
     * add a new sort with that name.
     */
    def Var(name: String, sort: String = "") = {
      val s = getOrUpdateSort(sort)
      TermFC[Var](GenVar(name,s),this)
    }

    def Var(name: Traversable[String], sort: String) = {
      val s = getOrUpdateSort(sort)
      name.map(x => TermFC[Var](GenVar(x,s),this))
    }

    def Term(op: String, argFC: TermFC[Term]): TermFC[Term] = Term(op, argFC :: Nil)
    def Term(op: String, argFC: TermFC[Term], sort: String): TermFC[Term] = Term(op, argFC :: Nil, sort)

    //how will you handle built in ops here?
    //todo, use a more general collection to allow java arrays for args
    def Term(op: String, argsFC: List[TermFC[Term]], sort: String = "") = {
      val newFC = 
	if(argsFC.isEmpty) 
	  this
	else 
	  argsFC map { _.fc }  reduce { _ merge _ }

      val args = argsFC map { _.t }
      val s = getOrUpdateSort(sort)
      val opArity = Arity(args map { _.sort }, s)

      //TODO- ignore DomElem constants
      val f = getOrUpdateOp(op, opArity)

      opArity match {
	case Arity(Nil,s) if(s.thyKind==BG) => TermFC[Const](BGConst(f), this)
	case Arity(Nil,s) => TermFC[Const](FGConst(f), this)
	case Arity(_,s) => TermFC[FunTerm](PFunTerm(f,args), this)
      }
    }

    def Const(name: String, sort: String = "") = Term(name,Nil,sort)

    /** Domain element- so far only integers */
    def DE(i: Int) = TermFC[DomElem[Int]](LIA.DomElemInt(i),this)
    
    /* Need predicates */
    val True = FormFC(TrueAtom,this)
    val False = FormFC(FalseAtom,this)

    def Atom(name: String, argsFC: List[TermFC[Term]]) = {
      val TermFC(t,fc2) = Term(name,argsFC,"$o") //ensures that operator is properly added
      FormFC(datastructures.PredEqn(t).toAtom,fc2)
    }

    /* formula construction- never changes sig */

    def Eqn(t1: TermFC[Term], t2: TermFC[Term]) = FormFC(Equation(t1.t,t2.t),t1.fc.merge(t2.fc))
    def And(f1: FormFC, f2: FormFC) = FormFC(fol.And(f1.f,f2.f),f1.fc.merge(f2.fc))
    def Or(f1: FormFC, f2: FormFC) = FormFC(fol.Or(f1.f,f2.f),f1.fc.merge(f2.fc))
    def Neg(g: FormFC) = FormFC(fol.Neg(g.f),g.fc)
    //TODO- should we check that vars have valid sorts wrt fc?
    def ForAll(xs: Var*)(f: FormFC) = FormFC(fol.Forall(xs.toList,f.f),f.fc) 
    def Exists(xs: Var*)(f: FormFC) = FormFC(fol.Exists(xs.toList,f.f),f.fc)

    //todo: let, ite
    //others, e.g. iff?

  }

  /* Formula entry via parsing of TFF or SMTLib files */

  /** The default signature for parsing */
  val fullSignature = 
    LFA.addStandardOperators(LRA.addStandardOperators(LIA.addStandardOperators(Signature.signatureEmpty)))

  /* simple workflow:
   * val in = parseTPTP(myFile)
   * saturate(in._1,in._2,defaultSettings)
   */

  //note that some values require a home directory to be set to search for axioms in.
  //to do this: import util._; flags.setValue("/full/path/to/home")

  def parseTPTP(file: String) = {
    val (tffs, outputSignature, haveConjecture,_) = 
      TPTPParser.parseTPTPFile(file,fullSignature,true)

    (tffs, new FormulaContext(outputSignature :: Nil))
  }

  def parseSMT(file: String) = {
    val (tffs, outputSignature, haveConjecture,_) = 
      SMTLibParser.parseSMTLibFile(file,true)
    (tffs, new FormulaContext(outputSignature :: Nil))
  }

  /* beagle init. e.g. flags etc. */

  /**
   * Produce a Flag object which contains all default settings for
   * a new beagle instance.
   */
  def defaultSettings = new util.Flags
  
  //could define commonly used variations of this?
  //e.g. debug, quiet, nosplit

  /* run */
  //todo- distinguish lemmas from formulas?
  //todo- where do we enter the main method?

  def saturate(formulas: List[Formula], fc: FormulaContext, settings: util.Flags) = {
    //rough version- ignores flags for now
    fol.Sigma=fc.sig
    bgtheory.setSolver("cooper-clauses")
    val cls = cnfconversion.formulasToClauses(formulas, false, haveLIA=true)
    main.run({},cls,Nil,100000)
  }

  //todo- cnf, bgsolvers etc

  //todo- can we inspect/communicate with a running instance?

}
