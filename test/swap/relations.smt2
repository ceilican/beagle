(set-option :pull-nested-quantifiers true)
(set-option :mbqi true)
(set-option :macro-finder true)
; built-in
; (declare-datatypes (T) ((List (nil) (cons (head T) (tail (List T))))))

(declare-fun inRange (Int (List Int)) Bool)
(assert (forall ((n0 Int) (l0 (List Int)))
		(= (inRange n0 l0) 
		   (or (= l0 (as nil (List Int))) 
		       (and (and (<= 0 (head l0))
				 (< (head l0) n0)) 
			    (inRange n0 (tail l0)))))))

;; z3 doesn't support recursive functions
;; (define-fun inRange ((n Int) (l (List Int))) Bool
;;   (or (= l (as nil (List Int)))
;;       (and (and (<= 0 (head l))
;; 		(< (head l) n)) 
;; 	   (inRange n (tail l)))))


;; Unprovable (conjectures below are already negated)

;; Problem 1: 0 sec
;; (assert (inRange 4 (insert 1 (insert 5 (insert 2 nil)))))

;; Problem 2: 0 sec
;; (assert (forall ((n Int)) (implies (> n 4) (inRange n (insert 1 (insert 5 (insert 2 nil)))))))

;; Problem 3: 0 sec
;; (assert (forall ((l (List Int)) (n Int)) (implies (inRange n (tail l)) (inRange n l))))

;; Problem 4: 0 sec
;; (assert (exists ((l (List Int)) (n Int)) (and (not (= l nil)) (inRange n l) (< (- n (head l)) 1))))

;; Problem 5: >60 sec
(assert (forall ((l (List Int)) (n Int)) (implies (inRange n l) (inRange (- n 1) l) )))

;; Problem 6: 0 sec
;; (assert 
;;  (forall ((l (List Int)) (n Int)) 
;; 	 (implies 
;; 	  (and 
;; 	   (not (= l nil)) 
;; 	   (inRange n l)) 
;; 	  (>= (- n (head l)) 2))))

;; Problem 7: 0 sec
;; (assert 
;;  (forall ((l0 (List Int)) (l1 (List Int)) (n Int)) 
;; 	 (implies 
;; 	  (and 
;; 	   (> n 0)
;; 	   (inRange n l0)
;; 	   (= l1 (insert (- n 2) l0)))
;; 	  (inRange n l1))))



;;;;;;;;;;;;;;;;;
;;query 1 (sat)
;;(assert (exists ((l (List Int)) (n Int)) (inRange l n)))

;;query 2 (unsat)
;; (assert (exists ((l (List Int)) (n Int))
;; 	(and 
;; 	  (not (= l (as nil (List Int))))
;; 	  (inRange l n)
;; 	  (not (inRange (tail l) n)))))

;;query 3 (unsat)
;; (assert (exists ((l (List Int)) (n Int))
;; 	(and 
;; 	  (not (= l (as nil (List Int))))
;; 	  (inRange l n)
;; 	  (not (inRange (insert (- (- n 1) (head l)) (tail l)) n)))))

;; query 4 (unsat)
;; (assert (exists ((l (List Int)) (n Int))
;; 	(and 
;; 	  (not (= l (as nil (List Int))))
;; 	  (inRange l n)
;; 	  (not (>= (- (- n 1) (head l)) 0)))))

;;query 5 (sat)

;; (declare-fun e1 () Int)
;; (declare-fun e2 () Int)
;; (declare-fun e3 () Int)
;; (define-fun  zl () (List Int) (insert e1 (insert e2 nil)))

;; (assert (exists ((l (List Int)) (n Int))
;; 	(and 
;; 	  (not (= l (as nil (List Int))))
;; 	  (= l zl)
;; 	  (inRange l n)
;; 	  (< (- n (head l)) 2))))

;;query 6 (sat)
;; (assert (exists ((l (List Int)) (n Int))
;; 	(and 
;; 	  (inRange l n)
;; 	  (not (inRange (tail l) n)))))


(check-sat)
