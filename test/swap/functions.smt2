(set-option :pull-nested-quantifiers true)
(set-option :mbqi true)
(set-option :macro-finder true)
; built-in
; (declare-datatypes (T) ((List (nil) (cons (head T) (tail (List T))))))

;; definition of inRange
(declare-fun inRange (Int (List Int)) Bool)
(assert (forall ((n Int) (l (List Int)))
		(= (inRange n l) 
		   (or (= l (as nil (List Int))) 
		       (and (and (<= 0 (head l))
				 (< (head l) n)) 
			    (inRange n (tail l)))))))

;; z3 doesn't support recursive functions
;; (define-fun inRange ((n Int) (l (List Int))) Bool
;;   (or (= l (as nil (List Int)))
;;       (and (and (<= 0 (head l))
;; 		(< (head l) n)) 
;; 	   (inRange n (tail l)))))

;; definition of length
(declare-fun length ((List Int)) Int)
(assert (= (length (as nil (List Int))) 0))
(assert (forall ((h Int) (t (List Int))) (= (length (insert h t)) (+ 1 (length t)))))


;; test
;; (assert (not (= (length (insert 1 (insert 2 (insert 3 nil)))) 3)))

;; Problem 1: 0 sec
;; (assert (forall ((l1 (List Int)) (l2 (List Int)))
;; 		(implies (= (length l1) (length l2))
;; 			 (= l1 l2))))

;; Problem 2: 0 sec
;; (assert (forall ((l (List Int)) (n Int))
;; 		(implies 
;; 		 (and (>= n 3)
;; 		      (>= (length l) 4))
;; 		 (inRange n l))))

;; definition of count
(declare-fun count (Int (List Int)) Int)
(assert (forall ((k Int)) (= (count k (as nil (List Int))) 0)))
(assert (forall ((k Int) (h Int) (t (List Int)))
		(implies 
		 (not (= k h))
		 (= (count k (insert h t)) (count k t)))))
(assert (forall ((k Int) (h Int) (t (List Int)))
		(implies 
		 (= k h)
		 (= (count k (insert h t)) (+ (count k t) 1)))))

;; test
;; (assert (not (= (count 2 (insert 2 (insert 1 (insert 2 nil)))) 2)))
;; (assert (= (count 2 (insert 2 (insert 1 (insert 2 nil)))) 3))

;; Problem 3: timeout
;; (assert (forall ((l (List Int)) (n Int)) 
;; 		(= (count n l) (count n (insert 1 l)))))

;; Problem 4: timeout
;; (assert (forall ((l (List Int)) (n Int)) 
;; 		(>= (count n l) (length l))))


;; Problem 5: timeout
;; (assert (forall ((l1 (List Int)) (l2 (List Int)))
;; 		(implies 
;; 		 (not (= l1 l2))
;; 		 (forall ((n Int)) 
;; 			 (not (= (count n l1) (count n l2)))))))
;; Append:

(declare-fun append ((List Int) (List Int)) (List Int))
(assert (forall ((l (List Int))) 
		(= (append (as nil (List Int)) l) l)))
(assert (forall ((i Int) (k (List Int)) (l (List Int))) 
		(= (append (insert i k) l) (insert i (append k l)))))
;; test
;; (assert (not (= (append (insert 1 (insert 2 nil)) (insert 3 (insert 4 nil))) (insert 1 (insert 2 (insert 3 (insert 4 nil)))))))

;; Problem 6: 0 sec
;; (assert (forall ((k (List Int)) (l (List Int)))
;; 		(= (length (append k l)) (length k))))

;; Problem 7: timeout
;; (assert (forall ((k (List Int)) (l (List Int)))
;; 		(implies
;; 		 (and (> (length k) 1)
;; 		      (> (length l) 1))
;; 		 (> (length (append k l)) 4))))


;; definition of in
(declare-fun in (Int (List Int)) Bool)
(assert (forall ((n Int) (l (List Int))) 
		(= (in n l)
		   (> (count n l) 0))))

;; test
;; (assert (not (in 2 (insert 1 (insert 2 (insert 3 nil))))))

;; Problem 8: timeout
(assert (forall ((m Int) (n Int) (k (List Int)) (l (List Int)) (l1 (List Int)))
		(implies 
		 (and (in n l)
		      (not (in m k))
		      (= l1 (append l (insert m k))))
		 (= (count n l1) (count n l)))))

(check-sat)
