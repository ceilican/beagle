tff(p_type,type,(
    dom: $int > $o )).

tff(a_type,type,(
    a: $int )).
tff(a_type,type,(
    b: $int )).
tff(a_type,type,(
    c: $int )).
tff(a_type,type,(
    d: $int )).
tff(a_type,type,(
    e: $int )).

tff(test, axiom,
    ( ! [X: $int] :
        ( ( X = 1 
	  | X = 2
	  | X = 3 
	  | X = 4 
	  | X = 5 
	  | X = 6 )
       <= dom(X)))).

%% tff(test, axiom,
%%     ( ! [X: $int] :
%%         ( ( $lesseq(1, X)
%%           & $lesseq(X, 6) )
%%        <= dom(X)))).



tff(test, axiom, dom(e)).
tff(test, axiom, 1 != e).
tff(test, axiom, 2 != e).
tff(test, axiom, 3 != e).
tff(test, axiom, 4 != e).
tff(test, axiom, 5 != e).
tff(test, axiom, 6 != e).

%% tff(test, axiom,
%%     ( ! [X: $int, Y: $int] :
%%         ( ( $lesseq(1, Y) 
%%           & $lesseq(Y, $sum(X, 2) ))
%%        => p(X) )) ).


%% tff(test, axiom,
%%     ( ! [X: $int, Y: $int] :
%%         ( ( $lesseq(1, Y) 
%%           & $lesseq(Y, 4)
%%           & Y != $product(2, X) )
%%        => p(X) )) ).

%% tff(test, axiom,
%%     ( ! [X: $int] :
%%         ( ( $lesseq(1, X) 
%%           & $lesseq(X, 2) )
%%        => p(X) )) ).

%% tff(testc, conjecture,(
%%   ? [X: $int] : p(X))).


%------------------------------------------------------------------------------
