%% include('/Users/baumgart/prover/workspace/beagle/test/lia-lemmas.p').
%% include('$BEAGLE/test/lia-lemmas.p').
tff(p_type,type,(
    p: $rat > $o )).
tff(q_type,type,(
    q: $i > $o )).
tff(r_type,type,(
    r: $i > $o )).

tff(r_type,type,(
    s: ($rat * $rat) > $i )).
tff(r_type,type,(
    t: ($rat * $rat) > $i )).

tff(p_type,type,(
    even: $rat > $o )).

tff(a_type,type,(
    a: $rat )).
tff(a_type,type,(
    b: $rat )).
tff(c_type,type,(
    c: $rat )).
tff(k_type,type,(
    k: $rat )).


%% tff(a, axiom, ! [X: $rat] : ( $sum(1/3, $sum(X, 1/3)) != $sum(X, 2/3))).
%% tff(a, axiom,  ! [X: $rat] : ( $sum(X, k) = 2/3 | $difference(X, k) = 3/2 )).
%% tff(a, axiom, (
%%    ! [X: $rat] : 
%%      ( ( $greatereq(X, 0/1)
%%        &  $lesseq($sum(X, k), k) )
%%     => X = 0/1 ))).
tff(a, axiom, ((a = c) | ~$greater(b,c) | (a = b))).
tff(a, axiom, (k != $sum(2/3, c))).

% tff(a, conjecture, ~(! [X:$int] : (? [Y:$int] : X=$uminus(Y)))).
%tff(a, conjecture, (! [X:$int] : $greater(X,0))).
%% tff(a, axiom, $lesseq(2,a)).
%% tff(a, axiom, $lesseq(a,3)).
%% tff(p, axiom, p(a)).
%% tff(p, axiom, ~p(2)).
%% tff(p, axiom, ~p(3)).
%% Purified
%% tff(p, axiom, ~p(b)).
%% tff(p, axiom, ~p(c)).
%% tff(b, axiom, b=2).
%% tff(b, axiom, c=3).


%% tff(a, axiom, ( ! [X: $int] : p($sum(X,$sum(a,1))))).
%% tff(a, axiom, ~p(2)).
%% tff(a, axiom, a=2).

%% tff(a, axiom, (a=3 | a=2)).
%% tff(a, axiom, p(a)).
%% tff(a, axiom, (! [X: $int] : p($sum(X,5)))).
%% tff(a, axiom, ~p($sum(1,1))).
%% tff(a, axiom, ~p($sum(1,2))).

%% tff(a, axiom, a=3).
%% tff(b, axiom, (p(7) | p($sum(3,1)))).
%% tff(b, axiom, (! [X, Z] : (q(X) | r(X) | r(Z)))).
%% tff(c, axiom, ~p(2)).
%% tff(c, conjecture, p(3)).

%% tff(test, conjecture,
%%      ( ! [X: $int] : 
%%          ? [Y: $int] : 
%%            ( X = $sum(Y,Y) 
%%            | X = $sum($sum(Y,Y),1) ))).

%% tff(test, axiom,
%%      ( ! [X: $int] : 
%%          ( even(X) 
%%         <= ( ? [Y: $int] : X = $sum(Y,Y) )))).


%% tff(f, axiom, ( ! [X: $int, Y: $int] : ( (p(X) | ~p(Y)) <= ( ? [Z: $int] : ( X != Z & Y != Z ))))).
%% tff(f, conjecture, ( ! [X: $int, Y: $int] : ( ? [Z: $int] : ( X != Z & Y != Z )))).
%% tff(f, conjecture, ( ! [X: $int, Y: $int] : ( ? [Z: $int] : ( X != Z & Y != Z )))).

%% tff(test, axiom,
%%      ( ! [X: $int] : 
%%          ( ( p(X)
%%            & even(X) )
%%         => p($sum(X,1))))).

%% tff(test, conjecture,
%%      ( ! [X: $int] : 
%%          ? [Y: $int] : 
%%            ( Y = $sum(X,1) ))).

%% tff(test, conjecture,
%%     ( ? [Y: $int] : 
%%         ( $less(Y, -3)
%%         & ( $greatereq(Y, 0) 
%%           | $greater($uminus(Y), 0) )
%%         & Y = a ))).
 


%------------------------------------------------------------------------------

%% tff(test, conjecture,
%%     ( $less(2, 3)) ).
%------------------------------------------------------------------------------
