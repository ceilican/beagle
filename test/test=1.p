%% include('/Users/baumgart/prover/workspace/beagle/test/lia-lemmas.p').
%% include('$BEAGLE/test/lia-lemmas.p').
tff(p_type,type,(
    p: $int > $o )).
tff(q_type,type,(
    q: $i > $o )).
tff(r_type,type,(
    r: $i > $o )).

tff(r_type,type,(
    s: ($int * $int) > $i )).
tff(r_type,type,(
    f: $int > $int )).
tff(r_type,type,(
    t: ($int * $int) > $i )).

tff(p_type,type,(
    even: $int > $o )).

tff(a_type,type,(
    a: $int )).
tff(a_type,type,(
    b: $int )).
tff(c_type,type,(
    c: $int )).

%% tff(a, axiom, p($sum(1, $sum(2, f(1))))).
%% tff(a, axiom, (! [X: $int] : ~p($sum(1, $sum(X, f(1)))))).
tff(a, axiom, p(a)).
tff(a, axiom, (! [X: $int] : ~p(X))).

tff(a1, axiom, ( ! [X:$int, Y:$int] : $sum($sum(X,$uminus(Y)),Y)  = X)).


%% tff(a, conjecture, ~(! [X:$int] : (? [Y:$int] : X=$uminus(Y)))).
%tff(a, conjecture, (! [X:$int] : $greater(X,0))).
%% tff(a, axiom, ($lesseq(2,a) | a=5)).
%% tff(a, axiom, $lesseq(a,3)).
%% tff(p, axiom, p(a)).
%% tff(p, axiom, ~p(2)).
%% tff(p, axiom, ~p(3)).
%% Purified
%% tff(p, axiom, ~p(b)).
%% tff(p, axiom, ~p(c)).
%% tff(b, axiom, b=2).
%% tff(b, axiom, c=3).


%% tff(a, axiom, ( ! [X: $int] : p($sum(X,$sum(a,1))))).
%% tff(a, axiom, ~p(2)).
%% tff(a, axiom, a=2).

%% tff(a, axiom, (a=3 | a=2)).
%% tff(a, axiom, p(a)).
%% tff(a, axiom, (! [X: $int] : p($sum(X,5)))).
%% tff(a, axiom, ~p($sum(1,1))).
%% tff(a, axiom, ~p($sum(1,2))).

%% tff(a, axiom, a=3).
%% tff(b, axiom, (p(7) | p($sum(3,1)))).
%% tff(b, axiom, (! [X, Z] : (q(X) | r(X) | r(Z)))).
%% tff(c, axiom, ~p(2)).
%% tff(c, conjecture, p(3)).

%% tff(test, conjecture,
%%      ( ! [X: $int] : 
%%          ? [Y: $int] : 
%%            ( X = $sum(Y,Y) 
%%            | X = $sum($sum(Y,Y),1) ))).

%% tff(test, axiom,
%%      ( ! [X: $int] : 
%%          ( even(X) 
%%         <= ( ? [Y: $int] : X = $sum(Y,Y) )))).


%% tff(f, axiom, ( ! [X: $int, Y: $int] : ( (p(X) | ~p(Y)) <= ( ? [Z: $int] : ( X != Z & Y != Z ))))).
%% tff(f, conjecture, ( ! [X: $int, Y: $int] : ( ? [Z: $int] : ( X != Z & Y != Z )))).
%% tff(f, conjecture, ( ! [X: $int, Y: $int] : ( ? [Z: $int] : ( X != Z & Y != Z )))).

%% tff(test, axiom,
%%      ( ! [X: $int] : 
%%          ( ( p(X)
%%            & even(X) )
%%         => p($sum(X,1))))).

%% tff(test, conjecture,
%%      ( ! [X: $int] : 
%%          ? [Y: $int] : 
%%            ( Y = $sum(X,1) ))).

%% tff(test, conjecture,
%%     ( ? [Y: $int] : 
%%         ( $less(Y, -3)
%%         & ( $greatereq(Y, 0) 
%%           | $greater($uminus(Y), 0) )
%%         & Y = a ))).
 


%------------------------------------------------------------------------------

%% tff(test, conjecture,
%%     ( $less(2, 3)) ).
%------------------------------------------------------------------------------
