%----There exists an identity element
cnf(left_identity,axiom,
    ( multiply(identity,X) = X )).

%----For any x in the group, there exists an element y such that x*y = y*x
%----= identity.
cnf(left_inverse,axiom,
    ( multiply(inverse(X),X) = identity )).

%----The operation '*' is associative
cnf(associativity,axiom,
    ( multiply(multiply(X,Y),Z) = multiply(X,multiply(Y,Z)) )).

%--------------------------------------------------------------------------

%------------------------------------------------------------------------------
%----Redundant two axioms
%% cnf(right_identity,axiom,
%%     ( multiply(a,identity) != a )).

cnf(right_inverse,axiom,
    ( multiply(a,inverse(a)) != identity )).

