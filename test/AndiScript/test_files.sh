#!/bin/bash

# #############################################
# HOWTO
# #############################################
# Call it like that:
#   ./test_files.sh Problems/\*.p
# to run beagle on all files *.p in Problems.
# (Note: You may need to escape * as above!)
#
# Or use:
#   ./test_files.sh --file-list input-files.txt
# to run beagle on all files listed in
# input-files.txt
# #############################################

# #############################################
# CONFIGURATION (edit as you see fit!)
# #############################################

BEAGLE="$HOME/Projects/beagle/beagle/trunk/beagle2 -format tff-rat "
OUTPUT_FILE="beagle-output.txt"
TIMEOUT_VAL=3  # timeout in seconds
TIMEOUT_PRG="timeout "$TIMEOUT_VAL

# #############################################
# MAIN PROGRAM (do not edit :-)
# #############################################

check_file() {
    printf '%s ' $1  # First, print file name / function argument

    theorem=`cat $1 | grep Status`
    output=`$TIMEOUT_PRG $BEAGLE $1`
    if [ "$?" -ne "0" ]; then
        echo aborted
        continue
    fi
    echo $1: >> $OUTPUT_FILE
    echo "$output" >> $OUTPUT_FILE
    echo "----------------------------------------------------------------------" >> $OUTPUT_FILE
    status=`echo $output | grep -o "SZS status.*\.p" | grep -o "status [a-z]*"`
    bgcalls=`echo $output | grep -o "\#BGSolverCalls.*"`

    if [[ $theorem =~ Theorem ]]; then
        echo is Theorem, solver says: $status \($bgcalls\)
    elif [[ $theorem =~ \ CounterSatisfiable ]]; then
        echo is CounterSatisfiable, solver says: $status
    elif [[ $theorem =~ \ Satisfiable ]]; then
        echo is Satisfiable, solver says: $status
    elif [[ $theorem =~ Unsatisfiable ]]; then
        echo is Unsatisfiable, solver says: $status
    else
        echo is unknown: $theorem
    fi
}

ARGS=("$@")
echo ------------------------------------------------------------------------
echo Call command: $TIMEOUT_PRG $BEAGLE
echo Detailed results in file: $OUTPUT_FILE
echo "----------------------------------------------------------------------" > $OUTPUT_FILE
echo Call command: $TIMEOUT_PRG $BEAGLE >> $OUTPUT_FILE
echo "----------------------------------------------------------------------" >> $OUTPUT_FILE
echo ------------------------------------------------------------------------

if [ "${ARGS[0]}" == "--file-list" ]; then
    cat ${ARGS[1]} |
    while read -r FILE; do
        check_file $FILE
    done
else
    for FILE in ${ARGS[0]}
    do
        check_file $FILE
    done
fi
