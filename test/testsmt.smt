(set-option :declare-list-datatype true)

(declare-sort Paar 2)
(declare-sort Color 0)
(declare-const k Int)
(declare-const r Real)
(declare-const red Color)
(declare-const green Color)
(declare-const blue Color)
(define-sort MyInt () Int)
(declare-const myk MyInt)
(define-sort Matrix (T) (Array T T))
(define-sort MyMatrix () (Matrix (Paar MyInt Color)))
(declare-fun f (Int MyInt (Array Int Color)) Int) 
(define-fun g ((i Int) (r Real) (j (Array Int Color))) MyInt (+ i k))
(define-fun h ((i MyInt)) Int (let ((k (+ i 1))) (+ k i)))
(define-fun tt ((i Int)) Bool (forall ((j Int)) (<= i j)))

;; (define-fun succ ((k Color)) Color (ite (= k red) green (ite (= k green) blue red)))

(assert (= red green blue))
; (assert (= red 1))
(declare-datatypes (T1 T2) ((Pair (mk-pair (first T1) (second T2)))))
(declare-const p1 (Pair Int Int))
(declare-const p2 (Pair Int Int))
(assert (= p1 p2))
(assert (> (second p1) 2))
(assert (not (= (first p1) (first p2))))

(declare-datatypes () ((S A B C)))
(declare-const x S)
(declare-const y S)
(declare-const z S)
(declare-const u S)
(assert (distinct x y z))
(assert (distinct x y z u))

(declare-datatypes (T) ((Lst nl (cons (hd T) (tl Lst)))))
(declare-const l1 (Lst Int))
(declare-const l2 (Lst Bool))

(assert (not (= (as nil (Lst Int)) (cons 2 l1))))

(declare-datatypes () ((IntLst intnil (intcons (inthd MyInt) (inttl IntLst)))))
(assert (= (inthd (intcons 2 (as intnil IntLst))) 3))
(assert (= (inthd (intcons myk intnil)) 3))
(assert (> (+ 2 3 4 5) 2))

(declare-parametric-fun (T) length ((List T)) Int)

(define-fun myintnil () (List Int) (as nil (List Int)))

(assert (= (length (insert 3 (as nil (List Int)))) 4))
;; error:
;; (assert (= (length (insert 3 (as nil (LList Int)))) 4))
(assert (= (length (insert 3 myintnil)) 4))
(assert (forall ((x Int)) (> x 5)))
(assert (> (- 6) (+ 5 3 4 5)))
;; (assert (let ((t true)) (=> t (and true (= 1 1)))))
(assert (> 2 (let ((x 1)) (+ 2 x))))



