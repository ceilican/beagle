tff(p_type,type,(
    p: $int > $o )).

tff(a_type,type,(
    a1: $int )).

tff(a_type,type,(
    a2: $int )).

tff(a_type,type,(
    a3: $int )).

tff(a_type,type,(
    a4: $int )).

tff(test, conjecture, 
  ( ( ? [A1: $int, A2: $int, A3: $int, A4: $int, A5: $int] : 
        ( (A1 !=  A2)
        & (A1 !=  A3)
        & (A1 !=  A4)
        & (A1 !=  A5)
        & (A2 !=  A3)
        & (A2 !=  A4)
        & (A2 !=  A5)
        & (A3 !=  A4)
        & (A3 !=  A5)
        & (A4 !=  A5 )
        & ( ? [D1: $int, D2: $int, D3: $int, D4: $int, D5: $int] : 
              ( ( D1 = A1  )
	      & ( D2 = A1 | D2 = A2  )
	      & ( D3 = A1 | D3 = A2 | D3 = A3  )
	      & ( D4 = A1 | D4 = A2 | D4 = A3 | D4 = A4 )
	      & ( D5 = A1 | D5 = A2 | D5 = A3 | D5 = A4 | D5 = A5 )
              & D1 = D5 
                )))))).


